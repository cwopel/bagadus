// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "NVENCWrapper.hpp"

#include <cuda.h>
#include <cuda_runtime.h>

#include "Helpers/logging.h"

/* Input / output points to beginning of chroma channels */
__global__ void convertToNV12_kernel(uint8_t * output, uint8_t * input,
        int outStride, int inStride, int height){

    const int max = (inStride * height) / 2;
    for(int idx=(blockIdx.x*blockDim.x + threadIdx.x);
            idx < max; idx += (blockDim.x*gridDim.x)){
		int x = idx%inStride;
		int y = (idx/inStride)<<1;
        
		uint8_t * dst = output + ((y/2) * outStride) + x*2;
        dst[0] = ((int)input[x + inStride*y] + (int)input[x + inStride*(y+1)])/2;
        dst[1] = ((int)input[x + inStride*(y+height)] + (int)input[x + inStride*(y+1+height)])/2;
    }
}

void NVENCWrapper::YUV422ToNV12::convert(void * output, void * input, size_t outputStride){
    /* Copy Y-plane */
    cudaSafe(cudaMemcpy2DAsync(output, outputStride, input, m_width,
                m_width, m_height, cudaMemcpyDeviceToDevice, m_stream));
    
    /* Convert UV-planes */
    uint8_t * out = (uint8_t*) output + outputStride*m_height;
    uint8_t * in  = (uint8_t*) input + m_width*m_height;
    convertToNV12_kernel<<< ((m_width*m_height)/(8*128)), 128, 0, m_stream >>>
        (out, in, outputStride, m_width/2, m_height);
    
    cudaSafe(cudaStreamSynchronize(m_stream));
}

