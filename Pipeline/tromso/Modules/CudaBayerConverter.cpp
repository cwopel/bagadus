// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CudaBayerConverter.hpp"

#include "Helpers/logging.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

void CudaBayerConverter::dbgPrintImages(struct frame * f){

	LOG_D("Writing images...");
	uint8_t * data = (uint8_t*) malloc(frameSize*4);
	assert(data);

	cudaSafe(cudaMemcpyFromArray((void*)data, f->cuArr, 0,0, frameSize*4, cudaMemcpyDeviceToHost));

	int h = height/numSources;
	for(int i=0; i<numSources; ++i){
		cv::Mat img(h, width, CV_8UC3);
		for(int y = 0; y<h; y++){
			for(int x = 0; x<width; x++){
				uint8_t * pix = (uint8_t*) data + ((y+i*h)*width*4 + x*4);
				double yVal = pix[0];
				double cbVal = pix[1] - 128;
				double crVal = pix[2] - 128;
				int b = (int) (yVal + 1.772 * cbVal);
				int g = (int) ((yVal - (0.34414 * cbVal)) - 0.71414 * crVal);
				int r = (int) (yVal + 1.402 * crVal);
				img.at<cv::Vec3b>(y,x)[0] = (char) (b<0 ? 0 : (b>255 ? 255 : b));
				img.at<cv::Vec3b>(y,x)[1] = (char) (g<0 ? 0 : (g>255 ? 255 : g));
				img.at<cv::Vec3b>(y,x)[2] = (char) (r<0 ? 0 : (r>255 ? 255 : r));
			}
        }
        std::ostringstream n;
        n << "outimg/" << f->hdr.timestamp.tv_sec << "_" << i;
        if(f->hdr.expoFirst){
            n << "_" << f->hdr.expoFirst << "_" << f->hdr.expoSecond;
        }
        n << ".png";
        cv::imwrite(n.str(), img);
	}

	free(data);
	LOG_D("Done writing images...");
}

using namespace CudaBayer;

CudaBayerConverter::CudaBayerConverter(){
	initialized = false;
}

CudaBayerConverter::~CudaBayerConverter(){
	if(!initialized) return;

	pthread_join(consumer, NULL);
	if(tmpBufferAllocated){
		cudaSafe(cudaFreeArray(tmpCudaArray));
	}
	deleteCudaArrays();
}

void CudaBayerConverter::init(FetchModule *producer, int width, int height, int numSources,
        bool printDebugImages){

	tmpBufferAllocated = false;
	inputIdx = 0;
	this->width = width;
	this->height = height*numSources;
	this->numSources = numSources;
	this->frameSize = width*height*numSources;
	this->producer = producer;
	this->printLastImages = printDebugImages;
	inputData[0] = new struct frame;
	inputData[1] = new struct frame;
	if(!inputData[0] || !inputData[1]){
		LOG_E("Failed to allocate cpu input buffer(s) in cudaBayerConverter.");
		perror("malloc");
		exit(EXIT_FAILURE);
	}

	initCudaArrays();

	pthread_barrier_init(&startWork, NULL, 2);
	if(pthread_create(&consumer, NULL, consumerStarter, (void*)this)){
		LOG_E("Failed to start cudaBayerConverter consumer thread.");
		perror("pthread_create");
		exit(EXIT_FAILURE);
	}
	initialized = true;
}

#ifdef BAYER_CONVERT_TO_PLANAR_YUV
/* Debug, so that output can be pushed into video encoder */
struct frame * CudaBayerConverter::getFrame(struct frame *buffer){
	pthread_barrier_wait(&startWork);
	inputIdx ^= 1;
	if(!inputData[inputIdx]) return NULL;

	if(!tmpBufferAllocated){
		tmpBufferAllocated = true;
		cudaChannelFormatDesc byte4Channel = cudaCreateChannelDesc<uchar4>();
		cudaSafe(cudaMallocArray(&tmpCudaArray,&byte4Channel, width,
		            height*numSources, cudaArraySurfaceLoadStore));
	}

	convertImage(inputData[inputIdx]->cuArr, tmpCudaArray);

	convertToPlanar(tmpCudaArray, buffer->cuArr);

	buffer->hdr = inputData[inputIdx]->hdr;

	cudaSafe(cudaStreamSynchronize(stream));

	return buffer;
}
#else
struct frame * CudaBayerConverter::getCudaFrame(struct frame *buffer){
	pthread_barrier_wait(&startWork);
	inputIdx ^= 1;
	if(!inputData[inputIdx]) return NULL;

	if(!tmpBufferAllocated){
		tmpBufferAllocated = true;
		cudaChannelFormatDesc byte4Channel = cudaCreateChannelDesc<uchar4>();
		cudaSafe(cudaMallocArray(&tmpCudaArray,&byte4Channel, width,
		            height*numSources, cudaArraySurfaceLoadStore));
	}

	convertImage(inputData[inputIdx]->cuArr, tmpCudaArray);

	cudaSafe(cudaMemcpyFromArrayAsync(buffer->cuArr, tmpCudaArray, 0, 0,
				frameSize*4, cudaMemcpyDeviceToDevice, stream));

	buffer->hdr = inputData[inputIdx]->hdr;

	cudaSafe(cudaStreamSynchronize(stream));

	return buffer;
}
#endif

struct frame * CudaBayerConverter::getCudaArrayFrame(struct frame *buffer) {
	pthread_barrier_wait(&startWork);
	inputIdx ^= 1;
	if(!inputData[inputIdx]){
		/* Debug: Print the last images */
        if(printLastImages){
            dbgPrintImages(buffer);
        }
		return NULL;
	}

	buffer->hdr = inputData[inputIdx]->hdr;
	convertImage(inputData[inputIdx]->cuArr, buffer->cuArr);

	return buffer;
}

void CudaBayerConverter::runConsumer(){
	int inIdx = inputIdx^1;
	while(true){
		struct frame * ret = producer->getCudaArrayFrame(inputData[inIdx]);
		if(!ret){
			cudaSafe(cudaFreeArray(inputData[inIdx]->cuArr));
			free(inputData[inIdx]);
			inputData[inIdx] = NULL;
			break;
		}
		pthread_barrier_wait(&startWork);
		inIdx ^= 1;
	}
	pthread_barrier_wait(&startWork);
}

void *CudaBayer::consumerStarter(void * context){
	((CudaBayerConverter *)context)->runConsumer();
	pthread_exit(0);
	return NULL;
}

