// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "NVENCWrapper.hpp"

#include <deque>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>
#include <sys/stat.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <helper_cuda_drvapi.h>
#include <helper_nvenc.h>
#include <nvEncodeAPI.h>

#include "Helpers/logging.h"

/**********************************************************
 * FileWriter management
 * ********************************************************/

std::string mkNewTimestamp(struct timeval *tv){
    struct tm * ti;
    std::ostringstream dstr, msec;

    ti = localtime(&(tv->tv_sec));

    //YYYY-MM-DD_hhmmss.usec
    dstr<< std::setfill('0') << std::setw(4) << ti->tm_year + 1900 << "-"
        << std::setfill('0') << std::setw(2) << ti->tm_mon  + 1    << "-"
        << std::setfill('0') << std::setw(2) << ti->tm_mday        << "_"
        << std::setfill('0') << std::setw(2) << ti->tm_hour
        << std::setfill('0') << std::setw(2) << ti->tm_min
        << std::setfill('0') << std::setw(2) << ti->tm_sec         << "."
        << std::setfill('0') << std::setw(6) << tv->tv_usec;

    return dstr.str();
}

bool mkRootDirectory(std::string filepath, std::string &result){
    std::string tempPath = filepath;
    int failed = mkdir(tempPath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    for(int i=0; failed; ++i){
        LOG_W("Create directory failed: %s. Trying again ...", strerror(errno));

        tempPath = filepath;
        if(tempPath.back() == '/') tempPath.erase(tempPath.length()-1,1);
        char tmp[10];
        sprintf(tmp,"_%d",i+1);
        tempPath.append(tmp);

        failed = mkdir(tempPath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        if(failed && i==5){
            LOG_E("Unable to create directory: %s", strerror(errno));
            return false;
        } else if(!failed) {
            LOG_I("Success");
        }
    }
    if(tempPath.back() != '/') tempPath.append("/");
    result = tempPath;
    return true;
}

class NVENCWrapper::FileWriter {
	private:
        std::string m_root;
        std::string m_currentFilename, m_currentManifest, m_currentLiveManifest;
        int m_fileIdx = 0;
        FILE * m_activeFile = NULL;
        int m_pipe;
        std::deque<header> m_hdrQueue;
        pthread_mutex_t m_fileLock, m_hdrLock;

        void closeCurrentFile(){
            fclose(m_activeFile);
            m_activeFile = NULL;

            FILE * f;
            std::string fname;

            /* Manifest file */
            f = fopen(m_currentManifest.c_str(), "a");
            if(f){
                fprintf(f, "%s\n", m_currentFilename.c_str());
                fclose(f);
            }else{
                LOG_E("Failed to write to manifest file \"%s\"", m_currentManifest.c_str());
            }

            /* Live Manifest file */
            f = fopen(m_currentLiveManifest.c_str(), "w");
            if(f){
                fprintf(f, "%s\n", m_currentFilename.c_str());
                fclose(f);
            }else{
                LOG_E("Failed to write to manifest file \"%s\"", m_currentLiveManifest.c_str());
            }

            if(m_pipe > 0){
                char buf[300];
                int ln = m_currentFilename.length() + 1;
                strncpy(buf, m_currentFilename.c_str(), ln);
                buf[ln-1] = '\n'; 
                ssize_t written = write(m_pipe, buf, ln);
                if(written < 0){
                    perror("Write to output pipe failed. Closing pipe, but continuing.");
                    close(m_pipe);
                    m_pipe = -1;
                }else if(written != ln){
                    LOG_W("Write to output pipe returned incorrect number of bytes, "
                            "%ld vs %d", written, ln);
                }
            }
            //printing only filename (not full path) to screen
            std::string filename = m_currentFilename;
            size_t found = filename.find_last_of("/");
            if(found != std::string::npos)
                filename.erase(filename.begin(),
                        filename.begin()+found+1);
            LOG_D("Encoder [new file] : %s", filename.c_str()); 
        }

        void openNewFile(struct timeval tv){
            /* Concaternates the string sources to form the finished filename. */
            std::ostringstream filename;
            std::string timestamp = mkNewTimestamp(&tv);

            pthread_mutex_lock(&m_fileLock);
            filename << m_root;
            filename << std::setfill('0') << std::setw(4) << m_fileIdx++ << "_";
            filename << timestamp; 
            filename << ".h264";

            m_currentManifest = std::string(m_root);
            m_currentManifest.append("manifest.txt");
            m_currentLiveManifest = std::string(m_root);
            m_currentLiveManifest.append("../livemanifest.txt");
            pthread_mutex_unlock(&m_fileLock);

            m_currentFilename = filename.str();
            if(!(m_activeFile = fopen(m_currentFilename.c_str(), "wb"))){
                LOG_E("Encoder: Unable to open output file \"%s\": %s",
                        m_currentFilename.c_str(),strerror(errno));
                exit(EXIT_FAILURE);
            }
        }

	public:
        bool init(std::string filepath, int pipe){
            if(!mkRootDirectory(filepath, m_root)) return false;
            pthread_mutex_init(&m_fileLock, NULL);
            m_pipe = pipe;
            return true;
        }

		void endStream(){
            if(m_activeFile) closeCurrentFile();
            pthread_mutex_destroy(&m_fileLock);
        }

		void pushData(void * data, size_t byteCount, bool keyframe){
            header hdr;
            pthread_mutex_lock(&m_hdrLock);
            if(m_hdrQueue.size() < 1){
                //Wonder what happens if this ever occurs... or what could cause it...
                LOG_W("NVENCWrapper: Queue with headers empty, got a mystery frame...");
            }
            hdr = m_hdrQueue.front();
            m_hdrQueue.pop_front();
            pthread_mutex_unlock(&m_hdrLock);
            if(keyframe){
                if(m_activeFile) closeCurrentFile();
                openNewFile(hdr.timestamp);
            }
            fwrite(data, byteCount, 1, m_activeFile);
        }

        bool setNewPath(std::string path){
            std::string next;
            if(!mkRootDirectory(path, next)) return false;
            pthread_mutex_lock(&m_fileLock);
            m_root = next;
            m_fileIdx = 0;
            pthread_mutex_unlock(&m_fileLock);
            //Even if we succeeded, return false if the path has a different name
            //Then it's up to caller to decide if we should proceed
            return !next.compare(path);
        }

        /* Frame headers are pushed in a fifo queue, then popped after a
         * potentially long pipeline of frames. */
        void pushFrameHeader(header hdr){
            pthread_mutex_lock(&m_hdrLock);
            m_hdrQueue.push_back(hdr);
            pthread_mutex_unlock(&m_hdrLock);
        }
};

/**********************************************************
 * NVENC Encoder implementation
 * ********************************************************/

//Abs min 2. More buffers may increase delays, but give some flexibility
//Allocated per sessions, and max depends on the memory of the GPU, don't make too many
#define NUM_INPUT_ENCODING_BUFFERS 8
//Minimum output buffers is also 2, but if bframes is turned on this MUST be increased to minimum
//2 + numConsecutiveBframes. F ex, if encoding looks like this 'i-b-b-p-b-b-p-b-b-p' you need 4
#define NUM_OUTPUT_ENCODING_BUFFERS 5

struct FrameBuffer {
	void * devPtr;
    int idx;
    size_t pitch;
    void * regPtr;
};

#define SET_VER(configStruct, type) {configStruct.version = type##_VER;}

const int BITRATE                = 50 * 1000 * 1000; //bits/sec
const GUID *PRESET               = &NV_ENC_PRESET_LOW_LATENCY_HP_GUID;
const GUID *PROFILE              = &NV_ENC_H264_PROFILE_HIGH_GUID;
const unsigned char CLIENT_KEY[] = { 230,127,93,88,29,83,108,73,133,106,93,230,163,146,36,165 };
GUID CLIENT_KEY_TEST             = { 0x0, 0x0, 0x0, { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 } };

//These need to persist across threads etc, created once per process
static NV_ENCODE_API_FUNCTION_LIST * nvencApi = NULL;
static CUcontext cudaContext = NULL;
//Create a cuda context and api reference. These have the lifetime of program execution
//NB! Not threadsafe, and exits upon failure
static void createRuntimeSession() {
    if(!cudaContext){
        CUdevice cuDevice = 0;
        CUcontext cuContextCurr;
        checkCudaErrors( cuInit(0) ); //Potentially very slow call
        checkCudaErrors( cuDeviceGet(&cuDevice, 0) );
        checkCudaErrors( cuCtxCreate(&cudaContext, 0, cuDevice) );
        checkCudaErrors( cuCtxPopCurrent(&cuContextCurr) );
    }
	if(!nvencApi){
        //Create nvenc api instance
        nvencApi = new NV_ENCODE_API_FUNCTION_LIST;
        memset(nvencApi, 0, sizeof(NV_ENCODE_API_FUNCTION_LIST));
        nvencApi->version = NV_ENCODE_API_FUNCTION_LIST_VER;
        checkNVENCErrors( NvEncodeAPICreateInstance(nvencApi) );
    }
}

class NVENCWrapper::NVENCImpl {
    private:

        bool m_initialized = false;
		bool m_terminateEncoding = false;
        bool m_terminateWriting = false;

		unsigned int m_gop, m_frameInGop;
        
		void * m_encoder;
		NV_ENC_INITIALIZE_PARAMS m_encInitParams;
		NV_ENC_PRESET_CONFIG m_presetConfig;

		//Buffers
		FrameBuffer m_buffers[NUM_INPUT_ENCODING_BUFFERS];
		NV_ENC_OUTPUT_PTR m_bitstreams[NUM_OUTPUT_ENCODING_BUFFERS];

		//Using absolute indices for a circular buffer
		int m_inHeadIdx = 0;
		int m_inTailIdx = 0;
		int m_outHeadIdx = 0;
		int m_outTailIdx = 0;

		//Thread communication variables
		pthread_t m_encoderThread, m_writerThread;
		pthread_mutex_t m_inputLock, m_outputLock;
		pthread_cond_t m_condNoInput, m_condNoOutput,  m_condNoFreeOutput;

        FileWriter * m_outputWriter;
        
#ifdef NVENC_PRINT_DEBUG_TIME
        double m_totalTime = 0;
        int m_totalFrames = 0;
#endif

        /* priv functions */

        void initializeEncoder(int fps, int width, int height, unsigned int gopLength){
            int fpsNum = fps * 1000;
            int fpsDen = 1000;

            //Set up encoding session
            NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS sessionParams = {0};
            memset(&sessionParams, 0, sizeof(NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS));
            SET_VER(sessionParams, NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS);
            sessionParams.apiVersion = NVENCAPI_VERSION;
//             sessionParams.clientKeyPtr = &CLIENT_KEY_TEST;
            sessionParams.clientKeyPtr = (GUID *) CLIENT_KEY;
            sessionParams.device = reinterpret_cast<void *>(cudaContext);
            sessionParams.deviceType = NV_ENC_DEVICE_TYPE_CUDA;

            checkNVENCErrors( nvencApi->nvEncOpenEncodeSessionEx(&sessionParams, &m_encoder) );

            //Set up initialization parameters
            memset(&m_encInitParams, 0, sizeof(NV_ENC_INITIALIZE_PARAMS));
            SET_VER(m_encInitParams, NV_ENC_INITIALIZE_PARAMS);
            memcpy(&m_encInitParams.encodeGUID, &NV_ENC_CODEC_H264_GUID, sizeof(GUID));

            //Preset and profile
            memcpy(&m_encInitParams.presetGUID, PRESET, sizeof(GUID));
            memset(&m_presetConfig, 0, sizeof(NV_ENC_PRESET_CONFIG));
            SET_VER(m_presetConfig, NV_ENC_PRESET_CONFIG);
            SET_VER(m_presetConfig.presetCfg, NV_ENC_CONFIG);
            checkNVENCErrors( nvencApi->nvEncGetEncodePresetConfig(m_encoder, m_encInitParams.encodeGUID,
                        m_encInitParams.presetGUID, &m_presetConfig) );
            m_encInitParams.encodeConfig = &m_presetConfig.presetCfg;
            memcpy(&m_encInitParams.encodeConfig->profileGUID, PROFILE, sizeof(GUID));

            //General parameters
            m_encInitParams.encodeWidth = width;
            m_encInitParams.encodeHeight = height;
            m_encInitParams.maxEncodeWidth = width;
            m_encInitParams.maxEncodeHeight = height;
            m_encInitParams.darWidth = width;
            m_encInitParams.darHeight = height;
            m_encInitParams.frameRateNum = fpsNum;
            m_encInitParams.frameRateDen = fpsDen;
            m_encInitParams.enablePTD = 0;
            m_encInitParams.encodeConfig->encodeCodecConfig.h264Config.repeatSPSPPS = 1;
//             m_encInitParams.encodeConfig->rcParams.averageBitRate = BITRATE;
            m_encInitParams.encodeConfig->gopLength = gopLength;
            m_gop = gopLength;
            m_frameInGop = 0;

            checkNVENCErrors( nvencApi->nvEncInitializeEncoder(m_encoder, &m_encInitParams) );
        }

        void allocateBuffers(int width, int height){
            //Allocate cuda input buffers
            CUcontext cuContextCurr;
            cuCtxPushCurrent(cudaContext);
            for(int i=0; i<NUM_INPUT_ENCODING_BUFFERS; ++i){
                NV_ENC_REGISTER_RESOURCE buffer;
                CUdeviceptr devPtrDevice;

                memset(&buffer, 0, sizeof(NV_ENC_REGISTER_RESOURCE));
                SET_VER(buffer, NV_ENC_REGISTER_RESOURCE);

                buffer.resourceType = NV_ENC_INPUT_RESOURCE_TYPE_CUDADEVICEPTR;
                buffer.width = width;
                buffer.height = height;

                checkCudaErrors( cuMemAllocPitch(&devPtrDevice, (size_t*)&buffer.pitch,
                            buffer.width, buffer.height * 3 / 2, 16) );
                checkCudaErrors( cuMemsetD2D8( devPtrDevice, buffer.pitch, 128,
                            buffer.width, buffer.height * 3 / 2) );

                buffer.resourceToRegister = (void*) devPtrDevice;
                checkNVENCErrors( nvencApi->nvEncRegisterResource(m_encoder, &buffer) );

                //Fill our own buffer struct
                m_buffers[i].devPtr = buffer.resourceToRegister;
                m_buffers[i].pitch = buffer.pitch;
                m_buffers[i].idx = i;
                m_buffers[i].regPtr = buffer.registeredResource;
            }
            cuCtxPopCurrent(&cuContextCurr);

            //Allocate bitstream output buffers
            for(int i=0; i<NUM_OUTPUT_ENCODING_BUFFERS; ++i){
                NV_ENC_CREATE_BITSTREAM_BUFFER bitstream;
                memset(&bitstream, 0, sizeof(NV_ENC_CREATE_BITSTREAM_BUFFER));
                SET_VER(bitstream, NV_ENC_CREATE_BITSTREAM_BUFFER);
                bitstream.size = 1024*1024;
                bitstream.memoryHeap = NV_ENC_MEMORY_HEAP_SYSMEM_CACHED;

                checkNVENCErrors( nvencApi->nvEncCreateBitstreamBuffer(m_encoder, &bitstream) );
                m_bitstreams[i] = bitstream.bitstreamBuffer;
            }
        }

        void runEncoderThread(){
            NV_ENC_PIC_PARAMS picParam;
            NV_ENC_MAP_INPUT_RESOURCE mappedBuffer;
            int eOutIdx = 0;
            bool endOfStream = false;
            while(true){
                int idx;
                if(!endOfStream){
                    //Wait for input buffer
                    pthread_mutex_lock(&m_inputLock);
                    while(m_inHeadIdx == m_inTailIdx && !m_terminateEncoding){
                        pthread_cond_wait(&m_condNoInput, &m_inputLock);
                    }
                    idx = m_inTailIdx % NUM_INPUT_ENCODING_BUFFERS;
                    endOfStream = m_terminateEncoding;
                    pthread_mutex_unlock(&m_inputLock);
                }

                //Set per-frame parameters
                memset(&picParam, 0, sizeof(NV_ENC_PIC_PARAMS));
                SET_VER(picParam, NV_ENC_PIC_PARAMS);
                if(endOfStream){
                    //Signal end of encoding
                    picParam.encodePicFlags = NV_ENC_PIC_FLAG_EOS;
                }else{
                    memcpy(&picParam.rcParams,&m_encInitParams.encodeConfig->rcParams,
                            sizeof(picParam.rcParams));
                    memcpy(&picParam.codecPicParams,&m_encInitParams.encodeConfig->encodeCodecConfig,
                            sizeof(picParam.codecPicParams));
                    picParam.inputWidth = m_encInitParams.encodeWidth;
                    picParam.inputHeight = m_encInitParams.encodeHeight;
                    picParam.inputPitch = m_buffers[idx].pitch;
                    picParam.bufferFmt = NV_ENC_BUFFER_FORMAT_NV12_PL;
                    picParam.pictureStruct = NV_ENC_PIC_STRUCT_FRAME;
                    picParam.outputBitstream = m_bitstreams[eOutIdx];
                    picParam.codecPicParams.h264PicParams.refPicFlag = 1;
                    picParam.codecPicParams.h264PicParams.displayPOCSyntax = m_frameInGop * 2;
                    picParam.encodePicFlags |= NV_ENC_PIC_FLAG_OUTPUT_SPSPPS;

                    if(m_frameInGop % m_gop == 0){
                        picParam.pictureType = NV_ENC_PIC_TYPE_IDR;
                    }else{
                        picParam.pictureType = NV_ENC_PIC_TYPE_P;
                    }
                    if(++m_frameInGop == m_gop) m_frameInGop = 0;

                    //Map the buffer to nvenc
                    memset(&mappedBuffer, 0, sizeof(NV_ENC_MAP_INPUT_RESOURCE));
                    SET_VER(mappedBuffer, NV_ENC_MAP_INPUT_RESOURCE);
                    mappedBuffer.registeredResource = m_buffers[idx].regPtr;
                    mappedBuffer.mappedBufferFmt = NV_ENC_BUFFER_FORMAT_NV12_PL;
                    checkNVENCErrors( nvencApi->nvEncMapInputResource(m_encoder, &mappedBuffer) );
                    picParam.inputBuffer = mappedBuffer.mappedResource;
                }

                NVENCSTATUS result = nvencApi->nvEncEncodePicture(m_encoder, &picParam);
                if(result == NV_ENC_SUCCESS){
                    if(!endOfStream){
                        pthread_mutex_lock(&m_outputLock);
                        m_outHeadIdx = m_inTailIdx + 1;
                        pthread_cond_signal(&m_condNoOutput);
                        pthread_mutex_unlock(&m_outputLock);
                    }
                }else if(result == NV_ENC_ERR_NEED_MORE_INPUT){
                    //do nothing
                }else{
                    LOG_E("nvEncEncodePicture failed, returned 0x%x\n",result);
                    exit(EXIT_FAILURE);
                }

                if(endOfStream) return;

                checkNVENCErrors( nvencApi->nvEncUnmapInputResource(m_encoder, mappedBuffer.mappedResource) );

                pthread_mutex_lock(&m_inputLock);
                m_inTailIdx++;
                pthread_cond_signal(&m_condNoInput);
                pthread_mutex_unlock(&m_inputLock);

                eOutIdx = (eOutIdx + 1) % NUM_OUTPUT_ENCODING_BUFFERS;

                //Wait for available output buffer
                pthread_mutex_lock(&m_outputLock);
                while((m_outHeadIdx - m_outTailIdx ) == NUM_OUTPUT_ENCODING_BUFFERS && !m_terminateEncoding){
                    pthread_cond_wait(&m_condNoFreeOutput, &m_outputLock);
                }
                endOfStream = m_terminateEncoding;
                pthread_mutex_unlock(&m_outputLock);

            }
        }

        void runWriterThread(){
            while(true){

                //Wait for buffer
                pthread_mutex_lock(&m_outputLock);
                while(m_outHeadIdx == m_outTailIdx  && !m_terminateWriting){
                    pthread_cond_wait(&m_condNoOutput, &m_outputLock);
                }
                if(m_terminateWriting){
                    pthread_mutex_unlock(&m_outputLock);
                    m_outputWriter->endStream();
                    return;
                }
                pthread_mutex_unlock(&m_outputLock);

                NV_ENC_LOCK_BITSTREAM bitstreamLock;
                memset(&bitstreamLock, 0, sizeof(NV_ENC_LOCK_BITSTREAM));
                SET_VER(bitstreamLock, NV_ENC_LOCK_BITSTREAM);
                bitstreamLock.outputBitstream = m_bitstreams[m_outTailIdx % NUM_OUTPUT_ENCODING_BUFFERS];

#ifdef NVENC_PRINT_DEBUG_TIME
                struct timeval start_time, stop_time;
                gettimeofday(&start_time, 0);
#endif

                NVENCSTATUS result;
                result = nvencApi->nvEncLockBitstream(m_encoder, &bitstreamLock);

#ifdef NVENC_PRINT_DEBUG_TIME
                gettimeofday(&stop_time, 0);
                float time;
                time = (stop_time.tv_sec * 1.0e3 + stop_time.tv_usec / 1.0e3)
                    - (start_time.tv_sec * 1.0e3 + start_time.tv_usec / 1.0e3);
                m_totalFrames++;
                if(m_totalFrames>20) m_totalTime += time;
#endif
                if(result != NV_ENC_SUCCESS){
                    LOG_W("NVENC lock bitstream failed. code=%d(%s)",
                            static_cast<unsigned int>(result), _nvencGetErrorEnum(result));
                }else{
                    m_outputWriter->pushData(bitstreamLock.bitstreamBufferPtr,
                            bitstreamLock.bitstreamSizeInBytes,
                            bitstreamLock.pictureType == NV_ENC_PIC_TYPE_IDR);

                    result = nvencApi->nvEncUnlockBitstream(
                            m_encoder,bitstreamLock.outputBitstream);
                    if(result != NV_ENC_SUCCESS){
                        LOG_W("NVENC unlock bitstream failed. code=%d(%s)",
                                static_cast<unsigned int>(result), _nvencGetErrorEnum(result));
                    }
                }

                pthread_mutex_lock(&m_outputLock);
                m_outTailIdx++;
                pthread_cond_signal(&m_condNoFreeOutput);
                pthread_mutex_unlock(&m_outputLock);
            }
        }

        static void * launchEncoderThread(void *p){
            ((NVENCImpl*)p)->runEncoderThread();
            pthread_exit(0);
        }
        static void * launchWriterThread(void *p){
            ((NVENCImpl*)p)->runWriterThread();
            pthread_exit(0);
        }

    public:
        ~NVENCImpl(){
            if(!m_initialized) return;

            //Signal flushing of the encoding and clean up encoding thread
            pthread_mutex_lock(&m_outputLock);	
            pthread_mutex_lock(&m_inputLock);	
            m_terminateEncoding = true;
            pthread_cond_broadcast(&m_condNoInput);
            pthread_cond_signal(&m_condNoFreeOutput);
            pthread_mutex_unlock(&m_inputLock);	
            pthread_mutex_unlock(&m_outputLock);	
            pthread_join(m_encoderThread, NULL);

            //Clean up writing thread
            pthread_mutex_lock(&m_outputLock);	
            m_terminateWriting = true;
            pthread_cond_signal(&m_condNoOutput);
            pthread_mutex_unlock(&m_outputLock);	
            pthread_join(m_writerThread, NULL);

            //Free buffers
            cuCtxPushCurrent(cudaContext);
            for(int i=0; i<NUM_INPUT_ENCODING_BUFFERS; ++i){
                checkNVENCErrors( nvencApi->nvEncUnregisterResource(m_encoder, m_buffers[i].regPtr) );
                checkCudaErrors( cuMemFree((CUdeviceptr) m_buffers[i].devPtr) );
            }
            CUcontext cuContextCurr;
            cuCtxPopCurrent(&cuContextCurr);

            //Free bitstreams
            for(int i=0; i<NUM_OUTPUT_ENCODING_BUFFERS; ++i){
                checkNVENCErrors( nvencApi->nvEncDestroyBitstreamBuffer(m_encoder, m_bitstreams[i]));
            }

            //Terminate encoder
            checkNVENCErrors( nvencApi->nvEncDestroyEncoder(m_encoder) );

#ifdef NVENC_PRINT_DEBUG_TIME
            if(m_totalFrames>20)
                LOG_I("Nvenc encoding time   : %7.3fms", m_totalTime/(m_totalFrames-20));
#endif
            m_initialized = false;
        }

		bool init(FileWriter * fw, int fps, int width, int height, unsigned int gopLength){
            //Initialize cuda and nvenc library
            createRuntimeSession();

            initializeEncoder(fps, width, height, gopLength);
            allocateBuffers(width, height);

            m_outputWriter = fw;

            pthread_mutex_init(&m_inputLock, NULL);
            pthread_mutex_init(&m_outputLock, NULL);
            pthread_cond_init(&m_condNoInput, NULL);
            pthread_cond_init(&m_condNoOutput, NULL);
            pthread_cond_init(&m_condNoFreeOutput, NULL);
            pthread_create(&m_encoderThread, NULL, this->launchEncoderThread, this);
            pthread_create(&m_writerThread, NULL, this->launchWriterThread, this);

            m_initialized = true;

            return true;
        }

		const FrameBuffer * getNextBuffer(){
            pthread_mutex_lock(&m_inputLock);

            while((m_inHeadIdx - m_inTailIdx) == NUM_INPUT_ENCODING_BUFFERS){
                pthread_cond_wait(&m_condNoInput, &m_inputLock);
            }

            int idx = m_inHeadIdx % NUM_INPUT_ENCODING_BUFFERS;
            pthread_mutex_unlock(&m_inputLock);

            return (const FrameBuffer *) &m_buffers[idx];
        }

		/* Fast non-blocking call. Notifies that previous buffer is ready for reading */
		void bufferReady(){
            pthread_mutex_lock(&m_inputLock);
            m_inHeadIdx++;
            pthread_cond_signal(&m_condNoInput);
            pthread_mutex_unlock(&m_inputLock);
        }
};

/**********************************************************
 * Conversion class
 * ********************************************************/

bool NVENCWrapper::YUV422ToNV12::init(int width, int height, NVENCImpl *enc){
    m_busy = false;
    m_terminate = false;
    m_firstSwap = true;
    m_width = width;
    m_height = height;
    m_enc = enc;
    m_idx = 0;
    
    pthread_mutex_init(&m_lock, NULL);
    pthread_cond_init(&m_condIdle, NULL);
    pthread_cond_init(&m_condBusy, NULL);
    cudaSafe(cudaMalloc(&m_ptrs[0], width * height * 2));
    cudaSafe(cudaMalloc(&m_ptrs[1], width * height * 2));
    cudaSafe(cudaStreamCreate(&m_stream));

    pthread_create(&m_thread, NULL, this->launchThread, (void*) this);
    return true;
}

void * NVENCWrapper::YUV422ToNV12::swapBuffer(){
    if(m_firstSwap){
        m_firstSwap = false;
        return m_ptrs[m_idx];
    }
    pthread_mutex_lock(&m_lock);
    while(m_busy) pthread_cond_wait(&m_condBusy, &m_lock);
    m_idx ^= 1;
    m_busy = true;
    pthread_mutex_unlock(&m_lock);
    pthread_cond_signal(&m_condIdle);
    return m_ptrs[m_idx];
}

NVENCWrapper::YUV422ToNV12::~YUV422ToNV12(){
    pthread_mutex_lock(&m_lock);
    m_terminate = true;
    pthread_mutex_unlock(&m_lock);
    pthread_cond_signal(&m_condIdle);
    pthread_join(m_thread, NULL);
    cudaSafe(cudaFree(m_ptrs[0]));
    cudaSafe(cudaFree(m_ptrs[1]));
    cudaSafe(cudaStreamDestroy(m_stream));
}

void NVENCWrapper::YUV422ToNV12::run(){
    pthread_mutex_lock(&m_lock);
    while(!m_busy && !m_terminate) pthread_cond_wait(&m_condIdle, &m_lock);
    while(!m_terminate){
        pthread_mutex_unlock(&m_lock);

        const FrameBuffer * fbuf = m_enc->getNextBuffer();
        convert(fbuf->devPtr, m_ptrs[m_idx ^ 1], fbuf->pitch);
        m_enc->bufferReady();

        pthread_mutex_lock(&m_lock);
        m_busy = false;
        pthread_mutex_unlock(&m_lock);
        pthread_cond_signal(&m_condBusy);
        pthread_mutex_lock(&m_lock);
        while(!m_busy && !m_terminate) pthread_cond_wait(&m_condIdle, &m_lock);
    }
    pthread_mutex_unlock(&m_lock);
}

void * NVENCWrapper::YUV422ToNV12::launchThread(void *p){
	((NVENCWrapper::YUV422ToNV12*)p)->run();
	pthread_exit(0);
}

/**********************************************************
 * Base wrapper class
 * ********************************************************/

void *consumerThreadStarter(void * context){
    ((NVENCWrapper *)context)->runConsumer();
    pthread_exit(0);
}

NVENCWrapper::~NVENCWrapper(){
    if(!m_initialized) return;

    if(!m_runAsMain){
        pthread_join(m_consumer, NULL);
    }
    if(m_converter){
        delete m_converter;
    }
    delete m_enc;
    delete m_fileWriter;
}

bool NVENCWrapper::init(std::string filepath, int width, int height, int fps, float sekPerFile,
        FetchModule *producer, bool runAsMain, int format, int outputPipe){
    m_runAsMain = runAsMain;
    m_producer = producer;
    m_enc = new NVENCWrapper::NVENCImpl();
    m_fileWriter = new NVENCWrapper::FileWriter();
    //TODO Clean up properly on failure
    if(!m_fileWriter->init(filepath, outputPipe)) return false;
    if(!m_enc->init(m_fileWriter, fps, width, height, (int)(sekPerFile*fps))) return false;

    if(format == X264_FORMAT_422){
        m_converter = new NVENCWrapper::YUV422ToNV12();
        m_converter->init(width, height, m_enc);
    }else if(format == X264_FORMAT_NV12){
        m_converter = NULL;
    }else{
        LOG_E("NvencWrapper doesn't support 420p format");
        return false;
    }

    if(!runAsMain){
        pthread_create(&m_consumer, NULL, consumerThreadStarter, this);
    }

    m_initialized = true;
    return true;
}

bool NVENCWrapper::setNewPath(std::string path){
    return m_fileWriter->setNewPath(path);
}

void NVENCWrapper::runConsumer(){
    frame frameCtx;
    while(true){
        if(m_converter){
            frameCtx.ptr = m_converter->swapBuffer();
        }else{
            const FrameBuffer * fb = m_enc->getNextBuffer();
            //TODO Fix hidden assertion that fb->pitch == width.
            //This is, in current conf, always the case, but a really bad assumption
            frameCtx.ptr = fb->devPtr;
        }

        struct frame * ret = m_producer->getCudaFrame(&frameCtx);
        if(!ret) break;

        m_fileWriter->pushFrameHeader(frameCtx.hdr);

        if(!m_converter) m_enc->bufferReady();
    }
}
