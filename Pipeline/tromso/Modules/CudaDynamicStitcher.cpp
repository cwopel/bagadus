// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CudaDynamicStitcher.hpp"

#include "Helpers/logging.h"

using namespace CudaStitcher;

CudaDynamicStitcher::CudaDynamicStitcher(){
	initialized = false;
	running[0]  = false;
	running[1]  = false;
}

CudaDynamicStitcher::~CudaDynamicStitcher(){
	if(!initialized) return;

	/* Here we wait untill the consumer thread has received a NULL value
	 * indicating end of stream / termination. Will currently never
	 * terminate unless this NULL-pointer is returned by producer module */
	pthread_join(consumer, NULL);
	/* Free the backup buffer */
	if(tmpBufferAllocated){
		cudaSafe(cudaFree(tmpCudaPointer));
	}
	deleteCudaArrays();
	pthread_barrier_destroy(&startWork);

    delete whiteBalanceAdjuster;
	// TODO Delete inputData
}

bool CudaDynamicStitcher::parseMap(char * mapName, ContextVariables * result,
        void (*wbCb)(int, RgbRatio), int exposurePilot){
	FILE *mapFile;
	if(! (mapFile = fopen(mapName, "rb"))){
	    LOG_E("Unable to open mapfile");
		perror("fopen");
		return false;
	}
#ifdef DONT_ROTATE
	int matches = fscanf(mapFile,"%d %d \n %d %d %d \n",
			&dstWidth, &dstHeight, &srcWidth, &srcHeight, &numSources);
#else
	/* This is directly (badly) linked with panoGen map file creation.
	 * This should be formalized better. src width is read into src height
	 * and vice versa, expecting image to be rotated. */
	int matches = fscanf(mapFile,"%d %d \n %d %d %d \n",
			&dstWidth, &dstHeight, &srcHeight, &srcWidth, &numSources);
#endif
	if(matches != 5){
	    LOG_E("Stitcher invalid dimensions");
		perror("cylStitch init fscanf dimensions");
		fclose(mapFile);
		return false;
	}
	result->numSources = numSources;
	result->inWidth = srcWidth;
	result->inHeight = srcHeight;
	result->outWidth = dstWidth;
	result->outHeight = dstHeight;

    int center = exposurePilot>0?exposurePilot:(numSources/2);
    whiteBalanceAdjuster = new WhiteBalanceAdjustment(numSources, wbCb, center);

	/* Read in all the individual stitch locations */
	images = new ImageStitch[numSources];
	for(int i=0; i<numSources; ++i){
		int dbg;
		if((dbg=fscanf(mapFile, "%d %d", &images[i].minX, &images[i].maxX)) != 2){
			perror("cylStitch init fscanf stitchareas");
			LOG_E("Was supposed to read 2 stitch-area tokens, read %d", dbg);
			fclose(mapFile);
			return false;
		}
	}
	fseek(mapFile, 2, SEEK_CUR); /* Skip past a space and newline... I hate myself... */
	for(int i=0; i<numSources; ++i){
		/* Create a new raw lookup table. For every pixel covered by the given input image
		 * we read in 2 floats, x/y coordinates to the source image. */
		int mapSize = dstHeight*(images[i].maxX-images[i].minX);
		images[i].cpuMap = new float2[mapSize];

		/* Read in the giant friggin array of floats from file. */
		int mapRead = (int) fread(images[i].cpuMap, sizeof(float2), mapSize, mapFile); 
		if(mapRead != mapSize){
			LOG_E("Failed reading file, read %d instead of %d", mapRead, mapSize);
			fclose(mapFile);
			return false;
		}
	}
	/* Ensure that we're at the end of the file. Otherwise something weird has happened */
	if(getc(mapFile) != EOF){
		LOG_E("Failed reading file, there are unread bytes in the mapfile");
		fclose(mapFile);
		return false;
	}
	fclose(mapFile);

	/* Create input buffers and allocate cuda stuff */
	tmpBufferAllocated = false;
	inputIdx = 0;
	inputData[0] = new struct frame;
	inputData[1] = new struct frame;

	initCudaArrays();

	pthread_barrier_init(&startWork, NULL, 2);
	initialized = true;
	return true;
}

void CudaDynamicStitcher::init(FetchModule *producer){
	if(!initialized){
		LOG_E("CudaDynamicStitcher: parseMap must be called before init");
		exit(EXIT_FAILURE);
	}

	/* Function merely needs to set who its producer is and start consumer thread */
	this->producer = producer;
	running[0] = true;
	running[1] = true;

	if(pthread_create(&consumer, NULL, consumerStarter, (void*)this)){
		LOG_E("Failed to start cudaDynamicStitcher consumer thread");
		perror("pthread_create");
		exit(EXIT_FAILURE);
	}
}

struct frame * CudaDynamicStitcher::getCudaFrame(struct frame *buffer){
	pthread_barrier_wait(&startWork);
	inputIdx ^= 1;
	/* Received end of input */
	if(!running[inputIdx]) return NULL;

	/* Copy along the resource header, ensuring that metainformation isnt lost */
	buffer->hdr = inputData[inputIdx]->hdr;
	/* Run all the kernels and operations required for a single frame / iteration */
	stitchImage(inputData[inputIdx]->cuArr, (uint8_t*)buffer->ptr);

	return buffer;
}

struct frame * CudaDynamicStitcher::getCudaArrayFrame(struct frame *buffer){
	/* This is merely a backup function, not intended to be used. Copies the data
	 * into a temporary buffer to comply with the cudaModule interface */

	pthread_barrier_wait(&startWork);
	inputIdx ^= 1;
	if(!running[inputIdx]) return NULL;
	if(!tmpBufferAllocated){
		tmpBufferAllocated = true;
		cudaSafe(cudaMalloc(&tmpCudaPointer,dstSize));
	}

	/* Run all the kernels and operations required for a single frame / iteration */
	stitchImage(inputData[inputIdx]->cuArr, (uint8_t*)tmpCudaPointer);

	cudaSafe(cudaMemcpyToArrayAsync(buffer->cuArr, 0, 0, tmpCudaPointer, 
				dstSize, cudaMemcpyDeviceToDevice, stream));

	/* Copy along the resource header, ensuring that metainformation isnt lost */
	buffer->hdr = inputData[inputIdx]->hdr;

	cudaSafe(cudaStreamSynchronize(stream));
	return buffer;
}

void CudaDynamicStitcher::runConsumer(){
	int inIdx = inputIdx^1;
	while(true){
		/* Get next frame set */
		struct frame * ret = producer->getCudaArrayFrame(inputData[inIdx]);
		if(!ret){
			/* Delete the array. The NULL pointer will indicate EOF */
			/* Hei Ragnar, dette bufferet brukes på nytt ...
			cudaSafe(cudaFreeArray(inputData[inIdx]->cuArr));
			free(inputData[inIdx]);
			inputData[inIdx] = NULL;
			*/
			/* Terminate the loop and stop consuming input */
			running[inIdx] = false;
			break;
		}
		pthread_barrier_wait(&startWork);
		inIdx ^= 1;
	}
	/* Still need to enter the barrier to allow the other thread to discover EOF */
	pthread_barrier_wait(&startWork);
}


void *CudaStitcher::consumerStarter(void * context){
	((CudaDynamicStitcher *)context)->runConsumer();
	pthread_exit(0);
	return NULL;
}

void WhiteBalanceAdjustment::add(RgbRatio * rgbRatios){
    
    if( callback == NULL) return;

    for(int i=0; i<numSources-1; ++i)
        accumulatedRatios[i] += rgbRatios[i];

    ++frameCounter;
    
    if(frameCounter == 0){
        
        for(int i=0; i<numSources-1; ++i)
            accumulatedRatios[i] = RgbRatio();

    }else if(frameCounter == WHITEBALANCE_FREQUENCY){
        //After applying an update, the next 10 frames are ignored
        frameCounter = -10;
        
        int idx;
        stage++;
        bool anyUpdate = false;
        
        idx = center - stage;
        if( idx >= 0){
            RgbRatio ratio = RgbRatio(2) - (accumulatedRatios[idx] / WHITEBALANCE_FREQUENCY);
            //Reduce adjustment by 50%
            ratio = (ratio - RgbRatio(1))/2 + RgbRatio(1);
            callback(idx, ratio);
            anyUpdate = true;
        }
        idx = center + stage;
        if( idx < numSources){
            RgbRatio ratio = accumulatedRatios[idx-1] / WHITEBALANCE_FREQUENCY;
            //Reduce adjustment by 50%
            ratio = (ratio - RgbRatio(1))/2 + RgbRatio(1);
            callback(idx, ratio);
            anyUpdate = true;
        }

        if(!anyUpdate) stage = 0;
        for(int i=0; i<numSources-1; ++i)
            accumulatedRatios[i] = RgbRatio();
    }
}

