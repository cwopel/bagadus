// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

texture<uint8_t, 2, cudaReadModeElementType> bayerTex;
texture<uchar2, 2, cudaReadModeElementType> bayerTexG;
surface<void, cudaSurfaceType2D> bayerSurfG;
texture<char4, 2, cudaReadModeElementType> bayerTexMask;
surface<void, cudaSurfaceType2D> bayerSurfMask;

#define DO_FAST_KERNEL

inline __device__ void getPixels(uchar2 *array, int x, int y){
	array[0] = tex2D(bayerTexG,x,y-1);
	array[1] = tex2D(bayerTexG,x+2,y-1);
	array[2] = tex2D(bayerTexG,x-1,y);
	array[3] = tex2D(bayerTexG,x,y);
	array[4] = tex2D(bayerTexG,x+1,y);
	array[5] = tex2D(bayerTexG,x,y+1);
	array[6] = tex2D(bayerTexG,x+1,y+1);
	array[7] = tex2D(bayerTexG,x+2,y+1);
	array[8] = tex2D(bayerTexG,x-1,y+2);
	array[9] = tex2D(bayerTexG,x+1,y+2);
}

inline __device__ uchar3 getRed(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(pix.y, pix.x, 0);

	pix = tex2D(bayerTexG,x-1,y-1); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y-1); tmp += (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x-1,y+1); tmp += (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y+1); tmp += (float)pix.y - pix.x;
	tmp = 0.25f*tmp + rgb.y;
	rgb.z = CLAMP_255(tmp);
	return rgb;
}

inline __device__ uchar3 getBlue(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, pix.y);

	pix = tex2D(bayerTexG,x-1,y-1); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y-1); tmp += (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x-1,y+1); tmp += (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y+1); tmp += (float)pix.y - pix.x;
	tmp = 0.25f*tmp + rgb.y;
	rgb.x = CLAMP_255(tmp);
	return rgb;
}

inline __device__ uchar3 getGreen1(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, 0);

	pix = tex2D(bayerTexG,x-1,y); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y); tmp += (float)pix.y - pix.x;
	tmp = 0.5f*tmp + rgb.y;
	rgb.x = CLAMP_255(tmp);

	pix = tex2D(bayerTexG,x,y-1); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x,y+1); tmp += (float)pix.y - pix.x;
	tmp = 0.5f*tmp + rgb.y;
	rgb.z = CLAMP_255(tmp);
	return rgb;
}

inline __device__ uchar3 getGreen2(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, 0);

	pix = tex2D(bayerTexG,x,y-1); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x,y+1); tmp += (float)pix.y - pix.x;
	tmp = 0.5f*tmp + rgb.y;
	rgb.x = CLAMP_255(tmp);

	pix = tex2D(bayerTexG,x-1,y); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y); tmp += (float)pix.y - pix.x;
	tmp = 0.5f*tmp + rgb.y;
	rgb.z = CLAMP_255(tmp);
	return rgb;
}

__global__ void bayerFirstPass(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*srcH); idx += 2*(blockDim.x*gridDim.x)){

		float tmp, ver_c, hor_c;
		uint8_t value, pix;
		char4 res;

		int x = idx%srcW;
		int y = idx/srcW;
		if(!(y&1)) x++;

		pix = tex2D(bayerTex,x,y);
		hor_c = 2.0f*pix-tex2D(bayerTex,x-2,y)-tex2D(bayerTex,x+2,y);
		ver_c = 2.0f*pix-tex2D(bayerTex,x,y-2)-tex2D(bayerTex,x,y+2);

		tmp = 0.5f*(tex2D(bayerTex,x-1,y)+tex2D(bayerTex,x+1,y)) + 0.25f*hor_c;
		res.x = value = CLAMP_255(tmp);
		res.z = pix - value;

		tmp = 0.5f*(tex2D(bayerTex,x,y-1)+tex2D(bayerTex,x,y+1)) + 0.25f*ver_c;
		res.y = value = CLAMP_255(tmp);
		res.w = pix - value;

		surf2Dwrite(res, bayerSurfMask, sizeof(char4)*(x>>1), y);
	}
}

__device__ uchar2 getD(int x, int y){
	char4 tmp;
	tmp = tex2D(bayerTexMask,x,y);
	uchar2 res = make_uchar2(tmp.z,tmp.w);	
	tmp = tex2D(bayerTexMask,x+1,y);
	res.x = abs(res.x - (uint8_t)tmp.z);
	tmp = tex2D(bayerTexMask,x,y+2);
	res.y = abs(res.y - (uint8_t)tmp.w);
	return res;
}

__device__ int getDirection(int x, int y){
	int2 delta = make_int2(0,0);	
	uchar2 tmp;

// 	tmp = getD(x-1,y-2);
// 	delta.x += tmp.x; delta.y += tmp.y;
	tmp = getD(x,y-2);
	delta.x += tmp.x; delta.y += 3*tmp.y;
// 	tmp = getD(x+1,y-2);
// 	delta.x += tmp.x; delta.y += tmp.y;

	tmp = getD(x,y-1);
	delta.x += tmp.x; delta.y += tmp.y;
	tmp = getD((y&1)?(x-1):(x+1),y-1);
	delta.x += tmp.x; delta.y += tmp.y;

	tmp = getD(x-1,y);
	delta.x += 3*tmp.x; delta.y += tmp.y;
	tmp = getD(x,y);
	delta.x += 6*tmp.x; delta.y += 6*tmp.y;
	tmp = getD(x+1,y);
	delta.x += 3*tmp.x; delta.y += tmp.y;

	tmp = getD(x,y+1);
	delta.x += tmp.x; delta.y += tmp.y;
	tmp = getD((y&1)?(x-1):(x+1),y+1);
	delta.x += tmp.x; delta.y += tmp.y;

// 	tmp = getD(x-1,y+2);
// 	delta.x += tmp.x; delta.y += tmp.y;
	tmp = getD(x,y+2);
	delta.x += tmp.x; delta.y += 3*tmp.y;
// 	tmp = getD(x+1,y+2);
// 	delta.x += tmp.x; delta.y += tmp.y;
	
	return delta.x - delta.y;
}

__global__ void bayerSecondPass(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*srcH); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = idx/srcW;

		uchar2 gc;
		int dir;
		char4 ginfo;

		if(y&1){ //BG row
			gc = make_uchar2(0, tex2D(bayerTex,x,y));
			dir = getDirection(x>>1,y);
			ginfo = tex2D(bayerTexMask,x>>1,y);
			gc.x = (dir < 0) ? ginfo.x : ginfo.y;
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);

			++x;
			gc = make_uchar2(tex2D(bayerTex,x,y), 0);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
		}else{ //GR row
			gc = make_uchar2(tex2D(bayerTex,x,y), 0);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
			
			++x;
			gc = make_uchar2(0, tex2D(bayerTex,x,y));
			dir = getDirection(x>>1,y);
			ginfo = tex2D(bayerTexMask,x>>1,y);
			gc.x = (dir < 0) ? ginfo.x : ginfo.y;
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
		}
	}
}

void CudaBayerConverter::freeExtraBuffers(){
	cudaSafe(cudaFreeArray(bayerFirstPassBuffer));
	cudaSafe(cudaFreeArray(bayerFirstPassMaskBuffer));
}

void CudaBayerConverter::allocateExtraBuffers(){
	cudaChannelFormatDesc byte4Channel = cudaCreateChannelDesc<char4>();
	cudaSafe(cudaMallocArray(&bayerFirstPassMaskBuffer, &byte4Channel,
				 width>>1, height, cudaArraySurfaceLoadStore));
	cudaSafe(cudaBindSurfaceToArray(bayerSurfMask, bayerFirstPassMaskBuffer));
	cudaSafe(cudaBindTextureToArray(bayerTexMask, bayerFirstPassMaskBuffer));
	bayerTexMask.normalized = 0;
	bayerTexMask.addressMode[0] = cudaAddressModeClamp;
	bayerTexMask.addressMode[1] = cudaAddressModeClamp;
	bayerTexMask.filterMode = cudaFilterModePoint;

	cudaChannelFormatDesc u2byteChannel = cudaCreateChannelDesc<uchar2>();
	cudaSafe(cudaMallocArray(&bayerFirstPassBuffer, &u2byteChannel,
				 width, height, cudaArraySurfaceLoadStore));
	cudaSafe(cudaBindSurfaceToArray(bayerSurfG, bayerFirstPassBuffer));
	cudaSafe(cudaBindTextureToArray(bayerTexG, bayerFirstPassBuffer));
	bayerTexG.normalized = 0;
	bayerTexG.addressMode[0] = cudaAddressModeClamp;
	bayerTexG.addressMode[1] = cudaAddressModeClamp;
	bayerTexG.filterMode = cudaFilterModePoint;
}

void CudaBayerConverter::performInitialPasses(){
	bayerFirstPass<<< grid, block, 0, stream >>>(width, height);
	cudaSafe(cudaStreamSynchronize(stream));
	bayerSecondPass<<< grid, block, 0, stream >>>(width, height);
	cudaSafe(cudaStreamSynchronize(stream));
}

