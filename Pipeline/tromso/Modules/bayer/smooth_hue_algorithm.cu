// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

texture<uint8_t, 2, cudaReadModeElementType> bayerTex;
texture<uchar2, 2, cudaReadModeElementType> bayerTexG;
surface<void, cudaSurfaceType2D> bayerSurfG;

#define FAST_SMOOTH_HUE

inline __device__ void getPixels(uchar2 *array, int x, int y){
	array[0] = tex2D(bayerTexG,x,y-1);
	array[1] = tex2D(bayerTexG,x+1,y-1);
	array[2] = tex2D(bayerTexG,x+2,y-1);
	array[3] = tex2D(bayerTexG,x-1,y);
	array[4] = tex2D(bayerTexG,x,y);
	array[5] = tex2D(bayerTexG,x+1,y);
	array[6] = tex2D(bayerTexG,x+2,y);
	array[7] = tex2D(bayerTexG,x-1,y+1);
	array[8] = tex2D(bayerTexG,x,y+1);
	array[9] = tex2D(bayerTexG,x+1,y+1);
	array[10] = tex2D(bayerTexG,x+2,y+1);
	array[11] = tex2D(bayerTexG,x-1,y+2);
	array[12] = tex2D(bayerTexG,x,y+2);
	array[13] = tex2D(bayerTexG,x+1,y+2);
}

__global__ void bayerGreenPass(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*srcH); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = idx/srcW;
		uchar2 gc;
		float tmp;
		float4 values;
		if(y&1){ //BG row
			gc = make_uchar2(0, tex2D(bayerTex,x,y));
			values = make_float4(
					tex2D(bayerTex,x,y-1), tex2D(bayerTex,x-1,y),
					tex2D(bayerTex,x+1,y), tex2D(bayerTex,x,y+1));
			tmp = 0.25f*(values.x + values.y + values.z + values.w);
			gc.x = CLAMP_255(tmp);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);

			++x;
			gc = make_uchar2(tex2D(bayerTex,x,y), 0);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
		}else{ //GR row
			gc = make_uchar2(tex2D(bayerTex,x,y), 0);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
			
			++x;
			gc = make_uchar2(0, tex2D(bayerTex,x,y));
			values = make_float4(
					tex2D(bayerTex,x,y-1), tex2D(bayerTex,x-1,y),
					tex2D(bayerTex,x+1,y), tex2D(bayerTex,x,y+1));
			tmp = 0.25f*(values.x + values.y + values.z + values.w);
			gc.x = CLAMP_255(tmp);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
		}
	}
}

// This is slower. Odd
// inline __device__ float getSquare(int x, int y){
// #define p(i,j) (tex2D(bayerTexG,x+i,y+j))
// #define g(i,j) ((float)tex2D(bayerTexG,x+i,y+j).x)
// #define c(i,j) ((float)tex2D(bayerTexG,x+i,y+j).y)
// 	uchar2 pix;
// 	float tmp;
// 	pix = p(-1,-1); tmp = (float)pix.y / (float)pix.x;
// 	pix = p(1,-1); tmp += (float)pix.y / (float)pix.x;
// 	pix = p(-1,1); tmp += (float)pix.y / (float)pix.x;
// 	pix = p(1,1); tmp += (float)pix.y / (float)pix.x;
// 	return 0.25f*g(0,0)*(tmp);
// #undef p
// #undef g
// #undef c
// }

inline __device__ float getSquare(int x, int y){
#define g(i,j) ((float)tex2D(bayerTexG,x+i,y+j).x)
#define c(i,j) ((float)tex2D(bayerTexG,x+i,y+j).y)
	return 0.25f*g(0,0)*( c(-1,-1)/g(-1,-1) + c(1,-1)/g(1,-1) + c(-1,1)/g(-1,1) + c(1,1)/g(1,1) );
#undef g
#undef c
}

inline __device__ float getVertical(int x, int y){
#define g(i,j) ((float)tex2D(bayerTexG,x+i,y+j).x)
#define c(i,j) ((float)tex2D(bayerTexG,x+i,y+j).y)
	return 0.5f*g(0,0)*( c(0,-1)/g(0,-1) + c(0,1)/g(0,1) );
#undef g
#undef c
}

inline __device__ float getHorizontal(int x, int y){
#define g(i,j) ((float)tex2D(bayerTexG,x+i,y+j).x)
#define c(i,j) ((float)tex2D(bayerTexG,x+i,y+j).y)
	return 0.5f*g(0,0)*( c(-1,0)/g(-1,0) + c(1,0)/g(1,0) );
#undef g
#undef c
}

inline __device__ uchar3 getRed(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(pix.y, pix.x, 0);

	tmp =  getSquare(x,y);
	rgb.z = CLAMP_255(tmp);
	return rgb;
}

inline __device__ uchar3 getBlue(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, pix.y);

	tmp =  getSquare(x,y);
	rgb.x = CLAMP_255(tmp);
	return rgb;
}

inline __device__ uchar3 getGreen1(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, 0);

	tmp = getHorizontal(x,y);
	rgb.x = CLAMP_255(tmp);

	tmp = getVertical(x,y);
	rgb.z = CLAMP_255(tmp);
	return rgb;
}

inline __device__ uchar3 getGreen2(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, 0);

	tmp = getVertical(x,y);
	rgb.x = CLAMP_255(tmp);

	tmp = getHorizontal(x,y);
	rgb.z = CLAMP_255(tmp);
	return rgb;
}

void CudaBayerConverter::freeExtraBuffers(){
	cudaSafe(cudaFreeArray(bayerFirstPassBuffer));
}

void CudaBayerConverter::allocateExtraBuffers(){
	cudaChannelFormatDesc u2byteChannel = cudaCreateChannelDesc<uchar2>();
	cudaSafe(cudaMallocArray(&bayerFirstPassBuffer, &u2byteChannel,
				 width, height, cudaArraySurfaceLoadStore));
	cudaSafe(cudaBindSurfaceToArray(bayerSurfG, bayerFirstPassBuffer));
	cudaSafe(cudaBindTextureToArray(bayerTexG, bayerFirstPassBuffer));
	bayerTexG.normalized = 0;
	bayerTexG.addressMode[0] = cudaAddressModeClamp;
	bayerTexG.addressMode[1] = cudaAddressModeClamp;
	bayerTexG.filterMode = cudaFilterModePoint;
}

void CudaBayerConverter::performInitialPasses(){
	bayerGreenPass<<< grid, block, 0, stream >>>(width, height);
	cudaSafe(cudaStreamSynchronize(stream));
}

