// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

texture<uint8_t, 2, cudaReadModeElementType> bayerTex;
texture<uchar2, 2, cudaReadModeElementType> bayerTexG;
surface<void, cudaSurfaceType2D> bayerSurfG;

#define DO_FAST_KERNEL

__device__ __inline float max(float4 a){
	return max(max(a.x,a.y),max(a.z,a.w));
}

__device__ __inline float min(float4 a){
	return min(min(a.x,a.y),min(a.z,a.w));
}

__device__ __inline float4 removeLower(float4 a, float thresh){
	if(a.x < thresh) a.x = 0;
	if(a.y < thresh) a.y = 0;
	if(a.z < thresh) a.z = 0;
	if(a.w < thresh) a.w = 0;
	return a;
}

__device__ float getWeightedGreen(int x, int y){
#define p(a,b) ((float)tex2D(bayerTex,x+a,y+b))
		float4 alphaW, dirG; 
		float tmp, tmp2;
#if DO_WEIGHTED_ADAPTIVE == 8
		float4 alphaW2, dirG2;
#endif
		float self = p(0,0);
// 		/* West */
// 		dirG.x = p(-1,0)+0.5f*(p(0,0)-p(-2,0));
// 		alphaW.x = 1.0f/(1 + fabsf(p(1,0)-p(-1,0)) + fabsf(p(-1,0)-p(-3,0))
// 				+ fabsf(p(0,0)-p(-2,0)) + 0.5f*(
// 					fabsf(p(0,-1)-p(-2,-1)) + fabsf(p(0,1)-p(-2,1))));
// 		/* East */
// 		dirG.y = p(1,0)+0.5f*(p(0,0)-p(2,0));
// 		alphaW.y = 1.0f/(1 + fabsf(p(-1,0)-p(1,0)) + fabsf(p(1,0)-p(3,0))
// 				+ fabsf(p(0,0)-p(2,0)) + 0.5f*(
// 					fabsf(p(0,1)-p(2,1)) + fabsf(p(0,-1)-p(2,-1))));
// 		/* North */
// 		dirG.z = p(0,-1)+0.5f*(p(0,0)-p(0,-2));
// 		alphaW.z = 1.0f/(1 + fabsf(p(0,1)-p(0,-1)) + fabsf(p(0,-1)-p(0,-3))
// 				+ fabsf(p(0,0)-p(0,-2)) + 0.5f*(
// 					fabsf(p(-1,0)-p(-1,-2)) + fabsf(p(1,0)-p(1,-2))));
// 		/* South */
// 		dirG.w = p(0,1)+0.5f*(p(0,0)-p(0,2));
// 		alphaW.w = 1.0f/(1 + fabsf(p(0,-1)-p(0,1)) + fabsf(p(0,1)-p(0,3))
// 				+ fabsf(p(0,0)-p(0,2)) + 0.5f*(
// 					fabsf(p(1,0)-p(1,2)) + fabsf(p(-1,0)-p(-1,2))));
		/* West */
		tmp = self-p(-2,0);
		dirG.x = p(-1,0)+0.5f*tmp;
		tmp2 = 1 + fabsf(p(1,0)-p(-1,0));
		alphaW.x = 1.0f/(tmp2 + fabsf(p(-1,0)-p(-3,0))
				+ fabsf(tmp) + 0.5f*(fabsf(p(0,-1)-p(-2,-1)) + fabsf(p(0,1)-p(-2,1))));
		/* East */
		tmp = self-p(2,0);
		dirG.y = p(1,0)+0.5f*tmp;
		alphaW.y = 1.0f/(tmp2 + fabsf(p(1,0)-p(3,0))
				+ fabsf(tmp) + 0.5f*(fabsf(p(0,1)-p(2,1)) + fabsf(p(0,-1)-p(2,-1))));
		/* North */
		tmp = self-p(0,-2);
		dirG.z = p(0,-1)+0.5f*tmp;
		tmp2 = 1 + fabsf(p(0,1)-p(0,-1));
		alphaW.z = 1.0f/(tmp2 + fabsf(p(0,-1)-p(0,-3)) + fabsf(tmp)
				+ 0.5f*(fabsf(p(-1,0)-p(-1,-2)) + fabsf(p(1,0)-p(1,-2))));
		/* South */
		tmp = self-p(0,2);
		dirG.w = p(0,1)+0.5f*tmp;
		alphaW.w = 1.0f/(tmp2 + fabsf(p(0,1)-p(0,3)) + fabsf(tmp)
				+ 0.5f*(fabsf(p(1,0)-p(1,2)) + fabsf(p(-1,0)-p(-1,2))));

#if DO_WEIGHTED_ADAPTIVE == 8
		/* NW */
		dirG2.x = 0.5f*(p(0,-1)+p(-1,0)+p(0,0)-0.5f*(p(0,-2)+p(-2,0)));
		alphaW2.x = 1.0f/(1 + fabsf(p(1,1)-p(-1,-1)) + fabsf(p(-1,-1)-p(-3,-3))
				+ fabsf(p(0,0)-p(-2,-2)) + 0.5f*(
					fabsf(p(0,1)-p(-2,-1) + fabsf(p(1,0)-p(-1,-2)))));
		/* SE */
		dirG2.y = 0.5f*(p(0,1)+p(1,0)+p(0,0)-0.5f*(p(0,2)+p(2,0)));
		alphaW2.y = 1.0f/(1 + fabsf(p(-1,-1)-p(1,1)) + fabsf(p(1,1)-p(3,3))
				+ fabsf(p(0,0)-p(2,2)) + 0.5f*(
					fabsf(p(0,-1)-p(2,1) + fabsf(p(-1,0)-p(1,2)))));
		/* NE */
		dirG2.z = 0.5f*(p(0,-1)+p(1,0)+p(0,0)-0.5f*(p(0,-2)+p(2,0)));
		alphaW2.z = 1.0f/(1 + fabsf(p(1,-1)-p(-1,1)) + fabsf(p(-1,1)-p(-3,3))
				+ fabsf(p(0,0)-p(-2,2)) + 0.5f*(
					fabsf(p(0,-1)-p(-2,1) + fabsf(p(1,0)-p(-1,2)))));
		/* SW */
		dirG2.w = 0.5f*(p(0,1)+p(-1,0)+p(0,0)-0.5f*(p(0,2)+p(-2,0)));
		alphaW2.w = 1.0f/(1 + fabsf(p(-1,1)-p(1,-1)) + fabsf(p(1,-1)-p(3,-3))
				+ fabsf(p(0,0)-p(2,-2)) + 0.5f*(
					fabsf(p(0,1)-p(2,-1) + fabsf(p(-1,0)-p(1,-2)))));

		/* Uncomment these for VNG variant */
// 		float minAlpha = min(min(alphaW2),min(alphaW));
// 		float maxAlpha = max(max(alphaW2),max(alphaW));
// 		float thresh =  maxAlpha - 0.5f*(maxAlpha-minAlpha);
// 		alphaW2 = removeLower(alphaW2,thresh);
#else
// 		float minAlpha = min(alphaW);
// 		float maxAlpha = max(alphaW);
// 		float thresh =  maxAlpha - (1.5f*minAlpha + 0.5f*(maxAlpha-minAlpha));
#endif
// 		if(alphaW.x < thresh && alphaW.y < thresh && alphaW.z < thresh && alphaW.w < thresh)
// 			printf("min %8.3f\t max %8.3f\t t %8.3f\t x %8.3f\t y %8.3f\n",
// 					minAlpha, maxAlpha, thresh, alphaW.x, alphaW.y);
// 		alphaW = removeLower(alphaW,thresh);

		tmp = dirG.x*alphaW.x + dirG.y*alphaW.y + dirG.z*alphaW.z + dirG.w*alphaW.w;
		tmp2 = alphaW.x+alphaW.y+alphaW.z+alphaW.w;
#if DO_WEIGHTED_ADAPTIVE == 8
		tmp += dirG2.x*alphaW2.x + dirG2.y*alphaW2.y + dirG2.z*alphaW2.z + dirG2.w*alphaW2.w;
		tmp2 += alphaW2.x+alphaW2.y+alphaW2.z+alphaW2.w;
#endif
		return tmp / tmp2;
#undef p
}

__global__ void bayerWeightedAdaptiveGreenPass(int srcW, int srcH){
	for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
			idx < (srcW*srcH); idx += 2*(blockDim.x*gridDim.x)){

		int x = idx%srcW;
		int y = idx/srcW;

		float tmp;
		uchar2 gc;
		if(y&1){ //BG row
			gc = make_uchar2(0, tex2D(bayerTex,x,y));
			tmp = getWeightedGreen(x,y);
			gc.x = CLAMP_255(tmp);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);

			++x;
			gc = make_uchar2(tex2D(bayerTex,x,y), 0);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
		}else{ //GR row
			gc = make_uchar2(tex2D(bayerTex,x,y), 0);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
			
			++x;
			gc = make_uchar2(0, tex2D(bayerTex,x,y));
			tmp = getWeightedGreen(x,y);
			gc.x = CLAMP_255(tmp);
			surf2Dwrite(gc, bayerSurfG, sizeof(uchar2)*x, y);
		}
	}
}

inline __device__ uchar3 getRed(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(pix.y, pix.x, 0);

	pix = tex2D(bayerTexG,x-1,y-1); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y-1); tmp += (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x-1,y+1); tmp += (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y+1); tmp += (float)pix.y - pix.x;
	tmp = 0.25f*tmp + rgb.y;
	rgb.z = rintf(CLAMP_255(tmp));
	return rgb;
}

inline __device__ uchar3 getBlue(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, pix.y);

	pix = tex2D(bayerTexG,x-1,y-1); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y-1); tmp += (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x-1,y+1); tmp += (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y+1); tmp += (float)pix.y - pix.x;
	tmp = 0.25f*tmp + rgb.y;
	rgb.x = rintf(CLAMP_255(tmp));
	return rgb;
}

inline __device__ uchar3 getGreen1(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, 0);

	pix = tex2D(bayerTexG,x-1,y); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y); tmp += (float)pix.y - pix.x;
	tmp = 0.5f*tmp + rgb.y;
	rgb.x = rintf(CLAMP_255(tmp));

	pix = tex2D(bayerTexG,x,y-1); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x,y+1); tmp += (float)pix.y - pix.x;
	tmp = 0.5f*tmp + rgb.y;
	rgb.z = rintf(CLAMP_255(tmp));
	return rgb;
}

inline __device__ uchar3 getGreen2(int x, int y){
	uchar3 rgb;
	uchar2 pix;
	float tmp;

	pix = tex2D(bayerTexG,x,y);
	rgb = make_uchar3(0, pix.x, 0);

	pix = tex2D(bayerTexG,x,y-1); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x,y+1); tmp += (float)pix.y - pix.x;
	tmp = 0.5f*tmp + rgb.y;
	rgb.x = rintf(CLAMP_255(tmp));

	pix = tex2D(bayerTexG,x-1,y); tmp = (float)pix.y - pix.x;
	pix = tex2D(bayerTexG,x+1,y); tmp += (float)pix.y - pix.x;
	tmp = 0.5f*tmp + rgb.y;
	rgb.z = rintf(CLAMP_255(tmp));
	return rgb;
}

inline __device__ void getPixels(uchar2 *array, int x, int y){
	array[0] = tex2D(bayerTexG,x,y-1);
	array[1] = tex2D(bayerTexG,x+2,y-1);
	array[2] = tex2D(bayerTexG,x-1,y);
	array[3] = tex2D(bayerTexG,x,y);
	array[4] = tex2D(bayerTexG,x+1,y);
	array[5] = tex2D(bayerTexG,x,y+1);
	array[6] = tex2D(bayerTexG,x+1,y+1);
	array[7] = tex2D(bayerTexG,x+2,y+1);
	array[8] = tex2D(bayerTexG,x-1,y+2);
	array[9] = tex2D(bayerTexG,x+1,y+2);
}

void CudaBayerConverter::freeExtraBuffers(){
	cudaSafe(cudaFreeArray(bayerFirstPassBuffer));
}

void CudaBayerConverter::allocateExtraBuffers(){
	cudaChannelFormatDesc u2byteChannel = cudaCreateChannelDesc<uchar2>();
	cudaSafe(cudaMallocArray(&bayerFirstPassBuffer, &u2byteChannel,
				 width, height, cudaArraySurfaceLoadStore));
	cudaSafe(cudaBindSurfaceToArray(bayerSurfG, bayerFirstPassBuffer));
	cudaSafe(cudaBindTextureToArray(bayerTexG, bayerFirstPassBuffer));
	bayerTexG.normalized = 0;
	bayerTexG.addressMode[0] = cudaAddressModeClamp;
	bayerTexG.addressMode[1] = cudaAddressModeClamp;
	bayerTexG.filterMode = cudaFilterModePoint;
}

void CudaBayerConverter::performInitialPasses(){
	bayerWeightedAdaptiveGreenPass<<< grid, block, 0, stream >>>(width, height);
	cudaSafe(cudaStreamSynchronize(stream));
}
