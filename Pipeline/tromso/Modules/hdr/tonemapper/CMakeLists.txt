set(GPU_HDR_SOURCES	
   ${GPU_HDR_SOURCES}
   ${CMAKE_CURRENT_SOURCE_DIR}/tonemapper.h
   ${CMAKE_CURRENT_SOURCE_DIR}/tonemapper.cpp
   ${CMAKE_CURRENT_SOURCE_DIR}/tonemapper_ward.h
   ${CMAKE_CURRENT_SOURCE_DIR}/tonemapper_ward.cpp
   ${CMAKE_CURRENT_SOURCE_DIR}/tonemapper_histward.h
   ${CMAKE_CURRENT_SOURCE_DIR}/tonemapper_histward.cpp
   ${CMAKE_CURRENT_SOURCE_DIR}/tonemapper_reinhard.h
   ${CMAKE_CURRENT_SOURCE_DIR}/tonemapper_reinhard.cpp

   PARENT_SCOPE	
)


