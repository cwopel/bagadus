// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "tonemapper_reinhard.h"
#include "../utils/utils.h"
#include "../utils/defines.h"
#include <cmath>

extern "C" void cu_tone_mapper_reinhard(cudaStream_t stream, cudaArray* output, double* d_tmp_image, struct header* metadata, int numSources, int width, int height, float scene_key_brightness);

extern "C" void cu_tone_mapper_reinhard_init(int numSources, int width, int height);


ToneMapperReinhard::ToneMapperReinhard()
{
	//so far do nothing
	initialized = false;
}


ToneMapperReinhard::~ToneMapperReinhard()
{
	if(initialized)
	{
		//TODO: free stuff
	}
		

}

void ToneMapperReinhard::init(int numSources, int width, int height)
{

	//cu_tone_mapper_reinhard_init(numSources, width, height, plan_image, plan_gauss_filter, plan_reverse, gauss_kernels);
	cu_tone_mapper_reinhard_init(numSources, width, height);
	
	//cuda_check(cudaMalloc(&d_tmp_image, sizeof(double) * width * height * numSources), "tonemapper_reinhard: init: failed to allocate temp image");
	
    	

	initialized = true;
}

/**
 * performs a single exectuion of the tone mapper e.g. upload everything, execute, download result
 */
void ToneMapperReinhard::run(cudaStream_t stream, cudaArray* output, struct header* metadata, int numSources, int width, int height, float scene_key_brightness)
{


	//cu_tone_mapper_reinhard(stream, output, d_tmp_image, metadata, numSources, width, height,  plan_image,  plan_gauss_filter,  plan_reverse, gauss_kernels, complex_input_image, complex_convolved_Images, real_convolved_Images);
	cu_tone_mapper_reinhard(stream, output, d_tmp_image, metadata, numSources, width, height, scene_key_brightness);

}
