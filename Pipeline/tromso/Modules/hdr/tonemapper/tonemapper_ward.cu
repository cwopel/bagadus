// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <iostream>
#include <cstdio>
#include "../utils/utils.h"
#include "../utils/defines.h"
#include "Modules/Module.hpp"




#ifdef DEBUG
__global__ void calc_accum_lum(cudaTextureObject_t input_tex, int width, int height);

#endif




/**
 * multiply each pixel with a fixed scale factor
 * @param settings     pointer to settings struct
 * @param scale_factor scale factor to multiply with
 */
__global__ void tone_mapper(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, int width, int height, float scale_factor)
{

    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {
        float3 rgb = {0.0f, 0.0f, 0.0f};
        //uchar4& in_out_pixel = *(uchar4*)&(settings->d_out_image[(y_pos * settings->d_image_dimensions.x + x_pos)<<2]);
        uchar4 in_pixel = read_pxl(input_tex, x_pos, y_pos, width);

        rp_to_flt3(&in_pixel, &rgb);



        float3 tmp_col = {0.0f, 0.0f, 0.0f};

        tmp_col = scale_factor * rgb;

    #ifdef USE_GAMMA_CORRECTION
        tmp_col.x = powf(tmp_col.x, 1.0f/GAMMA_VALUE);
        tmp_col.y = powf(tmp_col.y, 1.0f/GAMMA_VALUE);
        tmp_col.z = powf(tmp_col.z, 1.0f/GAMMA_VALUE);
    #endif

        rgb.x = (unsigned char)d_clip((int)(tmp_col.x * 255.0f), 0, 255);
        rgb.y = (unsigned char)d_clip((int)(tmp_col.y * 255.0f), 0, 255);
        rgb.z = (unsigned char)d_clip((int)(tmp_col.z * 255.0f), 0, 255);

        write_pxl_rgb(output_tex, x_pos, y_pos, width, rgb);


    }

}

/**
 * calculate scale factor according to paper based on the average luminance of the image and the DISPLAY_BRIGHTNESS
 * @param  settings              pointer to settings struct
 * @param  accumulated_luminance average luminance of the image
 * @return                       scale factor to be applied to each pixel
 */
double calculate_scale_factor(double accumulated_luminance)
{

    double scale_factor = 0.0;
    double display_luminance = DISPLAY_LUMINANCE;

    //accumulated_luminance = 0.1212;
    double numerator = 1.219 + pow((display_luminance/2), 0.4);
    double denumerator = 1.219 +  pow(accumulated_luminance, 0.4);
    scale_factor = pow((numerator/denumerator), 2.5) / display_luminance;
    //std::cout << "numerator: " << numerator << ", denumerator: " << denumerator << std::endl;
    return scale_factor;
}

extern "C" void cu_tone_mapper_ward(cudaStream_t stream, cudaArray* output, double* d_tmp_image, struct header * metadata, int numSources, int width, int height)
{

 //   std::cout << "starting tone mapper" << std::endl;
    cudaError_t err;

    height = height * numSources; //treat incoming images as one big

    dim3 threadsPerBlock(16, 16);
    dim3 numBlocks(divup(width, threadsPerBlock.x), divup(height, threadsPerBlock.y));


    //create a texture to read from
    cudaTextureObject_t input_tex = 0;
    cudaSurfaceObject_t output_tex = 0;
    create_tex_and_surf_obj(output, &input_tex, &output_tex);


    //first launch parallel reduction
    //double accumulated_luminance = 0.0f;
    double accumulated_luminance = 0.1212;
    accumulated_luminance = run_reduce<double>(width * height, width, input_tex, d_tmp_image, stream);
    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "reduction kernel launch fail: " << cudaGetErrorString(err) << std::endl;


    //calc_accum_lum<<<1,1>>>(input_tex, width, height);

    double scale_factor = calculate_scale_factor(accumulated_luminance);

    //std::cout << "cu_tone_mapper_ward: scale_factor: " << scale_factor << ", accumulated_luminance: " << accumulated_luminance << std::endl;

    //then the actual tonemapper
    tone_mapper<<<numBlocks, threadsPerBlock, 0, stream>>>(input_tex, output_tex, width, height, scale_factor);

    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "tone mapper kernel launch fail: " << cudaGetErrorString(err) << std::endl;

    cuda_check(cudaDestroyTextureObject(input_tex), "HDR: failed destroying texture object"); 
    cuda_check(cudaDestroySurfaceObject(output_tex), "HDR: failed destroying surface object"); 
}



#ifdef DEBUG
__global__ void calc_accum_lum(cudaTextureObject_t input_tex, int width, int height)
{
    uchar4 in_pixel_root = tex2D<uchar4>(input_tex, 0, 0);
    float3 float_pixel = {0.0f, 0.0f, 0.0f};
    rp_to_flt3(&in_pixel_root, &float_pixel);
    double offset = 0.0000001;
    int num_pixels = width * height;

    //double sum = (double)logf((float_pixel.x + float_pixel.y + float_pixel.z)/3.0f + offset);
    //double sum = (double) ( logf(float_pixel.x + offset) + logf(float_pixel.y + offset) + logf(float_pixel.z + offset)) / 3.0f;
    double sum = 0.0;
    sum = log((float_pixel.x + offset)*0.27 + (float_pixel.y + offset)*0.67 + (float_pixel.z + offset)*0.06);
    //sum = (double)pow(((float_pixel.x + offset)*0.27 + (float_pixel.y + offset)*0.67 + (float_pixel.z + offset)*0.06), (1.0/(double) num_pixels));
    double c = 0.0;
    int x = 0, y = 0;
    for (int pixel_it = 0; pixel_it < num_pixels; ++pixel_it)
    {
        x = pixel_it % width;
        y = pixel_it / width;
        in_pixel_root = tex2D<uchar4>(input_tex, x, y);
        rp_to_flt3(&in_pixel_root, &float_pixel);

        c =  log((float_pixel.x + offset)*0.27f + (float_pixel.y + offset)*0.67f + (float_pixel.z + offset)*0.06f);
        sum += c;
        //c =  pow( ((float_pixel.x + offset)*0.27f + (float_pixel.y + offset)*0.67f + (float_pixel.z + offset)*0.06f), (1.0/(double)num_pixels));
        //sum *= c;

    }

    sum = exp(sum/num_pixels);
    printf("1D kernel accumulated_luminance: %f\n", sum);

    in_pixel_root = tex2D<uchar4>(input_tex, 0, 0);
    sum = 0.0;
    rp_to_flt3(&in_pixel_root, &float_pixel);
    sum = (double) pow(((float_pixel.x + offset)*0.27 + (float_pixel.y + offset)*0.67 + (float_pixel.z + offset)*0.06), (1.0/(double)num_pixels));
    c = 0.0;

    for(int y_it = 0; y_it < height; ++y_it)
    {
        for(int x_it = 0; x_it < width; ++x_it)
        {
            in_pixel_root = tex2D<uchar4>(input_tex, x_it, y_it);
            rp_to_flt3(&in_pixel_root, &float_pixel);

            c =  pow( ((float_pixel.x + offset)*0.27f + (float_pixel.y + offset)*0.67f + (float_pixel.z + offset)*0.06f), (1.0/(double)num_pixels));
            sum *= c;

        }
    }

    printf("2D kernel accumulated_luminance: %f\n", sum);


}

#endif
