// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "../utils/image.h"


#ifdef DEBUG
//forward declarations
__device__ void cu_print_histogram_and_cdf(unsigned int* histogram, float* _cdf);
__device__ void cu_print_histogram(unsigned int* histogram);
__device__ void cu_print_cdf(float* cdf);

void print_histogram_and_cdf(unsigned int* histogram, float* _cdf);
void print_histogram(unsigned int* histogram);
void write_foveal_image(float3* _foveal_image, int2 img_dim) ;
#endif


/**
 * computes a low res image of the input. One pixel roughly corresponds to one degree in Field of View
 * @param settings      pointer to settings struct
 * @param[out] _foveal_image pointer where the result will be written to
 * @param img_dim       size of the resulting low-res image
 * @param filter_dim    number of input pixels merged into one resulting output pixel
 */

__global__ void compute_foveal_image(cudaTextureObject_t in_image, int width, int height, float3* _foveal_image,  int2 img_dim, int2 filter_dim)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    
   
    if(x_pos < img_dim.x  && y_pos < img_dim.y)
    {
        //sum up all pixels for that quadrant
        float3 pixel_sum = {0.0f, 0.0f, 0.0f};
        int2 curr_window_offset = {x_pos * filter_dim.x, y_pos * filter_dim.y};
        int2 offset_in_window = {0, 0};
        int num_pixels = 0;
        for(int y_it = 0; y_it < filter_dim.y; ++y_it)
        {
            for(int x_it = 0; x_it < filter_dim.x; ++x_it)
            {
                offset_in_window.x = curr_window_offset.x + x_it;
                offset_in_window.y = curr_window_offset.y + y_it;
                if(offset_in_window.x < width && offset_in_window.y < height)
                {    
                    uchar4 in_pixel = read_pxl(in_image, offset_in_window.x, offset_in_window.y, width);
                    float3 converted_in_pixel = {0.0f, 0.0f, 0.0f};
                    rp_to_flt3(&in_pixel, &converted_in_pixel);
                    


                        pixel_sum.x += (converted_in_pixel.x); 
                        pixel_sum.y += (converted_in_pixel.y); 
                        pixel_sum.z += (converted_in_pixel.z); 
                        ++num_pixels;
                }
            }
        }        

        pixel_sum.x /= num_pixels;
        pixel_sum.y /= num_pixels;
        pixel_sum.z /= num_pixels;

        write_pxl_rgb(_foveal_image, x_pos, y_pos, img_dim.x, pixel_sum);
    }
}
/**
 * gets the min and max value of a given foveal image
 * @param _foveal_image low-res image to find min and max of
 * @param img_dim       size of low-res image
 * @param[out] _min_max      found min and max
 */
__global__ void get_min_max(float3* _foveal_image, int2 img_dim, float2* _min_max)
{
    float luminosity = logf(read_pxl_yuv(_foveal_image, 0, 0, img_dim.x).x);
	float2 my_min_max = make_float2(luminosity,luminosity);
	int pixel_num = img_dim.y * img_dim.x;
	for(int i=threadIdx.x; i<pixel_num; i += blockDim.x)
	{
		int x_pos = i % img_dim.x;
		int y_pos = i / img_dim.x;
		luminosity = logf(read_pxl_yuv(_foveal_image, x_pos, y_pos, img_dim.x).x);
		my_min_max.x = min(my_min_max.x,luminosity);
		my_min_max.y = max(my_min_max.y,luminosity);
	}

	__shared__ float2 all_min_max[256];
	all_min_max[threadIdx.x] = my_min_max;
	__syncthreads();
	/* 256 values -> 128 values */
	if(threadIdx.x & 1) {
		all_min_max[threadIdx.x].x = min(all_min_max[threadIdx.x-1].x, all_min_max[threadIdx.x].x);
		all_min_max[threadIdx.x].y = max(all_min_max[threadIdx.x-1].y, all_min_max[threadIdx.x].y);
	}
	__syncthreads();
	/* 128 values -> 64 values */
	if((threadIdx.x & 3)==3) {
		all_min_max[threadIdx.x].x = min(all_min_max[threadIdx.x-2].x, all_min_max[threadIdx.x].x);
		all_min_max[threadIdx.x].y = max(all_min_max[threadIdx.x-2].y, all_min_max[threadIdx.x].y);
	}
	__syncthreads();
	/* 64 values -> 32 values */
	if((threadIdx.x & 7)==7) {
		all_min_max[threadIdx.x].x = min(all_min_max[threadIdx.x-4].x, all_min_max[threadIdx.x].x);
		all_min_max[threadIdx.x].y = max(all_min_max[threadIdx.x-4].y, all_min_max[threadIdx.x].y);
	}
	__syncthreads();
	/* 32 values -> 16 values */
	if((threadIdx.x & 15)==15) {
		all_min_max[threadIdx.x].x = min(all_min_max[threadIdx.x-8].x, all_min_max[threadIdx.x].x);
		all_min_max[threadIdx.x].y = max(all_min_max[threadIdx.x-8].y, all_min_max[threadIdx.x].y);
	}
	__syncthreads();
	/* 16 values -> 8 values */
	if((threadIdx.x & 31)==31) {
		all_min_max[threadIdx.x].x = min(all_min_max[threadIdx.x-16].x, all_min_max[threadIdx.x].x);
		all_min_max[threadIdx.x].y = max(all_min_max[threadIdx.x-16].y, all_min_max[threadIdx.x].y);
	}
	__syncthreads();
	/* Loop over remaining values */
	if(!threadIdx.x){
		my_min_max = all_min_max[31];
		for(int i=63; i<blockDim.x; i += 32){
			my_min_max.x = min(my_min_max.x, all_min_max[i].x);
			my_min_max.y = max(my_min_max.y, all_min_max[i].y);
		}
		*_min_max = my_min_max;
		//printf("min: %f, max: %f\n", _min_max->x, _min_max->y);
	}
}

/**
 * calculate histogram of given low-res image. Number of bins set by NUM_HISTOGRAM_BINS
 * @param _foveal_image input low-res image
 * @param img_dim       size of input low-res image
 * @param[out] histogram     histogram to write to
 * @param _min_max      min and max value of given low-res image
 */
__global__ void calculate_histogram(float3* _foveal_image, int2 img_dim, unsigned int* histogram, float2* _min_max)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < img_dim.x  && y_pos < img_dim.y)
    {
        float3 in_pixel = read_pxl_yuv(_foveal_image, x_pos, y_pos, img_dim.x);
        float luminosity = in_pixel.x;
        float log_lum = min(_min_max->y, max(_min_max->x, logf(luminosity)));
        
        //calculate bin
        int bin = saturate((log_lum - _min_max->x)/(_min_max->y - _min_max->x)) * NUM_HISTOGRAM_BINS;
//        if(x_pos == 2 && y_pos == 2)
//         {
//             printf("calculate_histogram: luminosity: %f, log_lum: %f, bin: %i\n, min: %f, max: %f", luminosity, log_lum, bin, _min_max->x, _min_max->y);
//         }
        atomicAdd(&histogram[bin], (unsigned int)1);
    }


}

/**
 * calculate the minimal noticeable difference of the human visual system. Values that differ below a certain threshold to a given input
 * value are perceived as the same. The higher the input, the higher that difference can be (see TODO: find that reference)
 * @param  _value input value to find threshold
 * @return        threshold of just noticeable difference
 */
__device__ float calculate_noticeable_difference(float _value)
{
    float return_value = 0.0f;
    float log_val = log10f(_value);
    if(log_val < -3.94f)
        return_value = -2.86f;
    else if (log_val >= -3.94f && log_val < -1.44f)
        return_value = powf(0.405f * log_val + 1.6f, 2.18f) - 2.86f;
    else if (log_val >= -1.44f && log_val < -0.0184f)
        return_value = log_val - 0.395f;
    else if (log_val >= -0.0184 && log_val < 1.9f)
        return_value = powf(0.249f * logf(_value) + 0.65f, 2.7f) - 0.72f;
    else if (log_val >= 1.9f)
        return_value = log_val - 1.255f;

    return expf(return_value);
}

/**
 * calculates the world luminance that corresponds to a specific bin in the histogram
 * @param  _min_max minimum and maximum value found in the histogram
 * @param  _bin_num number of the histogram bin for which to calculate the luminance
 * @return          luminance value of the given histogram bin
 */
__device__ float bin_to_worldlum(float2* _min_max, int _bin_num)
{

    float return_value = 0.0f;
    if(_bin_num <= NUM_HISTOGRAM_BINS)
    {
        return_value = ((float)_bin_num + 0.5f) / (float)NUM_HISTOGRAM_BINS * (_min_max->y - _min_max->x) + _min_max->x;
    }
    else
    {
        printf("#ERROR: bin_to_worldlum: bin out of range\n");
    }
    return return_value;
}

/**
 * looks up the value in the cummulative distribution function that corresponds to the given brightness 
 * @param  in_brightness luminance for which to look up the cdf
 * @param  _cdf          pointer to cdf in which to look up
 * @param  _min_max      min and max luminance encountered in the histogram
 * @return               cdf value that corresponds to given brightness or min/max if given brightness is out of range
 */
__device__ float map_luminance(float in_brightness, float* _cdf, float2* _min_max)
{
     //float log_lum = min(_min_max->y, max(_min_max->x, log10f(in_brightness)));
    float log_lum = min(_min_max->y, max(_min_max->x, log10f(in_brightness)));
    int bin = saturate((log_lum - _min_max->x)/(_min_max->y - _min_max->x)) * (NUM_HISTOGRAM_BINS - 1);
    return (_cdf[bin] > DISPLAY_MIN_LUMINANCE)?_cdf[bin]:DISPLAY_MIN_LUMINANCE; 
}

/**
 * calculates the ceiling (max value) for a given bin in the histogram. This ceiling is based on the human 
 * visual acuity and therefore doesn't exceed a certain threshold in contrast to the surrounding bins
 * @param  _T_num_samples number of samples in a specific bin i.e. bin count
 * @param  _min_max       min and max luminance encountered
 * @param  _bin_num       current bin number for which to calculate the ceiling
 * @param  _cdf           pointer to cummulative distribution function
 * @return                max number of samples in the given bin
 */
__device__ float calculate_ceiling_human_contrast(int* _T_num_samples, float2* _min_max, int _bin_num, float* _cdf)
{
 float return_value = 0.0f;
 
 float delta_b_bin_step_size = (_min_max->y - _min_max->x)/(float)NUM_HISTOGRAM_BINS;
 float in_luminance = expf(_min_max->x + _bin_num * delta_b_bin_step_size + 0.5f * delta_b_bin_step_size);
 float out_luminance = map_luminance(in_luminance, _cdf, _min_max);

 float contrast_sensitivity_ratio = calculate_noticeable_difference(out_luminance) / calculate_noticeable_difference(in_luminance);
 float ranges_ratio = ( delta_b_bin_step_size * (*_T_num_samples) ) / (DISPLAY_MAX_LUMINANCE - DISPLAY_MIN_LUMINANCE);
 float luminance_ratio = in_luminance / out_luminance;
 return_value = contrast_sensitivity_ratio * ranges_ratio * luminance_ratio;
 return round(return_value);   
}


/**
 * calculates the ceiling (max value) for a histogram bin. This makes sure that the values in the cdf of the histogram don't increase faster
 * tthan a linear function set from lowest to highest value
 * @param  _T_num_samples number of samples in a given histogram bin i.e. bin count
 * @param  _min_max       min and max luminance encountered in the histogram
 * @return                max value of samples for a given histogram bin
 */
__device__ float calculate_ceiling_linear(int* _T_num_samples, float2* _min_max)
{
    float delta_b_bin_step_size = (_min_max->y - _min_max->x)/(float)NUM_HISTOGRAM_BINS;
    float divisor = (logf(DISPLAY_MAX_LUMINANCE) - logf(DISPLAY_MIN_LUMINANCE));
    return (*_T_num_samples * delta_b_bin_step_size) / divisor;
}

/**
 * iterates of the histogram and sums up the number of samples i.e. sum of all bin counts
 * @param  _histogram pointer to the histogram of which to calculate the sum
 * @return            total number of samples found in the given histogram
 */
__device__ int get_num_samples(unsigned int* _histogram)
{
    int num_samples = 0;
    for(int bin_it = 0; bin_it < NUM_HISTOGRAM_BINS; ++ bin_it)
    {
        num_samples += _histogram[bin_it];
    }
    return num_samples;
}

/**
 * calculates the cummulative distribution for each bin of a given histogram
 * @param _histogram   pointer to histogram to calculate
 * @param[out] _ccd         reference to the cummulative distribution of the histogram
 * @param _num_samples total number of samples in the given histogram
 */
__device__ void calculate_cummulative_distribution(unsigned int* _histogram, float* _ccd, int _num_samples)
{
    // printf("-------------------");
    // cu_print_histogram_and_cdf(_histogram, _ccd);
    // printf("-------------------");
    unsigned int sample_counter = 0;
    for(int bin_it = 0; bin_it < NUM_HISTOGRAM_BINS; ++bin_it)
    {
        sample_counter += _histogram[bin_it];
        _ccd[bin_it] = (float)sample_counter / (float)_num_samples;
    }
}



/**
 * iterative process that adjusts the histogram to be more evenly distributed thus removing stark contrast
 * @param  histogram histogram to be adjusted
 * @param  _cdf      cummulative distribution corresponding to given histogram
 * @param  _min_max  min and max luminance in histogram
 * @return           false on success TODO: make that less stupid and contradicting
 */
__device__ bool histogram_adjustment(unsigned int* histogram, float* _cdf, float2* _min_max)
{
    bool need_more_iterations = true;
    int trimmings = 0;
    int tolerance = (float)get_num_samples(histogram) * 0.025f;
    int ceiling = 0;
    int iteration_count = 0;
    int T_num_samples = get_num_samples(histogram);
    calculate_cummulative_distribution(histogram, _cdf, T_num_samples);

    while(need_more_iterations)
    {
        trimmings = 0;
        T_num_samples = get_num_samples(histogram);
        
        #ifdef DEBUG
//        printf("[%i]: num_samples: %i, tolerance: %i\n", iteration_count, T_num_samples, tolerance);
        #endif

        if(T_num_samples < tolerance)
        {
            need_more_iterations = false;
            return need_more_iterations;
        }
        
        #ifndef HUMAN_CONTRAST
        ceiling = calculate_ceiling_linear(&T_num_samples, _min_max);
        #endif
        for(int bin_it = 0; bin_it < NUM_HISTOGRAM_BINS; ++bin_it)
        {
            #ifdef HUMAN_CONTRAST
            ceiling = calculate_ceiling_human_contrast(&T_num_samples, _min_max, bin_it, _cdf);
            #endif
      
            
            if(histogram[bin_it] > ceiling)
            {
                trimmings += histogram[bin_it] - ceiling;
                histogram[bin_it] = max(ceiling, 0);
            }
        }
        if(trimmings < tolerance)
            need_more_iterations = false;
        ++iteration_count;
    }

    return need_more_iterations;
}

/**
 * performes histogram equualization (or histogram adjustment as it is called in the paper)
 * @param histogram histogram to adjust
 * @param[out] _cdf      pointer to memory to calculate cummulative distribution function
 * @param _min_max  min and max luminance in histogram
 */
__global__ void calibrate_histogram(unsigned int* histogram, float* _cdf, float2* _min_max)
{
    int num_samples = get_num_samples(histogram);
    calculate_cummulative_distribution(histogram, _cdf, num_samples);
    bool success = histogram_adjustment(histogram, _cdf, _min_max);
    //cu_print_histogram(histogram);
    if(!success) //just apply a linear operator, no histogram equalization needed
    {
        //TODO: apply linear operator
    }
    num_samples = get_num_samples(histogram);
    calculate_cummulative_distribution(histogram, _cdf, num_samples);

    //cu_print_histogram_and_cdf(histogram, _cdf);
}

/**
 * this is the actual tone-mapping step. here the incoming pixels are scaled using a lookup in the cummulative distribution function of 
 * the previously adjusted histogram
 * @param settings pointer to the settings struct
 * @param _cdf     pointer to cummulative distribution function
 * @param _min_max min and max luminance in the histogram
 */

__global__ void apply_histogram(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, int width, int height, float* _cdf, float2* _min_max)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {
        
        uchar4 in_pixel = read_pxl(input_tex, x_pos, y_pos, width);

        float3 converted_in_pixel = {0.0f, 0.0f, 0.0f};
        rp_to_flt3(&in_pixel, &converted_in_pixel);
     
        
        //calculate bin
        float luminosity = (0.27f * converted_in_pixel.x + 0.67f * converted_in_pixel.y + 0.06f * converted_in_pixel.z);
        float log_lum = min(_min_max->y, max(_min_max->x, logf(luminosity)));
        
        int bin = saturate((log_lum - _min_max->x)/(_min_max->y - _min_max->x)) * (NUM_HISTOGRAM_BINS - 1);
        //calculate value
        float P_cummulative_sum = _cdf[bin];
        float Bde_display_brightness = P_cummulative_sum;
     

        //float scaling = out01 / luminosity;
        float scaling = Bde_display_brightness/luminosity;
        converted_in_pixel.x *= scaling;
        converted_in_pixel.y *= scaling;
        converted_in_pixel.z *= scaling;
       
        #ifdef USE_GAMMA_CORRECTION
            converted_in_pixel.x = powf(converted_in_pixel.x, 1.0f/GAMMA_VALUE);
            converted_in_pixel.y = powf(converted_in_pixel.y, 1.0f/GAMMA_VALUE);
            converted_in_pixel.z = powf(converted_in_pixel.z, 1.0f/GAMMA_VALUE);

        #endif

        float3 out_pixel = {0.0f, 0.0f, 0.0f};
        out_pixel.x = (unsigned char)d_clip((int)(converted_in_pixel.x * 255.0f), 0, 255);
        out_pixel.y = (unsigned char)d_clip((int)(converted_in_pixel.y * 255.0f), 0, 255);
        out_pixel.z = (unsigned char)d_clip((int)(converted_in_pixel.z * 255.0f), 0, 255);
        write_pxl_rgb(output_tex, x_pos, y_pos, width, out_pixel);

     
    }  
}


/**
 * converts the luminance of each pixel to be around a given middle point also called a key. This in essence lightens or darkens
 * the entire picture depending on the chosen key value
 * @param settings pointer to settings struct
 * @param key      value that is used as a midpoint
 * @param avg      average brightness of the image
 */
__global__ void scale_to_midtone(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, int width, int height, float key, float avg)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {
        uchar4 in_pixel = read_pxl(input_tex, x_pos, y_pos, width);
        float3 converted_in_pixel = {0.0f, 0.0f, 0.0f};

        rp_to_flt3(&in_pixel, &converted_in_pixel);
        
        #ifdef INPUT_COLORSPACE_RGB
            PixelRGBToYUV(&converted_in_pixel);
        #endif
        converted_in_pixel.x *= ((1.0f / avg) * key);
        converted_in_pixel.y *= ((1.0f / avg) * key) * COLOR_BOOST;
        converted_in_pixel.z *= ((1.0f / avg) * key) * COLOR_BOOST;
        
        #ifdef INPUT_COLORSPACE_RGB
            PixelYUVToRGB(&converted_in_pixel);
        #endif
        flt3_to_rp(&converted_in_pixel, &in_pixel);
        write_pxl(output_tex, x_pos, y_pos, width, in_pixel);   
    } 
}

/**
 * converts given degrees to radians
 * @param  _degrees degrees to convert
 * @return          radians representation of the incoming degrees
 */
float deg_to_rad(float _degrees)
{
    return (_degrees * (M_PI / 180.0f));
}

/**
 * finds the min and max luminance in the given low-res image
 * TODO: initial performance measurements showed, that this is a bottle neck
 * @param  _foveal_image low-res image to find extremes of
 * @param  img_dim       size of given low-res image
 * @return               min and nax luminance of the low-res image (min in .x and max in .y)
 */
float2 get_min_max(float3* _foveal_image, int2 img_dim)
{
    float2 min_max = {0.0f, 0.0f};
    if(!_foveal_image)
    {
        printError("tonemapper_histward.cu: get_min_max: no foveal image given");
        return min_max;
    }

    float luminosity = logf(0.27f * _foveal_image[0].x + 0.67f * _foveal_image[0].y + 0.06f * _foveal_image[0].z);
    min_max.x = min_max.y = luminosity;
    for(int pix_it = 0; pix_it < img_dim.x * img_dim.y; ++pix_it)
    {

        luminosity = logf(0.27f * _foveal_image[pix_it].x + 0.67f * _foveal_image[pix_it].y + 0.06f * _foveal_image[pix_it].z);
        if(luminosity < min_max.x)
            min_max.x = luminosity;
        if(luminosity > min_max.y)
            min_max.y = luminosity;
    }

    return min_max;
}

extern "C" void cu_tone_mapper_histogram_ward(cudaStream_t stream, cudaArray* output, double* d_tmp_image, struct header * metadata, int numSources, int width, int height, float scene_key_brightness, float3* d_foveal_image, int2 foveal_img_dim, unsigned int* d_histogram, float* d_cdf)
{
    
    cudaError_t err;
    
    //treat everything as one huge image
    height = height * numSources;

    //calculate filter size
    int2 filter_size;
    filter_size.x = width / foveal_img_dim.x;
    filter_size.y = height / foveal_img_dim.y;

    cudaTextureObject_t input_tex = 0;
    cudaSurfaceObject_t output_tex = 0;
    create_tex_and_surf_obj(output, &input_tex, &output_tex);
    double accumulated_luminance = 0.1212;
    accumulated_luminance = run_reduce<double>(width * height, width, input_tex, d_tmp_image, stream);
    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "reduction kernel launch fail: " << cudaGetErrorString(err) << std::endl;

    //scale image to midtone
    dim3 threadsPerBlock(16,16);
    dim3 numBlocks(divup(width, threadsPerBlock.x), divup(height, threadsPerBlock.y));
    scale_to_midtone<<<numBlocks, threadsPerBlock, 0, stream>>>(input_tex, output_tex, width, height, scene_key_brightness, accumulated_luminance);

    //launch foveal kernel
    numBlocks.x = divup(foveal_img_dim.x, threadsPerBlock.x);
    numBlocks.y = divup(foveal_img_dim.y, threadsPerBlock.y);

    compute_foveal_image<<<numBlocks, threadsPerBlock, 0, stream>>>(input_tex, width, height, d_foveal_image, foveal_img_dim, filter_size);
    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "fovial image kenel launch fail: " << cudaGetErrorString(err) << std::endl;
    //write_foveal_image(foveal_image, foveal_img_dim);

    //get min and max values in foveal image
    float2* min_max;
    cuda_check(cudaMalloc(&min_max, sizeof(float2)), "tonemapper_histward: tonemapper_histward: failed to allocate min & max");

    get_min_max<<<1,256,0,stream>>>(d_foveal_image, foveal_img_dim, min_max);
    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "get_min_max kenel launch fail: " << cudaGetErrorString(err) << std::endl;

    //calculate
    cuda_check(cudaMemset(d_histogram, 0, sizeof(float) * NUM_HISTOGRAM_BINS), "tonemapper_histward: tonemapper_histward: failed to copy cummulative density function");
    calculate_histogram<<<numBlocks,threadsPerBlock, 0, stream>>>(d_foveal_image, foveal_img_dim, d_histogram, min_max);
    
    //print_histogram(histogram);

    //allocate cummulative density function
    cuda_check(cudaMemset(d_cdf, 0, sizeof(float) * NUM_HISTOGRAM_BINS), "tonemapper_histward: tonemapper_histward: failed to copy cummulative density function");

    //perform histogram adjustment and calculate cdf 
    calibrate_histogram<<<1,1, 0, stream>>>(d_histogram, d_cdf, min_max);
    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "calibrate_histogram kenel launch fail: " << cudaGetErrorString(err) << std::endl;

    #ifdef DEBUG   
     //print_histogram_and_cdf(d_histogram, d_cdf);
    #endif
    //apply histogram
    numBlocks.x = divup(width, threadsPerBlock.x);
    numBlocks.y = divup(height, threadsPerBlock.y);
    apply_histogram<<<numBlocks,threadsPerBlock, 0, stream>>>(input_tex, output_tex, width, height, d_cdf, min_max);
    
    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "apply_histogram kenel launch fail: " << cudaGetErrorString(err) << std::endl;

    cuda_check(cudaDestroyTextureObject(input_tex), "HDR: failed destroying texture object"); 
    cuda_check(cudaDestroySurfaceObject(output_tex), "HDR: failed destroying surface object"); 
    
}


#ifdef DEBUG

/**
 * writes the sample count and corresponding cummulative distribution function for a histogram
 * @param histogram histogram to print
 * @param _cdf      corresponding cdf to print
 */
__device__ void cu_print_histogram_and_cdf(unsigned int* histogram, float* _cdf)
{
    for(int bin_it = 0; bin_it < NUM_HISTOGRAM_BINS; ++bin_it)
    {
        printf("[%i]: %i, cdf: %f\n",bin_it, histogram[bin_it], _cdf[bin_it] );
    }
}

/**
 * like above but only histogram without cdf
 * @param histogram histogram to print
 */
__device__ void cu_print_histogram(unsigned int* histogram)
{
    for(int bin_it = 0; bin_it < NUM_HISTOGRAM_BINS; ++bin_it)
    {
        printf("[%i]: %i\n",bin_it, histogram[bin_it]);
    }
}

/**
 * like above but only the cummulative distribution function
 * @param cdf cdf to print
 */
__device__ void cu_print_cdf(float* cdf)
{
    for(int bin_it = 0; bin_it < NUM_HISTOGRAM_BINS; ++bin_it)
    {
        printf("[%i]: %f\n",bin_it, cdf[bin_it]);
    }
}

/**
 * like above but on the host instead of the device
 * @param histogram histogram to pirnt
 * @param _cdf      corresponding cdf to print
 */
void print_histogram_and_cdf(unsigned int* histogram, float* _cdf)
{
    unsigned int host_histogram[NUM_HISTOGRAM_BINS];
    float host_cdf[NUM_HISTOGRAM_BINS];
    cudaError_t err;
    err = cudaMemcpy(host_histogram,histogram, sizeof(unsigned int) * NUM_HISTOGRAM_BINS, cudaMemcpyDeviceToHost);
    if(err != cudaSuccess)
        std::cout << "ToneMapperHistogramWard::downloadResultFromGPU fail: " << cudaGetErrorString(err) << std::endl;

    err = cudaMemcpy(host_cdf,_cdf, sizeof(float) * NUM_HISTOGRAM_BINS, cudaMemcpyDeviceToHost);
    if(err != cudaSuccess)
        std::cout << "ToneMapperHistogramWard::downloadResultFromGPU fail: " << cudaGetErrorString(err) << std::endl;

    for(int hist_it = 0; hist_it < NUM_HISTOGRAM_BINS; ++hist_it)
    {
        std::cout << "[" << hist_it << "]: " << host_histogram[hist_it] << ", cdf: " << host_cdf[hist_it] << std::endl;
    }
}

void print_histogram(unsigned int* histogram)
{
    unsigned int host_histogram[NUM_HISTOGRAM_BINS];
    cudaError_t err;
    err = cudaMemcpy(host_histogram,histogram, sizeof(unsigned int) * NUM_HISTOGRAM_BINS, cudaMemcpyDeviceToHost);
    if(err != cudaSuccess)
        std::cout << "ToneMapperHistogramWard::downloadResultFromGPU fail: " << cudaGetErrorString(err) << std::endl;

    
    for(int hist_it = 0; hist_it < NUM_HISTOGRAM_BINS; ++hist_it)
    {
        std::cout << "[" << hist_it << "]: " << host_histogram[hist_it] << std::endl;
    } 
}

/**
 * writes the calculated low-res image to a file called "foveal_image.png"
 * @param _foveal_image low-res image to save
 * @param img_dim       size of the low-res image
 */
/*
void write_foveal_image(float3* _foveal_image, int2 img_dim) 
{
    //create image
    cudaError_t err;
    Image* fov_img = new Image(img_dim.x, img_dim.y);
    err = cudaMemcpy(fov_img->image.data(),_foveal_image,  sizeof(float3) * img_dim.x * img_dim.y, cudaMemcpyDeviceToHost);
    if(err != cudaSuccess)
        std::cout << "ToneMapperHistogramWard::downloadResultFromGPU fail: " << cudaGetErrorString(err) << std::endl;

    fov_img->savePng("foveal_image.png");
}
*/
#endif 
