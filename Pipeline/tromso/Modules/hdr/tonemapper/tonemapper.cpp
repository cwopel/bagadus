// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "tonemapper.h"
#include "../utils/utils.h"
#include "../utils/defines.h"
#include <cmath>


extern "C"
bool isPow2(unsigned int x)
{
    return ((x&(x-1))==0);
}

ToneMapper::ToneMapper()
{

	
}


ToneMapper::~ToneMapper()
{
	// std::cout << "this destructor called ~ToneMapper" << std::endl;
	// if(img_in)
	//	delete img_in;
	// if(img_out)
	// 	//delete img_out;
}

// int ToneMapper::setImage(Image* img)
// {
// 	int error = ERROR_OK;
// 	if(img != 0)
// 	{
// 		img_in = img;
// 	}
// 	else
// 	{
// 		printError("ToneMapper::setImage image is null");
// 		error = ERROR_INVALID_ARGUMENTS;
// 	}	
// 	return error;
// }

// Image* ToneMapper::getResult()
// {
// 	if(img_out == 0)
// 	{
// 		printError("ToneMapper::getResult no result");
// 	} 
// 	return img_out;
// }

void ToneMapper::h_rp_to_flt3(uchar4* v_in, float3* v_out) {
    const int e = v_in->w;

    if (e == 0) {
        v_out->x = 0.0f;
        v_out->y = 0.0f;
        v_out->z = 0.0f;
        return;
    }

    const float v = ldexp(1.0f/256.0f, e - 128);
    v_out->x = ((float) v_in->x + .5f) * v;
    v_out->y = ((float) v_in->y + .5f) * v;
    v_out->z = ((float) v_in->z + .5f) * v;
}


