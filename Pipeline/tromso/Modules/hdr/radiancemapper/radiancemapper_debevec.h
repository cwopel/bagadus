// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once

#include "../utils/defines.h"

#include <cuda.h>
#include <cuda_runtime.h> 
#include "radiancemapper.h"


extern "C" void cu_radiance_mapper_debevec(cudaStream_t stream, cudaArray* input, cudaArray* output, struct header * metadata, float* response_function, double* weight_function, int num_sources, int num_images, int width, int height); 

extern "C" void surface_test();

class RadianceMapperDebevec: public RadianceMapper
{
public: 
	void run(cudaStream_t stream, cudaArray* input, cudaArray* output ,struct header * metadata, int num_sources, int num_images, int width, int height);
	void debug_run();
	void init(WeightFunction* wf);
	RadianceMapperDebevec();
	~RadianceMapperDebevec();
	
	
	int readResponseFunctionFromFile(const  char* path);
	std::vector<float>* getResponseFunction();
	int setWeightFunction(WeightFunction* wf);
	
	#ifdef DEBUG
	void setResponseFunction(unsigned int pos, float val);
	void printResponseFunction();
	void printExposureTimes();
	#endif



private:
	bool initialized;
	//ex member of struct
	int numImages;
	std::vector<float>* exposureTimes;
	std::vector<float>* responseFunction;
	
	float calculateExposureTime(float _exp_time, float _aperture);

	double* d_weight_function;
	float* d_response_function;	
	
};
