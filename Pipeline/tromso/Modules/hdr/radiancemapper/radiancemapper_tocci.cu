// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 



/*
//forward declarations
#ifdef DEBUG
__global__ void texture_test(cset_radiancemapper* settings );
__global__ void test_binary_search(cset_radiancemapper* settings);
__global__ void test_LE_to_HE(cset_radiancemapper* settings);
__global__ void test_LE_to_HE_image(cset_radiancemapper* settings);
__global__ void print_exposure_times(cset_radiancemapper* settings);
__device__ void write_used_exposures(cset_radiancemapper* settings, int x_pos, int y_pos, int _used_exposure);
__device__ void print_num_pixels(cset_radiancemapper* settings, int x_pos, int y_pos, int num_pixels, int num_saturated_pixels);
__device__ void write_cases(cset_radiancemapper* settings, int x_pos, int y_pos, bool main_saturate, bool neighbourhood_saturate);
#endif
*/

/**
 * converts the luminance of a lower exposure to a corresponding value in a higher exposed pixel
 * @param  settings  pointer to settings struct
 * @param  _in_pixel low exposure pixel to be converted
 * @param  _img_it   number of the low exposure image in the array of images
 * @return           high exposure equivalent of input low exposure pixel
 */

__device__ float3 convert_LE_to_HE(cudaTextureObject_t input_tex, int width, int height, float expo_first, float expo_second, float3 _in_pixel, int _img_it, float* d_response_function)
{
    float3 result = {0.0f, 0.0f, 0.0f};
    float high_exposure_time = expo_second;
    float low_exposure_time = expo_first;

    //difference in exposure time between the two images
    float factor = high_exposure_time / low_exposure_time;
    float3 response = {0.0f, 0.0f, 0.0f};

    //response (luminance) of the low exposure image
    response.x = calcResponse(d_response_function, _in_pixel.x);
    response.y = calcResponse(d_response_function, _in_pixel.y);
    response.z = calcResponse(d_response_function, _in_pixel.z);

    //scale by difference in exposure
    response.x = expf(response.x) * factor;
    response.y = expf(response.y) * factor;
    response.z = expf(response.z) * factor;

    //perform reverse lookup in response function
    result.x = calcInverseResponse(d_response_function, logf(response.x));
    result.y= calcInverseResponse(d_response_function, logf(response.y));
    result.z= calcInverseResponse(d_response_function, logf(response.z));

    //printf("inside convert_LE_to_HE: factor: %f, _in_pixel.x: %i, in_pixel.y: %i, _in_pixel.z: %i, response.x: %f, y: %f, z: %f\n", factor, _in_pixel.x, _in_pixel.y, _in_pixel.z, response.x, response.y, response.z);

    return result;
}

/**
 * first case simply returns the current pixel since it nor its neighbours are saturated
 * @param  _pixel        current pixel
 * @return               current pixel
 */
__device__ float3 perform_case_1(float3 _pixel)
{
    return _pixel;
}

/**
 * merges the high exposed pixel with the low exposed pixel
 * @param  settings              pointer to the settings struct
 * @param  x_pos                 x position of the current pixel in the image
 * @param  y_pos                 y position of the current pixel in the image
 * @param  _num_pixels           number of valid pixels in the neighbourhood (those that were not clamped by an edge case)
 * @param  _num_saturated_pixels number of saturated pixels in the current pixel's neighbourhood
 * @param  _img_it               number of the current image in the array of images
 * @param  _hdr_value            current value used for current pixel
 * @param  _used_exposure        currently used exposure of current pixel
 * @return                       merged pixels
 */

__device__ float3 perform_case_2(cudaTextureObject_t input_tex, int width, int height, int num_images, int x_pos, int y_pos, int _num_pixels, int _num_saturated_pixels, float3 _hdr_value, int* _used_exposure, float expo_first, float expo_second, float* d_response_function)
{
    float3 response = {0.0f, 0.0f, 0.0f};
    float3 pixel_high_exposure = _hdr_value;
    *_used_exposure = 0;

    //proportion of all pixels to saturated pixels in neighbourhood
    float scaling_factor = 0.0f;
    if(_num_pixels != 0)
    {
        scaling_factor = ((float)_num_pixels - (float)_num_saturated_pixels) / (float)_num_pixels;
    }
    else
    {
        scaling_factor = 0;
    }


    float3 pixel_low_exposure = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 0);

    response.x += (scaling_factor * pixel_high_exposure.x);
    response.y += (scaling_factor * pixel_high_exposure.y);
    response.z += (scaling_factor * pixel_high_exposure.z);

    pixel_low_exposure = convert_LE_to_HE(input_tex, width, height, expo_first, expo_second, pixel_low_exposure, 0, d_response_function);

    response.x += (1.0f - scaling_factor) * pixel_low_exposure.x;
    response.y += (1.0f - scaling_factor) * pixel_low_exposure.y;
    response.z += (1.0f - scaling_factor) * pixel_low_exposure.z;

    return response;
}

/**
 * merges low exposure and high exposure pixel taking the neighbourhood into account
 * @param  settings              pointer to settings struct
 * @param  _pixel                currently used value for the current pixel
 * @param  x_pos                 x position of current pixel in the whole image
 * @param  y_pos                 y position of current pixel in the whole image
 * @param  _img_it               number of current image in the image array
 * @param  _num_pixels           number of valid pixels in the neighbourhood (those not clipped by edge cases)
 * @param  _num_saturated_pixels number of saturated pixels in that neighbourhood
 * @param  _used_exposure        exposure time of the current pixel
 * @return                       merged pixel
 */

__device__ float3 perform_case_3(cudaTextureObject_t input_tex, int width, int height, int num_images, float3 _pixel, int x_pos, int y_pos, int _num_pixels, int _num_saturated_pixels, int* _used_exposure, float expo_first, float expo_second, float* d_response_function)
{
    //calculate scaling factor
    float scaling_factor = ((float)_num_pixels - (float)_num_saturated_pixels) / (float)_num_pixels;
    // if(x_pos == 410 && y_pos == 40)
    // {
    // 	printf("case 3 output -----------------------------\n scaling_factor: %f, _img_it: %i\n", scaling_factor, _img_it);
    // }


    //read neighbouring pixels
    int num_neighbourhood_pixels = (2*TOCCI_FILTER_SIZE + 1) * (2*TOCCI_FILTER_SIZE + 1);
    float3 low_exposure_neighbourhood[(2*TOCCI_FILTER_SIZE + 1) * (2*TOCCI_FILTER_SIZE + 1)];
    float3 high_exposure_neighbourhood[(2*TOCCI_FILTER_SIZE + 1) * (2*TOCCI_FILTER_SIZE + 1)];
    {
        int counter_neigh = 0;
#pragma unroll
        for(int filter_x = x_pos - TOCCI_FILTER_SIZE; filter_x <= x_pos + TOCCI_FILTER_SIZE; ++filter_x)
        {
#pragma unroll
            for(int filter_y = y_pos - TOCCI_FILTER_SIZE; filter_y <= y_pos + TOCCI_FILTER_SIZE; ++ filter_y)
            {
                low_exposure_neighbourhood[counter_neigh] = read_pxl_rgb(input_tex, filter_x, filter_y, width, height, 0);
                high_exposure_neighbourhood[counter_neigh] = read_pxl_rgb(input_tex, filter_x, filter_y, width, height, 1);
                ++counter_neigh;
            }
        }
    }


    //calculate ratio map of neighbourhood
    float3 center_pixel_low_exposure = {0.0f, 0.0f, 0.0f};
    float3 ratio_pixels[(2*TOCCI_FILTER_SIZE + 1) * (2*TOCCI_FILTER_SIZE + 1)];
    {

        center_pixel_low_exposure = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 0);
#pragma unroll
        for(int ratio_it = 0; ratio_it < num_neighbourhood_pixels; ++ratio_it)
        {
            ratio_pixels[ratio_it].x = center_pixel_low_exposure.x / (low_exposure_neighbourhood[ratio_it].x + 1.0f);
            ratio_pixels[ratio_it].y = center_pixel_low_exposure.y / (low_exposure_neighbourhood[ratio_it].y + 1.0f);
            ratio_pixels[ratio_it].z = center_pixel_low_exposure.z / (low_exposure_neighbourhood[ratio_it].z + 1.0f);

        }


    }


    float3 estimate = {0.0f, 0.0f, 0.0f};
    //calculate estimate of HE pixel using LE ratio pixels
    {
        float luminance = 0.0f;
        float3 estimate_sum = {0.0f, 0.0f, 0.0f};
#pragma unroll
        for(int neighbour_it = 0; neighbour_it < num_neighbourhood_pixels; ++neighbour_it)
        {
            luminance = (0.27f * (float)high_exposure_neighbourhood[neighbour_it].x + 0.67f * (float)high_exposure_neighbourhood[neighbour_it].y + 0.06f * (float)high_exposure_neighbourhood[neighbour_it].z);
            if(luminance < SATURATED_PIXEL)
            {
                estimate_sum.x += ratio_pixels[neighbour_it].x * (float)high_exposure_neighbourhood[neighbour_it].x;
                estimate_sum.y += ratio_pixels[neighbour_it].y * (float)high_exposure_neighbourhood[neighbour_it].y;
                estimate_sum.z += ratio_pixels[neighbour_it].z * (float)high_exposure_neighbourhood[neighbour_it].z;
            }
        }

        if(_num_pixels - _num_saturated_pixels > 0)
        {
            estimate.x = (float)estimate_sum.x / ((float)(_num_pixels - _num_saturated_pixels));
            estimate.y = (float)estimate_sum.y / ((float)(_num_pixels - _num_saturated_pixels));
            estimate.z = (float)estimate_sum.z / ((float)(_num_pixels - _num_saturated_pixels));
        }
        else
        {
            estimate.x = (float)estimate_sum.x;
            estimate.y = (float)estimate_sum.y;
            estimate.z = (float)estimate_sum.z;
        }

    }

    center_pixel_low_exposure = convert_LE_to_HE(input_tex, width, height, expo_first, expo_second, center_pixel_low_exposure, 0, d_response_function);

    //merge the high estimate with the low exposure value
    float3 result = {0.0f, 0.0f, 0.0f};
    result.x = scaling_factor * estimate.x + (1.0f - scaling_factor) * center_pixel_low_exposure.x;
    result.y = scaling_factor * estimate.y + (1.0f - scaling_factor) * center_pixel_low_exposure.y;
    result.z = scaling_factor * estimate.z + (1.0f - scaling_factor) * center_pixel_low_exposure.z;

    //return _pixel;
    return result;
}

/**
 * read pixel of image with lower exposure time
 * @param  settings       pointer to the struct of settings
 * @param  x_pos          x position of the current pixel in the image
 * @param  y_pos          y position of the current pixel in the image
 * @param  _img_it        number of the current image in the array of images
 * @param  _used_exposure exposure used by current pixe
 * @return                pixel in image with next lower exposure
 */

__device__ float3 perform_case_4(cudaTextureObject_t input_tex, int width, int height, int num_images, int x_pos, int y_pos, int* _used_exposure)
{
    float3 response = {0, 0, 0};

    response = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 0);

    *_used_exposure = 0;

    return response;
}

/**
 * starts at highest exposure and iterates over lower exposure images.
 * If the pixel or it's neighbourhood are saturated (i.e. 90% of white value) the current exposure and the next are merged in different
 * ways (see various perform_case_x())
 * @param settings pointer to the struct of settings
 */

__global__ void radiance_mapper_tocci(cudaTextureObject_t input_tex, cudaSurfaceObject_t output_tex, float* d_response_function, int num_images, int width, int height, float expo_first, float expo_second)
{
    int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
    if(x_pos < width  && y_pos < height)
    {
        //define outpixel
        //uchar4& out_pixel = *(uchar4*)&(settings->d_out[((y_pos * settings->d_image_dimensions.x + x_pos)<<2)]);
        float3 out_value = {0.0f, 0.0f, 0.0f};
        //define neighbourhood
        //int curr_window_offset = ((y_pos - TOCCI_FILTER_SIZE) * settings->d_image_dimensions.x + x_pos - TOCCI_FILTER_SIZE );
        //int offset_in_window = 0;
        //int pixels_per_image = width * height;

        //int pixel_pos = 0;

        //copy highest exposed to output image
        float3 hdr_value = {0.0f, 0.0f, 0.0f};
        int used_exposure = 0;
        //hdr_value = read_pxl_rgb(settings->d_image_data, x_pos, y_pos, settings->d_image_dimensions.x, settings->d_image_dimensions.y, settings->d_num_images-1);
        hdr_value = read_pxl_rgb(input_tex, x_pos, y_pos, width, height, 0);

        //for(int img_it = settings->d_num_images - 1; img_it > 0; --img_it)

        int num_pixels = 0;
        int num_saturated_pixels = 0;
        bool valid_pixel = true;
#pragma unroll
        for (int y_it = -TOCCI_FILTER_SIZE; y_it <= TOCCI_FILTER_SIZE ; ++y_it)
        {
#pragma unroll
            for(int x_it = -TOCCI_FILTER_SIZE; x_it <= TOCCI_FILTER_SIZE; ++x_it)
            {
                valid_pixel = true;


                //perform actual operations
                if(valid_pixel)
                {
                    float3 in_pixel = read_pxl_rgb(input_tex, x_pos + x_it, y_pos + y_it, width, height, 1);
                    float luminance = (0.27f * in_pixel.x + 0.67f * in_pixel.y + 0.06f * in_pixel.z);
                    ++num_pixels;
                    if(luminance > SATURATED_PIXEL)
                        ++num_saturated_pixels;

                }
            }
        }

        //find out which of the four cases we have
        bool main_saturate = false;
        bool neighbourhood_saturate = false;


        float luminance = (0.27f * (float)hdr_value.x + 0.67f * (float)hdr_value.y + 0.06f * (float)hdr_value.z);
        if(luminance > SATURATED_PIXEL)
            main_saturate = true;
        if(num_saturated_pixels > 0)
            neighbourhood_saturate = true;
        // if(x_pos == 300 && y_pos == 1)
        // {
        // 	printf("main: num_pixels: %i, num_saturated_pixels: %i\n", num_pixels, num_saturated_pixels);
        // }

        if(!main_saturate && !neighbourhood_saturate)
        {
            hdr_value = perform_case_1(hdr_value);
        }

        if(!main_saturate && neighbourhood_saturate)
        {
            hdr_value = perform_case_2(input_tex, width, height, num_images, x_pos, y_pos, num_pixels, num_saturated_pixels, hdr_value, &used_exposure, expo_first, expo_second, d_response_function);
        }

        if(main_saturate && !neighbourhood_saturate)
        {
            hdr_value = perform_case_3(input_tex, width, height, num_images, hdr_value, x_pos, y_pos, num_pixels, num_saturated_pixels, &used_exposure, expo_first, expo_second, d_response_function);

        }

        if(main_saturate && neighbourhood_saturate)
        {
            hdr_value = perform_case_4(input_tex, width, height, num_images, x_pos, y_pos, &used_exposure);
        }

        // if(x_pos == 330 && y_pos == 140)
        // {
        // 	printf("[%i, %i ,%i]: main_saturate: %d, neighbourhood_saturate: %d, luminance: %f\n hdr_value.x: %i, y: %i, z: %i\n ------\n",x_pos, y_pos, img_it ,main_saturate, neighbourhood_saturate, luminance, hdr_value.x, hdr_value.y, hdr_value.z);
        // }




        float exposure_time = 0.0f;
        (used_exposure == 0)?exposure_time = expo_first : exposure_time = expo_second;



        out_value.x = expf(calcResponse(d_response_function, hdr_value.x) - (float)logf(exposure_time));
        out_value.y = expf(calcResponse(d_response_function, hdr_value.y) - (float)logf(exposure_time));
        out_value.z = expf(calcResponse(d_response_function, hdr_value.z) - (float)logf(exposure_time));




        //		out_value.x = hdr_value.x;
        //		out_value.y = hdr_value.y;
        //		out_value.z = hdr_value.z;
        if(x_pos == 1 && y_pos == 1)
        {
            //printf("out_value.x: %f, y: %f, z: %f\n hdr_value.x: %f, y: %f, z: %f\n, used_exposure: %i\n", out_value.x, out_value.y, out_value.z, hdr_value.x, hdr_value.y, hdr_value.z, used_exposure);
            //printf("radiance mapper: out_value.x: %f, y: %f, z: %f\n", out_value.x, out_value.y, out_value.z);
            //printf("response: .x: %f, .y: %f, .z: %f, log_exposure: %f, exposure: %f, expo_first: %f, expo_second: %f\n", calcResponse(d_response_function, hdr_value.x), calcResponse(d_response_function, hdr_value.y), calcResponse(d_response_function, hdr_value.z), logf(exposure_time), exposure_time, expo_first, expo_second);
            //out_value.x = 255.0f; out_value.y = 0.0f; out_value.z = 0.0f;
        }

        if(out_value.x > 100000.0f || out_value.y > 100000.0f || out_value.z > 100000.0f)
        {
            printf("radiancemapper: [%i, %i] out_value.x: %f, y: %f, z:%f\n", x_pos, y_pos, out_value.x, out_value.y, out_value.z);
        }

        uchar4 out_pixel = {0, 0, 0, 0};
        flt3_to_rp(&out_value, &out_pixel);

        write_pxl(output_tex, x_pos, y_pos, width, out_pixel);
        //write_used_exposures(settings, x_pos, y_pos, used_exposure);
    }
}



extern "C" void cu_radiance_mapper_tocci(cudaStream_t stream, cudaArray* input,
        cudaArray* output, struct header * metadata, float* response_function,
        int num_sources, int num_images, int width, int height)
{
    height = height * num_sources;

    cudaError_t err;
    dim3 threadsPerBlock(16,16);
    dim3 numBlocks(divup(width, threadsPerBlock.x), divup(height, threadsPerBlock.y));

    cudaTextureObject_t input_tex = 0;
    cudaSurfaceObject_t output_tex = 0;
    create_tex_obj(input, &input_tex);
    create_surf_obj(output, &output_tex);



    //print_exposure_times<<<1,1>>>(setaddr);
    //test_binary_search<<<1,1>>>(setaddr);
    //test_LE_to_HE<<<1,1>>>(setaddr);
    //test_LE_to_HE_image<<<numBlocks, threadsPerBlock>>>(setaddr);

    //	cudaEvent_t start, stop;
    //	cudaEventCreate(&start);
    //	cudaEventCreate(&stop);

    //	cudaEventRecord(start);

    radiance_mapper_tocci<<<numBlocks, threadsPerBlock>>>(input_tex, output_tex, response_function, num_images, width, height, metadata->expoFirst/1000000.0f, metadata->expoSecond/1000000.0f);

    //	cudaEventRecord(stop);

    //	cudaEventSynchronize(stop);
    //	float milliseconds = 0;
    //	cudaEventElapsedTime(&milliseconds, start, stop);
    //	std::cout << "elapsed time radiance mapper tocci " << milliseconds << std::endl;

    err = cudaGetLastError();
    if(err != cudaSuccess)
        std::cout << "radiance mappper kernel launch fail: " << cudaGetErrorString(err) << std::endl;

    cuda_check(cudaDestroyTextureObject(input_tex), "HDR: failed destroying texture object"); 
    cuda_check(cudaDestroySurfaceObject(output_tex), "HDR: failed destroying surface object"); 

}


//########################################################################################################################
//########################################################################################################################
//########################################################################################################################
//########################################################################################################################
//############# DEBUG ####################################################################################################
//########################################################################################################################
//########################################################################################################################
//########################################################################################################################
//########################################################################################################################



#ifdef DEBUG

/**
 * simple test if binding textures work. copies input to output
 * @param settings pointer to struct of settings
 */
/*
   __global__ void texture_test(cset_radiancemapper* settings )
   {
//just read input texture and copy to output
int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
if(x_pos < settings->d_image_dimensions.x  && y_pos < settings->d_image_dimensions.y)
{
uchar4 input_pixel = tex2D(input01, x_pos, y_pos);
float3 output_value = {(float)input_pixel.x/255.0f, (float)input_pixel.y/255.0f, (float)input_pixel.z/255.0f};
flt3_to_rp(&output_value, &input_pixel);
write_pxl_rgb(settings->d_out, x_pos, y_pos, settings->d_image_dimensions.x, input_pixel);
}

}
*/
/**
 * testcase written to test binary search implemented for reverse response function lookup
 * function call that sets value of "correct" should always return true while "wrong" should always return false
 * @param settings pointer to struct of settings
 */
/*
   __global__ void test_binary_search(cset_radiancemapper* settings)
   {
   printf("testing is_this_value\n");
//easy values
bool correct = is_this_value(settings, 2.24, 252);
bool wrong = is_this_value(settings, 2.235, 150);
printf("easy is_this_value: correct: %d, wrong: %d\n", correct, wrong);

//negative values
correct = is_this_value(settings, -7.3045, 3);
wrong = is_this_value(settings, -7.30426, 7);
printf("negative is_this_value: correct: %d, wrong: %d\n", correct, wrong);

//corner cases
correct = is_this_value(settings, -7.48787, 0);
wrong = is_this_value(settings, -7.48788, 1);
printf("low corner is_this_value: correct: %d, wrong: %d\n", correct, wrong);

correct = is_this_value(settings, 2.29558, 255);
wrong = is_this_value(settings, 2.29559, 254);
printf("high corner is_this_value: correct: %d, wrong: %d\n", correct, wrong);


printf("testing binary search.\n");

//easy
int pos = calcInverseResponse(settings, 2.235);
printf("easy found pos: %i, correct pos: %i\n", pos, 252);

//negative
pos = calcInverseResponse(settings, -7.3045);
printf("negative found pos: %i, correct pos: %i\n", pos, 3);

//corner cases
pos = calcInverseResponse(settings, -7.48787);
printf("negative found pos: %i, correct pos: %i\n", pos, 0);

pos = calcInverseResponse(settings, 2.29558);
printf("negative found pos: %i, correct pos: %i\n", pos, 255);

//interpolation
pos = calcInverseResponse(settings, 2.0);
printf("negative found pos: %i, correct pos: %i\n", pos, 240);


printf("testing done\n");
}
*/
/**
 * testcase that performs conversion of low exposure to high exposure and then compares the result to the actual pixel in the high exposre
 * difference is then written to debug image (higher -> bigger difference)
 * @param settings pointer to settings struct
 */
/*
   __global__ void test_LE_to_HE_image(cset_radiancemapper* settings)
   {
   int x_pos = blockIdx.x * blockDim.x + threadIdx.x, y_pos = blockIdx.y * blockDim.y + threadIdx.y;
   if(x_pos < settings->d_image_dimensions.x  && y_pos < settings->d_image_dimensions.y)
   {
   uchar4 pixel_low_exposure = read_pxl_rgb(settings->d_image_data, x_pos, y_pos, settings->d_image_dimensions.x, settings->d_image_dimensions.y, 4);
   uchar4 pixel_high_exposure = read_pxl_rgb(settings->d_image_data, x_pos, y_pos, settings->d_image_dimensions.x, settings->d_image_dimensions.y, 3);
   pixel_low_exposure = convert_LE_to_HE(settings, pixel_low_exposure, 4);
   uchar4 outvalue = {0, 0, 0, 255};

   outvalue.x = pixel_high_exposure.x - pixel_low_exposure.x;
   outvalue.y = pixel_high_exposure.y - pixel_low_exposure.y;
   outvalue.z = pixel_high_exposure.z - pixel_low_exposure.z;
#ifdef DEBUG_IMAGE
write_pxl(settings->d_debug, x_pos, y_pos, settings->d_image_dimensions.x, outvalue);
#else
write_pxl(settings->d_out, x_pos, y_pos, settings->d_image_dimensions.x, outvalue);
#endif
}
}
*/
/**
 * testcase that performes a conversion from a low exposure image to a high exposure image and prints the actual high exposure and the
 * calculated high exposure
 * @param settings [description]
 */
/*
   __global__ void test_LE_to_HE(cset_radiancemapper* settings)
   {
   printf("test_LE_to_HE start\n");
   uchar4 pixel_low_exposure = read_pxl_rgb(settings->d_image_data, 50, 50, settings->d_image_dimensions.x, settings->d_image_dimensions.y, 3);
   uchar4 pixel_high_exposure = read_pxl_rgb(settings->d_image_data, 50, 50, settings->d_image_dimensions.x, settings->d_image_dimensions.y, 2);
   printf("before pixel_low_exposure.x: %i, y: %i, z: %i, pixel_high_exposure.x: %i, y: %i, z: %i\n", pixel_low_exposure.x, pixel_low_exposure.y, pixel_low_exposure.z, pixel_high_exposure.x, pixel_high_exposure.y, pixel_high_exposure.z);

   pixel_low_exposure = convert_LE_to_HE(settings, pixel_low_exposure, 3);
   printf("after pixel_low_exposure.x: %i, y: %i, z: %i, pixel_high_exposure.x: %i, y: %i, z: %i\n", pixel_low_exposure.x, pixel_low_exposure.y, pixel_low_exposure.z, pixel_high_exposure.x, pixel_high_exposure.y, pixel_high_exposure.z);
   printf("test_LE_to_HE end\n");

   }
   */
/**
 * writes the number of pixels in the neighbourhood of the current pixel into the current pixel. The whiter a pixel, the more neighbour pixels
 * @param settings             pointer to settings struct
 * @param x_pos                x position or current pixel in the image
 * @param y_pos                y position of current pixel in the image
 * @param num_pixels           number of valid pixels in the neighbourhood of current pixel (i.e. the ones not croped by edge cases)
 * @param num_saturated_pixels number of saturated pixels in neighbourhood
 */
/*
   __device__ void print_num_pixels(cset_radiancemapper* settings, int x_pos, int y_pos, int num_pixels, int num_saturated_pixels)
   {
   uchar4 out_value = {num_pixels, num_pixels, num_pixels, 255};
   out_value.x += num_saturated_pixels;
#ifdef DEBUG_IMAGE
write_pxl_rgb(settings->d_debug, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
#else
write_pxl_rgb(settings->d_out, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
#endif
}
*/
/**
 * prints the exposure times of the used images to console
 * @param settings pointer to settings struct
 */
/*
   __global__ void print_exposure_times(cset_radiancemapper* settings)
   {
   printf("Exposures--------------\n");
   for(int exp_it = 0; exp_it < settings->d_num_images; ++exp_it)
   {
   printf("[%i]: %f\n", exp_it, settings->d_exposure_times[exp_it]);
   }
   }
   */

/**
 * writes the currently used exposure for each pixel into the debug image. For explanation of used colors look in comments below
 * @param settings       pointer to the settings struct
 * @param x_pos          x position of the current pixel in the image
 * @param y_pos          y position of the current pixel in the image
 * @param _used_exposure the currently used exposure of current pixel (this changes after each iteration of the main loop of the radiance mapper)
 */
/*
   __device__ void write_used_exposures(cset_radiancemapper* settings, int x_pos, int y_pos, int _used_exposure)
   {
   uchar4 out_value = {0, 0, 0, 255};
//float multiplier = 255.0f / (float)settings->d_num_images;

switch(_used_exposure)
{
case 0: out_value.x = 255; break; 										//red
case 1: out_value.y = 255; break; 										//green
case 2: out_value.z = 255; break; 										//blue
case 3: out_value.y = 255; out_value.z = 255; break; 					//turquoise
case 4: out_value.x = 255; out_value.y = 255; out_value.z = 255; break; //white
}
// out_value.x = _used_exposure * multiplier;
// out_value.y = _used_exposure * multiplier;
// out_value.z = _used_exposure * multiplier;

#ifdef DEBUG_IMAGE
write_pxl(settings->d_debug, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
#else
write_pxl(settings->d_out, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
#endif
}
*/

/**
 * writes which of the four cases were encountered in the current iteration of the radiance mapper
 * for information about used colors look in comments below
 * @param settings               pointer to the settings struct
 * @param x_pos                  x position of the current pixel in the image
 * @param y_pos                  y position of the current pixel in the image
 * @param main_saturate          bool if the main pixel is currently saturated
 * @param neighbourhood_saturate bool if any pixel in the neighbourhood of current pixel is saturated
 */
/*
   __device__ void write_cases(cset_radiancemapper* settings, int x_pos, int y_pos, bool main_saturate, bool neighbourhood_saturate)
   {
   uchar4 out_value = {0, 0, 0, 255};
   if(!main_saturate && !neighbourhood_saturate)
   out_value.x = 255;								// 1 = red

   if(!main_saturate && neighbourhood_saturate)
   out_value.y = 255;								// 2 = green

   if(main_saturate && !neighbourhood_saturate)
   out_value.z = 255;								// 3 = blue

   if(main_saturate && neighbourhood_saturate)
   {
   out_value.x = 255; out_value.y = 255;			// 4 = yellow
   }
#ifdef DEBUG_IMAGE
write_pxl(settings->d_debug, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
#else
write_pxl(settings->d_out, x_pos, y_pos, settings->d_image_dimensions.x, out_value);
#endif
}
*/
#endif
