// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once

#include <iostream>
#include <sstream>

#include <math.h>

#include <cuda.h>
#include <cuda_runtime.h>

//platform independent printing
void printMessage(const char* msg);
void printMessage(std::stringstream msg);
void printError(const char* error);
void printDebug(const char* msg);
void printDebug(std::stringstream msg);

//clips the incoming number to values lower and upper
int clip(int in, int lower, int upper);

//divides and rounds up
int divup(int a, int b);

//handles calls to the cuda api
void cuda_check(cudaError_t err, const char* msg);
void cuda_check(cudaError_t err, std::stringstream msg);

void create_tex_obj(cudaArray* in_arr, cudaTextureObject_t* out_tex_obj);
void create_surf_obj(cudaArray* in_arr, cudaSurfaceObject_t* out_surf_obj);
void create_tex_and_surf_obj(cudaArray* in_arr, cudaTextureObject_t* out_tex_obj, cudaSurfaceObject_t* out_surf_obj);



// template <typename T>
// T normal_pdf(T x, T m, T s);
template <typename T>
T normal_pdf(T x, T m, T s)
{
    static const T inv_sqrt_2pi = 0.3989422804014327;
    T a = (x - m) / s;

    return inv_sqrt_2pi / s * exp(-T(0.5) * a * a);
}

template <typename T>
typename T::value_type get_largest_element(T* in)
{
	typename T::iterator it = in->begin();
	typename T::value_type largest = *it;
	for(it = in->begin(); it != in->end(); ++it)
	{
		if(*it > largest)
			largest = *it;
	}

	return largest;
}