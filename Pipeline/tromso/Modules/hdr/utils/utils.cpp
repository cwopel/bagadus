// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "utils.h"
#include "defines.h"

#include "Helpers/logging.h"

#include <cstring>

#ifdef PLATFORM_WINDOWS
#include <Windows.h>
#include <string>
#include <iostream>
#endif

#ifdef PLATFORM_LINUX
#include <iostream>
#endif

void printMessage(const char* msg)
{
	#ifdef PLATFORM_WINDOWS
	if(msg)
	{
		std::string result, tmp;
		tmp = msg;
		result = tmp + "\n";
		OutputDebugString(result.c_str());
	}
	else
		OutputDebugString("??ERROR: no message to display");
	#endif
	#ifdef PLATFORM_LINUX
	if(msg)
		LOG_I("%s", msg);
	else
		LOG_E("No error message to display");
	#endif
}

void printMessage(std::stringstream msg)
{
	#ifdef PLATFORM_WINDOWS
		std::string s_msg;
		msg << std::endl;
		msg >> s_msg;
		OutputDebugString(msg.c_str());
	#endif
	#ifdef PLATFORM_LINUX
		LOG_I("%s", msg.str().c_str());
	#endif
}

void printError(const char* msg)
{
	#ifdef PLATFORM_WINDOWS
	if(msg)
	{
		std::string err, result;
		err = msg;
		result = "##ERROR: " + err + "\n";
		OutputDebugString(result.c_str());
	}
	else
	{
		OutputDebugString("??ERROR: no message to display");
	}
	#endif
	#ifdef PLATFORM_LINUX
		if(msg)
			LOG_E("%s", msg);
		else
			LOG_E("No error message to display");
	#endif
}

void printError(std::stringstream msg)
{
	#ifdef PLATFORM_WINDOWS
		std::string s_msg;
		std::stringstream tmp_msg;
		tmp_msg << "$$ERROR: " << msg << std::endl;
		tmp_msg >> s_msg;
		OutputDebugString(msg.c_str());
	#endif
	#ifdef PLATFORM_LINUX
		LOG_E("%s", msg.str().c_str());
	#endif
}

void printDebug(const char* msg)
{
	#ifdef PLATFORM_WINDOWS
	if(msg)
	{
		std::string err, result;
		err = msg;
		result = "$$DEBUG: " + err + "\n";
		OutputDebugString(result.c_str());
	}
	else
	{
		OutputDebugString("??ERROR: no message to display");
	}
	#endif
	#ifdef PLATFORM_LINUX
		LOG_D("%s", msg);
	#endif
}

void printDebug(std::stringstream msg)
{
	#ifdef PLATFORM_WINDOWS
		std::string s_msg;
		std::stringstream tmp_msg;
		tmp_msg << "$$DEBUG: " << msg << std::endl;
		tmp_msg >> s_msg;
		OutputDebugString(msg.c_str());
	#endif
	#ifdef PLATFORM_LINUX
		LOG_D("%s", msg.str().c_str());
	#endif
}

int clip(int n, int lower, int upper)
{
	if (n < lower)
	{
		 n= lower;
	} 
	else if (n > upper)
	{ 
		n= upper;
	}
	
	return n;
}

void cuda_check(cudaError_t err, const char* msg)
{
	if(err != cudaSuccess)
	{
		#ifdef PLATFORM_LINUX
			LOG_E("Cuda error: %s, %s", msg, cudaGetErrorString(err));
		#endif
		#ifdef PLATFORM_WINDOWS
			if(msg)
			{
				std::string result, tmp;
				tmp = msg;
				result = tmp + cudaGetErrorString(err) + "\n";
				OutputDebugString(result.c_str());
			}
			else
				OutputDebugString("??ERROR: no message to display");
		#endif
	}
}

void cuda_check(cudaError_t err, std::stringstream msg)
{
	if(err != cudaSuccess)
	{
		#ifdef PLATFORM_LINUX
			if(msg)
				LOG_E("Cuda error: %s, %s", msg.str().c_str(),
						cudaGetErrorString(err));
			else
				LOG_E("Cuda error: %s", cudaGetErrorString(err));
		#endif 
		#ifdef PLATFORM_WINDOWS
			std::string s_msg;
			std::stringstream tmp_msg;
			if(msg)
				tmp_msg << "##CUDA ERROR: " << msg << cudaGetErrorString(err) << std::endl;
			else
				tmp_msg << "##CUDA ERROR: " << cudaGetErrorString(err) << std::endl;
			tmp_msg >> s_msg;
			OutputDebugString(msg.c_str());
		#endif
	}
}

void create_tex_obj(cudaArray* in_arr, cudaTextureObject_t* out_tex_obj)
{
	struct cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypeArray;
    res_desc.res.array.array = in_arr;


    struct cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.normalizedCoords = 0;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModePoint;
    cuda_check(cudaCreateTextureObject(out_tex_obj, &res_desc, &tex_desc, NULL), "utils::create_tex_obj: failed to create texture object");	
}

void create_surf_obj(cudaArray* in_arr, cudaSurfaceObject_t* out_surf_obj)
{
	struct cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypeArray;
    res_desc.res.array.array = in_arr;

	cuda_check(cudaCreateSurfaceObject(out_surf_obj, &res_desc), "utils::create_surf_obj: failed to create output surface object");


}

void create_tex_and_surf_obj(cudaArray* in_arr, cudaTextureObject_t* out_tex_obj, cudaSurfaceObject_t* out_surf_obj)
{
	struct cudaResourceDesc res_desc;
    memset(&res_desc, 0, sizeof(res_desc));
    res_desc.resType = cudaResourceTypeArray;
    res_desc.res.array.array = in_arr;


    struct cudaTextureDesc tex_desc;
    memset(&tex_desc, 0, sizeof(tex_desc));
    tex_desc.normalizedCoords = 0;
    tex_desc.readMode = cudaReadModeElementType;
    tex_desc.addressMode[0] = cudaAddressModeClamp;
    tex_desc.addressMode[1] = cudaAddressModeClamp;
    tex_desc.filterMode = cudaFilterModePoint;
    cuda_check(cudaCreateTextureObject(out_tex_obj, &res_desc, &tex_desc, NULL), "utils::create_tex_and_surf_obj: failed to create texture object");	
    cuda_check(cudaCreateSurfaceObject(out_surf_obj, &res_desc), "utils::create_tex_and_surf_obj: failed to create output surface object");

}

int divup(int a, int b)
{
    return (a+b-1)/b;
}

/*
template <typename T>
T normal_pdf(T x, T m, T s)
{
    static const T inv_sqrt_2pi = 0.3989422804014327;
    T a = (x - m) / s;

    return inv_sqrt_2pi / s * exp(-T(0.5) * a * a);
} */
