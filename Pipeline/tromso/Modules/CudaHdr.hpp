// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef CUDA_HDR_HPP
#define CUDA_HDR_HPP

#include "Modules/Module.hpp"
#include "Helpers/logging.h"

#include "./hdr/radiancemapper/radiancemapper.h"
#include "./hdr/tonemapper/tonemapper.h"
#include "./hdr/weightfunction/weightfunction.h"

#include <pthread.h>
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <cstring>

#define GPU_HDR_PRINT_DEBUGTIME

class CudaHdr : public FetchModule {
    public:
        CudaHdr();
        ~CudaHdr();
        void init(FetchModule *producer, int width, int height, int numSources,
                bool useReinhard=true);

        /* See CudaModuleInterface */
        struct frame * getCudaFrame(struct frame *buffer);
        struct frame * getCudaArrayFrame(struct frame *buffer);

        /* Retrieves the next set of frames from the producer-module */
        void runConsumer();
    private:
        bool initialized;
        /* Size of 1 image. Total size = frameSize*numSources*2 + sizeof resourceHdr */
        size_t frameSize;
        int width, height, numSources;
        FetchModule *producer;
        /* 2 buffers allocated. One for consumer thread to insert next frame, one for
         * producer-thread as input for computations. Swaps upon each barrier-iteration */
        struct frame inputData[2];
        /* Determines which input buffer should be used when calling getResource */
        int inputIdx;
        pthread_barrier_t startWork;
        pthread_t consumer;
        cudaStream_t stream;

        bool tmpBufferAllocated;
        cudaArray * tmpCudaArray;

        RadianceMapper* radianceMapper;
        ToneMapper* toneMapper;
        WeightFunction* weightFunction;


        dim3 block, grid;

        /* Output is half the size of input */
        void runKernels(cudaArray *output, cudaArray *input,
                struct header *header, float scene_key);
        void initCudaArrays();
        void deleteCudaArrays();
};

namespace CudaHdrNamespace{
    void *consumerStarter(void * context);
};

#endif
