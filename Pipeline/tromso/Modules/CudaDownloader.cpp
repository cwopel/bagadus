// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CudaDownloader.hpp"

#include "Helpers/logging.h"

#include <sys/time.h>

using namespace CudaDownload;

CudaDownloader::CudaDownloader(){
	initialized = false;
}

CudaDownloader::~CudaDownloader(){
	if(!initialized) return;

	pthread_join(consumer, NULL);
	cudaSafe(cudaStreamDestroy(stream));
	if(inputData[0]){
		cudaSafe(cudaFree(inputData[0]->cuArr));
		free(inputData[0]);
	}
	if(inputData[1]){
		cudaSafe(cudaFree(inputData[1]->cuArr));
		free(inputData[1]);
	}
#ifndef HAVE_CUDA
	cudaSafe(cudaFreeHost(hostBuffer));
#endif
	if(totalFrames>20)
		LOG_I("Download time: %7.3fms", totalTime/(totalFrames-20));
}

void CudaDownloader::init(FetchModule *producer, size_t frameSize){

	inputIdx = 0;
	this->frameSize = frameSize;
	this->producer = producer;
	cudaSafe(cudaStreamCreate(&stream));
#ifdef HAVE_CUDA
	hostBuffer = NULL;
#else
	cudaSafe(cudaMallocHost(&hostBuffer,frameSize));
#endif
	inputData[0] = new struct frame;
	inputData[1] = new struct frame;
	if(!inputData[0] || !inputData[1]){
		LOG_E("Failed to allocate cpu input buffer(s) in cudaDownloader");
		exit(EXIT_FAILURE);
	}
	cudaSafe(cudaMalloc(&inputData[0]->cuArr,frameSize));
	cudaSafe(cudaMalloc(&inputData[1]->cuArr,frameSize));

	pthread_barrier_init(&startWork, NULL, 2);
	if(pthread_create(&consumer, NULL, consumerStarter, (void*)this)){
		LOG_E("Failed to start downloader consumer thread");
		perror("pthread_create");
		exit(EXIT_FAILURE);
	}

	totalTime = 0;
	totalFrames = 0;

	initialized = true;
}

struct frame * CudaDownloader::getFrame(struct frame *buffer){
	pthread_barrier_wait(&startWork);
	inputIdx ^= 1;
	if(!inputData[inputIdx]) return NULL;
    if(!buffer){
        LOG_E("CudaDownloader, getFrame: buffer=NULL not implemented");
        return NULL;
    }

	struct timeval start_time, stop_time;
	gettimeofday(&start_time, 0);

    buffer->hdr = inputData[inputIdx]->hdr;

    //TODO This isn't perfect. I'm making an assumption here that the next module is
    //the x264 encoder, which will malloc pinned memory if this is defined. However, if
    //another module is used that doesn't do this, it will perform only partly async transfer
#ifdef HAVE_CUDA
	/* Copy data into (hopefully) pinned memory asynchronously */
	cudaSafe(cudaMemcpyAsync(buffer->ptr, inputData[inputIdx]->cuArr,
				frameSize, cudaMemcpyDeviceToHost, stream));
	cudaSafe(cudaStreamSynchronize(stream));
#else
	/* Copy data into pinned memory asynchronously */
	cudaSafe(cudaMemcpyAsync(hostBuffer, inputData[inputIdx]->cuArr,
				frameSize, cudaMemcpyDeviceToHost, stream));
	cudaSafe(cudaStreamSynchronize(stream));
	/* Copy the data again, out of pinned memory and into target buffer */
	memcpy(buffer->ptr, hostBuffer, frameSize);
#endif

	gettimeofday(&stop_time, 0);
 	float time;
	time = (stop_time.tv_sec * 1.0e3 + stop_time.tv_usec / 1.0e3)
		- (start_time.tv_sec * 1.0e3 + start_time.tv_usec / 1.0e3);
	totalFrames++;
	if(totalFrames>20) totalTime += time;

	return buffer;
}

void CudaDownloader::runConsumer(){
	int inIdx = inputIdx^1;
	while(true){
		struct frame * ret = producer->getCudaFrame(inputData[inIdx]);
		if(!ret){
			cudaSafe(cudaFree(inputData[inIdx]->cuArr));
			free(inputData[inIdx]);
			inputData[inIdx] = NULL;
			break;
		}
		pthread_barrier_wait(&startWork);
		inIdx ^= 1;
	}
	pthread_barrier_wait(&startWork);
}

void *CudaDownload::consumerStarter(void * context){
	((CudaDownloader *)context)->runConsumer();
	pthread_exit(0);
	return NULL;
}
