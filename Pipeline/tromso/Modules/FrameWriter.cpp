// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "FrameWriter.hpp"

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>

#include "Helpers/logging.h"

//
// ### Helper functions
//

// Check if a buffer is full
inline bool FrameWriter::bufferIsFull()
{
    bool ret = false;
#if FRAMESYNCER_USE_DOLPHIN
    sci_sequence_status_t status;
    sci_error_t err;

    do {
        status = SCIStartSequence(seq_rw, NO_FLAGS, &err);
    } while (status != SCI_SEQ_OK);
#endif

    //fprintf(stderr, "Read: %d, Write %d\n", *read, *write);
    if (*read == (*write + 1) % FRAMESYNCER_BUFFER_COUNT) {
        ret = true;
    }

#if FRAMESYNCER_USE_DOLPHIN
    // Check sequence for errors
    status = SCICheckSequence(seq_rw, NO_FLAGS, &err);
    if (status != SCI_SEQ_OK) {
        LOG_E("Something went wrong when trying to read "
                "the pointer. Error code 0x%X", status);
        return true;
    }
#endif

    return ret;
}

// This function wait's until there's a free buffer, or a timeout has occured.
inline bool FrameWriter::waitForBuffer()
{
    int count = 0;
    while (bufferIsFull()) {
        // First we must check for a timeout, and if a timeout has occured, we
        // return false to indicate a timeout.
        // TODO: Wait according to frame rate
        if (count > 25) {
            return false;
        }

        // While we are waiting, it might be usefull to invoke the scheduler,
        // either by calling some sort of sleep method or by using scheduler
        // APIs if available.
        usleep(1000);

        count++;
    }

    // If we got out of the loop above, we know that there's a free buffer.
    return true;
}

FrameWriter::FrameWriter(uint8_t cam_id, size_t frame_size, unsigned int id)
{
    this->cam_id      = cam_id;
    this->frame_size  = frame_size;

#if FRAMESYNCER_USE_DOLPHIN
    this->sid_rw      = id;
    this->sid_headers = id + 1;
    this->sid_frames  = id + 2 + cam_id;
#endif
}

//
// ### Setup
//

// Set up the writer
bool FrameWriter::initialize(uint8_t *r_ptr, uint8_t *w_ptr,
        struct header *h_ptr, void *f_ptr)
{
    if (initialized) return true;

#if FRAMEWRITER_USE_CUDA
    LOG_E("Cuda is currently not supported for the frame writer");
    return false;
#endif

#if FRAMESYNCER_USE_DOLPHIN

    if (r_ptr || w_ptr || h_ptr || f_ptr) {
        LOG_W("Pointers were passed to FrameWriter::initialize, "
                "these are ignored because it is compiled for Dolphin use.");
    }

    sci_error_t err;

    //
    // ** Virtual devices **
    //

    // Create SISCI virtual descriptors. Each descriptor can only handle one
    // connection, so we need three, one for each of the remote segments we are
    // going to connect to.

    SCIOpen(&sd_frames, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIOpen", open_frames_failure);

    SCIOpen(&sd_headers, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIOpen", open_hdr_failure);

    SCIOpen(&sd_rw, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIOpen", open_rw_failure);

    //
    // ** Frame segment **
    //

    // The frames are first copied into a local segment, and then transferred
    // from the local segment to a remote segment. Here the segment is created,
    // marked as private because it will only be used locally and not accept
    // any connections. Then it is prepared as a DMA source, and finally the
    // segment is mapped into user space memory.

    SCICreateSegment(sd_frames, &seg_frames, sid_frames, frame_size, NULL,
            NULL, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCICreateSegment", create_failure);

    SCIPrepareSegment(seg_frames, adapter, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIPrepareSegment", prepare_failure);

    frames = SCIMapLocalSegment(seg_frames, &map_frames, 0,
            frame_size, NULL, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIMapLocalSegment", map_failure);

    //
    // ** DMA queue **
    //

    // We use DMA to transfer the frame data, so we need a DMA queue.

    SCICreateDMAQueue(sd_frames, &dma_queue, adapter, 1, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCICreateDMAQueue", dma_failure);

#else

    if (!h_ptr || !f_ptr || !w_ptr || !r_ptr) {
        return false;
    }

    frames = f_ptr;
    headers = h_ptr;

    write = w_ptr;
    read = r_ptr;

#endif

    initialized = true;

    return true;

#if FRAMESYNCER_USE_DOLPHIN

    //
    // ** Error handling **
    //
    // If something goes wrong, try our best to clean up from the point where
    // things went wrong.

dma_failure:
    SCIUnmapSegment(map_frames, NO_FLAGS, &err);
    if (err != SCI_ERR_OK) {
        LOG_W("Unable to unmap segment");
    }

map_failure:
prepare_failure:
    SCIRemoveSegment(seg_frames, NO_FLAGS, &err);
    if (err != SCI_ERR_OK) {
        LOG_W("Unable to remove segment");
    }

create_failure:
open_rw_failure:
    SCIClose(sd_headers, NO_FLAGS, &err);
    if (err != SCI_ERR_OK) {
        LOG_W("Unable to close virtual dolphin device.");
    }

open_hdr_failure:
    SCIClose(sd_frames, NO_FLAGS, &err);
    if (err != SCI_ERR_OK) {
        LOG_W("Unable to close virtual dolphin device.");
    }

open_frames_failure:

    return false;

#endif
}

bool FrameWriter::connect(unsigned int node)
{
#if FRAMESYNCER_USE_DOLPHIN
    if (!initialized) return false;
    if (connected) return true;

    sci_error_t err;
    unsigned int seg_size;
    volatile uint8_t *rw;

    //
    // ** Connect to segments **
    //
    // Connect to each of the three segments we use.

    SCIConnectSegment(sd_frames, &rseg_frames, node, sid_frames, adapter,
            NULL, NULL, SCI_INFINITE_TIMEOUT, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIConnectSegment", connect_frames_failure);

    SCIConnectSegment(sd_rw, &rseg_rw, node, sid_rw, adapter,
            NULL, NULL, SCI_INFINITE_TIMEOUT, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIConnectSegment", connect_rw_failure);

    SCIConnectSegment(sd_headers, &rseg_headers, node, sid_headers, adapter,
            NULL, NULL, SCI_INFINITE_TIMEOUT, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIConnectSegment", connect_headers_failure);

    //
    // ** Map segments **
    //
    // Map each of the segments we just connected to into user space. The read
    // and write segment is mapped two times, one for the read pointer and one
    // for the write pointer.

    seg_size = SCIGetRemoteSegmentSize(rseg_rw);

    //LOG_D("Segment size: %u", seg_size);

    rw = (volatile uint8_t *)SCIMapRemoteSegment(rseg_rw, &map_rw, 0,
            seg_size, NULL, NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIMapRemoteSegment", map_rw_failure);

    read = rw + cam_id;
    write = read + (seg_size >> 1);

    headers = (volatile struct header *)SCIMapRemoteSegment(rseg_headers,
            &map_headers,
            FRAMESYNCER_BUFFER_COUNT * sizeof(struct header) * cam_id,
            FRAMESYNCER_BUFFER_COUNT * sizeof(struct header), NULL,
            NO_FLAGS, &err);
    SCIErrorGoto(err, "SCIMapRemoteSegment", map_headers_failure);

    //
    // ** Sequence descriptors **
    //

    // For each of the mapped remote segments, create a sequence descriptor so
    // that we can check for errors.

    SCICreateMapSequence(map_rw, &seq_rw, SCI_FLAG_FAST_BARRIER, &err);
    SCIErrorGoto(err, "SCICreateMapSequence", seq_rw_failure);

    SCICreateMapSequence(map_headers, &seq_headers, SCI_FLAG_FAST_BARRIER,
            &err);
    SCIErrorGoto(err, "SCICreateMapSequence", seq_headers_failure);

    connected = true;
#endif

    return true;

#if FRAMESYNCER_USE_DOLPHIN

    //
    // ** Error handling **
    //
    // Try to clean up as good as posible if something goes wrong.

seq_headers_failure:
    SCIRemoveSequence(seq_rw, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIRemoveSequence");

seq_rw_failure:
    SCIRemoveDMAQueue(dma_queue, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIRemoveDMAQueue");

map_headers_failure:
    SCIUnmapSegment(map_rw, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIUnmapSegment");

map_rw_failure:
    SCIDisconnectSegment(rseg_headers, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIDisconnectSegment");

connect_headers_failure:
    SCIDisconnectSegment(rseg_rw, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIDisconnectSegment");

connect_rw_failure:
    SCIDisconnectSegment(rseg_frames, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIDisconnectSegment");

connect_frames_failure:

    return false;
#endif
}

// Set the input module to get frames from
bool FrameWriter::setInput(FetchModule *m)
{
    pthread_mutex_lock(&input_lock);
    in = m;
    pthread_mutex_unlock(&input_lock);

    return true;
}

//
// ### Running
//

// Run the writer in the current thread. This will block all other operations.
bool FrameWriter::run()
{

#if FRAMEWRITER_USE_CUDA
    // TODO: Cuda support is not implemented yet
    return false;
#endif

    // Initialize a buffer for the frame to read
    struct frame f;

#if FRAMESYNCER_USE_DOLPHIN
    f.ptr = frames;
#else
    f.ptr = malloc(frame_size);
#endif

    // When calling function that shouldn't be interrupted, we disable thread
    // cancelling. When doing this, we must keep track of the previous state
    // in order to restore it afterwards.
    int cancel_state;

#if FRAMESYNCER_USE_DOLPHIN
    sci_error_t err;
#endif

    for (;;) {
        //fprintf(stderr, "Writer is asking for a frame\n");
        //struct timeval before;
        //struct timeval after;
        //struct timeval total;

        // Get a frame from the input module
        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, &cancel_state);
        pthread_mutex_lock(&input_lock);

        //gettimeofday(&before, NULL);
        if (!in) {
            LOG_E("No imput module set");

            // We must reenable the disabling before continuing
            pthread_mutex_unlock(&input_lock);
            pthread_setcancelstate(cancel_state, NULL);

            continue;
        } else if (!in->getFrame(&f)) {
            LOG_E("Get frame timed out");

            // We must reenable the disabling before continuing
            pthread_mutex_unlock(&input_lock);
            pthread_setcancelstate(cancel_state, NULL);

            break;
        }
        //gettimeofday(&after, NULL);
        pthread_mutex_unlock(&input_lock);
        pthread_setcancelstate(cancel_state, NULL);

        //timersub(&after, &before, &total);
        //LOG_D("Get frame took %2ld.%06d\n", total.tv_sec, total.tv_usec);

        //fprintf(stderr, "Writer got a frame\n");

        // Before we can write the frame to the other side, we must make sure
        // that there's a free buffer to write to.
        if (waitForBuffer()) {
#if FRAMESYNCER_USE_DOLPHIN
            // Start error checking sequence
            // TODO: Timeout
            sci_sequence_status_t status;
            do {
                status = SCIStartSequence(seq_rw, NO_FLAGS, &err);
            } while (status != SCI_SEQ_OK);
#endif

            // There's now a free buffer we can write to
            uint8_t buffer = *write;

#if FRAMESYNCER_USE_DOLPHIN
            // Check sequence for errors
            status = SCICheckSequence(seq_rw, NO_FLAGS, &err);
            if (status != SCI_SEQ_OK) {
                LOG_E("Something went wrong when trying to read "
                        "the write position. Error code 0x%X", status);
                continue;
            }
#endif

            size_t offset = buffer * frame_size;

            // Do actual writing
#if FRAMESYNCER_USE_DOLPHIN
            // DMA the frame
            SCIStartDmaTransfer(dma_queue, seg_frames, rseg_frames,
                    0, frame_size, offset, NULL, NULL, NO_FLAGS, &err);
            if (err != SCI_ERR_OK) {
                LOG_D("Using buffer %d, Offset %ld, Frame size %ld, Cam id: %d",
                        buffer, offset, frame_size, cam_id);
                LOG_E("Start DMA failed, dropping frame. - Error 0x%X", err);
                continue;
            }

            // Wait for the DMA transfer to complete. If it takes longer than
            // 15 ms, something is wrong and we drop the frame.
            SCIWaitForDMAQueue(dma_queue, 15, NO_FLAGS, &err);
            if (err != SCI_ERR_OK) {
                LOG_E("DMA transfer failed - Error code 0x%X", err);
                continue;
            }

            // Start error checking sequence
            // TODO: Timeout
            do {
                status = SCIStartSequence(seq_headers, NO_FLAGS, &err);
            } while (status != SCI_SEQ_OK);

            // Copy the header
            memcpy((void *)&headers[buffer], (const void *)&f.hdr,
                    sizeof(struct header));

            // Check sequence for errors
            status = SCICheckSequence(seq_headers, NO_FLAGS, &err);
            if (status != SCI_SEQ_OK) {
                LOG_E("Something went wrong when trying to write "
                        "the frame header. Error code 0x%X", status);
                continue;
            }
#else
            // Calculate the address to write to, and then copy the frame data
            // to that address.
            void *b = (uint8_t *)frames + offset;
            memcpy(b, f.ptr, frame_size);

            // Copy the header data into the correct buffer
            headers[buffer] = f.hdr;
#endif

#if FRAMESYNCER_USE_DOLPHIN
            // Start error checking sequence
            // TODO: Timeout
            do {
                status = SCIStartSequence(seq_rw, NO_FLAGS, &err);
            } while (status != SCI_SEQ_OK);
#endif
            if (buffer != *write) {
                LOG_E("Holy fuck, jesus++");
            }
            // Update shared writer location.
            *write = (buffer + 1) % FRAMESYNCER_BUFFER_COUNT;

#if FRAMESYNCER_USE_DOLPHIN
            // Check sequence for errors
            status = SCICheckSequence(seq_rw, NO_FLAGS, &err);
            if (status != SCI_SEQ_OK) {
                LOG_E("Something went wrong when trying to write "
                        "the buffer index. Error code 0x%X", status);
                continue;
            }
#endif

            //fprintf(stderr, "Wrote a frame %d:%d\n", camera_id, *write);
        } else {
            // A timeout occured before we could write the frame. This means
            // that the frame will be dropped
            //LOG_W("Timed out waiting for a buffer");
        }
    }

    running = false;
    return false;
}

bool FrameWriter::threadStart()
{
    if (running) return true;

    int err = pthread_create(&thread, NULL, FrameWriter::thread_run, this);
    if (err != 0) {
        LOG_E("Error creating thread.");
        return false;
    }

    running = true;

    return true;
}

bool FrameWriter::threadStop()
{
    if (!running) return true;

    int err = pthread_cancel(thread);
    if (err != 0) {
        LOG_E("Error stopping thread.");
        return false;
    }

    running = false;

    return true;
}

bool FrameWriter::threadIsRunning()
{
    return running;
}

bool FrameWriter::disconnect()
{
#if FRAMEWRITER_USE_DOLPHIN
    if (!connected) return true;

    sci_error_t err;

    //
    // ** Unmap segments **
    //

    SCIUnmapSegment(map_rw, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIUnmapSegment");

    SCIUnmapSegment(map_headers, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIUnmapSegment");

    //
    // ** Disconnect **
    //

    SCIDisconnectSegment(rseg_rw, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIDisconnectSegment");

    SCIDisconnectSegment(rseg_headers, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIDisconnectSegment");

    SCIDisconnectSegment(rseg_frames, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIDisconnectSegment");

    connected = false;
#endif

    return true;
}

bool FrameWriter::terminate()
{
    if (!initialized) return true;

#if FRAMEWRITER_USE_DOLPHIN
    sci_error_t err;

    //
    // ** Error check sequences **
    //

    SCIRemoveSequence(seq_rw, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIRemoveSequence");

    SCIRemoveSequence(seq_headers, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIRemoveSequence");

    //
    // ** DMA queue **
    //

    SCIRemoveDMAQueue(dma_queue, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIRemoveDMAQueue");

    //
    // ** Frame segment **
    //

    SCIUnmapSegment(map_frames, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIUnmapSegment");

    SCIRemoveSegment(seg_frames, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIRemoveSegment");

    //
    // ** Virtual devices **
    //

    SCIClose(sd_frames, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIClose");

    SCIClose(sd_headers, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIClose");

    SCIClose(sd_rw, NO_FLAGS, &err);
    SCIErrorWarn(err, "SCIClose");

#endif

    initialized = false;

    return true;
}

void *FrameWriter::thread_run(void *ctx)
{
    FrameWriter *w = (FrameWriter *)ctx;

    w->run();

    return NULL;
}
