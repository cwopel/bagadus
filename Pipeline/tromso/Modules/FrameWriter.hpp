// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifndef writer_hpp
#define writer_hpp

#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

#include "config.h"

#if FRAMESYNCER_USE_DOLPHIN
#include <sisci_api.h>
#endif

#include "Module.hpp"

class FrameWriter
{
    public:
        FrameWriter(uint8_t cam_id, size_t frame_size=2040*1080,
                unsigned int id=0x10);

        //
        // ### Setup
        //

        // The setup function behaves differently depending on how the writer
        // was compiled. If it's compiled with Dolphin support, none of the
        // arguments will be used, so it can safely be called without
        // arguments. If it's not compiled with Dolphin support all arguments
        // must be specified as it will not automatically connect to the frame
        // syncer.
        //
        // This setup allows the syncer to be used in multiple ways. If you
        // want to use different processes you can use it with shared memory,
        // if you want to run it in different threads, you can directly pass in
        // pointers to the FrameSyncer's memory. There should be no
        // posibilities of deadlocks or race conditions under any conditions.
        bool initialize(uint8_t *r_ptr=NULL, uint8_t *w_ptr=NULL,
                struct header *h_ptr=NULL, void *f_ptr=NULL );

        // Connect to the remote machine. This is currently only needed when
        // used with Dolphin. If `true` is returned, that means that we have
        // successfully connected to the remote machine. If `false` is
        // returned, something went wrong.
        bool connect(unsigned int remote);

        // Change the input module
        bool setInput(FetchModule *m);

        //
        // ### Running
        //

        // Run the frame writer in the current thread
        bool run();

        // Run the frame writer in a new thread
        bool threadStart();
        bool threadStop();
        bool threadIsRunning();

        //
        // ### Termination
        //

        bool disconnect();

        bool terminate();

    private:
        // Keep track of the current state of the syncer.
        bool initialized = false;
        bool running = false;
#if FRAMESYNCER_USE_DOLPHIN
        bool connected = false;
#endif

        // The ID of the camera we are getting frames from. This is used to
        // write the data to the correct locations.
        uint8_t cam_id;

        // This is the thread if we run the writer in a thread.
        pthread_t thread;

        // The module to read frames from
        FetchModule *in = NULL;

        // The size of a single frame.
        size_t frame_size;

        // Pointer to the header and frame arrays. These are the locations that
        // the data will be written to. If dolphin is enabled only the headers
        // are mapped into user space. The frame data will only be transferred
        // using DMA requests.
#if FRAMESYNCER_USE_DOLPHIN
        volatile struct header *headers;
#else
        struct header *headers;
#endif

        // Pointer to the frame buffers. When dolphin is enabled, this is a
        // local dolphin segment mapped into user space.
        void *frames;

        // Pointers to the read and write locations
#if FRAMESYNCER_USE_DOLPHIN
        volatile uint8_t *write;
        volatile uint8_t *read;
#else
        uint8_t *write;
        uint8_t *read;
#endif

        // TODO REMOVE THIS
        uint8_t r = 0, w = 0;

        // Just to be thread safe, have a lock used in the set function to make
        // sure we never change the input module while it is being used.
        pthread_mutex_t input_lock = PTHREAD_MUTEX_INITIALIZER;

#if FRAMESYNCER_USE_DOLPHIN
        // SISCI virtual device descriptors
        sci_desc_t sd_rw;
        sci_desc_t sd_headers;
        sci_desc_t sd_frames;

        // Segment IDs
        unsigned int sid_rw;
        unsigned int sid_headers;
        unsigned int sid_frames;

        // Local segments
        sci_local_segment_t seg_frames;

        // Remote segments
        sci_remote_segment_t rseg_rw;
        sci_remote_segment_t rseg_headers;
        sci_remote_segment_t rseg_frames;

        // Map descriptors
        sci_map_t map_rw;
        sci_map_t map_headers;
        sci_map_t map_frames;

        // DMA queue
        sci_dma_queue_t dma_queue;

        // Error checking sequence for the mapped segments
        sci_sequence_t seq_rw;
        sci_sequence_t seq_headers;

        unsigned int adapter = 0;
#endif

        //
        // ### Helper functions
        //

        // Check if the output buffer is full. Returns `true` if the buffer is
        // full and `false` when there is available slots to write to.
        inline bool bufferIsFull();

        // Waits until there is a free slot in the output buffer or a timeout
        // occurs. If a timeout occured, `false` is returned. If `true` is
        // returned it means that there is an available buffer.
        inline bool waitForBuffer();

        // Entry point for pthread. We pass this as an argument and this
        // function then calls `run()` on the object in a new thread.
        static void *thread_run(void *ctx);
};

#endif
