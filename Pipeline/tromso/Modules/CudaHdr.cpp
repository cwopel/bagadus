// AUTHOR(s): Lorenz Kellerer,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "CudaHdr.hpp"

#include <iostream>
#include <cstdio>
#include "./hdr/radiancemapper/radiancemapper_debevec.h"
#include "./hdr/radiancemapper/radiancemapper_tocci.h"
#include "./hdr/radiancemapper/radiancemapper_robertson.h"

#include "./hdr/tonemapper/tonemapper_ward.h"
#include "./hdr/tonemapper/tonemapper_histward.h"
#include "./hdr/tonemapper/tonemapper_reinhard.h"

#include "./hdr/weightfunction/weightfunction_simple.h"
#include "./hdr/weightfunction/weightfunction_linear.h"

#include "./hdr/utils/defines.h"

using namespace CudaHdrNamespace;

CudaHdr::CudaHdr(){
    initialized = false;
}

CudaHdr::~CudaHdr(){
    if(!initialized) return;

    pthread_join(consumer, NULL);
    if(tmpBufferAllocated){
        cudaSafe(cudaFreeArray(tmpCudaArray));
    }
    deleteCudaArrays();
}

void CudaHdr::init(FetchModule *producer, int width, int height,
        int numSources, bool useReinhard)
{
    inputIdx = 0;
    this->width = width;
    this->height = height;
    this->numSources = numSources;
    this->frameSize = width*height*4; //4B/pixel
    this->producer = producer;
    tmpBufferAllocated = false;



    //most reliable radiance mapper should me used most of the time
    radianceMapper = new RadianceMapperDebevec();

    //basically the same as Debevec, but tends to warp colours a bit
    // radianceMapper = new RadianceMapperRobertson();

    //VERY slow and poor visual quality, only use if response function is broken
    // radianceMapper = new RadianceMapperTocci();

    //very quick but rather poor quality. Performs no warping of the input, good to
    //visualize raw output of radiancemapper
    // toneMapper = new ToneMapperWard();

    if(useReinhard){
        //visual good result, but too slow and requires tons of memory
        //maybe use in future with more powerful GPU
        toneMapper = new ToneMapperReinhard();
    }else{
        //use this. compresses input best, but tends to exxagerate colors
        toneMapper = new ToneMapperHistogramWard();
    }


    //hat like weightfunciton that gives pixels in middle range the most weight
    // weightFunction = new WeightFunctionSimple(0, 255.0);

    //flat line weight. No matter what input always returns same weight
    weightFunction = new WeightFunctionLinear(0, 255.0);

    radianceMapper->init(weightFunction);
    toneMapper->init(numSources, width, height);

    initCudaArrays();

    pthread_barrier_init(&startWork, NULL, 2);
    if(pthread_create(&consumer, NULL, consumerStarter, (void*)this)){
        LOG_E("Failed to start cudaBayerConverter consumer thread");
        perror("pthread_create");
        exit(EXIT_FAILURE);
    }
    initialized = true;
}


struct frame * CudaHdr::getCudaFrame(struct frame *buffer)
{
    pthread_barrier_wait(&startWork);
    inputIdx ^= 1;
    if(!inputData[inputIdx].cuArr) return NULL;

    if(!tmpBufferAllocated){
        tmpBufferAllocated = true;
        cudaChannelFormatDesc byte4Channel = cudaCreateChannelDesc<uchar4>();
        cudaSafe(cudaMallocArray(&tmpCudaArray,&byte4Channel, width,
                    height*numSources, cudaArraySurfaceLoadStore));
    }

    buffer->hdr = inputData[inputIdx].hdr;

    //TODO: replace SCENE_KEY_BRIGHTNESS with dynamic value based on scene brightness
    runKernels(tmpCudaArray, inputData[inputIdx].cuArr, &buffer->hdr,
            SCENE_KEY_BRIGHTNESS);

    cudaSafe(cudaMemcpyFromArrayAsync(buffer->cuArr, tmpCudaArray, 0, 0,
                frameSize*numSources, cudaMemcpyDeviceToDevice, stream));

    cudaSafe(cudaStreamSynchronize(stream));

    return buffer;
}

struct frame * CudaHdr::getCudaArrayFrame(struct frame *buffer)
{
    pthread_barrier_wait(&startWork);
    inputIdx ^= 1;
    if(!inputData[inputIdx].cuArr) return NULL;

    buffer->hdr =  inputData[inputIdx].hdr;

    //TODO: replace SCENE_KEY_BRIGHTNESS with dynamic value based on scene brightness
    runKernels(buffer->cuArr, inputData[inputIdx].cuArr, &buffer->hdr,
            SCENE_KEY_BRIGHTNESS);

    return buffer;
}

void CudaHdr::runConsumer(){
    int inIdx = inputIdx^1;
    while(true){
        struct frame * ret = producer->getCudaArrayFrame(&inputData[inIdx]);
        if(!ret){
            cudaSafe(cudaFreeArray(inputData[inIdx].cuArr));
            inputData[inIdx].cuArr = NULL;
            break;
        }
        pthread_barrier_wait(&startWork);
        inIdx ^= 1;
    }
    pthread_barrier_wait(&startWork);
}

void *CudaHdrNamespace::consumerStarter(void * context){
    ((CudaHdr *)context)->runConsumer();
    pthread_exit(0);
    return NULL;
}


