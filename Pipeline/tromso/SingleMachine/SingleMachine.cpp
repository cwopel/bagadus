// AUTHOR(s): Ragnar Langseth, Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <cstdlib>
#include <cstdio>
#include <getopt.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <pthread.h>

#define FRAMESYNCER_BUFFER_COUNT 3

//Custom includes
#include <logging.h>
#include <PylGigE.hpp>
#include <Modules/Module.hpp>
#include <Modules/FrameSyncer.hpp>
#include <Modules/FrameWriter.hpp>
#include <Modules/NVENCWrapper.hpp>
#include <Modules/CudaUploader.hpp>
#include <Modules/CudaHdr.hpp>
#include <Modules/CudaBayerConverter.hpp>
#include <Modules/CudaDynamicStitcher.hpp>

//Defaults
static char MAP_FILE_NAME[] = "map3Dynamic.data";
static char OUTPUT_FOLDER[] = "./recording/";
static std::vector<std::string> CAMERA_MACS = {"003053161436","003053161421","003053151E2C"};
static int sec_per_file = 2;
static int fps = 30;
static int output_pipe = -1;
static int seconds = 15;
static int hdr = 0;
static int pilot_camera = 1;
static bool save_images_on_termination = false;
static char *output_folder = OUTPUT_FOLDER;
static char *map_file = MAP_FILE_NAME;

void print_help() {
    fprintf(stdout, "Usage:\n");
    fprintf(stdout, "-f --frate     Desired frame rate\n");
    fprintf(stdout, "-m --map       Input map file for stitcher\n");
    fprintf(stdout, "-o --output    Output folder for recording\n");
    fprintf(stdout, "-p --pipe      Named pipe for printing completed files\n");
    fprintf(stdout, "-s --seconds   Number of seconds to run\n");
    fprintf(stdout, "-i --img       Save last set of frames to file\n");
    fprintf(stdout, "-d --hdr       Turn on HDR mode\n");
}

class CameraWrapper : public FetchModule {
    public:
        CameraWrapper(PylonGigE::PylonStream * s, bool hdr){
            stream = s;
            hdrMode = hdr;
        }
        struct frame *getHdrFrame(struct frame *ret){
            PylonGigE::PylonFrame * f1, * f2;
            f1 = stream->getFrame();
            if(!f1) return NULL;
            while(!f1->darkExposure){
                delete f1;
                f1 = stream->getFrame();
                if(!f1) return NULL;
            }
            f2 = stream->getFrame();
            if(!f2) return NULL;
            while(f2->darkExposure){
                delete f2;
                f2 = stream->getFrame();
                if(!f2) return NULL;
            }

            struct frame *target;
            if(ret){
                target = ret;
            }else{
                target = new struct frame;
                target->ptr = malloc(f1->size + f2->size);
            }
//             LOG_D("F[%10p]: %8ld.%6ld", this, f2->timeStamp.tv_sec, f2->timeStamp.tv_usec);
            target->hdr.timestamp = f2->timeStamp;
            target->hdr.frameNum = f2->frameCounter/2;
            target->hdr.expoFirst = f1->exposureTime;
            target->hdr.expoSecond = f2->exposureTime;
            target->hdr.flags = 0;
            target->hdr.size = f1->size + f2->size;
            memcpy(target->ptr, f1->pixels, f1->size);	
            memcpy((void*)((uint8_t*)target->ptr + f1->size), f2->pixels, f2->size);	
            delete f1;
            delete f2;
            return target;
        }

        struct frame *getFrame(struct frame *ret){

            if(hdrMode) return getHdrFrame(ret);

            PylonGigE::PylonFrame * f = stream->getFrame();
            if(!f) return NULL;
            struct frame *target;
            if(ret){
                target = ret;
            }else{
                target = new struct frame;
                target->ptr = malloc(f->size);
            }
//             LOG_I("F[%6d]: %8ld.%6ld", f->frameCounter, f->timeStamp.tv_sec, f->timeStamp.tv_usec);
            target->hdr.timestamp = f->timeStamp;
            target->hdr.frameNum = f->frameCounter;
            target->hdr.expoFirst = 0;
            target->hdr.expoSecond = 0;
            target->hdr.flags = 0;
            target->hdr.size = f->size;
            memcpy(target->ptr, f->pixels, f->size);	
            delete f;
            return target;
        }
    private:
        PylonGigE::PylonStream *stream;
        bool hdrMode;
};

class CameraHdrWrapper : public FetchModule {
    public:
        CameraHdrWrapper(PylonGigE::PylonStream * s){
            stream = s;
        }
        struct frame *getFrame(struct frame *ret){
            PylonGigE::PylonFrame * f1, * f2;
            f1 = stream->getFrame();
            if(!f1) return NULL;
            while(!f1->darkExposure){
                delete f1;
                f1 = stream->getFrame();
                if(!f1) return NULL;
            }
            while(f2->darkExposure){
                delete f2;
                f2 = stream->getFrame();
                if(!f2) return NULL;
            }

            struct frame *target;
            if(ret){
                target = ret;
            }else{
                target = new struct frame;
                target->ptr = malloc(f1->size + f2->size);
            }
            // 			LOG_I("F: %8ld.%6ld", f->timeStamp.tv_sec, f->timeStamp.tv_usec);
            target->hdr.timestamp = f2->timeStamp;
            target->hdr.frameNum = f2->frameCounter/2;
            target->hdr.expoFirst = f1->exposureTime;
            target->hdr.expoSecond = f2->exposureTime;
            target->hdr.flags = 0;
            target->hdr.size = f1->size + f2->size;
            memcpy(target->ptr, f1->pixels, f1->size);	
            memcpy((void*)((uint8_t*)target->ptr + f1->size), f2->pixels, f2->size);	
            delete f1;
            delete f2;
            return target;
        }
    private:
        PylonGigE::PylonStream *stream;
};

static PylonGigE::PylGigE *cameras = NULL;
static std::vector<PylonGigE::PylonStream*> streams;
static CameraWrapper **wrappers = NULL;
static FrameWriter **writers = NULL;
static FrameSyncer *syncer = NULL;
static CudaUploader *uploader = NULL;
static CudaHdr *hdrModule = NULL;
static CudaBayerConverter *converter = NULL;
static CudaDynamicStitcher *stitcher = NULL;
static NVENCWrapper *h264enc = NULL;
static uint32_t cams = 0;

// Context read from file
static ContextVariables context;

std::string mkRecordingDir(std::string root){

    time_t t = time(0);
    struct tm * now = localtime(&t);

    std::ostringstream directoryPath;
    //YYYY-MM-DD_hhmm/
    directoryPath << root
        << (now->tm_year + 1900) << "-"
        << std::setw(2) << std::setfill('0') << (now->tm_mon + 1) << "-"
        << std::setw(2) << std::setfill('0') << now->tm_mday << "_"
        << std::setw(2) << std::setfill('0') << now->tm_hour
        << std::setw(2) << std::setfill('0') << now->tm_min
        << "/";

    return directoryPath.str();
}

pthread_cond_t sleepCond;
pthread_mutex_t sleepMutex;
//Catch SIGINT
void interruptHandler(int ignore=0){

    signal(SIGINT, interruptHandler);

    pthread_mutex_lock(&sleepMutex);
    fprintf(stderr, "\nCaught CTRL+C. Terminating early\n\n");
    pthread_cond_signal(&sleepCond);
    pthread_mutex_unlock(&sleepMutex);
}

bool startSyncers(){
    size_t frame_size = context.inHeight*context.inWidth*(hdr?2:1);
    // Allocate shared memory for the frame syncing process.
    uint8_t *read;
    uint8_t *write;

    struct header **headers;

    void **frames;

    // Memory for read and write indexes
    read = (uint8_t *)malloc(cams * sizeof(uint8_t));
    if (!read) {
        LOG_E("Unable to allocate read memory");
        return false;
    }

    write = (uint8_t *)malloc(cams * sizeof(uint8_t));
    if (!write) {
        LOG_E("Unable to allocate write memory");
        return false;
    }

    // Memory for the frames
    frames = (void **)malloc(
            cams * sizeof(uint8_t *));
    if (!frames) {
        LOG_E("Unable to allocate frame array memory");
        return false;
    }

    // Allocate memory for each of the cameras
    for (int i = 0; i < (int)cams; i++) {
        frames[i] = (void *)malloc(
                FRAMESYNCER_BUFFER_COUNT * frame_size);
        if (!frames[i]) {
            LOG_E("Unable to allocate frame %d memory", i);
            return false;
        }
    }

    // Memory for the headers
    headers = (struct header **)malloc(
            cams * sizeof(struct frame *));
    if (!headers) {
        LOG_E("Unable to allocate header array memory");
        return false;
    }

    // Allocate memory for each of the buffers
    for (int i = 0; i < (int)cams; i++) {
        headers[i] = (struct header *)malloc(
                FRAMESYNCER_BUFFER_COUNT * sizeof(struct header));
        if (!headers[i]) {
            LOG_E("Unable to allocate header %d memory", i);
            return false;
        }
        memset((void *)headers[i], 0,
                FRAMESYNCER_BUFFER_COUNT * sizeof(struct header));
    }

    syncer->setFrameRate(fps);
    syncer->setHDRMode(hdr);
    if (!syncer->initialize(read, write, headers, frames)) {
        LOG_E("Unable to initialize the syncer");
        return false;
    }
    for (int i = 0; i < (int)cams; i++) {
        LOG_I("Frame writer %d of %d", i+1, cams);

        writers[i] = new FrameWriter(i, frame_size);
        if (!writers[i]->initialize(&read[i], &write[i], headers[i],
                    frames[i])) {
            LOG_E("Unable to initialize writer %d", i);
            return false;
        }

        LOG_I("Frame size: %ld", frame_size);

        wrappers[i] = new CameraWrapper(streams[i],hdr);
        writers[i]->setInput(wrappers[i]);

        // Try to start the thread
        writers[i]->threadStart();
    }
    LOG_I("Syncer running");
    return true;
}

int main(int argc, char ** argv){
#ifdef LOG_TIMESTAMP
    logging_start_timer();
#endif

    // Define options
    static struct option long_options[] = {
        {"frate",   required_argument, NULL, 'f'},
        {"map",     required_argument, NULL, 'm'},
        {"output",  required_argument, NULL, 'o'},
        {"pipe",    required_argument, NULL, 'c'},
        {"sec",     required_argument, NULL, 's'},
        {"hdr",     no_argument,       NULL, 'd'},
        {"img",     no_argument,       NULL, 'i'},
        {"help",    no_argument,       NULL, 'h'},
        {0,         0,                 NULL,  0 }
    };

    // Get options
    int opt = 0, long_index =0;
    while ((opt = getopt_long(argc, argv, "f:m:do:p:s:ih",
                    long_options, &long_index )) != -1) {
        switch (opt) {
            case 'f' : fps = atoi(optarg);
                       break;
            case 'm' : map_file = optarg;
                       break;
            case 'd' :
                       hdr = 1;
                       break;
            case 'o' : output_folder = optarg;
                       break;
            case 'p' : output_pipe = open(optarg, O_WRONLY);
                       if(output_pipe <= 0)
                           LOG_E("Unable to open output pipe %s", optarg);
                       break;
            case 's' : seconds = atoi(optarg);
                       break;
            case 'i' : save_images_on_termination = true;
                       break;
            case 'h' :
            default  : print_help();
                       exit(EXIT_FAILURE);
        }
    }


    // Set up local modules
    stitcher = new CudaDynamicStitcher();
    if(!stitcher->parseMap(map_file, &context)){
        LOG_E("Unable to parse map file");
        exit(EXIT_FAILURE);
    }
    cams = context.numSources;
    if(CAMERA_MACS.size() != cams){
        LOG_E("Map file reports different number of cameras (%d) from the configuration(%d).",
                cams, (int)CAMERA_MACS.size());
        exit(EXIT_FAILURE);
    }

    cameras = new PylonGigE::PylGigE();
    uploader = new CudaUploader();
    syncer = new FrameSyncer(context.inWidth*context.inHeight, cams);
    wrappers = new CameraWrapper*[cams]();
    writers = new FrameWriter*[cams]();
    converter = new CudaBayerConverter();
    h264enc = new NVENCWrapper();
    hdrModule = new CudaHdr();

    //Set up cameras
    PylonGigE::initializePylon();

    PylonGigE::PylonConfig conf = PylonGigE::PylonConfig();
    conf.width = context.inWidth;
    conf.height = context.inHeight;
    conf.fps = fps * (hdr?2:1);
    conf.exposurePilotHandle = pilot_camera;
    conf.exposureUpdateFrequency = fps*6;
    conf.hdrConf.toggle = hdr;
    conf.hdrConf.targetDark = 50;
    conf.hdrConf.targetLight = 80;
//     conf.exposureUpdateFrequency = fps*2;
    if(!cameras->open(CAMERA_MACS,streams, conf)){
        LOG_E("Failed to open cameras");
        PylonGigE::terminatePylon();
        exit(EXIT_FAILURE);
    }

    if(!startSyncers()){
        cameras->close();
        PylonGigE::terminatePylon();
        exit(EXIT_FAILURE);
    }

    std::string recording_dir = mkRecordingDir(std::string(output_folder));

    syncer->streamStart();

    uploader->init(syncer, context.inWidth*context.inHeight*context.numSources*(hdr?2:1));
    converter->init(uploader, context.inWidth, context.inHeight, context.numSources*(hdr?2:1),
            save_images_on_termination);
    if(hdr){
        hdrModule->init(converter, context.inWidth, context.inHeight,
                context.numSources, false); //Dont use reinhard
        stitcher->init(hdrModule);
    }else{
        stitcher->init(converter);
    }
    if(!h264enc->init(recording_dir, context.outWidth, context.outHeight,
                fps, sec_per_file, stitcher, false, X264_FORMAT_422, output_pipe)) {
        LOG_E("Unable to initialize h264enc");
        cameras->close();
        PylonGigE::terminatePylon();
        exit(EXIT_FAILURE);
    }

    if(!cameras->startStreaming()){
        cameras->close();
        PylonGigE::terminatePylon();
        exit(EXIT_FAILURE);
    }

    printf("Pipeline starting:\n\
            Cameras      :\t %9d\n\
            Frame Rate   :\t %9d\n\
            Input        :\t %4d:%4d\n\
            Output       :\t %4d:%4d\n\
            Output Path  :\t %s \n",
            cams, fps, context.inWidth, context.inHeight,
            context.outWidth, context.outHeight, recording_dir.c_str());

    pthread_mutex_init(&sleepMutex, NULL);
    pthread_cond_init(&sleepCond, NULL);

    pthread_mutex_lock(&sleepMutex);

    //Handle interrupts
    signal(SIGINT, interruptHandler);

    //Figure out how long to sleep
    struct timespec sleepTime;
    if(clock_gettime(CLOCK_REALTIME,&sleepTime)) perror("Clock_gettime");
    sleepTime.tv_sec += seconds;

    /* Sleep untill end of input or CTRL+C */
    pthread_cond_timedwait(&sleepCond, &sleepMutex, &sleepTime);
    pthread_mutex_unlock(&sleepMutex);

    //Reset signal handling in case something hangs
    signal(SIGINT, SIG_DFL);

    //Stop reading frames

    syncer->streamStop();
    cameras->stopStreaming();

    /* sleep to align the multi-thread printouts :P */
    usleep(5000);

    //Terminate
    for (uint32_t i = 0; i < cams; i++) {
        writers[i]->threadStop();
        delete writers[i];
        delete wrappers[i];
    }
    syncer->terminate();
    cameras->close();
    delete cameras;
    delete h264enc;
    delete stitcher;
    delete hdrModule;
    delete converter;
    delete uploader;
    delete syncer;
    delete writers;
    delete wrappers;

    PylonGigE::terminatePylon();
    return EXIT_SUCCESS;
}

