// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once

#include "PylGigE.hpp"

namespace PylonGigE {

    enum HdrState {
        HdrState_UNINITIALIZED,
        HdrState_INITIALIZED,
        HdrState_GET_DARK_GAIN,
        HdrState_GET_DARK_EXPO,
        HdrState_GET_LIGHT_EXPO,
        HdrState_UPDATE_DARK,
        HdrState_UPDATE_LIGHT
    };

    class PylGigEHdrConfig : public PylGigEConfig, public Pylon::CBaslerGigECameraEventHandler {
        public:
            PylGigEHdrConfig(PylGigE * master, const PylonConfig &conf) :
                PylGigEConfig(master,conf), _isOpen(false) { }
            ~PylGigEHdrConfig();
            bool beginExposureLoop();
            bool setConfig();
            void checkUpdateExposure(Pylon::CGrabResultPtr ptr);
            void setExposure(float newExposureValue, float newGainValue);
            void OnCameraEvent( Pylon::CBaslerGigEInstantCamera& camera,
                    intptr_t userProvidedId, GenApi::INode* pNode);
            void addExposure(PylonFrame *f);
            void reconnect();

            //Internal thread-loop
            void exposureLoop(int idx);
            void exposureUpdateLoop();
        private:

            friend class PylGigE;

            //Current status variables
            bool _isOpen;
            int _currentIdx;
            float _currentExposure[2];
            HdrState _pilotState;

            AreaOfInterest _aois[2];
            double _autoTarget[2];

            pthread_t * _threads;
            pthread_mutex_t * _sleepMutex;
            pthread_cond_t * _sleepCond;
            int * _frameCounter;
            int _framesUntillNextExposureUpdate;

            pthread_t _exposureSleepThread;
            pthread_mutex_t _exposureSleepMutex;
            pthread_cond_t _exposureSleepCond;
            int _exposureNewIdx;

            bool applyConfig(size_t idx);
            bool setAutoFunctionRegion(AreaOfInterest &aoi, Pylon::CBaslerGigEInstantCamera &cam,
                    bool first);
            bool checkValidAoi(AreaOfInterest &aoi1, AreaOfInterest &aoi2);
            double getAverageGrayscale(Pylon::CGrabResultPtr image, int aoi);

    };//end PylGigEHdrConfig

};//end namespace

