// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "PylGigEBasicConfig.hpp"

using namespace PylonGigE;
using namespace Pylon;
using namespace Basler_GigECamera;
using namespace Basler_GigECameraParams;

/* ------------------- Constants --------------------------- */
const float gainMaximumUpperLimit = 200;//dB, or a derivative of dB at least
const int exposureSafeMarginUsec = 5000; 
const int exposureSafeMarginHdrUsec = 4000;
const int cameraHeartbeatInterval = 10000;//ms
const float defaultMaximumFramerate = 54.0f; 

/* ------------- Public member functions ------------------- */

PylGigEBasicConfig::~PylGigEBasicConfig(){
    if(!_isOpen || _pilot == -1) return;

    pthread_mutex_lock(&_exposureSleepMutex);
    _isOpen = false;
    pthread_cond_signal(&_exposureSleepCond);
    pthread_mutex_unlock(&_exposureSleepMutex);
    pthread_join(_expoThread, NULL);
    pthread_mutex_destroy(&_exposureSleepMutex);
    pthread_cond_destroy(&_exposureSleepCond);
}

bool PylGigEBasicConfig::beginExposureLoop(){
    _currentExposure = 5000;//Default values, wont be used.
    _currentGain = 50;
    _isOpen = true;
    if(_pilot != -1){
        pthread_mutex_init(&_exposureSleepMutex, NULL);
        pthread_cond_init(&_exposureSleepCond, NULL);
        _framesUntillNextExposureUpdate = 10;
        pthread_create(&_expoThread, NULL, exposureLoopStarterBasic, (void *) this);
    }
    return true;
}

bool PylGigEBasicConfig::setConfig(){
    for(size_t i=0; i<_size; ++i)
        if( !applyConfig(i) ) return false;
    return true;
}

void PylGigEBasicConfig::checkUpdateExposure(CGrabResultPtr){
	--_framesUntillNextExposureUpdate;
	if(_framesUntillNextExposureUpdate == 0){
		try{
			_gige->_cameras[_pilot].ExposureAuto = ExposureAuto_Once;
			_gige->_cameras[_pilot].GainSelector = GainSelector_All;
			_gige->_cameras[_pilot].GainAuto = GainAuto_Once;
		}catch(GenICam::GenericException &ex){
			LOG_W("Setting auto exposure failed: %s", ex.GetDescription());
			_framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
		}
	}else if(_framesUntillNextExposureUpdate < -6){
		//It can nearly always reach the correct value within 6 frames, however,
		//there may be some fluctuations that prolong it. Lets cancel the auto now
		try{
			_gige->_cameras[_pilot].ExposureAuto = ExposureAuto_Off;
			_gige->_cameras[_pilot].GainAuto = GainAuto_Off;
			_framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
			_currentExposure = _gige->_cameras[_pilot].ExposureTimeAbs();
			_currentGain = _gige->_cameras[_pilot].GainRaw();
			pthread_cond_signal(&_exposureSleepCond);
		}catch(GenICam::GenericException &ex){
			LOG_W("Reading new exposure failed: %s", ex.GetDescription());
			_framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
		}
	}else if(_framesUntillNextExposureUpdate < 0){
		try{
			ExposureAutoEnums eEnum = _gige->_cameras[_pilot].ExposureAuto();
			GainAutoEnums gEnum = _gige->_cameras[_pilot].GainAuto();
			if(eEnum != ExposureAuto_Once && gEnum != GainAuto_Once){
				_framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
				_currentExposure = _gige->_cameras[_pilot].ExposureTimeAbs();
				_currentGain = _gige->_cameras[_pilot].GainRaw();
				pthread_cond_signal(&_exposureSleepCond);
			}
		}catch(GenICam::GenericException &ex){
			LOG_W("Reading new exposure failed: %s", ex.GetDescription());
			_framesUntillNextExposureUpdate = _configuration.exposureUpdateFrequency;
		}
	}

}

void PylGigEBasicConfig::setExposure(float newExposureValue, float newGainValue){
	if(!_gige->_cameras.IsGrabbing()) return;
	LOG_D("Updated exposure and gain: [%.1f : %.1f]",newExposureValue,newGainValue);
	try{
		for(size_t i=0; i<_size; ++i){
            if((int)i != _pilot){
                //These 2 calls take up to 8ms each, per camera.
                //Up to 400 usec of active CPU time each
                //A lot faster if the value is the same as previous (caching)
                if(GenApi::IsWritable(_gige->_cameras[i].ExposureTimeAbs)){
                    _gige->_cameras[i].ExposureTimeAbs = newExposureValue;
                }
                if(GenApi::IsWritable(_gige->_cameras[i].GainRaw)){
                    _gige->_cameras[i].GainRaw = newGainValue;
                }
            }
		}
	}catch(GenICam::GenericException &ex){
		//Some exceptions may occur naturally every now and again due to multiple threads.
        //If another thread changes the camera state while this function is running, i.e., stops
		//the stream, an exception is thrown. This should be fairly rare, unless the pilot exposure
		//update frequency is really low (<1sek). Continuous warnings indicate a problem
		LOG_W("Set exposure value caused exception: %s", ex.GetDescription());
	}
}

void PylGigEBasicConfig::addExposure(PylonFrame *f){
    f->darkExposure = false; //Not set
    f->exposureTime = (uint32_t) _currentExposure;
}

/* -------------------- Shared config ---------------------- */
bool PylGigEConfig::applySharedConfig(size_t idx){

	CBaslerGigEInstantCamera &cam = _gige->_cameras[idx];

	//Set camera dimensions
	int w = _configuration.width;
	int h = _configuration.height;
	try{
		if(GenApi::IsReadable(cam.WidthMax) && GenApi::IsReadable(cam.HeightMax)){
			if(w == 0 || w > cam.WidthMax()){
				w = cam.WidthMax();
				if(_configuration.width){
					LOG_W("Too large width (%d) for camera %d. Using max (%d)",
							_configuration.width, (int)idx, w);
				}
			}
			if(h == 0 || h > cam.HeightMax()){
				h = cam.HeightMax();
				if(_configuration.height){
					LOG_W("Too large height (%d) for camera %d. Using max (%d)",
							_configuration.height, (int)idx, w);
				}
			}
		}else if(h == 0 || w == 0){
			//This should always be readable, but just in case
			LOG_E("Unable to read the camera dimensions. Cannot auto configure!");
			return false;
		}
		cam.Width = w;
		cam.Height = h;
	}catch(GenICam::GenericException &ex){
		LOG_E("Failed to configure camera %d dimensions: %s", (int)idx, ex.GetDescription());
		return false;
	}
	
	//Set pixel format
	try{
		cam.PixelFormat.FromString(_configuration.pixelFormat.c_str());
	}catch(GenICam::GenericException &ex){
		LOG_E("Pixel format '%s' not supported by device", _configuration.pixelFormat.c_str());
		std::cout << "\nListing valid pixel formats:\n";
		GenApi::NodeList_t fmtNodes;
		cam.PixelFormat.GetEntries(fmtNodes);
		for(GenApi::NodeList_t::const_iterator it = fmtNodes.begin();
				it != fmtNodes.end(); ++it){
			if(GenApi::IsReadable(*it) && GenApi::IsImplemented(*it)){
				std::string name = (*it)->GetName().c_str();
				//Strip away "EnumEntry_PixelFormat_" beginning and print
				std::cout << name.substr(22, std::string::npos) << std::endl;
			}
		}
		std::cout << std::endl;
		return false;
	}
	
	//Activate extra information appended to each frame
	try{
		// enable chunk mode
		cam.ChunkModeActive = true;

		// Enable time stamp chunks
		cam.ChunkSelector = ChunkSelector_Timestamp;
		cam.ChunkEnable = true;

		// Enable frame counter chunks
		cam.ChunkSelector = ChunkSelector_Framecounter;
		cam.ChunkEnable = true;

	}catch(GenICam::GenericException &ex){
		LOG_W("Exception occurred setting chunk mode. Camera may not support it, or be in "
				"an invalid state. Exception: %s", ex.GetDescription());
	}

	//Failure handling, camera timeout
	try{
		cam.GevHeartbeatTimeout = cameraHeartbeatInterval; 
		//This behavior is odd. Cameras always, consistently, time out at heartbeat-timeout
		//no matter what duration it is, unless we send messages to the device.
		//Even if we just want to stream continuously, without manually changing config, we
		//still have to read/write some value from the device. F ex, it works when
		//continually reading timestamp from the camera.
		//Another solution is to set the timeout to maximum, 1-2 months.
		//This could *potentially* be a problem if application crashes
		//and cameras cannot be restarted (i.e. remote use).
		//cam.GevHeartbeatTimeout = cam.GevHeartbeatTimeout.GetMax();
	}catch(GenICam::GenericException &ex){
		LOG_W("Failed to set heartbeat interval. Exception: %s", ex.GetDescription());
	}

    int mtu;
    std::string ifaceName;
    getIfaceInfo(_gige->_camDevInfo[idx], mtu, ifaceName);
    if(mtu < 8000){
        LOG_D("MTU on interface %s is only %d, jumbo frames (8192) is recommended for optimal transfer",
                ifaceName.c_str(), mtu);
    }

	//Control the transport ethernet link
	try{
		cam.GevStreamChannelSelector = GevStreamChannelSelector_StreamChannel0;
		cam.GevSCPSDoNotFragment = true;
		cam.GevSCPSPacketSize = mtu; //Activate jumbo frames if interface supports it
		cam.GevSCPD = 0; // inter packet delay (in ticks of 8ns)
		cam.GevSCFTD = 0; // frame transmission delay (in ticks of 8ns)
		cam.GevSCBWR = 3; // reserve 3% of the bandwith for control
		cam.GevSCBWRA = 1; 
	}catch(GenICam::GenericException &ex){
		LOG_W("Exception when configuring ethernet link: %s", ex.GetDescription());
		//Not necessarily fatal, but can limit framerate
	}
	
	//Set lighting and exposure configuration
	try{
		
		//Want to reset previous white balance, may be completely wonky and tends to stick
		//even if you turn white balance off
		if(GenApi::IsWritable(cam.BalanceWhiteAuto)){
            cam.BalanceWhiteAuto = BalanceWhiteAuto_Off;
        }//else, white balance not supported
		if(GenApi::IsWritable(cam.BalanceWhiteReset)){
            cam.BalanceWhiteReset();
        }
		
		cam.GainSelector = GainSelector_All;
        //Determines how much we can change the image from frame to frame during auto-exposure
		if(GenApi::IsWritable(cam.GrayValueAdjustmentDampingAbs)){
            cam.GrayValueAdjustmentDampingAbs =	cam.GrayValueAdjustmentDampingAbs.GetMax();
        }
        cam.GainAuto = GainAuto_Off;
        cam.ExposureAuto = ExposureAuto_Off;
        //Set some basic values that should be overwritten soon 
        if(_pilot == (int)idx) cam.GainRaw = _currentGain = cam.GainRaw.GetMin();
        else cam.GainRaw = cam.GainRaw.GetMin();
        cam.ExposureTimeAbs = 5000; //usec

		if(GenApi::IsWritable(cam.LightSourceSelector)){
            if(_configuration.whiteBalanceOn){
                if(_configuration.indoorLighting){
                    //Florescent lights only
                    cam.LightSourceSelector = LightSourceSelector_Tungsten;
                }else if(_configuration.outsideDaylight){
                    //Suitable for regular sunlight
                    cam.LightSourceSelector = LightSourceSelector_Daylight6500K;
                }else{
                    //Suitable for Stadium spotlights / late evening / shadow
                    cam.LightSourceSelector = LightSourceSelector_Daylight;
                }
            }else{
                cam.LightSourceSelector = LightSourceSelector_Off;
            }
        }

		if(GenApi::IsWritable(cam.GammaEnable)){
			cam.GammaEnable = false;
		}

		if(GenApi::IsWritable(cam.ColorAdjustmentEnable)){
			cam.ColorAdjustmentEnable = false;
		}

		if(GenApi::IsWritable(cam.ColorTransformationMatrixFactor)){
			//When turned up, this over-intensifies colors to the extreme
            //However, documentation seems off here. GetMin returns 0, which should mean that
            //the white balance is nulled away, but this isn't the case. Strange
			cam.ColorTransformationMatrixFactor = cam.ColorTransformationMatrixFactor.GetMin();
		}


		if(GenApi::IsWritable(cam.ProcessedRawEnable)){
			//Should be on when reading raw bayer, otherwise it wont be writable anyways
			cam.ProcessedRawEnable = true;
		}

        cam.ExposureMode = ExposureMode_Timed;

	}catch(GenICam::GenericException &ex){
		LOG_E("Exception occurred when configuring exposure: %s", ex.GetDescription());
		return false;
	}

	//Set framerate
	float activeFps = 1.0f;
	try{
		cam.AcquisitionMode = AcquisitionMode_Continuous;

		if((_configuration.hdrConf.toggle || !_configuration.triggerMode)
                && _configuration.fps != 0){
			cam.AcquisitionFrameRateEnable = true;
            cam.AcquisitionFrameRateAbs = _configuration.fps;
		}else{
			cam.AcquisitionFrameRateEnable = false;
        }

        if(_configuration.fps == 0){
            //This only affects the exposure duration, but we need a high default for this.
            //If the cameras were using the triggerbox last time, they believe that the maximum
            //possible frame rate is the old triggersignal, even if that was really low. So, we
            //can't just ask the camera what it's maximum frame rate is.
            activeFps = defaultMaximumFramerate;
        }else{
            activeFps = _configuration.fps;
        }
	}catch(GenICam::GenericException &ex){
		LOG_E("Exception occured while setting framerate: %s", ex.GetDescription());
		return false;
	}
   
	//Determine auto function region of interest
    if((int)idx == _pilot ||
            (_configuration.exposureContinuousAuto && !_configuration.hdrConf.toggle)){
        try{
            cam.AutoTargetValue = _configuration.autoTargetValue; 
            //Set a maximum limit to the exposure time. We don't want to drop frames because the
            //exposure takes too long. Then we rather wish to increase gain-value
            float maxUpperLimit = cam.AutoExposureTimeAbsUpperLimit.GetMax();
            float upperLimitExposure = std::min(maxUpperLimit,
                    (1000000/activeFps) - 
                    (_configuration.hdrConf.toggle ? exposureSafeMarginHdrUsec : exposureSafeMarginUsec));
            cam.AutoExposureTimeAbsUpperLimit = upperLimitExposure;

            //We also wish to limit the gain value. If things get really dark, the image will go to shits
            //anyways, and the image quality will be so poor that we no longer care. Very high gain may cause
            //very very challenging images to encode (noise), making it better to have a too dark scene.
            //But at least it doesn't fuck over the rest of the system.
            //This doesn't affect regular lighted conditions (daylight / spotlights)
            cam.AutoGainRawUpperLimit = gainMaximumUpperLimit;

            cam.AutoFunctionAOISelector = AutoFunctionAOISelector_AOI1;
            if(GenApi::IsWritable(cam.AutoFunctionAOIUsageIntensity)){
                cam.AutoFunctionAOIUsageIntensity = true;
            }
            if(GenApi::IsWritable(cam.AutoFunctionAOIUsageWhiteBalance)){
                cam.AutoFunctionAOIUsageWhiteBalance = true;
            }

            //Turn this one off since we wont use it
            cam.AutoFunctionAOISelector = AutoFunctionAOISelector_AOI2;
            if(GenApi::IsWritable(cam.AutoFunctionAOIUsageIntensity)){
                cam.AutoFunctionAOIUsageIntensity = false;
            }
            if(GenApi::IsWritable(cam.AutoFunctionAOIUsageWhiteBalance)){
                cam.AutoFunctionAOIUsageWhiteBalance = false;
            }
        }catch(GenICam::GenericException &ex){
            LOG_E("Exception occurred when configuring exposure AOI: %s", ex.GetDescription());
            return false;
        }
    }

    return true;
}

void PylGigEConfig::adjustWhitebalance(size_t idx, float r, float g, float b){
    CBaslerGigEInstantCamera &cam = _gige->_cameras[idx];

    //Won't be writable if white balance is turned off f ex
    if(GenApi::IsWritable(cam.BalanceRatioSelector)){
        try{
            cam.BalanceRatioSelector = BalanceRatioSelector_Red;
            cam.BalanceRatioAbs = cam.BalanceRatioAbs() * r;
            cam.BalanceRatioSelector = BalanceRatioSelector_Green;
            cam.BalanceRatioAbs = cam.BalanceRatioAbs() * g;
            cam.BalanceRatioSelector = BalanceRatioSelector_Blue;
            cam.BalanceRatioAbs = cam.BalanceRatioAbs() * b;
        }catch(GenICam::GenericException &ex){
            LOG_E("Exception occurred when adjusting whitebalance live: %s", ex.GetDescription());
        }
    }else{
        LOG_W("Attempting live white balance adjustment, but WhiteBalance is not writable");
    }
}


/* ------------- Private member functions ------------------ */

void PylGigEBasicConfig::exposureLoop(){

	pthread_mutex_lock(&_exposureSleepMutex);
	while(true){
		if(!_isOpen) break;
		pthread_cond_wait(&_exposureSleepCond, &_exposureSleepMutex);
		float cE;
		float cG;
		if(!_isOpen) break;
        cE = _currentExposure;
        cG = _currentGain;
		setExposure(cE, cG);
		if(_configuration.exposureCallback){
			_configuration.exposureCallback(cE,cG);
		}
	}
	pthread_mutex_unlock(&_exposureSleepMutex);

}

bool PylGigEBasicConfig::applyConfig(size_t idx){

    if(!applySharedConfig(idx)) return false;

	CBaslerGigEInstantCamera &cam = _gige->_cameras[idx];

	//Set triggermode
	try{
		if(GenApi::IsWritable(cam.TriggerSelector)){
			if(_configuration.triggerMode){
				//Triggerbox is now king. We allow the camera to start the
				//acquisition of a new frame that the rising edge of the trigger signal,
				//and start the capture of the frame at the falling edge. No other frame rate
				//that interferes with frame capture, except if the triggerbox is running
				//faster than the gigabit link allows.
				cam.TriggerSelector = TriggerSelector_AcquisitionStart;
				cam.TriggerMode = TriggerMode_On;
				cam.TriggerSource = TriggerSource_Line1;
				cam.TriggerActivation = TriggerActivation_RisingEdge;
				cam.TriggerSelector = TriggerSelector_FrameStart;
				cam.TriggerMode = TriggerMode_On;
				cam.TriggerSource = TriggerSource_Line1;
				cam.TriggerActivation = TriggerActivation_FallingEdge;
			}else{
				cam.TriggerSelector = TriggerSelector_AcquisitionStart;
				cam.TriggerMode = TriggerMode_Off;
				cam.TriggerSelector = TriggerSelector_FrameStart;
				cam.TriggerMode = TriggerMode_Off;
			}
		}else{
			if(_configuration.triggerMode){
				LOG_E("TriggerSelector not writable. Cannot control cameras triggermodus");
				return false;
			}
		}
	}catch(GenICam::GenericException &ex){
		LOG_E("Failed to set triggermode. Exception: %s", ex.GetDescription());
		return false;
	}

	//Determine auto function region of interest
    if((int)idx == _pilot || _configuration.exposureContinuousAuto){
        try{
            cam.AutoFunctionAOISelector = AutoFunctionAOISelector_AOI1;

            AreaOfInterest &aoi = _configuration.autoUpdateRegion;
            if(aoi.centerX + (aoi.scaleX*0.5f) > 0.99f ||
                    aoi.centerX - (aoi.scaleX*0.5f) < 0.01f ||
                    aoi.centerY + (aoi.scaleY*0.5f) > 0.99f ||
                    aoi.centerY - (aoi.scaleY*0.5f) < 0.01f){
                LOG_E("Auto update region is outside image boundaries\n");
                return false;
            }
            int aoiW = (int) (_configuration.width * aoi.scaleX);
            int aoiH = (int) (_configuration.height * aoi.scaleY);
            int aoiOx = (int) (_configuration.width * aoi.centerX - (aoiW/2));
            int aoiOy = (int) (_configuration.height * aoi.centerY - (aoiH/2));
            cam.AutoFunctionAOIOffsetX = 0;//Reset it to top-left corner before trying to set dim
            cam.AutoFunctionAOIOffsetY = 0;
            cam.AutoFunctionAOIWidth = aoiW;
            cam.AutoFunctionAOIHeight = aoiH;
            cam.AutoFunctionAOIOffsetX = aoiOx;
            cam.AutoFunctionAOIOffsetY = aoiOy;

        }catch(GenICam::GenericException &ex){
            LOG_E("Exception occurred when configuring exposure AOI: %s", ex.GetDescription());
            return false;
        }
    }

    return true;
}

/* ------------------- Helper functions --------------- */

void * PylonGigE::exposureLoopStarterBasic(void * param){
	((PylGigEBasicConfig *)param)->exposureLoop();
	pthread_exit(0);
	return NULL;
}



