// AUTHOR(s): Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#ifdef HAVE_PG

#include <stdbool.h>
#include <time.h>
#include <stdlib.h>

#include "schedule.h"
#include "logging.h"
#include "config.h"

#include <string.h>
#include <libpq-fe.h>
#include <signal.h>

#define MAX_TIMEOUT 60 // Update every minute

struct schedule
{
    uint32_t current_id;
    struct event *e;
    struct event *sig;
    char *filepath;

    schedule_filepath_cb path_cb;
    void *path_cb_arg;

    schedule_record_cb rec_cb;
    void *rec_cb_arg;
};

//
// ### Event callbacks
//

//
// Schedule callback called whenever a timeout occures or
// a SIGHUP signal is received.
//
void schedule_callback(evutil_socket_t fd, short events, void *ctx)
{
    schedule_t *s = ctx;
    schedule_update(s);
}

//
// ### Updating
//

//
// Check the database and start or stop recoring
//
void schedule_update(schedule_t *s) {
    // Timeout until next callback
    struct timeval tv = {MAX_TIMEOUT, 0};
    uint32_t id;

    // Connect to the database
    /*
       PGconn *conn = PQconnectdb("postgresql://"PG_USER":\""PG_PASS"\"@"
       PG_HOST":"PG_PORT"/"PG_DB);
       */
    PGconn *conn = PQconnectdb(
            "host="PG_HOST" "
            "port="PG_PORT" "
            "dbname="PG_DB" "
            "user="PG_USER" "
            "password="PG_PASS);

    if (!conn) {
        LOG_E("No database connection returned\n");

        goto fail;
    } else if (PQstatus(conn) != CONNECTION_OK) {
        LOG_E("Unable to connect to database: %s", PQerrorMessage(conn));

        // Close the postgres connection
        PQfinish(conn);

        goto fail;
    }

    // Query the database for the next scheduled event
    PGresult *res = PQexec(conn, "SELECT match_id::int4, "
            "start::TIMESTAMP WITH TIME ZONE AT TIME ZONE 'UTC' as start, "
            "stop::TIMESTAMP WITH TIME ZONE AT TIME ZONE 'UTC' AS stop, "
            "path FROM " PG_TABLE " ORDER BY start LIMIT 1;");

    if (!res) {
        LOG_E("No result returned from database");

        // Close the postgres connection
        PQfinish(conn);

        goto fail;

    } else if (PQresultStatus(res) != PGRES_TUPLES_OK) {

        LOG_E("Unable to query database: %s", PQresultErrorMessage(res));

        // Close the postgres connection
        PQfinish(conn);

        goto fail;
    }

    // If we found any results, we must start recording, if we're not already
    // recording
    if (PQntuples(res) > 0) {
        char *tmp;
        struct tm tm;
        time_t start, stop;

        // Get the current time
        time_t now = time(NULL);

        // Get ID of the current recoring
        int col = PQfnumber(res, "match_id");
        id = ntohl(*((uint32_t *)PQgetvalue(res, 0, col)));

        // Get and parse the start time
        col = PQfnumber(res, "start");
        tmp = PQgetvalue(res, 0, col);

        strptime(tmp, "%Y-%m-%d %H:%M:%S", &tm);
        start = timegm(&tm);

        // Get and parse the stop time
        col = PQfnumber(res, "stop");

        if (PQgetisnull(res, 0, col)) {
            // If no stop time is specified, use any timestamp
            // after the next timeout, for simplicity and to
            // be sure, we use the max timeout times 2
            stop = now + MAX_TIMEOUT * 2;
        } else {
            tmp = PQgetvalue(res, 0, col);
            strptime(tmp, "%Y-%m-%d %H:%M:%S", &tm);
            stop = timegm(&tm);
        }

        // If the ID has changed, we need to update the output folder
        if (s->current_id != id || s->filepath == NULL) {
            col = PQfnumber(res, "path");
            if (PQgetisnull(res, 0, col)) {

                // If no path is set, we reset by calling the callback with
                // `NULL` as argument
                if (s->path_cb) s->path_cb(s, NULL, s->path_cb_arg);

            } else {
                tmp = PQgetvalue(res, 0, col);

                size_t len = strnlen(tmp, 1024);

                if (len > 0) {
                    // Allocate a string and copy the path
                    char *path = malloc(len);
                    strncpy(path, tmp, len-1);
                    path[len-1] = 0;

                    // Call the filepath callback (if set)
                    if (s->path_cb) s->path_cb(s, path, s->path_cb_arg);

                    // Store the path
                    if (s->filepath != NULL) {
                        free(s->filepath);
                    }
                    s->filepath = path;

                } else {
                    if (s->path_cb) s->path_cb(s, NULL, s->path_cb_arg);
                }
            }

            if (s->filepath == NULL) s->filepath = "";

            s->current_id = id;
        }

        // We have now used all we needed from the database, and it's OK to
        // free all memmory allocated
        PQclear(res);

        // Set the timeout
        time_t delay = start - now;
        LOG_D("Next event is in %ld seconds", delay);
        //LOG_D("Stop is in %ld seconds", stop - now);
        if (delay <= 0) {
            // Start recording
            if (s->rec_cb) {
                //LOG_D("Starting recording");
                s->rec_cb(s, true, s->rec_cb_arg);
            } else {
                LOG_D("No recording callback");
            }

            // Change delay
            delay = stop - now;
        } else {
            // Stop recording
            if (s->rec_cb) {
                //LOG_D("Stopping recording");
                s->rec_cb(s, false, s->rec_cb_arg);
            } else {
                LOG_D("Not stopping, no callback");
            }
        }
        tv.tv_sec = (delay < MAX_TIMEOUT ? delay : MAX_TIMEOUT);
        tv.tv_usec = 0;
    } else {
        LOG_D("No more events in the database");

        // No recoring scheduled, stop
        if (s->rec_cb) s->rec_cb(s, false, s->rec_cb_arg);

        // Set next callback in 30 minutes
        tv.tv_sec = MAX_TIMEOUT;
        tv.tv_usec = 0;
    }

    // Close the postgres connection
    PQfinish(conn);

    //LOG_D("Next timeout in %ld seconds", tv.tv_sec);

fail:

    // Schedule next callback
    event_add(s->e, &tv);
}

//
// ### Initialization
//

//
// Initialize a new schedule hander
//
schedule_t *schedule_init(struct event_base *base)
{
    schedule_t *s = malloc(sizeof(schedule_t));
    if (!s) {
        LOG_E("Unable to allocate memory fot the schedule.");
        return NULL;
    }

    s->path_cb = NULL;
    s->rec_cb = NULL;
    s->current_id = 0;
    s->filepath = NULL;

    // Create an event for the timeout
    s->e = event_new(base, -1, 0, schedule_callback, s);

    // Handle SIGHUP to refresh from the database
    s->sig = evsignal_new(base, SIGHUP, schedule_callback, s);

    // Schedule an imidiate schedule update
    struct timeval tv = {0,0};
    event_add(s->e, &tv);

    // Listen to the signal
    evsignal_add(s->sig, NULL);

    return s;
}

//
// ### Callbacks
//

//
// Add a callback to be called whenever a recording should
// be started or stopped.
//
void schedule_add_record_cb(schedule_t *s, schedule_record_cb cb, void *ctx)
{
    s->rec_cb = cb;
    s->rec_cb_arg = ctx;
}

//
// Add a callback to be called whenever the output path of
// the current recording should be changed.
//
void schedule_add_filepath_cb(schedule_t *s, schedule_filepath_cb cb, void *ctx)
{
    s->path_cb = cb;
    s->path_cb_arg = ctx;
}

#endif // HAVE_PG
