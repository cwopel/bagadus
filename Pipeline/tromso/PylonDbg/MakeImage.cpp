// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <cstdlib>
#include <vector>
#include <string>
#include <iostream>
#include <iomanip>
#include <pthread.h>
#include <getopt.h>

#include <PylGigE.hpp>
#include <logging.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

const int seconds = 6; //Give a little time for decent auto-exposure
const int imgDimX = 2040;
const int imgDimY = 1080;

void saveImg(int id, uint8_t * data, int width, int height);

std::vector<PylonGigE::PylonStream*> streams;

void * readingThread(void * s){
	int * ptr = (int*) s;
	int idx = *ptr;
	delete ptr;
	PylonGigE::PylonStream * stream = streams[idx];

	PylonGigE::PylonFrame * cur = NULL;
	PylonGigE::PylonFrame * prev = NULL;
	while( (cur = stream->getFrame()) != NULL){
		if(prev) delete prev;
		prev = cur;
	}
	if(!prev){
		LOG_E("Unable to fetch any frame from the camera stream");
	}else{
		LOG_D("Writing image %d : timestamp %8ld : exposure time %6d\n",
                idx, prev->timeStamp.tv_usec, prev->exposureTime);
		saveImg(idx, prev->pixels, imgDimX, imgDimY);
	}

	pthread_exit(0);
	return NULL;
}

void print_help(){
    printf("Usage:\n");
    printf("-t    Set auto target gray value (50-200)\n");
    printf("-h                             Print help\n");
}

int main(int argc, char ** argv){

    // Define options
    static struct option long_options[] = {
        {"target grayvalue", required_argument, NULL, 't'},
        {"help            ",       no_argument, NULL, 'h'},
        {0                 ,                 0, NULL,  0 }
    };

    int targetValue = -1;

    // Get options
    int opt = 0, long_index =0;
	while ((opt = getopt_long(argc, argv, "t:h",
					long_options, &long_index )) != -1) {
		switch (opt) {
			case 't' : targetValue = atoi(optarg);
					   break;
			case 'h' :
			default  : print_help();
					   exit(EXIT_FAILURE);
		}
	}

	PylonGigE::initializePylon();
	PylonGigE::printAvailableDevices();
	{
		PylonGigE::PylGigE cams;
		PylonGigE::PylonConfig conf;//Default parameters set
		conf.fps = 25; //Just a guess, it may be lower, but that doesnt matter
		conf.triggerMode = true;
        conf.exposureUpdateFrequency = 25;
		conf.exposureContinuousAuto = false;
        if(targetValue != -1)
            conf.autoTargetValue = targetValue;
		conf.pixelFormat = "YUV422_YUYV_Packed";
		conf.width = imgDimX;
		conf.height = imgDimY;
		if(!cams.open(streams, conf)){
			LOG_E("Open cameras failed");
			PylonGigE::terminatePylon();
			return EXIT_FAILURE;
		}
		pthread_t threads[streams.size()];
		for(size_t i=0; i<streams.size(); ++i){
			pthread_create(&threads[i], NULL, readingThread, (void*) new int(i));
		}
		if(!cams.startStreaming()){
			LOG_E("Start Streaming failed");
			cams.close();
			PylonGigE::terminatePylon();
			return EXIT_FAILURE;
		}

		//Sleep for a few seconds to give decent exposure
		for(int st=seconds; st > 0; st = sleep(st));

		for(size_t i=0; i<streams.size(); ++i){
			streams[i]->terminateStream();
		}
		for(size_t i=0; i<streams.size(); ++i){
			pthread_join(threads[i], NULL);
		}

		cams.stopStreaming();
		cams.close();
	}
	PylonGigE::terminatePylon();
	return EXIT_SUCCESS;
}

void saveImg(int id, uint8_t * input, int width, int height){
	using namespace cv;
	Mat img(height, width, CV_8UC3);

	for(int y = 0; y<height; y++){
		for(int x = 0; x<width; x++){
			double yVal = input[y*width*2 + x*2];
			double crVal;
			double cbVal;
			if(x % 2){
				cbVal = input[y*width*2 + x*2 - 1] - 128;
				crVal = input[y*width*2 + x*2 + 1] - 128;
			}else{
				cbVal = input[y*width*2 + x*2 + 1] - 128;
				crVal = input[y*width*2 + x*2 + 3] - 128;
			}
			/* See http://www.fourcc.org/fccyvrgb.php JFIF Clarification
			 * for reasoning / constants */
			int b = (int) (yVal + 1.772 * cbVal);
			int g = (int) ((yVal - (0.34414 * cbVal)) - 0.71414 * crVal);
			int r = (int) (yVal + 1.402 * crVal);
			img.at<Vec3b>(y,x)[0] = (char) (b<0 ? 0 : (b>255 ? 255 : b));
			img.at<Vec3b>(y,x)[1] = (char) (g<0 ? 0 : (g>255 ? 255 : g));
			img.at<Vec3b>(y,x)[2] = (char) (r<0 ? 0 : (r>255 ? 255 : r));
		}
	}
	std::ostringstream n;
	n << id;
	n << ".png";
	imwrite(n.str(), img);
}
