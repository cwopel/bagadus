/*
 *
 * SAMPLE CONFIGURATION FILE
 *
 * You should copy this file to config.h and change the
 * variables as needed.
 *
 * TODO
 *  - Make proper config file
 */

#ifndef config_h
#define config_h

//
// ### Processor configuration
//

// Remember to change this if you add or remove something,
// and you also need to update this in the main.cpp file
#define PROCESSOR_CONFIG_VERSION 7

#define PROCESSOR_PORT      1357
#define PROCESSOR_ADAPTER   0
#define PROCESSOR_SECONDS   0
#define PROCESSOR_BASE_EXPOSURE  64
#define PROCESSOR_FPS       30
#define PROCESSOR_HDR       0
#define PROCESSOR_OUTPUT_FOLDER "./bagadus_recordings/"
#define PROCESSOR_MAP_FILE "map5Dynamic.data"
#define PROCESSOR_OUTPUT_PIPE (-1)
#define PROCESSOR_SEC_PER_FILE 3

// New in version 2
#define PROCESSOR_CLIENT_COUNT 2

// New in version 4
#define PROCESSOR_PILOT_CAM 2

// PostgreSQL connection, new in version 5
#define PG_USER  "username"
#define PG_PASS  "password"
#define PG_HOST  "hostname"
#define PG_PORT  "5432"
#define PG_DB    "bagadussi"
#define PG_TABLE "schedule"

//
// ### Recorder configuration
//

// Remember to change this if you add or remove something,
// and you also need to update this in the main.cpp file
#define RECORDER_CONFIG_VERSION 3

// Communications setup
#define RECORDER_HOSTNAME "127.0.0.1"
#define RECORDER_PORT 2222
#define RECORDER_ADAPTER 0
#define RECORDER_NODE 0

// Camera setup
#define RECORDER_NUM_CAMS 2
#define RECORDER_CAM_IDS {1,2}
#define RECORDER_CAM_MACS { \
    "003053138FBA", \
    "00305314BC75"}

#define FRAMESYNCER_BUFFER_COUNT 3

//New in version 3

// #define RECORDER_EXPOSURE_ROI {0.33f, 0.5f, 0.5f, 0.9f} // Ullevaal
#define RECORDER_EXPOSURE_ROI {0.6f, 0.5f, 0.6f, 0.9f} // Alfheim

#endif
