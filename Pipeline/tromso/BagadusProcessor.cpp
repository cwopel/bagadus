// AUTHOR(s): Ragnar Langseth, Sigurd Ljodal,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <iomanip>
#include <iostream>
#include <sstream>

#include <sisci_api.h>

#include <event2/event.h>

#include "config.h"
#if PROCESSOR_CONFIG_VERSION < 7
#error Outdated configuration, please se config_sample.h and update your file.
#endif

#include "Helpers/logging.h"
#include "Helpers/server.h"

#ifdef HAVE_PG
#include "Helpers/schedule.h"
#endif

#include "Modules/FrameSyncer.hpp"
#include "Modules/NVENCWrapper.hpp"
#include "Modules/CudaUploader.hpp"
#include "Modules/CudaHdr.hpp"
#include "Modules/CudaBayerConverter.hpp"
#include "Modules/CudaDynamicStitcher.hpp"

// Static strings
static char str_output_folder[] = PROCESSOR_OUTPUT_FOLDER;
static char str_map_file[] =      PROCESSOR_MAP_FILE;

// Default settings (defined in config.h)
static uint16_t port =          PROCESSOR_PORT;
static int adapter =            PROCESSOR_ADAPTER;
static int seconds =            PROCESSOR_SECONDS;
static int baseExposure =       PROCESSOR_BASE_EXPOSURE;
static int fps =                PROCESSOR_FPS;
static int hdr =                PROCESSOR_HDR;
static int output_pipe =        PROCESSOR_OUTPUT_PIPE;
static int sec_per_file =       PROCESSOR_SEC_PER_FILE;
static char *output_folder =    str_output_folder;
static char *map_file =         str_map_file;
static int client_count =       PROCESSOR_CLIENT_COUNT;
static int pilot_camera =       PROCESSOR_PILOT_CAM;

// Configuration package
static struct pkg_config conf;

// libevent event base
static struct event_base *base;

// Context read from file
static ContextVariables context;

// Modules
static FrameSyncer *syncer = NULL;
static CudaUploader *uploader = NULL;
static CudaBayerConverter *converter = NULL;
static CudaDynamicStitcher *stitcher = NULL;
static CudaHdr *hdrModule;
static NVENCWrapper *h264enc = NULL;

// Helpers
server_t     *server = NULL;

#ifdef HAVE_PG
schedule_t *schedule = NULL;
#endif

// State
bool recording = false;
bool ready     = false;
char *filepath = NULL;

static bool save_images_on_termination = false;

//
// ### Usage
//

void print_help()
{
    fprintf(stdout, "Usage:\n");
    fprintf(stdout, "-f --frate     Desired output frame rate (half of camera "
            "frame rate for HDR mode\n");
    fprintf(stdout, "-a --adapter   Dolphin adapter\n");
    fprintf(stdout, "-m --map       Input map file for stitcher\n");
    fprintf(stdout, "-d --hdr       Turn on HDR mode\n");
    fprintf(stdout, "-o --output    Output folder for recording\n");
    fprintf(stdout, "-p --port      Port to listen for clients on\n");
    fprintf(stdout, "-e --expo      Change baseline exposure (bright/dark)\n");
    fprintf(stdout, "-g --gain      Change camera gain parameter\n");
    fprintf(stdout, "-c --pipe      Named pipe for printing completed files\n");
    fprintf(stdout, "-s --sec       Predetermined duration / seconds of "
            "runtime\n");
    fprintf(stdout, "-n --clients   Number of clients to wait for before "
            "starting\n");
    //fprintf(stdout, "-D --deamonize Run as a daemon\n");
    //fprintf(stdout, "-p --pidfile   The file to write the PID to\n");
    fprintf(stdout, "-i --img       Save last set of frames to file\n");
    fprintf(stdout, "-h --help      Print this message\n");
}

//
// ### Helpers
//

std::string mkRecordingDir(std::string root){


	time_t t = time(0);
	struct tm * now = localtime(&t);

	std::ostringstream directoryPath;
    //YYYY-MM-DD_hhmm/
	directoryPath << root
        << (now->tm_year + 1900) << "-"
        << std::setw(2) << std::setfill('0') << (now->tm_mon + 1) << "-"
        << std::setw(2) << std::setfill('0') << now->tm_mday << "_"
        << std::setw(2) << std::setfill('0') << now->tm_hour
        << std::setw(2) << std::setfill('0') << now->tm_min
        << "/";

	return directoryPath.str();
}

//
// ### Recording
//
// Start or stop the recording pipeline.

bool rec_start()
{
    // If all clients have not connected yet, we can't start the recording yet.
    if (!ready) return false;

    // Check if we are already recording
    if (recording) return true;

    // Tell the syncer to start delivering frames.
    syncer->streamStart();

    //
    // Initialize modules
    //

    size_t in_frame_size = context.inWidth * context.inHeight
        * context.numSources * (hdr ? 2 : 1);
    size_t out_frame_size = context.outWidth * context.outHeight * 2;

    uploader = new CudaUploader();
    uploader->init(syncer, in_frame_size);

    converter = new CudaBayerConverter();
    converter->init(uploader, context.inWidth, context.inHeight,
            context.numSources * (hdr ? 2 : 1), save_images_on_termination);

    hdrModule = new CudaHdr();
    if (hdr) {
        hdrModule->init(converter, context.inWidth, context.inHeight,
                context.numSources);
        stitcher->init(hdrModule);
    } else {
        stitcher->init(converter);
    }

    std::string recording_dir;
    if (filepath != NULL) {
        recording_dir = std::string(filepath);
    } else {
        recording_dir = mkRecordingDir(std::string(output_folder));
    }

    h264enc = new NVENCWrapper();
    if(!h264enc->init(recording_dir, context.outWidth, context.outHeight,
                fps, sec_per_file, stitcher, false, X264_FORMAT_422,
                output_pipe)) {
        LOG_E("Unable to initialize h264enc");

        goto h264_fail;
    }

    // Tell all remote machines to start recording
    struct pkg_record pkg;
    pkg.header.size = sizeof(struct pkg_record);
    pkg.header.type = pkg_type_record;
    pkg.record = 1;

    server_broadcast(server, &pkg, pkg.header.size);

    recording = true;

    return true;

h264_fail:
    delete stitcher;
    delete converter;
    delete uploader;

    return false;
}

bool rec_stop()
{
    // Check if we are actually recording
    if (!recording) return true;

    // Stop the recording machines
    struct pkg_record pkg;
    pkg.header.size = sizeof(struct pkg_record);
    pkg.header.type = pkg_type_record;
    pkg.record = 0;

    server_broadcast(server, &pkg, pkg.header.size);

    // Stop the stream
    syncer->streamStop();

    // Delete all modules
    delete h264enc;
    delete hdrModule;
    delete converter;
    delete uploader;

    // Set all pointer to `NULL` so we can check if modules exists.
    h264enc        = NULL;
    converter   = NULL;
    uploader    = NULL;

    recording = false;

    return true;
}

//
// ### Callbacks
//

void recording_timeout_cb(int fd, short event, void *ctx)
{
    // Stop local pipeline
    rec_stop();

    // Break the event loop. This will cause the program to stop.
    event_base_loopbreak(base);
}

#ifdef HAVE_PG
void schedule_cb(schedule_t *s, bool record, void *ctx)
{
    if (record) {
        rec_start();
    } else {
        rec_stop();
    }
}

void filepath_cb(schedule_t *s, const char *path, void *ctx)
{
    char *old = filepath;
    filepath = NULL;

    std::string dir;

    if (path) {
        size_t len = strlen(path) + 1;
        filepath = (char *)malloc(len);
        if (!filepath) {
            LOG_E("Unable to allocate memory for filepath");
            return;
        }

#ifdef __APPLE__
        if (strlcpy(filepath, path, len) != len) {
            LOG_W("Length did not match strlen when copying filepath.");
        }
#else
        stpncpy(filepath, path, len-1);
        filepath[len-1] = 0;
#endif

        dir = std::string(filepath);
    } else {
        dir = mkRecordingDir(std::string(output_folder));
    }

    // If h264enc is already running, ask it to change path
    if (h264enc) {
        h264enc->setNewPath(dir);
    }

    if (old != NULL) {
        free(old);
    }
}
#endif // HAVE_PG

void all_connected_cb(server_t *s, void *ctx)
{
    LOG_D("All clients connected.");

    ready = true;

    // Schedule stop if seconds is set
    if (seconds > 0) {
        // Start the local pipeline
        rec_start();

        // Initalize the event
        struct event *timeout;
        struct timeval tv;

        // TODO I'm creating a memory leak here!
        timeout = evtimer_new(base, recording_timeout_cb, NULL);

        // Schedule the event
        timerclear(&tv);
        tv.tv_sec = seconds;
        event_add(timeout, &tv);

        LOG_D("Recording started, stop scheduled in %d seconds.", seconds);
    } else {
#ifdef HAVE_PG
        schedule_update(schedule);
        LOG_D("Schedule updated");
#else
        LOG_W("No stop time and no database, quitting!");
        event_base_loopbreak(base);
#endif
    }
}

void shutdown_function(evutil_socket_t sd, short events, void *ctx)
{
    fprintf(stderr, "Disconnecting all clients\n");

    server_t *s = (server_t *)ctx;

    // Disconnect all clients
    server_shutdown(s);

    // Exit the event base loop
    event_base_loopbreak(base);
}

void exposure_cb(const struct pkg_header *pkg, void *ctx)
{
    server_t *s = (server_t *)ctx;

    // Broadcast the exposure package to all clients
    server_broadcast(s, (void *)pkg, pkg->size);
}

void whitebalance_cb(int cam_idx, CudaStitcher::RgbRatio ratio)
{
    struct pkg_whitebalance pkg;
    pkg.header.size = sizeof(struct pkg_whitebalance);
    pkg.header.type = pkg_type_whitebalance;

    pkg.cam_idx = cam_idx;
    pkg.red = ratio.r;
    pkg.green = ratio.g;
    pkg.blue = ratio.b;

    server_broadcast(server, &pkg, pkg.header.size);
}

//
// ### Main
//

// Main function. Parse options, initialize (and if requested, daemonize) and
// start event base.
int main(int argc, char *argv[])
{
#ifdef LOG_TIMESTAMP
	logging_start_timer();
#endif
    // Initialize variables here so we can use goto
    struct event *e;

    // Define options
    static struct option long_options[] = {
        {"frate",   required_argument, NULL, 'f'},
        {"adapter", required_argument, NULL, 'a'},
        {"map",     required_argument, NULL, 'm'},
        {"hdr",     no_argument,       NULL, 'd'},
        {"output",  required_argument, NULL, 'o'},
        {"port",    required_argument, NULL, 'p'},
        {"expo",    required_argument, NULL, 'e'},
        {"pipe",    required_argument, NULL, 'c'},
        {"sec",     required_argument, NULL, 's'},
        {"clients", required_argument, NULL, 'n'},
        {"img",     no_argument,       NULL, 'i'},
        {"help",    no_argument,       NULL, 'h'},
        {0,         0,                 NULL,  0 }
    };

    // Get options
    int opt = 0, long_index =0;
    while ((opt = getopt_long(argc, argv, "f:a:m:do:p:e:c:s:ih",
                    long_options, &long_index )) != -1) {
        switch (opt) {
            case 'f' : fps = atoi(optarg);
                       break;
            case 'a' : adapter = atoi(optarg);
                       break;
            case 'm' : map_file = optarg;
                       break;
            case 'd' : hdr = 1;
                       break;
            case 'o' : output_folder = optarg;
                       break;
            case 'p' : port = atoi(optarg);
                       break;
            case 'e' : baseExposure = atoi(optarg);
                       if(baseExposure > 200 || baseExposure < 50){
                           LOG_E("Exposure value out of range (50 - 200)");
                           exit(EXIT_FAILURE);
                       }
                       break;
            case 'c' : output_pipe = open(optarg, O_WRONLY);
                       if(output_pipe <= 0)
                           LOG_E("Unable to open output pipe %s", optarg);
                       break;
            case 's' : seconds = atoi(optarg);
                       break;
            case 'n' : client_count = atoi(optarg);
                       break;
            case 'i' : save_images_on_termination = true;
                       break;
            case 'h' :
            default  : print_help();
                       exit(EXIT_FAILURE);
        }
    }
    if(save_images_on_termination && seconds <= 0){
        LOG_E("Writing last images requires setting a recording duration, not daemon mode");
        exit(EXIT_FAILURE);
    }

    // Set up local modules
    stitcher = new CudaDynamicStitcher();
    if(!stitcher->parseMap(map_file, &context, whitebalance_cb, pilot_camera)){
        LOG_E("Unable to parse map file");
        exit(EXIT_FAILURE);
    }

    size_t frame_size  = context.inWidth*context.inHeight;

    uint32_t cams = context.numSources;

    // Initialize sisci
    sci_error_t error;
    SCIInitialize(0, &error);
    if (error != SCI_ERR_OK) {
        LOG_E("Unable to initalize dolphin. Error: 0x%X", error);
        exit(EXIT_FAILURE);
    }

    uint32_t node = 0;
    // Dynamically get node
    if (node == 0) {
        sci_error_t error;
        SCIGetLocalNodeId(adapter, &node, 0, &error);
        if (error != SCI_ERR_OK) {
            LOG_E("Unable to get dolphin node id. Error: 0x%X", error);
            SCITerminate();
            exit(EXIT_FAILURE);
        }
    }

    syncer = new FrameSyncer(frame_size, cams);
    syncer->setFrameRate(fps);
    syncer->setHDRMode(hdr);
    if (!syncer->initialize()) {
        LOG_E("Unable to initialize frame syncer");
        exit(EXIT_FAILURE);
    }
    // Set up the configuration package
    conf.header.type = pkg_type_config;
    conf.header.size = sizeof(struct pkg_config);
    conf.fps = hdr ? fps * 2 : fps;
    conf.baseExposure = baseExposure;
    conf.hdr_mode = hdr;
    conf.width = context.inWidth;
    conf.height = context.inHeight;
    conf.pilot_cam = pilot_camera;
    conf.buffer_size = frame_size*(hdr?2:1);
    conf.node_id = node;

    // Create a libevent event base
    base = event_base_new();

    // Set up the server
    server = server_init(base, port);
    if (server == NULL) {
        LOG_E("Unable to initialize server.");
        goto fail;
    }

#ifdef HAVE_PG
    // Set up the schedule handler
    if (seconds <= 0) {
        schedule = schedule_init(base);
        if (!schedule) {
            LOG_E("Unable to initialize schedule helper");
        }

        // Add callbacks
        schedule_add_filepath_cb(schedule, filepath_cb, NULL);
        schedule_add_record_cb(schedule, schedule_cb, NULL);

        LOG_D("Schedule initialized");
    }
#endif

    // Register the callback for when all clients have connected
    server_register_ready_cb(server, client_count, all_connected_cb, server);

    // Register callback for exposure packages
    server_register_cb(server, pkg_type_exposure, exposure_cb, server);

    // Handle ctrl-c
    e = evsignal_new(base, SIGINT, shutdown_function, server);
    evsignal_add(e, NULL);

    // Tell the server to send our configuration package to new clients
    server_set_config_pkg(server, &conf);

    LOG_D("Starting libevent run loop");

    // Start libevent runloop
    event_base_dispatch(base);

    // TODO clear up memory etc.
    event_base_free(base);

    // Free the stitcher
    delete stitcher;

    // Free the syncer
    syncer->terminate();
    delete syncer;

    // Terminate the SISCI library
    SCITerminate();

	if(output_pipe > 0) close(output_pipe);

    return EXIT_SUCCESS;

// Free up any resources allocated during failed initialization
// TODO
// - Add multiple steps so this can be used all times,
//   and use multiple labels to that we can jump to the
//   appropriate point depending on where things went to
//   hell.
//   E.g: dolphin_fail if something went wrong when setting
//   up dolphin
fail:
    delete stitcher;

    syncer->terminate();
    delete syncer;

    // Terminate the SISCI library
    SCITerminate();

    // TODO
    // Clean up other memory and stuffz

    return EXIT_FAILURE;
}
