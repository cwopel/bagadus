Instructions on how to configure the Panorama Stitcher for when the cameras have moved,
or when creating a new configuration.

First, capture still images from the cameras. This can be done with MakeImage (single machine)
or using the BagadusProcessor (multiple machines) with the options '-s 15 -i' (15sek recording,
save last image-set on termination). Note that the latter alternative requires having a valid
panorama mapping already.

Second, use a machine with OpenCV (compiled with GUI):
Compile the program (Cmake in this folder).

Make a sample config file, just copy this:
$ cat BASE_CONFIG.txt
4096 1680 0 0
1080 2040 37.5 5
-1.0 0 0 0 0 1.03
-0.5 0 0 0 0 1.03
 0.0 0 0 0 0 1.03
 0.5 0 0 0 0 1.03
 1.0 0 0 0 0 1.03

The first 2 numbers indicate output resolution, second two numbers must be 0.
Next line, input image resolution (if the cameras are rotated, as in Tromso, remember that the width
and height are inverted, as above). 37.5 indicate the horizontal field of view of the cameras
(values below 2*PI are interpreted as radians) and final number determines the number of cameras.
The last number is the focal multiplier, image scaling factor. Increasing this will reduce the
size of the panorama, which can be useful if you wish to scale the image up or down a little. Above,
the are all scaled down a little to fit into 4096 resolution.


run: ./panoramaGeneration BASE_CONFIG.txt img0.png img1.png img2.png img3.png img4.png

This will use the previous configuration as a baseline.
Note that the input images must be included in left-to-right order.
If they are swapped, re-run with swapped order, you cannot change it at runtime.

This should open an endless loop of displaying the mapped panorama.
You can tweak values (see below) untill satisfied with the output,
escape will terminate and save the result.

Creates PANO_CONFIG.txt (Might want to avoid calling the original this,
so it isnt accidentally overwritten :P)
and mapDynamic.data (this is the lookup table used by the pipeline).
You can re-run the program with the output config file to produce the same mapping,
if you wish to modify it or reset to the original.

Tweaking values:
Might take a little time to get the hang of, try not to warp too much.
The below keys changes the projection parameters in some way.
All changes use the same modifier to determine the intensity of the change,
but all keys respond quite differently to this modifier. Just need some trial
and error for this.

NB! numlock must be off due to OpenCV being annoying.

tab:			[*]switch image (red number = active)
+,-:			[*]increase/decrease the global multiplier.
1,2:			[*]scale preview panorama (no effect on output)
j,l,i,k:		[*]Move image left/right/up/down
arrowkeys:		[ ]Move all images left/right/up/down
shift+arrow:	[ ]Change output dimension of panorama
q,E:			[*]Rotate image around Z-axis
w,s:			[ ]Rotate image around X-axis (avoid whenever possible, distorts image)
a,d:			[ ]Rotate image around Y-axis (Dont use this. At all. Ever.)
u,o:			[*]Change focal length / scale of the image
p:				[ ]Print parameters of the targeted camera (rotations, focal, etc)
ESC:			[*]Terminate and save result

[*] = You're going to have to use this

Start by leaving the center camera alone, and getting the 3 center cameras correct, and finally
the outermost cameras. 

As of right now, in Tromso, the second and fourth camera always require some X-rotation.
Sample_PANO_CONFIG.txt shows the current configuration, as of writing this, and may give
indicators towards how the mapping may look. If you are unsure how badly the image is warped,
pressing 'p' can give some indication on this.

When terminating, the resulting lookup table is stored as 'mapDynamic.data'.
The panorama pipeline typically expects this to be named 'map5Dynamic.data',
located in its build folder.

