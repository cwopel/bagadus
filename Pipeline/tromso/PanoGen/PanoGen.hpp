// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 


#pragma once

#include <iostream>
#include <fstream>
#include <cassert>
#include <cstdlib>
#include <cmath>
#include <chrono>
#include <pthread.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

/* Crops 2 pixels from left/right/up/down from inputimage.
 * Doesnt matter much, but means edges arent an issue when interpolating bayer */
#define CROP_FOR_BAYER
#define DYNAMIC_STITCH_AREA 168 /* MUST BE A MULTIPLE OF 4 */
#define DYNAMIC_STITCH_AREA_HALF (DYNAMIC_STITCH_AREA/2)

#define _USE_MATH_DEFINES
typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::duration<double> sec;
#define OUTPUT_CONFIG_NAME "PANO_CONFIG.txt"
#define OUTPUT_DYN_MAP_NAME "mapDynamic.data"

#ifdef __APPLE__
// OS X doesn't include pthread_barrier* as it's an optional part of POSIX
typedef int pthread_barrierattr_t;
typedef struct
{
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int count;
    int tripCount;
} pthread_barrier_t;
#endif

class InputImage{
	public:
		double angleOffset; 
		int yOffset;

		unsigned int width;	
		unsigned int height;
		unsigned char * img;

		double rotX, rotY, rotZ;
		double focalMultiplier; /* default is 1 */

		double rotMatrix[3][3];
		double focalLength;

		void createConstants(double radius);

		/* For simplicity, these angles may be in degrees */
		void init(cv::Mat src, double angle, int yOff, double focal, double xR, double yR, double zR, bool inRadians);

		void updateRotation(double xR, double yR, double zR);
};

extern "C"{
struct DynPixPoint{
	float x; //-1 if not there
	float y; //-1 if not there
};
};

void * stitcherThreadStarter(void * p);

class OutputImage{
	public:
		InputImage * sources;
		unsigned int numSources;

		int srcWidth;
		int srcHeight;
		float srcFov;
		float radius;

		int destWidth;
		int destHeight;
		// these two variables cannot be altered at runtime,
		// but kept to not break old config files
		int xShift, yShift; 
		unsigned char * destImage;

		DynPixPoint * map;
		int * stitches;

		pthread_t *stitcherThreads;
		pthread_barrier_t stitcherBarrier;
		pthread_mutex_t idxMutex;
		int curIdx;

		OutputImage(InputImage * input, unsigned int numInput, double fov, int width, int height, int shiftRight, int shiftDown, bool inRadians);

		~OutputImage();

		int getId();

		void changeDim(double w, double h);

		/* This is the main function for projecting a vertical column on the output image,
		 * from a single source image. Correct source image must be identified first */
		void projectX(InputImage * in, DynPixPoint * mapping, int dstX, int stride, int minX);

		void makeIndividualMap();

		unsigned char * fillPixels();

		void makeMapping();

		void runStitcherThread();
};

