= Overview =

This page details how to configure the GigE-cameras used in the bagadussii project.

= Assumptions =

We assume a lot in this guide. Not all assumptions are critical, but may need slightly different configuration.

* Ubuntu 12.04 installation, may be differences on other distributions. Ubuntu 14.04 also works without major changes.

* Pylon version 4.0.0 for Linux 64bit is assumed. Currently, 3.2.1 is also known to work.

* Pylon install directory is assumed to be /opt/pylon4. or /opt/pylon3 of v3.2.1

* Cameras connected directly to the machine, or through only a local switch, not across the internet (this requires very different network config, dhcp settings and bandwidth-configuration in software.

* It is assumed that DHCP is used to give all cameras a valid IP. This is not required, but what we currently do. /opt/pylon3/bin/IpConfigurator (requires X / X-forwarding) can be used to configure the cameras.

* This guide assumes 2 connected cameras for simplicity. It is indicated where additional camera information is to be added, it is simply more interfaces to configure...

* It is assumed that a triggerbox is used to synchronize the triggering of frames. '''NB!''' In software, if you specify triggerMode, the requested frame rate will rely on the triggerbox and not the requested frame rate. If you choose to capture, in software, without triggerMode then the triggerbox can be set to anything. In summary, if triggerMode is selected then the software-framerate is ignored, otherwise the triggerbox-signal is ignored. If you fail to get the output frame rate you expect, check the triggerbox configuration, then check the network requirements of your configuration.

= Getting Pylon =
I recommend using the version currently available in the bagadussii repository. This is '''4.0.0.62''' as of writing this. A bug in the (which fails to compile with c++11), has been fixed in the tar-ball included in bagadussii.

Otherwise, latest version of Pylon SDK can be found at http://www.baslerweb.com/Downloads-Software-43868.html


= Environment =

Pylon SDK requires several environment variables to be properly set.

Append to '''/etc/environment'''

<pre>
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/cuda/bin:/opt/pylon4/bin"
LC_ALL="en_US.UTF-8"
PYLON_ROOT=/opt/pylon4
GENICAM_ROOT_V2_3=/opt/pylon4/genicam
GENICAM_CACHE="/tmp"
GENICAM_CACHE_V2_3="/tmp"
LD_LIBRARY_PATH=/opt/pylon4/genicam/bin/Linux64_x64/GenApi/Generic:$LD_LIBRARY_PATH
LD_LIBRARY_PATH=/opt/pylon4/lib64:/opt/pylon4/genicam/bin/Linux64_x64:$LD_LIBRARY_PATH
</pre>

These variables may of course also need to include other paths, depending on other programs etc.

= Hardware configuration =

=== Simula homemade triggerbox configuration ===
Connect each camera to the machine with an ethernet cable, into a network card that supports 8192 MTU (jumbo frames). Connect the camera power cables to the triggerbox, turn on the triggerbox, set the desired Hz (fps) and hit "Store".

=== Arduino + POE-injector configuration ===
Connect each camera to the POE-injector, and another cable from the machine to the corresponding POE-injector port.

TODO: stub

= Network setup =

=== Interface configuration ===
Make sure that every network interface that uses cameras is named the same on each boot.
This is explained here http://ubuntuforums.org/showpost.php?p=11142836&postcount=10 

It may be useful to look at the MAC/HWaddr of the interfaces to determine which interface belong to the same network card.
We assume that the interfaces used for cameras are named eth1 eth2 .. ethX, and we assume eth0 is your outward internet connection. Adjust your names accordingly, if local configuration differs from this.

First, edit '''/etc/network/interfaces'''

You can normally leave eth0 config as it was before, you may have a different config.
<pre>
#This part is subject to change depending on your config
auto eth0
iface eth0 inet dhcp
#

#camera interfaces
auto eth1
iface eth1 inet static
  address 10.0.1.1
  netmask 255.255.255.0
  broadcast 10.0.1.255
  mtu 8192


auto eth2
iface eth2 inet static
  address 10.0.2.1
  netmask 255.255.255.0
  broadcast 10.0.2.255
  mtu 8192

#Add more interfaces if needed
</pre>

=== Configure DHCP server ===
Make sure dhcpd is installed. There are many dhcp-servers to choose from, we assume isc-dhcp-server:
<pre>
sudo apt-get install isc-dhcp-server
</pre>

Edit '''/etc/default/isc-dhcp-server'''
<pre>
# Defaults for dhcp initscript
# sourced by /etc/init.d/dhcp
# installed at /etc/default/isc-dhcp-server by the maintainer scripts

#
# This is a POSIX shell fragment
#

# On what interfaces should the DHCP server (dhcpd) serve DHCP requests?
#requestsSeparate multiple interfaces with spaces, e.g. "eth0 eth1".
INTERFACES="eth1 eth2"
#Add more interfaces here for more cameras
</pre>
Note that this depends on how many interfaces you use, they correspond to the interfaces set up earlier.

Now edit '''/etc/dhcp/dhcpd.conf'''

Add the following entries, and add new entries depending on your config at the bottom.
You may also wish to add a domain name, but this is not necessary.

<pre>
not authoritative; #This can be changed to "authorative", depending on your needs
subnet 10.0.1.0 netmask 255.255.255.0 {
    interface eth1;
    range 10.0.1.10 10.0.1.99;
}
subnet 10.0.2.0 netmask 255.255.255.0 {
    interface eth2;
    range 10.0.2.10 10.0.2.99;
}
#Add more subnets below for more cameras
</pre>

=== Optimize TCP ++ ===

First we tune TCP for high data rate transfers. This is based on http://fasterdata.es.net/host-tuning/linux/

Append this to '''/etc/sysctl.conf'''

<pre>
# increase TCP max buffer size settable using setsockopt()
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
# increase Linux autotuning TCP buffer limit 
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 65536 16777216
# increase the length of the processor input queue
net.core.netdev_max_backlog = 30000
# recommended default congestion control is htcp 
net.ipv4.tcp_congestion_control=htcp
# recommended for hosts with jumbo frames enabled
net.ipv4.tcp_mtu_probing=1

</pre>

You may also want to increase packet transmit queue length as well. This can be beneficial to do with eth0 as well. Obviously, add additional interfaces at the end.
Append this to (before 'exit 0') '''/etc/rc.local'''

<pre>
ifconfig eth0 txqueuelen 10000
ifconfig eth1 txqueuelen 10000
ifconfig eth2 txqueuelen 10000
</pre>

Next, allow user applications to set realtime thread priority, and lower nice value.
Depending on what the machine is used for, you may wish to restrict this to specific user-names or -groups, instead '*'.

Append to '''/etc/security/limits.conf'''
<pre>
* - rtprio 99
* - nice −10
</pre>


= When you are done =
Either restart the computer or run:
<pre>
sudo service isc-dhcp-server restart
sudo /etc/init.d/networking restart
</pre>
If you choose not to restart, you must log out and back in for environment variables to be set.

If you are working with an X-environement, you can test if the everything is properly configured by running 'PylonViewerApp' (/opt/pylon3/bin/PylonViewerApp). This is a gui application that allows you to configure and capture video/images from the cameras.

= Debugging =
If you have connection problems, check that the DHCP-server does not listen on eth0, check that eth0 is in fact your internet-interface, and check that your gateway is correct.
If some cameras does not show up, check that the hw-address/mac address is correct in the DHCP-setup

Look at all your detected interfaces with:
<pre>
ifconfig -a
</pre>

To manually bring up or down interfaces:
<pre>
ifconfig eth0 up
ifconfig eth0 down
</pre>

Check your gateway setup:
<pre>
netstat -nr
</pre>

Add and remove gateways in the routing table with:
<pre>
route add default gw [IP ADDRESS]
route del default
</pre>

== Compiler error: "unable to find string literal operator ‘operator"" FMT_I64’" ==
This is a c++11 bug that occurs when using distribution downloaded directly from Basler.
It can be easily fixed by this command:
<pre>
find /opt/pylon4/genicam/ -name "*.h" -print0 | sudo xargs -0 sed -i 's/\"FMT_I64\"/\"\ FMT_I64\ \"/g'
</pre>
Adds surrounding spaces around all occurrences of '"FMT_I64"' in the headers.

== Error: Grab Failed. Reason: (#e1000014) ==
This error can occur either from incorrectly setting MTU, forgetting to set systcl.conf, forgetting to set realtime thread priorities or the system is simply too stressed.

It essentially means that the local Pylon thread was not able to process an arriving frame in time, which is typically a result of badly optimized network link, large packet loss or too slow thread scheduling. You may see this if the machine is simply too stressed and can't handle the CPU workload.

== Unable to open camera ==
If an application crashes or terminates without releasing the required resources then the camera can often not be re-opened again for several seconds (up to a minute or twp), as only one application can actively control the camera at once. Short therm solution is to toggle power / restart cameras, but it is recommended to write software to never prematurely exit without correctly closing and destroying the Pylon-related objects.

Also remember to call these functions once per process. Pylon-related code will fail without being placed between these.
<pre>
PylonGigE::initializePylon(); //Allocates lower level process-wide resources
...
PylonGigE::terminatePylon(); //Frees lower level process-wide resources
</pre>

= Bagadussii Camera Modules =
The bagadussii repository includes a library wrapper for capturing from multiple GigE-cameras.
It is assumed that all cameras controlled are identical, however, you can control different models in the same process by creating a different PylGigE-object for each camera model.
This wrapper is located in 'bagadussii/src/tromso/Helpers/Pylon'. It is written for version '''3.2.1''' and primarily 'acA2000-50gc' model. Other cameras may behave slightly differently, and some configuration parameters may therefore need to be altered. Likely requires major changes to support USB3-based cameras etc.

* 'ListCameras' is a simple application included in bagadussii to print some information on all connected cameras. This is the easiest way to see if they have been properly configured.

* 'MakeImage' also uses OpenCV, and can be used to capture a still image from all connected cameras. Images will be captured at the same time, and exposure time will be printed.

* 'PylonDbg' is a simple target for testing reading from cameras and configuring them. This is essentially a playground / learning program to see how they are used.


