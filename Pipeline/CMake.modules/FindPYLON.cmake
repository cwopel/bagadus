# - Try to find Pylon
# Once done, this will define
#
#  PYLON_FOUND - system has Pylon
#  PYLON_INCLUDE_DIRS - include directories
#  PYLON_LIBRARIES - pylon and depending libs
#  PYLON_LIBRARY_DIRS - library search path
#  PYLON_CFLAGS - compile flags
#  PYLON_LDFLAGS - linker flags
#

if (NOT ENABLE_PYLON)
    message("-- PylonSDK disabled")
    set (PYLON_FOUND FALSE)
	return ()
endif ()

if (NOT Pylon_FIND_QUIETLY)
	message (STATUS "Checking for PylonSDK...")
endif ()

if (EXISTS /opt/pylon4/lib64/pylon-libusb-1.0.so )
	set (PYLON_BASE /opt/pylon4)
    set (PYLON_VERSION "4")
elseif (EXISTS /opt/pylon/lib64/pylon-libusb-1.0.so )
	set (PYLON_BASE /opt/pylon)
    set (PYLON_VERSION "4")
elseif (EXISTS /opt/pylon3/include/pylon/InstantCameraArray.h)
	set (PYLON_BASE /opt/pylon3)
    set (PYLON_VERSION "3")
elseif (EXISTS /opt/pylon/include/pylon/InstantCameraArray.h)
	set (PYLON_BASE /opt/pylon)
    set (PYLON_VERSION "3")
else ()
	set (PYLON_FOUND FALSE)
	message ( WARNING "Could not find Pylon v3 or v4")
	return ()
endif()

set (PYLON_FOUND TRUE)
set (PYLON_BASE_LIBS "${PYLON_BASE}/lib64")
set (GENICAM_BASE "${PYLON_BASE}/genicam")
set (GENICAM_BASE_LIBS "${GENICAM_BASE}/bin/Linux64_x64")

set (PYLON_LDFLAGS "-Wl,-E")
set (PYLON_CFLAGS "-DUSE_GIGE -isystem ${PYLON_BASE}/include")
#Pylon is included as "system headers" to remove all the annoying warnings
set (PYLON_LIBRARY_DIRS
	${PYLON_BASE}/lib64
	${GENICAM_BASE_LIBS}
	${GENICAM_BASE_LIBS}/GenApi/Generic
	)
set (PYLON_INCLUDE_DIRS
	${PYLON_BASE}/include
	${GENICAM_BASE}/library/CPP/include
	)

if (PYLON_VERSION EQUAL "3" )
    set (PYLON_CFLAGS "-DPYL_VER_3 ${PYLON_CFLAGS}")
	set (PYLON_LIBRARIES 
		libGCBase_gcc40_v2_3.so
		libLog_gcc40_v2_3.so
		libGenApi_gcc40_v2_3.so
		libXerces-C_gcc40_v2_7.so
		libXalanMessages_gcc40_v1_10.so
		libXalan-C_gcc40_v1_10.so
		libXMLLoader_gcc40_v2_3.so
		liblog4cpp_gcc40_v2_3.so
		libMathParser_gcc40_v2_3.so
		libpylonbase.so
		libXerces-C_gcc40_v2_7.so
		${PYLON_BASE}/lib64/pylon/tl/pyloncamemu.so
		${PYLON_BASE}/lib64/pylon/tl/pylongige.so
		libpylongigesupp.so
		libpylonutility.so
		libgxapi.so
		)
elseif (PYLON_VERSION EQUAL "4" )
    set (PYLON_CFLAGS "-DPYL_VER_4 ${PYLON_CFLAGS}")
	set (PYLON_LIBRARIES 
        libpylonutility-4.0.0.so
        libgxapi.so
        libpylonbase-4.0.0.so
        libgxapi-4.0.0.so
        libuxapi.so
        libuxapi-4.0.0.so
        libXerces-C_gcc40_v2_7_1.so
        libpylonutility.so
        libpylongigesupp.so
        libpylongigesupp-4.0.0.so
        libpylonbase.so
        ${PYLON_BASE_LIBS}/pylon/tl/pylonusb-4.0.0.so
        ${PYLON_BASE_LIBS}/pylon/tl/pylonusb.so
        ${PYLON_BASE_LIBS}/pylon/tl/pyloncamemu-4.0.0.so
        ${PYLON_BASE_LIBS}/pylon/tl/pylongige-4.0.0.so
        ${PYLON_BASE_LIBS}/pylon/tl/pyloncamemu.so
        ${PYLON_BASE_LIBS}/pylon/tl/pylongige.so
        ${PYLON_BASE_LIBS}/pylon-libusb-1.0.so
        ${PYLON_BASE}/bin/libPylonQtBase.so.1.0
        ${PYLON_BASE}/bin/libPylonViewerSdk.so.1
        ${PYLON_BASE}/bin/libPylonQtStyle.so.1
        ${PYLON_BASE}/bin/libQtGui.so.4.8.5
        ${PYLON_BASE}/bin/libQtCore.so.4
        ${PYLON_BASE}/bin/libPylonQtWidgets.so
        ${PYLON_BASE}/bin/libPylonViewerSdk.so
        ${PYLON_BASE}/bin/libPylonViewerSdk.so.1.0.0
        ${PYLON_BASE}/bin/libQtNetwork.so
        ${PYLON_BASE}/bin/libQtCore.so.4.8
        ${PYLON_BASE}/bin/libQtCore.so.4.8.5
        ${PYLON_BASE}/bin/libQtNetwork.so.4.8
        ${PYLON_BASE}/bin/libPylonQtWidgets.so.1.0
        ${PYLON_BASE}/bin/libQtXml.so.4.8.5
        ${PYLON_BASE}/bin/libQtNetwork.so.4.8.5
        ${PYLON_BASE}/bin/libPylonQtStyle.so.1.0.0
        ${PYLON_BASE}/bin/libPylonQtStyle.so
        ${PYLON_BASE}/bin/libPylonViewerSdk.so.1.0
        ${PYLON_BASE}/bin/libQtXml.so
        ${PYLON_BASE}/bin/libQtGui.so
        ${PYLON_BASE}/bin/libPylonQtWidgets.so.1
        ${PYLON_BASE}/bin/libPylonQtBase.so.1.0.0
        ${PYLON_BASE}/bin/libQtNetwork.so.4
        ${PYLON_BASE}/bin/libQtGui.so.4
        ${PYLON_BASE}/bin/libPylonQtBase.so.1
        ${PYLON_BASE}/bin/libQtXml.so.4
        ${PYLON_BASE}/bin/libPylonQtStyle.so.1.0
        ${PYLON_BASE}/bin/libPylonQtBase.so
        ${PYLON_BASE}/bin/libQtCore.so
        ${PYLON_BASE}/bin/libQtGui.so.4.8
        ${PYLON_BASE}/bin/libPylonQtWidgets.so.1.0.0
        ${PYLON_BASE}/bin/libQtXml.so.4.8
        ${GENICAM_BASE_LIBS}/libMathParser_gcc40_v2_3.so
        ${GENICAM_BASE_LIBS}/liblog4cpp_gcc40_v2_3.so
        ${GENICAM_BASE_LIBS}/GenApi/Generic/libXMLLoader_gcc40_v2_3.so
        ${GENICAM_BASE_LIBS}/GenApi/Generic/libXalan-C_gcc40_v1_10_1.so
        ${GENICAM_BASE_LIBS}/GenApi/Generic/libXalanMessages_gcc40_v1_10_1.so
        ${GENICAM_BASE_LIBS}/GenApi/Generic/libXerces-C_gcc40_v2_7_1.so
        ${GENICAM_BASE_LIBS}/libGCBase_gcc40_v2_3.so
        ${GENICAM_BASE_LIBS}/libGenApi_gcc40_v2_3.so
        ${GENICAM_BASE_LIBS}/libLog_gcc40_v2_3.so
		)
else ()
	set (PYLON_FOUND FALSE)
endif ()


