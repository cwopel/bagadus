// AUTHOR(s): Martin Alexander Wilhelmsen,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <utils.h>

#include <algorithm>
#include <unordered_map>
#include <string>

#include <boost/network/protocol/http/server.hpp>
#include <boost/network/utils/thread_pool.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <boost/bind.hpp>
#include <boost/function.hpp>


struct PostReaderData {
	const char *p;
	size_t l;

	PostReaderData() : p(0), l(0) {}
	PostReaderData(const char *p, size_t l)
		: p(p)
		  , l(l)
	{}
};

template<typename server>
struct PostReader : public boost::enable_shared_from_this<PostReader<server>> {
	private:
		typename server::request req_;
		typename server::connection_ptr conn_;

		bool doneParsing_;
		unsigned int expectedSize_;
		std::string incoming_;
		std::unordered_map<std::string, PostReaderData> data_;

		using CallbackType = std::function<
			void(bool error, std::unordered_map<std::string, PostReaderData> &)
			>;
		CallbackType callback_;

		bool parseData() {

			// I do not feel like creating a proper parser right now
			//  so we only support multiform-shit ATM. It may be possible
			//  to reuse the HTTP-parser in netlib-cpp...

			// Read the first line of incoming_. It should contain the delimer.
			//  As always with HTTP, lines are divided by \r\n
			std::string delimer;
			{
				auto n = incoming_.find('\r');
				if(n == std::string::npos) {
					std::cerr << "No delimer found..." << std::endl;
					return false;
				}

				delimer = incoming_.substr(0, n);
			}



			// Find all the occurences of delimer in string
			std::vector<size_t> delimers;
			{
				size_t n = 0;
				size_t p;
				while((p = incoming_.find(delimer, n)) != std::string::npos) {
					delimers.push_back(p);
					n = p + 1;
				}
			}

			// We can not work with less than two delimers
			if(delimers.size() <= 1)
				return false;

			// OK, so now we have the delimers. A well-behaving browser
			// will send a last delimer to end the sequence. 
			// E.g., we have delimers.size() - 1 fields.
			int i = 0;
			for(auto p : delimers) {
				// make a copy of the delimer position.
				// will come in handy when we have parsed the data
				// and only will be looking for the \r\n\r\n-end of header-delimer
				auto p0 = p;

				// Skip the last delimer we found
				if(i == delimers.size() - 1) break;
				i++; // i is now indexing the next delimer

				// Skip past the delimer.
				p += delimer.size() + strlen("\r\n");

				std::string contentDisposition;
				const char *contentDispositionTarget = "Content-Disposition";


				while(1) {
					if(p >= incoming_.size()) {
						std::cout << "1" << std::endl;
						return false;
					}

					// p should now point to the line after the delimer.
					// this contains info about the field
					auto peol = incoming_.find("\r\n", p);

					if(peol == std::string::npos) {
						std::cout << "2" << std::endl;
						return false;
					}

					if(peol == p) {
						// we found the empty line between the multipart header
						// and the data

						// we can not work with this data if we're still in this loop...
						std::cout << "3" << std::endl;
						return false;
					}

					// The line we are looking for looks like this:
					// Content-Disposition: form-data; name="BLAH"; filename="BLBALBAL.txt"
					contentDisposition = incoming_.substr(p, peol - p);

					if(contentDisposition.substr(0, strlen(contentDispositionTarget))
							== contentDispositionTarget) {
						// got it, now parse it.
						break;
					}
					else {
						// try next line
						p = peol + strlen("\r\n");
					}
				}

				// contentDisposition should now contain the Content-Disposition header
				// we only care about the name="...";

				// find name
				auto pname = contentDisposition.find("name=");
				if(pname == std::string::npos) {
					std::cout << "4" << std::endl;
					return false;
				}

				// find first of ; or eos from name
				auto pnameend = contentDisposition.find(';', pname);
				if(pnameend == std::string::npos) {
					pnameend = contentDisposition.size();
				}

				// the name should now be between pname and pnamened
				pname += strlen("name=");
				std::string name = contentDisposition.substr(pname, pnameend - pname);

				if(name.size() == 0) {
					std::cout << 5 << std::endl;
					return false;
				}

				// if the name starts with an \" and ends with an \", strip the \"s
				if(name.front() == '\"' && name.back() == '\"')
					name = name.substr(1, name.size() - 2);

				// the data should reside from the first occurence of \r\n\r\n
				// until the next delimer
				auto start = incoming_.find("\r\n\r\n", p0);
				if(start == std::string::npos) {
					std::cout << 6 << std::endl;
					return false;
				}
				start += strlen("\r\n\r\n");

				// the end of file is always 2 chars before the beginning of the
				// next delimer, i.e., the \r\n
				auto end = delimers[i] - strlen("\r\n");

				PostReaderData data(incoming_.data() + start, end - start);

				data_.emplace(std::make_pair(name, data));
			}

			return true;
		}

		void recvCB(typename server::connection::input_range range,
				boost::system::error_code error, size_t size) {
// 			struct timespec t0;
// 			clock_gettime(CLOCK_MONOTONIC, &t0);
// 			LOG_D("D %10ld", (t0.tv_sec*1000*1000 + t0.tv_nsec/1000));
			if (!error) {
				if(size)
					incoming_.append(boost::begin(range), size);

				if(incoming_.size() < expectedSize_) {
					recv();
				} else {
					if(parseData()) {
						doneParsing_ = true;
						callback_(false, data_);
					}
					else {
						doneParsing_ = true;
						callback_(true, data_);
					}
				}
			}
			else {
				std::cerr << "Got ws error: " << error << std::endl;
			}
		}

		void recv() {
			conn_->read(boost::bind(&PostReader::recvCB,
						this->shared_from_this(),
						_1, _2, _3));
		}

		std::string findHeader(std::string name) {
			auto n = boost::to_lower_copy(name);
			auto hs = req_.headers;

			for(auto it = hs.begin(); it != hs.end(); ++it) {
				if(boost::to_lower_copy(it->name) == n)
					return it->value;
			}

			return "";
		}

	public:

		PostReader(typename server::request req,
				typename server::connection_ptr conn,
				CallbackType callback) 
			: req_(req)
			  , conn_(conn)
			  , doneParsing_(false)
			  , expectedSize_(0)
			  , callback_(callback)
	{}

		bool operator()() {
			// Read content length
			{
				auto lenStr = findHeader("Content-Length");
				std::istringstream ss(lenStr);
				if(!(ss >> expectedSize_)) return false;
			}

			// Some kind of insanity check... :D
			if(expectedSize_ > 200 * 1024 * 1024) {
				return false;
			}

// 			std::cout << "Expecting: " << expectedSize_ << std::endl;
			incoming_.reserve(expectedSize_);

			// Then start to read until the data is received
			recv();

			return true;
		}
};
