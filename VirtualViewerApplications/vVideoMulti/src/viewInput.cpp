// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <viewInput.h>

void ViewInput::runReader(int pipe){
	uint8_t * cpuBuf[2];

	cudaSafe(cudaMallocHost((void**)&cpuBuf[0], m_framesize));
	cudaSafe(cudaMallocHost((void**)&cpuBuf[1], m_framesize));

	usleep(100*1000);
	for(int bufIdx = 0; !m_eof; bufIdx^=1){

		ssize_t ret;
		for(size_t readCnt = 0; readCnt < m_framesize; readCnt += ret){
			//PIPE_BUF == 4096 or 8192
			size_t r = std::min<size_t>(PIPE_BUF, m_framesize-readCnt);
			ret = read(pipe, (void*) (cpuBuf[bufIdx] + readCnt), r);
			if(ret == -1 && errno == EAGAIN){
				usleep(1000);
			}else if(ret == 0){
				LOG_I("Read EOF");
				m_eof = true;
				break;
			}else if(ret < 0){
				LOG_E("Read returned %ld : %s", ret, strerror(errno));
				m_eof = true;
				break;
			}
		}
		if(m_eof) break;

		for(unsigned int i=0; i< m_uploaders.size(); ++i){
			//This is an async call which hangs if previous call is not yet completed
			m_uploaders[i]->upload(cpuBuf[bufIdx]);
		}

	}

	//This serves two purposes. Notify termination,
	//and ensuring that the previous buffers are no longer used before freeing
	for(unsigned int i=0; i< m_uploaders.size(); ++i){
		m_uploaders[i]->upload(NULL);
	}
	cudaSafe(cudaFreeHost(cpuBuf[0]));
	cudaSafe(cudaFreeHost(cpuBuf[1]));
}

int ViewInput::getLeastUtilizedDevice(){
	int best = 0;
	float fastest = m_uploaders[0]->m_prevExecTime;

	for(unsigned int i=0; i<m_uploaders.size(); ++i){
		float t = m_uploaders[i]->m_prevExecTime;
		if(t < fastest){
			fastest = t;
			best = i;
		}
	}
	return best;
// 	return 0; //TODO TODO TODO WARNING WARNING :P
}

bool ViewInput::attachViewer(Viewer * v, int devId){
	Uploader * up = m_uploaders[devId].get();
	pthread_mutex_lock(&up->m_viewMutex);
	up->m_viewers.push_back(v);
	pthread_mutex_unlock(&up->m_viewMutex);
	return true;
}

bool ViewInput::detachViewer(Viewer * v, int devId){
	Uploader * up = m_uploaders[devId].get();
	pthread_mutex_lock(&up->m_viewMutex);
	uint32_t i = 0;
	
	for(i=0; i<up->m_viewers.size(); ++i){
		if(up->m_viewers[i] == v) break;
	}

	if(i == up->m_viewers.size()){
		pthread_mutex_unlock(&up->m_viewMutex);
		return false;
	}
	up->m_viewers.erase(up->m_viewers.begin() + i);
	pthread_mutex_unlock(&up->m_viewMutex);
	return true;
}

void ViewInput::terminate(){
	m_eof = true;
	//May need something more complex here
	//Won't be able to terminate atm if input has stops writing without closing pipe
}

/* --------------------- Upload ---------------- */

ViewInput::Uploader::Uploader(size_t w, size_t h, int devId){
	m_terminated = false;
	m_width = w;
	m_height = h;
	m_bufIdx = m_nextIdx = 0;
	m_devId = devId;
	m_prevExecTime = 0;
	cudaSafe(cudaSetDevice(devId));
	cudaSafe(cudaStreamCreate(&m_stream));
	cudaSafe(cudaMallocPitch((void **)&m_gpuBufY[0], &m_strideY, w, h));
	cudaSafe(cudaMallocPitch((void **)&m_gpuBufY[1], &m_strideY, w, h));
	cudaSafe(cudaMallocPitch((void **)&m_gpuBufUV[0], &m_strideUV, w/2, h*2));
	cudaSafe(cudaMallocPitch((void **)&m_gpuBufUV[1], &m_strideUV, w/2, h*2));
	pthread_mutex_init(&m_mutex, NULL);
	pthread_mutex_init(&m_viewMutex, NULL);
	pthread_cond_init(&m_cond, NULL);
	pthread_create(&m_thread, NULL, &ViewInput::Uploader::runUploaderThread, this);
}

void ViewInput::Uploader::runUploader(){

	struct timespec t1, t2;
	cudaSafe(cudaSetDevice(m_devId));
	int dbg = 0;

	while(true){
		pthread_mutex_lock(&m_mutex);
		while(!m_terminated && m_bufIdx == m_nextIdx)
			pthread_cond_wait(&m_cond, &m_mutex);
		if(m_terminated){
			pthread_mutex_unlock(&m_mutex);
			return;
		}
		cudaSafe(cudaStreamSynchronize(m_stream));
		m_bufIdx ^= 1;
		pthread_cond_signal(&m_cond);
		pthread_mutex_unlock(&m_mutex);

		pthread_mutex_lock(&m_viewMutex);

		clock_gettime(CLOCK_MONOTONIC, &t1);
		//Start all viewers asynchronously. This should produce better cuda scheduling / utilization
		//than starting and waiting for all one by one.
		//This may sacrifice a tiny bit in latency, but usually all viewers will be started before
		//the first is finished (i.e., no latency sacrifice).
		for(size_t i = 0; i<m_viewers.size(); ++i){
			m_viewers[i]->view( m_gpuBufY[m_bufIdx ^ 1], m_gpuBufUV[m_bufIdx ^ 1],
					m_strideY, m_strideUV);
		}
// 		clock_gettime(CLOCK_MONOTONIC, &t2);
// 		float t = (t2.tv_sec*1.0e6 + t2.tv_nsec*1.0e-3)
// 			- (t1.tv_sec*1.0e6 + t1.tv_nsec*1.0e-3);
// 		if(!m_devId && ++dbg % 10 == 0) fprintf(stderr, "L %7.3f\n", t/1000.0);

		//Wait for all viewers to complete
		for(size_t i = 0; i<m_viewers.size(); ++i){
			m_viewers[i]->endView();
		}
		clock_gettime(CLOCK_MONOTONIC, &t2);
		m_prevExecTime = (t2.tv_sec*1.0e6 + t2.tv_nsec*1.0e-3)
			- (t1.tv_sec*1.0e6 + t1.tv_nsec*1.0e-3);

// 		if(!m_devId && dbg % 10 == 0) fprintf(stderr, "V %7.3f\n", m_prevExecTime/1000.0);

		if(++dbg % 90 == 0) LOG_D("View [%2d]:  %9.2f", dbg, m_prevExecTime);

		pthread_mutex_unlock(&m_viewMutex);
	}
}

void ViewInput::Uploader::upload(const uint8_t * cpuBuf){
	pthread_mutex_lock(&m_mutex);
	if(cpuBuf == NULL) {
		m_terminated = true;
		pthread_cond_signal(&m_cond);
		pthread_mutex_unlock(&m_mutex);
		pthread_join(m_thread, NULL);
		return;
	}
	while(m_bufIdx != m_nextIdx)
		pthread_cond_wait(&m_cond, &m_mutex);
	
	cudaSafe(cudaSetDevice(m_devId));
	cudaSafe(cudaMemcpy2DAsync((void*)m_gpuBufY[m_bufIdx], m_strideY, cpuBuf, m_width,
				m_width, m_height, cudaMemcpyHostToDevice, m_stream));
	cudaSafe(cudaMemcpy2DAsync((void*)m_gpuBufUV[m_bufIdx], m_strideUV,
				((uint8_t*)cpuBuf+m_width*m_height), m_width/2,
				m_width/2, m_height*2, cudaMemcpyHostToDevice, m_stream));
	m_nextIdx ^= 1;
	pthread_cond_signal(&m_cond);
	pthread_mutex_unlock(&m_mutex);
}

