// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <errno.h>
#include <stdlib.h>
#include <string>
#include <deque>
#include <pthread.h>
#include <fcntl.h>
#include <boost/thread.hpp>
#include <dirent.h>
#include <sys/types.h>
#include <vector>
#include <algorithm>
#include <panoDefines.h>

std::vector <std::string> readDirectory(const std::string& path){
	std::vector <std::string> result;
	const std::string ending = ".h264";
	const std::string beginning = path.back() == '/' ? path : path + '/';
	dirent* de;
	DIR* dp;
	errno = 0;
	dp = opendir(path.c_str());
	if(dp){
		while (true) {
			errno = 0;
			de = readdir( dp );
			if (de == NULL) break;
			std::string name = std::string( de->d_name );
			if(name.length() >= ending.length() &&
					!(name.compare(name.length() - ending.length(), ending.length(), ending))){
				result.push_back( beginning + name );
			}
		}
		closedir(dp);
		std::sort( result.begin(), result.end() );
	}
	if(errno){
		LOG_E("Error reading directory: %s", strerror(errno));
	}
	return result;
}

inline bool fileExists(const char * name) {
	struct stat buffer;   
	return (stat (name, &buffer) == 0); 
}

inline bool fileExists(const std::string& name) {
	struct stat buffer;   
	return (stat (name.c_str(), &buffer) == 0); 
}

class PipeWriter {
	
	private:
		bool m_terminated;
		size_t m_combinedSize;
		pthread_mutex_t m_lock;
		pthread_cond_t m_noInput, m_full;
		std::string m_loopDir;
		std::deque<std::string> m_queue;

	public:
		PipeWriter(int pipe, std::string loopDirectory = std::string() ) :
			m_terminated(false), m_combinedSize(0), m_loopDir(loopDirectory)
	{
		pthread_cond_init(&m_noInput, NULL);
		pthread_cond_init(&m_full, NULL);
		pthread_mutex_init(&m_lock, NULL);
		boost::thread t([&,pipe]() { this->run(pipe); });
	}
		~PipeWriter(){
			pthread_mutex_lock(&m_lock);
			m_terminated = true;
			pthread_cond_signal(&m_noInput);
			pthread_mutex_unlock(&m_lock);
		}
		bool pushData(const char *data, size_t len){
			pthread_mutex_lock(&m_lock);

			if(m_queue.size() > 2){
				pthread_mutex_unlock(&m_lock);
				return false;
			}

			m_queue.push_back(std::string(data,len));
			m_combinedSize += len;
			pthread_cond_signal(&m_noInput);
			pthread_mutex_unlock(&m_lock);
			return true;
		}
		void run(int pipe){
			//If loop directory is provided, we loop through these alphabetically when there is no incoming input.
			//Otherwise, we just hang and wait for input.
			//WARN: Ensure that these files have the correct resolution!
			std::vector< std::string > files;
			if( !m_loopDir.empty() ) files = readDirectory(m_loopDir);

			bool loopDefault = files.size() != 0;

            //TODO Temporary, I wanna start in the middle of the action!
			unsigned int readFileNum = files.size() / 4;
// 			unsigned int readFileNum = 0;
			while(true){
				bool readDefault = false;
				std::string next;
				pthread_mutex_lock(&m_lock);
				if(!m_terminated && m_queue.size() == 0){
					if(loopDefault){
						readDefault = true;
					}else{
						pthread_cond_wait(&m_noInput, &m_lock);
					}
				}
				if(m_terminated) break;
				if(!readDefault){
					next = m_queue.front();
					m_queue.pop_front();
					m_combinedSize -= next.size();
				}
				pthread_mutex_unlock(&m_lock);

				if(readDefault){
					std::ifstream t(files[readFileNum]);
					next = std::string((std::istreambuf_iterator<char>(t)),
							                 std::istreambuf_iterator<char>());
					readFileNum++;
					if(readFileNum >= files.size()) readFileNum = 0;
				}

				size_t ret = write(pipe, next.data(), next.size());
				if(ret != next.size()){
					LOG_W("Error writing to pipe %d, return value %ld : %s",
							pipe, ret, strerror(errno));
				}
			}
			pthread_mutex_unlock(&m_lock);
		}
};

