// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "imgGenerator.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

void convertToJpeg(std::vector<unsigned char> &output, uint8_t * src,
        uint32_t width, uint32_t height){
	
    cv::Mat inputBGR(cv::Size(width,height),CV_8UC3, src);

    bool success = cv::imencode(".jpg", inputBGR, output);
    if(!success){
        LOG_W("cv::imEncode failed to convert to jpg");
    }
}

void ImgGenerator::pushRaw(const uint8_t *gpuImgData,
        uint32_t width, uint32_t height, uint32_t stride){

	cudaSafe(cudaSetDevice(m_devId));
    pthread_mutex_lock(&m_lock);

    //If no one has fetched the previous data yet, we should ensure the previous memcopy is complete
    if(m_state == IMG_GEN_NEW){
        cudaSafe(cudaStreamSynchronize(m_cudaStream));
    }

    //Test if we need to allocate new buffers, either due to first time running or dimension changed
    if(m_state == IMG_GEN_NONE || height != m_lastHeight || width != m_lastWidth){
        if(m_cpuBuffer) cudaSafe(cudaFreeHost(m_cpuBuffer));
        if(m_gpuBuffer) cudaSafe(cudaFree(m_gpuBuffer));
        cudaSafe(cudaMallocHost((void**)&m_cpuBuffer, (width * height * 3)));
        cudaSafe(cudaMalloc((void**)&m_gpuBuffer, (width * height * 3)));
    }

    m_lastWidth = width;
    m_lastHeight = height;

    cudaSafe(cudaMemcpy2DAsync(m_gpuBuffer, width*3, gpuImgData, stride, width*3, height,
                cudaMemcpyDeviceToDevice, m_cudaStream));
    cudaSafe(cudaStreamSynchronize(m_cudaStream));
    cudaSafe(cudaMemcpyAsync(m_cpuBuffer, m_gpuBuffer, width*height*3,
                cudaMemcpyDeviceToHost, m_cudaStream));

    m_state = IMG_GEN_NEW;
    pthread_mutex_unlock(&m_lock);
}

bool ImgGenerator::getLastJpeg(std::vector<unsigned char> &output){
    
    pthread_mutex_lock(&m_lock);

    if(m_state == IMG_GEN_NONE){
        pthread_mutex_unlock(&m_lock);
        return false;
    }

    //New image, must convert raw to jpg format
    if(m_state == IMG_GEN_NEW){

        //First, lets release the lock if old data already exists. May take a while to convert,
        //so it's better if other requests will simply return the old data.
        //If there is no old data to return, we keep the lock.
        if(m_lastGenerated.size()){
            pthread_mutex_unlock(&m_lock);
            m_state = IMG_GEN_OLD;
        }

        cudaSafe(cudaSetDevice(m_devId));
        //We don't know if the memcpy to CPU memory has completed yet. Wait for it
        cudaSafe(cudaStreamSynchronize(m_cudaStream));

        convertToJpeg(output, m_cpuBuffer, m_lastWidth, m_lastHeight); 

        //If we unlocked it earlier, we need to re-lock it now and swap the image to the new one
        if(m_lastGenerated.size()) pthread_mutex_lock(&m_lock);
        m_state = IMG_GEN_OLD;

        m_lastGenerated.resize(output.size());
        memcpy(&m_lastGenerated[0], output.data(), output.size());
    }else{
        //Simple memcpy existing generated jpeg data
        output.resize(m_lastGenerated.size());
        memcpy(&output[0], m_lastGenerated.data(), m_lastGenerated.size());
    }
    
    pthread_mutex_unlock(&m_lock);
	return true;
}

