// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <streamGenerator.h>
#include "PreviewStreamGenerator.hpp"
#include "SlaveStreamGenerator.hpp"

const unsigned int defaultPreviewWidth = 320;
const unsigned int defaultPreviewFps = 15; //If changed, not guaranteed to be exact, see where its used
const unsigned int defaultPreviewBitrate = 1000;

// const unsigned int defaultPreviewWidth = 1920;
// const unsigned int defaultPreviewFps = 30;
// const unsigned int defaultPreviewBitrate = 5000;

StreamGenerator::~StreamGenerator(){
	m_inputSrc->detachViewer(&m_viewer, m_devId);
	m_enc.terminate();
	pthread_mutex_lock(&m_previewLock);
	if(m_previewStream) delete m_previewStream;
	pthread_mutex_unlock(&m_previewLock);
}

void StreamGenerator::pushData(void * data, size_t byteCount, bool keyframe){

	std::string fullData;
	std::vector<unsigned char> frameData;

	pthread_mutex_lock(&m_slaveLock);
	for(auto it : m_slaves) it->pushData(data, byteCount, keyframe);
	pthread_mutex_unlock(&m_slaveLock);

// 	recordTime();

	std::vector<unsigned char> inData = std::vector<unsigned char>(byteCount);
	memcpy(&inData[0], data, byteCount);
	if(STREAM_ENABLE_BLOATED_MUXING){
		m_muxer.muxFrame(inData, frameData);
		size_t bytesToSend;
		if(m_frameSizeRest >= m_targetFrameSize){
			bytesToSend = 1;
			m_frameSizeRest -= m_targetFrameSize;
		}else{
			bytesToSend = m_targetFrameSize - m_frameSizeRest;
			m_frameSizeRest = 0;
		}
		inflateMKV(bytesToSend, fullData);
		fullData.append(frameData.begin(), frameData.end());
		m_frameSizeRest += (long)frameData.size();
		if(m_frameSizeRest > m_targetFrameSize*5) m_frameSizeRest = m_targetFrameSize*5;
	}else{
		m_muxer.muxFrame(inData, fullData);
	}

	pthread_mutex_lock(&m_frameLock);
	while(m_gotFrame) pthread_cond_wait(&m_condFull, &m_frameLock);

	m_currentFrame = fullData;
	m_gotFrame = true;
	pthread_mutex_unlock(&m_frameLock);

	pthread_mutex_lock(&m_cbLock);
	if(m_callback){
		m_callback();
		m_callback = NULL;
	}
	pthread_mutex_unlock(&m_cbLock);
}


std::string StreamGenerator::operator()(){
	std::string ret;
	pthread_mutex_lock(&m_frameLock);

	if(!m_headerSent){
		std::vector<unsigned char> headerData;
		getMkvHeader(headerData);
		ret = std::string(headerData.begin(), headerData.end());
		m_headerSent = true;
	}else{
		ret = m_currentFrame;
		m_gotFrame = false;
		pthread_cond_signal(&m_condFull);
	}
	pthread_mutex_unlock(&m_frameLock);

	return ret;
}

std::shared_ptr<SlaveStreamGenerator> StreamGenerator::getPreview(){
	pthread_mutex_lock(&m_previewLock);
	if(!m_previewStream){
		unsigned int fps = defaultPreviewFps;
		unsigned int interval = round((float)m_stats.fps / fps);
		fps = m_stats.fps / interval;

		unsigned int width =  defaultPreviewWidth;
		width = (width+31)&~31; //Pad to 32byte alignment
		unsigned int height = width / ((float)m_inWidth/m_inHeight);
		height = (height+7)&~7; //Pad to 8byte alignment
		
		m_previewStream = new PreviewStreamGenerator(&m_viewer, m_devId, interval,
				StreamStats(m_id, fps, width, height, defaultPreviewBitrate));
	}
	auto ret = std::make_shared<SlaveStreamGenerator>(m_previewStream, true);
	pthread_mutex_unlock(&m_previewLock);
	return ret;
}
