// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <viewEnc.h>
#include <stdexcept>
#include <map>

#define LAZY_ENCODING false

#define SET_VER(configStruct, type) {configStruct.version = type##_VER;}

const GUID *PRESET               = &NV_ENC_PRESET_LOW_LATENCY_HP_GUID;
const GUID *PROFILE              = &NV_ENC_H264_PROFILE_HIGH_GUID;
// const unsigned char CLIENT_KEY[] = { /* NB! HIDDEN FOR OPENSOURCE DISTRIBUTION */ };
GUID CLIENT_KEY_TEST             = { 0x0, 0x0, 0x0, { 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0 } };
//Creating a cuda context takes a lot of memory, but there seems to sometimes be an upper limit on
//number of encoders per context. This upper limit depends on the resolution and card :/
//Numbers are WIDELY DIFFERENT for a gpu that is running X. May need to edit this.
const int ENCS_PER_CUDA_CONTEXT  = 16;

//Session wide parameters, shared between multiple instances
static NV_ENCODE_API_FUNCTION_LIST * nvencApi = NULL;

//Object responsible for allocating cuda contexts. They aren't destroyed, but "re-used"
//Can handle multiple devices.
//Multiple encoders can use the same cuda context, but at some point they will run out of memory.
//If we have too few encoders per cuda context, we waste A TON of memory (an actual problem, wtf?)
//and may run out of device memory. If we run out of memory, we segfault. YAY! (TODO)
struct CudaContextAllocator {
	std::map<unsigned int, std::map<CUcontext, unsigned int> > cuDevices;
	pthread_mutex_t lock;
	CudaContextAllocator(){
		pthread_mutex_init(&lock, NULL);
	}
	CUcontext getContext(unsigned int cuDeviceID){
		CUcontext cudaContext;

		pthread_mutex_lock(&lock);
		std::map<CUcontext, unsigned int> *contexts;
		try{
			contexts = &(cuDevices.at(cuDeviceID));
		}catch(const std::out_of_range& oor){
			cuDevices[cuDeviceID] = std::map<CUcontext, unsigned int>();
			contexts = &(cuDevices[cuDeviceID]);
		}

		bool set = false;
		for( auto &it : *contexts ){
			if(it.second < ENCS_PER_CUDA_CONTEXT){
				set = true;
				cudaContext = it.first;
				it.second++;
				break;
			}
		}
		if(!set){
			CUdevice cuDevice = 0;
			CUcontext cuContextCurr;
			checkCudaErrors( cuInit(0) ); //Potentially very slow call
			checkCudaErrors( cuDeviceGet(&cuDevice, cuDeviceID) );
			checkCudaErrors( cuCtxCreate(&cudaContext, 0, cuDevice) );
			checkCudaErrors( cuCtxPopCurrent(&cuContextCurr) );

			(*contexts)[cudaContext] = 1;
		}

		pthread_mutex_unlock(&lock);

		return cudaContext;
	}
	void releaseContext(CUcontext ctx){
		pthread_mutex_lock(&lock);
		//This loop will never ever be long, easier to just loop through everything.
		for( auto &dev : cuDevices ){
			for( auto &it : dev.second ){
				if(it.first == ctx){
					it.second--;
					break;
				}
			}
		}
		pthread_mutex_unlock(&lock);
	}
};

static CudaContextAllocator cudaContextAlloc = CudaContextAllocator();

//Create a cuda context and api reference. These have the lifetime of program execution
//NB! Not threadsafe, and exits upon failure
static CUcontext createRuntimeSession(unsigned int cuDeviceID) {

	CUcontext cudaContext = cudaContextAlloc.getContext(cuDeviceID);

	if(nvencApi) return cudaContext;//Already created

	//Create nvenc api instance
	nvencApi = new NV_ENCODE_API_FUNCTION_LIST;
	memset(nvencApi, 0, sizeof(NV_ENCODE_API_FUNCTION_LIST));
	nvencApi->version = NV_ENCODE_API_FUNCTION_LIST_VER;
	checkNVENCErrors( NvEncodeAPICreateInstance(nvencApi) );
	return cudaContext;
}

/* ----------------Public member functions ------------------- */
ViewEnc::~ViewEnc(){
	this->terminate(); //If already called it will just return
	cudaContextAlloc.releaseContext(m_cudaContext);
	LOG_D("ViewEnc terminated");
}

void ViewEnc::terminate(){
	
	if(!m_initialized) return;

	//Signal flushing of the encoding and clean up encoding thread
	pthread_mutex_lock(&m_outputLock);	
	pthread_mutex_lock(&m_inputLock);	
	m_terminateEncoding = true;
	pthread_cond_broadcast(&m_condNoInput);
	pthread_cond_signal(&m_condNoFreeOutput);
	pthread_mutex_unlock(&m_inputLock);	
	pthread_mutex_unlock(&m_outputLock);	
	pthread_join(m_encoderThread, NULL);

	//Clean up writing thread
	pthread_mutex_lock(&m_outputLock);	
	m_terminateWriting = true;
	pthread_cond_signal(&m_condNoOutput);
	pthread_mutex_unlock(&m_outputLock);	
	pthread_join(m_writerThread, NULL);

	//Free buffers
    cuCtxPushCurrent(m_cudaContext);
	for(int i=0; i<NUM_INPUT_ENCODING_BUFFERS; ++i){
		checkNVENCErrors( nvencApi->nvEncUnregisterResource(m_encoder, m_buffers[i].regPtr) );
		checkCudaErrors( cuMemFree((CUdeviceptr) m_buffers[i].devPtr) );
	}
    CUcontext cuContextCurr;
	cuCtxPopCurrent(&cuContextCurr);

	//Free bitstreams
	for(int i=0; i<NUM_OUTPUT_ENCODING_BUFFERS; ++i){
		checkNVENCErrors( nvencApi->nvEncDestroyBitstreamBuffer(m_encoder, m_bitstreams[i]));
	}

	//Terminate encoder
	checkNVENCErrors( nvencApi->nvEncDestroyEncoder(m_encoder) );

	m_initialized = false;
}

bool ViewEnc::init(int fps, size_t width, size_t height, int devId,
		unsigned int rate, unsigned int gopLength){
	//Set dimensions
	m_width = (width+31)&~31; //Pad to 32byte alignment
	m_height = (height+7)&~7; //Pad to 8byte alignment
	m_fps = fps;

	//Create runtime (if not already created).
	//TODO Assumption that we are using cuda device 0, may not always be the case
	m_cudaContext = createRuntimeSession(devId);

	initializeEncoder(fps, rate, gopLength);

	allocateBuffers();

	m_accumulatedRate = 0;
	m_currentBitrate = 0;

	m_terminateEncoding = false;
	m_terminateWriting = false;

	m_forceKeyframe = true;
	m_inHeadIdx = 0;
	m_inTailIdx = 0;
	m_outHeadIdx = 0;
	m_outTailIdx = 0;
	pthread_mutex_init(&m_inputLock, NULL);
	pthread_mutex_init(&m_outputLock, NULL);
	pthread_mutex_init(&m_outStreamLock, NULL);
	pthread_cond_init(&m_condNoInput, NULL);
	pthread_cond_init(&m_condNoOutput, NULL);
	pthread_cond_init(&m_condNoFreeOutput, NULL);
	pthread_create(&m_encoderThread, NULL, launchEncoderThread, this);
	pthread_create(&m_writerThread, NULL, launchWriterThread, this);

	m_initialized = true;
	return true;
}

void ViewEnc::attachOutputStream(OutStreamIface * stream){
	pthread_mutex_lock(&m_outStreamLock);
	m_outStreams.push_back(stream);
	pthread_mutex_unlock(&m_outStreamLock);
}

void ViewEnc::detatchOutputStream(OutStreamIface * stream){
	pthread_mutex_lock(&m_outStreamLock);
	m_outStreams.erase(
			std::remove(m_outStreams.begin(), m_outStreams.end(), stream),
			m_outStreams.end());
	pthread_mutex_unlock(&m_outStreamLock);
}

const FrameBuffer * ViewEnc::getNextBuffer(){
	pthread_mutex_lock(&m_inputLock);
	
	if(m_terminateEncoding){
		pthread_mutex_unlock(&m_inputLock);
		return NULL;
	}

	//Lazy encoding. If no one waits for the stream, no need to make it
	if(LAZY_ENCODING && m_outStreams.size()==0){
		pthread_mutex_unlock(&m_inputLock);
		return NULL;
	}

	int idx = m_inHeadIdx % NUM_INPUT_ENCODING_BUFFERS;
	if((m_inHeadIdx - m_inTailIdx) == NUM_INPUT_ENCODING_BUFFERS){
		//No more available buffers
		pthread_mutex_unlock(&m_inputLock);
		LOG_D("[%12p]: Skipped frame",this);
		return NULL;
	}

	pthread_mutex_unlock(&m_inputLock);

	m_buffers[idx].width = m_width;
	m_buffers[idx].height = m_height;
	return (const FrameBuffer *) &m_buffers[idx];
}

const FrameBuffer * ViewEnc::getNextBufferBlocking(){
	pthread_mutex_lock(&m_inputLock);

	//Lazy encoding. If no one waits for the stream, no need to make it
	if(LAZY_ENCODING && m_outStreams.size()==0){
		pthread_mutex_unlock(&m_inputLock);
		return NULL;
	}
	
	while(!m_terminateEncoding && (m_inHeadIdx - m_inTailIdx) == NUM_INPUT_ENCODING_BUFFERS){
		pthread_cond_wait(&m_condNoInput, &m_inputLock);
	}
	if(m_terminateEncoding){
		pthread_mutex_unlock(&m_inputLock);
		return NULL;
	}

	int idx = m_inHeadIdx % NUM_INPUT_ENCODING_BUFFERS;
	pthread_mutex_unlock(&m_inputLock);

	m_buffers[idx].width = m_width;
	m_buffers[idx].height = m_height;
	return (const FrameBuffer *) &m_buffers[idx];
}

void ViewEnc::bufferReady(){
	pthread_mutex_lock(&m_inputLock);
	m_inHeadIdx++;
	pthread_cond_signal(&m_condNoInput);
	pthread_mutex_unlock(&m_inputLock);
}

/* --------------- Private functions ------------------ */

void ViewEnc::runEncoderThread(){
// 	struct timespec t1, t2;
// 	int dbg = 0;

	NV_ENC_PIC_PARAMS picParam;
	NV_ENC_MAP_INPUT_RESOURCE mappedBuffer;
	int eOutIdx = 0;
	bool endOfStream = false;
	while(true){

		int idx;
		if(!endOfStream){
			//Wait for input buffer
			pthread_mutex_lock(&m_inputLock);
			while(m_inHeadIdx == m_inTailIdx && !m_terminateEncoding){
				pthread_cond_wait(&m_condNoInput, &m_inputLock);
			}
			idx = m_inTailIdx % NUM_INPUT_ENCODING_BUFFERS;
			endOfStream = m_terminateEncoding;
			pthread_mutex_unlock(&m_inputLock);
		}

// 		clock_gettime(CLOCK_MONOTONIC, &t1);

		//Set per-frame parameters
		memset(&picParam, 0, sizeof(NV_ENC_PIC_PARAMS));
		SET_VER(picParam, NV_ENC_PIC_PARAMS);
		if(endOfStream){
			//Signal end of encoding
			picParam.encodePicFlags = NV_ENC_PIC_FLAG_EOS;
		}else{
			memcpy(&picParam.rcParams,&m_encInitParams.encodeConfig->rcParams,
					sizeof(picParam.rcParams));
			memcpy(&picParam.codecPicParams,&m_encInitParams.encodeConfig->encodeCodecConfig,
					sizeof(picParam.codecPicParams));
			picParam.inputWidth = m_width;
			picParam.inputHeight = m_height;
			picParam.inputPitch = m_buffers[idx].pitch;
			picParam.bufferFmt = NV_ENC_BUFFER_FORMAT_NV12_PL;
			picParam.pictureStruct = NV_ENC_PIC_STRUCT_FRAME;
			picParam.outputBitstream = m_bitstreams[eOutIdx];
			picParam.codecPicParams.h264PicParams.refPicFlag = 1;
			picParam.codecPicParams.h264PicParams.displayPOCSyntax = m_frameInGop * 2;
			picParam.encodePicFlags |= NV_ENC_PIC_FLAG_OUTPUT_SPSPPS;

			if(m_forceKeyframe || m_frameInGop % m_gop == 0){
				m_forceKeyframe = false;
				picParam.pictureType = NV_ENC_PIC_TYPE_IDR;
			}else{
				picParam.pictureType = NV_ENC_PIC_TYPE_P;
			}
			if(++m_frameInGop == m_gop) m_frameInGop = 0;

			//Map the buffer to nvenc
			memset(&mappedBuffer, 0, sizeof(NV_ENC_MAP_INPUT_RESOURCE));
			SET_VER(mappedBuffer, NV_ENC_MAP_INPUT_RESOURCE);
			mappedBuffer.registeredResource = m_buffers[idx].regPtr;
			mappedBuffer.mappedBufferFmt = NV_ENC_BUFFER_FORMAT_NV12_PL;
			checkNVENCErrors( nvencApi->nvEncMapInputResource(m_encoder, &mappedBuffer) );
			picParam.inputBuffer = mappedBuffer.mappedResource;
		}

		NVENCSTATUS result = nvencApi->nvEncEncodePicture(m_encoder, &picParam);
		if(result == NV_ENC_SUCCESS){
			if(!endOfStream){
				pthread_mutex_lock(&m_outputLock);
				m_outHeadIdx = m_inTailIdx + 1;
				pthread_cond_signal(&m_condNoOutput);
				pthread_mutex_unlock(&m_outputLock);
			}
		}else if(result == NV_ENC_ERR_NEED_MORE_INPUT){
			//do nothing
		}else{
			LOG_E("nvEncEncodePicture failed, returned 0x%x\n",result);
			exit(EXIT_FAILURE);
		}

		if(endOfStream) return;

		checkNVENCErrors( nvencApi->nvEncUnmapInputResource(m_encoder, mappedBuffer.mappedResource) );

// 		clock_gettime(CLOCK_MONOTONIC, &t2);
// 		float t = (t2.tv_sec*1.0e6 + t2.tv_nsec*1.0e-3)
// 			- (t1.tv_sec*1.0e6 + t1.tv_nsec*1.0e-3);
// 		if(++dbg % 30 == 0) fprintf(stderr, "E %7.3f\n", t/1000.0);

		pthread_mutex_lock(&m_inputLock);
		m_inTailIdx++;
		pthread_cond_signal(&m_condNoInput);
		pthread_mutex_unlock(&m_inputLock);

		eOutIdx = (eOutIdx + 1) % NUM_OUTPUT_ENCODING_BUFFERS;

		//Wait for available output buffer
		pthread_mutex_lock(&m_outputLock);
		while((m_outHeadIdx - m_outTailIdx ) == NUM_OUTPUT_ENCODING_BUFFERS && !m_terminateEncoding){
			pthread_cond_wait(&m_condNoFreeOutput, &m_outputLock);
		}
		endOfStream = m_terminateEncoding;
		pthread_mutex_unlock(&m_outputLock);
	}
}

void ViewEnc::runWriterThread(){
// 	struct timespec t1, t2;
// 	int dbg = 0;

	while(true){

		//Wait for buffer
		pthread_mutex_lock(&m_outputLock);
		while(m_outHeadIdx == m_outTailIdx  && !m_terminateWriting){
			pthread_cond_wait(&m_condNoOutput, &m_outputLock);
		}
		if(m_terminateWriting){
			pthread_mutex_unlock(&m_outputLock);
			for(unsigned int i=0; i<m_outStreams.size(); ++i)
				m_outStreams[i]->endStream();
			return;
		}
		pthread_mutex_unlock(&m_outputLock);

// 		clock_gettime(CLOCK_MONOTONIC, &t1);

		NV_ENC_LOCK_BITSTREAM bitstreamLock;
		memset(&bitstreamLock, 0, sizeof(NV_ENC_LOCK_BITSTREAM));
		SET_VER(bitstreamLock, NV_ENC_LOCK_BITSTREAM);
		bitstreamLock.outputBitstream = m_bitstreams[m_outTailIdx % NUM_OUTPUT_ENCODING_BUFFERS];

// 		checkNVENCErrors( nvencApi->nvEncLockBitstream(m_encoder, &bitstreamLock) );
		NVENCSTATUS result;
		result = nvencApi->nvEncLockBitstream(m_encoder, &bitstreamLock);
		if(result != NV_ENC_SUCCESS){
			LOG_W("NVENC lock bitstream failed. code=%d(%s)",
					static_cast<unsigned int>(result), _nvencGetErrorEnum(result));
		}else{

// 			if(bitstreamLock.pictureType == NV_ENC_PIC_TYPE_IDR) LOG_D("KEY");
			size_t byteCount = bitstreamLock.bitstreamSizeInBytes;
			pthread_mutex_lock(&m_outStreamLock);
			for(unsigned int i=0; i<m_outStreams.size(); ++i)
				m_outStreams[i]->pushData(bitstreamLock.bitstreamBufferPtr,
						byteCount,
						bitstreamLock.pictureType == NV_ENC_PIC_TYPE_IDR);
			pthread_mutex_unlock(&m_outStreamLock);

			//Calculate moving average bitrate
			size_t prevValue;
			if(m_prevFrameSizes.size() == (size_t)m_fps*5){
				prevValue = m_prevFrameSizes.front();
				m_prevFrameSizes.pop_front();
			}else prevValue = 0;
			m_prevFrameSizes.push_back(byteCount);
			m_accumulatedRate = m_accumulatedRate + byteCount - prevValue;
			m_currentBitrate = m_accumulatedRate / (125.0 * m_prevFrameSizes.size()) * m_fps;

			result = nvencApi->nvEncUnlockBitstream(
					m_encoder,bitstreamLock.outputBitstream);
			if(result != NV_ENC_SUCCESS){
				LOG_W("NVENC unlock bitstream failed. code=%d(%s)",
						static_cast<unsigned int>(result), _nvencGetErrorEnum(result));
			}
		}

// 		clock_gettime(CLOCK_MONOTONIC, &t2);
// 		float t = (t2.tv_sec*1.0e6 + t2.tv_nsec*1.0e-3)
// 			- (t1.tv_sec*1.0e6 + t1.tv_nsec*1.0e-3);
// 		if(++dbg % 30 == 0) fprintf(stderr, "D %7.3f\n", t/1000.0);

		pthread_mutex_lock(&m_outputLock);
		m_outTailIdx++;
		pthread_cond_signal(&m_condNoFreeOutput);
		pthread_mutex_unlock(&m_outputLock);
	}
}

void * ViewEnc::launchEncoderThread(void *p){
	((ViewEnc*)p)->runEncoderThread();
	pthread_exit(0);
}

void * ViewEnc::launchWriterThread(void *p){
	((ViewEnc*)p)->runWriterThread();
	pthread_exit(0);
}

void ViewEnc::initializeEncoder(int fps, unsigned int bitrate, unsigned int gopLength){
	int fpsNum = fps * 1000;
	int fpsDen = 1000;
	//Set up encoding session
    NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS sessionParams = {0};
    memset(&sessionParams, 0, sizeof(NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS));
    SET_VER(sessionParams, NV_ENC_OPEN_ENCODE_SESSION_EX_PARAMS);
    sessionParams.apiVersion = NVENCAPI_VERSION;
    sessionParams.clientKeyPtr = &CLIENT_KEY_TEST;
//     sessionParams.clientKeyPtr = (GUID *) CLIENT_KEY;
	sessionParams.device = reinterpret_cast<void *>(m_cudaContext);
	sessionParams.deviceType = NV_ENC_DEVICE_TYPE_CUDA;

    checkNVENCErrors( nvencApi->nvEncOpenEncodeSessionEx(&sessionParams, &m_encoder) );

	//Set up initialization parameters
    memset(&m_encInitParams, 0, sizeof(NV_ENC_INITIALIZE_PARAMS));
    SET_VER(m_encInitParams, NV_ENC_INITIALIZE_PARAMS);
	memcpy(&m_encInitParams.encodeGUID, &NV_ENC_CODEC_H264_GUID, sizeof(GUID));

	//Preset and profile
	memcpy(&m_encInitParams.presetGUID, PRESET, sizeof(GUID));
    memset(&m_presetConfig, 0, sizeof(NV_ENC_PRESET_CONFIG));
    SET_VER(m_presetConfig, NV_ENC_PRESET_CONFIG);
    SET_VER(m_presetConfig.presetCfg, NV_ENC_CONFIG);
	checkNVENCErrors( nvencApi->nvEncGetEncodePresetConfig(m_encoder, m_encInitParams.encodeGUID,
				m_encInitParams.presetGUID, &m_presetConfig) );
	m_encInitParams.encodeConfig = &m_presetConfig.presetCfg;
	memcpy(&m_encInitParams.encodeConfig->profileGUID, PROFILE, sizeof(GUID));

	//General parameters
	m_encInitParams.encodeWidth = m_width;
	m_encInitParams.encodeHeight = m_height;
	m_encInitParams.maxEncodeWidth = m_width;
	m_encInitParams.maxEncodeHeight = m_height;
	m_encInitParams.darWidth = m_width;
	m_encInitParams.darHeight = m_height;
	m_encInitParams.frameRateNum = fpsNum;
	m_encInitParams.frameRateDen = fpsDen;
	m_encInitParams.enablePTD = 0;
// 	m_encInitParams.encodeConfig->encodeCodecConfig.h264Config.repeatSPSPPS = 1;
	if(bitrate) m_encInitParams.encodeConfig->rcParams.averageBitRate = bitrate;
	if(gopLength) m_encInitParams.encodeConfig->gopLength = gopLength;
	else m_encInitParams.encodeConfig->gopLength = NVENC_INFINITE_GOPLENGTH;
	m_gop = m_encInitParams.encodeConfig->gopLength;
	m_frameInGop = 0;

	checkNVENCErrors( nvencApi->nvEncInitializeEncoder(m_encoder, &m_encInitParams) );
}

void ViewEnc::allocateBuffers(){
	//Allocate cuda input buffers
    CUcontext cuContextCurr;
    cuCtxPushCurrent(m_cudaContext);
	for(int i=0; i<NUM_INPUT_ENCODING_BUFFERS; ++i){
		NV_ENC_REGISTER_RESOURCE buffer;
		CUdeviceptr devPtrDevice;

		memset(&buffer, 0, sizeof(NV_ENC_REGISTER_RESOURCE));
		SET_VER(buffer, NV_ENC_REGISTER_RESOURCE);

		buffer.resourceType = NV_ENC_INPUT_RESOURCE_TYPE_CUDADEVICEPTR;
		buffer.width = m_width;
		buffer.height = m_height;

		checkCudaErrors( cuMemAllocPitch(&devPtrDevice, (size_t*)&buffer.pitch,
					buffer.width, buffer.height * 3 / 2, 16) );
		checkCudaErrors( cuMemsetD2D8( devPtrDevice, buffer.pitch, 128,
					buffer.width, buffer.height * 3 / 2) );

		buffer.resourceToRegister = (void*) devPtrDevice;
		checkNVENCErrors( nvencApi->nvEncRegisterResource(m_encoder, &buffer) );

		//Fill our own buffer struct
		m_buffers[i].devPtr = buffer.resourceToRegister;
		m_buffers[i].pitch = buffer.pitch;
		m_buffers[i].idx = i;
		m_buffers[i].regPtr = buffer.registeredResource;
	}
	cuCtxPopCurrent(&cuContextCurr);

	//Allocate bitstream output buffers
	for(int i=0; i<NUM_OUTPUT_ENCODING_BUFFERS; ++i){
		NV_ENC_CREATE_BITSTREAM_BUFFER bitstream;
		memset(&bitstream, 0, sizeof(NV_ENC_CREATE_BITSTREAM_BUFFER));
		SET_VER(bitstream, NV_ENC_CREATE_BITSTREAM_BUFFER);
        bitstream.size = 1024*512;
        bitstream.memoryHeap = NV_ENC_MEMORY_HEAP_SYSMEM_CACHED;

        checkNVENCErrors( nvencApi->nvEncCreateBitstreamBuffer(m_encoder, &bitstream) );
		m_bitstreams[i] = bitstream.bitstreamBuffer;
	}
}


