// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 


#include <stdlib.h>
#include <fcntl.h>
#include <signal.h>
#include <memory>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <unordered_map>

#define BOOST_NETWORK_NO_LIB
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/regex.hpp>
#include <boost/network/protocol/http/server.hpp>
#include <boost/network/utils/thread_pool.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <boost/thread/mutex.hpp>
#include <boost/lexical_cast.hpp>

#include <utils.h>

#include "PostReader.hpp"
#include "EQGeneratorStream.hpp"
#include "H264StreamGenerator.hpp"
#include "PipeWriter.hpp"

namespace http = boost::network::http;
namespace utils = boost::network::utils;

struct HTTPHandler {

	typedef http::async_server<HTTPHandler> server;

	ViewInput * m_inputSource;
	PipeWriter m_pipeWriter;

	std::map<boost::regex, boost::function<void(server::request const &,
			server::connection_ptr, boost::smatch &)> > m_sites;
	std::map<std::string, std::shared_ptr<ViewGenerator>> m_streams;
	pthread_mutex_t m_streamLock;

	HTTPHandler(int pipe) : m_pipeWriter(PipeWriter(pipe)) {
		m_sites[boost::regex("^/$")] = 
			boost::bind(&HTTPHandler::index, this, _1, _2, _3);
		m_sites[boost::regex("^/video/([[:xdigit:]]+)$")] = 
			boost::bind(&HTTPHandler::videoPost, this, _1, _2, _3);
		m_sites[boost::regex("^/stream/([[:xdigit:]]+)$")] = 
			boost::bind(&HTTPHandler::videoGet, this, _1, _2, _3);
		m_sites[boost::regex("^/path/([[:xdigit:]]+)/([[:xdigit:]]+)$")] = 
			boost::bind(&HTTPHandler::pathPost, this, _1, _2, _3);
		m_sites[boost::regex("^/terminate/([[:xdigit:]]+)$")] = 
			boost::bind(&HTTPHandler::terminateRequest, this, _1, _2, _3);
		m_sites[boost::regex("^/jpeg/([[:xdigit:]]+)$")] = 
			boost::bind(&HTTPHandler::jpeg, this, _1, _2, _3);

		pthread_mutex_init(&m_streamLock, NULL);
	}

	void operator()(server::request const &request,
			server::connection_ptr conn) {
		LOG_D("REQUEST: %s", request.destination.c_str());
		for (auto &it : m_sites) {
			boost::smatch what;
			if (boost::regex_match(request.destination, what, it.first)) {
				it.second(request, conn, what);
				return;
			}
		}

		simplePage(conn, server::connection::not_found, "404");
	}

	void simplePage(server::connection_ptr conn,
			server::connection::status_t status, std::string s) {
		std::vector<server::response_header> cheaders = {
			{ "Connection", "close" }, { "Content-Type", "text/html" },
		};

		conn->set_status(status);
		conn->set_headers(
				boost::make_iterator_range(cheaders.begin(), cheaders.end()));
		conn->write(s);
	}

	void simpleResponse(server::connection_ptr conn,
			server::connection::status_t status) {
		std::vector<server::response_header> cheaders = {
			{ "Connection", "close" },
		};
		conn->set_status(status);
		conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));
	}

	void index( server::connection_ptr conn ){
		std::ifstream f("html/index_post.html");
		std::string html((std::istreambuf_iterator<char>(f)),
				std::istreambuf_iterator<char>());
		simplePage(conn, server::connection::ok, html);
	}

	void index(server::request const &request, server::connection_ptr conn, boost::smatch &matches){
		index(conn);
	}

	void videoGet(server::request const &request,
			server::connection_ptr conn, boost::smatch &matches){
		if(request.method != "GET") {
			LOG_W("Received non-get data on /stream/ url");
			simplePage(conn, server::connection::not_found, "404");
			return;
		}

		std::string pathID = matches[1];

		pthread_mutex_lock(&m_streamLock);
		std::shared_ptr<ViewGenerator> stream;
		try{ stream = this->m_streams.at(pathID);
		}catch(std::out_of_range e) { stream = NULL; }
		pthread_mutex_unlock(&m_streamLock);

		if(!stream){
			LOG_W("Received GET request on unknown pathID %s", pathID.c_str());
			simplePage(conn, server::connection::not_found, "404");
			return;
		}

		auto s = std::make_shared<H264StreamGenerator>(stream);
		makeGeneratorStream<EQGeneratorStreamConfig::NO_HTTP_CHUNKING>(conn, s)->run();

		std::vector<server::response_header> cheaders = {
			{ "Connection", "close" },
		};
		conn->set_status(server::connection::ok);
		conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));

	}

	void videoPost(server::request const &request,
			server::connection_ptr conn, boost::smatch &matches){
		if(request.method != "POST") {
			LOG_W("Received non-post data on /video/ url");
			simplePage(conn, server::connection::not_found, "404");
			return;
		}
		std::string vidID = matches[1];//Not used

		auto reader = boost::make_shared<PostReader<server>>(
				request, conn, [conn, request, this, vidID]
				(bool err, std::unordered_map<std::string, PostReaderData> &data) {

				if(err) {
					this->simplePage(conn, server::connection::ok, 
						"AYAYAYA, got some errors while parsing");
				} else {
					PostReaderData content;
					content = data["file"];

					if(content.p != 0) {
						while(!m_pipeWriter.pushData(content.p, content.l)) usleep(500000);
					} else {
						LOG_W("Received empty video-post");
					}
				}
				simplePage(conn, server::connection::ok, "");
			}
		);

		if((*reader)()) {
			return;
		} else {
			simplePage(conn, server::connection::ok, "Error");
		}
	}

	void jpeg(server::request const &request,
			server::connection_ptr conn, boost::smatch &matches){

		std::string id = matches[1];

		pthread_mutex_lock(&m_streamLock);
		std::shared_ptr<ViewGenerator> stream;
		try{ stream = this->m_streams.at(id);
		}catch(std::out_of_range e) { stream = NULL; }
		pthread_mutex_unlock(&m_streamLock);

		if(stream){
			std::vector<unsigned char> jpg;
			if(stream->getJpeg(jpg)){
				std::vector<server::response_header> cheaders = {
					{ "Connection", "close" },
					{ "Content-Type", "image/jpeg" },
					{ "Cache-Control", "no-cache, no-store, must-revalidate" },
					{ "Pragma", "no-cache" },
					{ "Expires", "0" },
					{ "Content-Type", "image/jpeg" },
				};
				conn->set_status(server::connection::ok);
				conn->set_headers(boost::make_iterator_range(cheaders.begin(), cheaders.end()));
				conn->write(std::string(jpg.begin(), jpg.end()));
			}else{
				simplePage(conn, server::connection::not_found, "Temporarily unavailable");
			}
		}else{
			simplePage(conn, server::connection::not_found, "Unknown stream");
		}
	}

	void parsePathPostData(std::unordered_map<std::string, PostReaderData> &data, 
			unsigned int &width, unsigned int &height,
			float &previewScale, unsigned int &bitrate){
		PostReaderData content;

		//Set some defaults, then override them if possible
		width = 1920;
		height = 1080;
		previewScale = 30.0f;
		bitrate = 0;

		content = data["width"];
		if(content.p != 0) {
			try{
				width = boost::lexical_cast<unsigned int>(std::string(content.p,content.l));
			} catch( boost::bad_lexical_cast const& ) { /* Stitck to default */ }
		}

		content = data["height"];
		if(content.p != 0) {
			try{
				height = boost::lexical_cast<unsigned int>(std::string(content.p,content.l));
			} catch( boost::bad_lexical_cast const& ) { /* Stitck to default */ }
		}

		content = data["bitrate"];
		if(content.p != 0) {
			try{
				bitrate = boost::lexical_cast<unsigned int>(std::string(content.p,content.l));
			} catch( boost::bad_lexical_cast const& ) { /* Stitck to default */ }
		}

		content = data["preview"];
		if(content.p != 0) {
			try{
				previewScale = boost::lexical_cast<float>(std::string(content.p,content.l));
			} catch( boost::bad_lexical_cast const& ) { /* Stitck to default */ }
		}

		previewScale /= 100.0f;
	}

	void terminateRequest(server::request const &request,
			server::connection_ptr conn, boost::smatch &matches){

		std::string id = matches[1];

		std::shared_ptr<ViewGenerator> stream;
		pthread_mutex_lock(&m_streamLock);
		try{
			stream = this->m_streams.at(id);
			//Erase it, now that we know it exists
			m_streams.erase(id);
		}catch(std::out_of_range e) { stream = NULL; }
		pthread_mutex_unlock(&m_streamLock);

		if(stream){
			simplePage(conn, server::connection::ok, "");
		}else{
			simplePage(conn, server::connection::not_found, "Unknown stream");
		}
	}

	void pathPost(server::request const &request,
			server::connection_ptr conn, boost::smatch &matches){
		if(request.method != "POST") {
			LOG_W("Received non-post data on /path/ url");
			return;
		}

		std::string vidID = matches[1];
		std::string id = matches[2];

		// Create a reader with will read the HTTP post body from the connection.
		//  It will then run the callback as demonstrated below.
		auto reader = boost::make_shared<PostReader<server>>(
				request, conn, [conn, request, this, id]
				(bool err, std::unordered_map<std::string, PostReaderData> &data) {

				if(err) {
					this->simplePage(conn, server::connection::ok, 
						"AYAYAYA, got some errors while parsing");
				} else {
					
					pthread_mutex_lock(&m_streamLock);
					std::shared_ptr<ViewGenerator> stream;
					try{ stream = this->m_streams.at(id);
					}catch(std::out_of_range e) { stream = NULL; }
					pthread_mutex_unlock(&m_streamLock);

					if(!stream){
						//First time seeing this pid, need to create the stream

						unsigned int width, height, bitrate;
						float previewScale;
						int fps = PANORAMA_FPS;
						parsePathPostData(data, width, height, previewScale, bitrate);

						stream = std::make_shared<ViewGenerator>(id, m_inputSource,
								width, height, previewScale, fps, bitrate);

						LOG_D("Starting stream: %s %dx%d (%.2fMbit/s, preview=%f)",
								id.data(), width, height, bitrate/1000.0f, previewScale);

						pthread_mutex_lock(&m_streamLock);
						m_streams[id] = stream;
						pthread_mutex_unlock(&m_streamLock);
					}

					PostReaderData content;
					content = data["path"];
					if(content.p != 0) {
						std::string contentData(content.p, content.l);
						stream->applyCommandData(contentData);
					} else {
						LOG_W("Received empty path-post");
					}
					simpleResponse(conn, server::connection::ok);
				}
			}
		);

		if(! (*reader)()) {
			simplePage(conn, server::connection::ok, "Error");
		}
	}

};

int main(int argc, char ** argv){

	if(argc < 2){
		LOG_E("Incorrect number of arguments. give me ffmpegs input-pipe!");
		return -1;
	}
	int fd = open(argv[1], O_WRONLY);
	if(fd < 0){
		LOG_E("Failed to open pipe for writing: %s", argv[1]);
		exit(EXIT_FAILURE);
	}

	//Ignore broken pipes, handled through return of calls instead
	signal(SIGPIPE,SIG_IGN);   

	ViewInput inputstream = ViewInput(PANORAMA_WIDTH,PANORAMA_HEIGHT);

	HTTPHandler handler = HTTPHandler(fd);
	http::async_server<HTTPHandler>::options options(handler);
	
	handler.m_inputSource = &inputstream;

	std::stringstream sport;
	if(argc > 2) sport << argv[2];
	else sport << "8089";

	options.address("0.0.0.0").port(sport.str())
		.io_service(boost::make_shared<boost::asio::io_service>())
		.thread_pool(boost::make_shared<boost::network::utils::thread_pool>(8))
		.reuse_address(true);

	//Start async server
	http::async_server<HTTPHandler> instance(options);
	boost::thread ht([&]() { instance.run(); });

	LOG_D("SERVER STARTED...");

	int infd;
	if(argc > 3){
		infd = open(argv[2], O_RDONLY);
		if(infd < 0){
			LOG_E("Failed to open pipe: %s", argv[3]);
			exit(EXIT_FAILURE);
		}
	}else infd = STDIN_FILENO;
	//Run eternal loop of reading input
	inputstream.runReader(infd);

	exit(EXIT_SUCCESS);
}
