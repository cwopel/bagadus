// AUTHOR(s): Martin Alexander Wilhelmsen,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <memory>

#include <boost/network/protocol/http/server.hpp>
#include <boost/network/utils/thread_pool.hpp>
#include <boost/program_options.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/function.hpp>
#include <boost/bind.hpp>

#include <boost/generator_iterator.hpp>
#include <boost/uuid/sha1.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/ostream_iterator.hpp>
#include <boost/archive/iterators/transform_width.hpp>

template <class server>
class Websocket : public boost::enable_shared_from_this<Websocket<server>> {
	public:
		using MessageHandler = boost::function<bool(Websocket<server> *ws, std::string&)>;

	private:
		typename server::request req_;
		typename server::connection_ptr conn_;
		MessageHandler handler_;

		std::string incoming_;

		bool process() {

			while (1) {
				if (incoming_.size() < 2) {
					return true;
				}

				unsigned int headerSize = 1;
				unsigned char byte1 = incoming_[0];
				if (byte1 != 0x81) {
					return false;
				}

				unsigned char byte2 = incoming_[1];
				unsigned char isMasked = !!(byte2 & 0x80);
				unsigned char len = byte2 & 0x7F;

				headerSize++;

				// longer frames not supported yet
				if (len == 127) { 
					return false;
				}

				// not enough data yet
				if (incoming_.size() - headerSize < len) return true;

				// enough when counting the mask too?
				if (isMasked && incoming_.size() - (headerSize + 4) < len) return true;

				unsigned char maskKey[4];
				if(isMasked) {
					for(int i = 0; i < 4; i++) {
						maskKey[i] = incoming_[headerSize + i];
					}
					headerSize += 4;
				}

				std::string message;
				message = incoming_.substr(headerSize, len);

				if(isMasked) {
					for(unsigned int i = 0, l = message.size(); i < l; i++) {
						message[i] = ((unsigned char)message[i]) ^ maskKey[i % 4];
					}
				}

				incoming_ = incoming_.substr(headerSize + len);

				if (!handler_(this, message)) return false;
			}
		}

		void recvCB(typename server::connection::input_range range,
				boost::system::error_code error, size_t size) {
			if (!error) {
				incoming_.append(boost::begin(range), size);

				if (process()) {
					recv();
				}
			}
			else {
				std::cerr << "Got ws error: " << error << std::endl;
			}
		}

		void recv() {
			conn_->read(boost::bind(&Websocket::recvCB,
						Websocket::shared_from_this(), _1, _2, _3));
		}

		void sendCB(boost::system::error_code const &ec) {
			if(ec.value() != 0) {
				std::cerr << "Error writing to websocket" << std::endl;
			}
		}

		// Stolen from here:  http://stackoverflow.com/a/12784770
		static uint base64_encode(char *dest, unsigned const char *src, uint len) {
			using namespace boost::archive::iterators;

			char tail[3] = { 0, 0, 0 };
			typedef base64_from_binary<transform_width<const char *, 6, 8>>
				base64_enc;

			uint one_third_len = len / 3;
			uint len_rounded_down = one_third_len * 3;
			uint j = len_rounded_down + one_third_len;

			std::copy(base64_enc(src), base64_enc(src + len_rounded_down), dest);

			if (len_rounded_down != len) {
				uint i = 0;
				for (; i < len - len_rounded_down; ++i) {
					tail[i] = src[len_rounded_down + i];
				}

				std::copy(base64_enc(tail), base64_enc(tail + 3), dest + j);

				for (i = len + one_third_len + 1; i < j + 4; ++i) {
					dest[i] = '=';
				}

				return i;
			}

			return j;
		}

	public:
		~Websocket() {
			std::cout << "Websocket destroyed" << std::endl;
		}

		void send(std::string s) {
			//return;
			std::vector<unsigned char> data;

			data.reserve(2 + data.size());
			data.push_back(0x81);
			data.push_back((unsigned char)s.size());

			for(auto e : s) {
				data.push_back(e);
			}

			conn_->write(
					data,
					boost::bind(&Websocket::sendCB, Websocket::shared_from_this(), _1));
		}    

		Websocket(typename server::request const &req,
				typename server::connection_ptr conn, MessageHandler handler)
			: req_(req), conn_(conn), handler_(handler) {}

		bool run() {
			std::string websocket_key = "";
			std::string websocket_protocol = "";

			typename server::request::headers_container_type const &hs =
				req_.headers;
			for (typename server::request::headers_container_type::const_iterator
					it = hs.begin();
					it != hs.end(); ++it) {
				if (boost::to_lower_copy(it->name) == "sec-websocket-key") {
					websocket_key = it->value;
				} else if (boost::to_lower_copy(it->name) == "sec-websocket-key") {
					websocket_protocol = it->value;
				}

// 				std::cout << it->name << ": " << it->value << std::endl;
			}

			if (websocket_key == "") {
				return false;
			}

			std::vector<typename server::response_header> headers = {
				{ "Upgrade", "websocket" }, { "Connection", "Upgrade" }
			};

			if (websocket_protocol != "") {
				headers.push_back({ "Sec-WebSocket-Protocol", "chat" });
			}

			// Do the magic to create the response key
			{
				boost::uuids::detail::sha1 s;
				unsigned int digest[5];

				std::string tmp_response_key =
					websocket_key + "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";

				s.process_bytes(tmp_response_key.c_str(), tmp_response_key.size());

				s.get_digest(digest);

				// convert digest to something base64_from_binary can understand
				unsigned char converted_digest[5 * 4] = { 0 };
				for (int i = 0; i < 5; i++) {
					converted_digest[(i * 4) + 3] = digest[i] & 0x000000ff;
					converted_digest[(i * 4) + 2] = (digest[i] & 0x0000ff00) >> 8;
					converted_digest[(i * 4) + 1] = (digest[i] & 0x00ff0000) >> 16;
					converted_digest[(i * 4) + 0] = (digest[i] & 0xff000000) >> 24;
				}

				char base64_encoded[128];
				uint n = base64_encode(base64_encoded, converted_digest, 5 * 4);
				base64_encoded[n] = 0;

				headers.push_back({ "Sec-WebSocket-Accept", base64_encoded });
			}

			conn_->set_status(typename server::connection::status_t(101)); // force to 101
			conn_->set_headers(
					boost::make_iterator_range(headers.begin(), headers.end()));

			recv();

			return true;
		}
};
