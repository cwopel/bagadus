// AUTHOR(s):  Ragnar Langseth
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <viewControl.h>
#include <panoDefines.h>


/* ------------------------- Threaded View Control ------------------------------------ */
void * ThreadedViewControl::runControllerThread(void *p){
	((ThreadedViewControl*)p)->runController();
	return NULL;
}

/* Read from a pipe and parse input commands untill EOF, or termination */
void ThreadedViewControl::runController(){
    ssize_t ret;
    char buf[200];
    while(!m_terminate){
        char * str = buf;
        for(int ln=1; ln < 200; ++ln){
            if( (ret = read(m_pipe, str, 1)) <= 0) goto end;
			if(m_terminate) goto terminated;
			if(CONTROL_COMMAND_DELIMITERS.find(*str) != std::string::npos){
				if(ln != 1){
					*str = '\0';
					parse(buf, ln-1);
				}
				str = buf;
				ln = 0;
            }else{
                str++;
            }
        }
    }

end:
    if(ret<0){
        LOG_E("Read returned %ld : %s", ret, strerror(errno));
    }
	close(m_pipe);
	if(m_terminateCallback) m_terminateCallback(m_terminateCbParam);
	return;

terminated:
	close(m_pipe);
}

void ThreadedViewControl::terminate(){
	m_terminate = true;
	//TODO This is a potential deadlock,
	//when input just stops writing without closing pipe, this thread won't wake up
	pthread_join(m_controlThread, NULL);
}

/* ---------------------------- View Control ------------------------------------ */

long checkArray(char **str, int * ln){
    //Skip whitespaces
    for( ; *ln && isspace((unsigned char)**str); ++*str,--*ln) ; if(!**str) return -1; 

    //Find number
    char * end;
    long value = strtol(*str, &end, 10);
    if(end == *str) return -1; //No valid number
    //Update indices
    *ln -= end-*str;
    *str = end;

    //Skip whitespaces
    for( ; *ln && isspace((unsigned char)**str); ++*str,--*ln) ; if(!**str) return -1; 

    if(**str != ']') return -1;
    ++*str; --*ln;
   
    //Skip whitespaces
    for( ; *ln && isspace((unsigned char)**str); ++*str,--*ln) ; if(!**str) return -1; 

    return value;
}

/* Parses a string containing multiple commands */
void ViewControl::parseCommands(char * str, int ln){
	for(int idx = 0, cur = 0; idx < ln; ++idx){
		char c = str[idx];
		if(!c) return; //Reached end of text, caller lied about the length... bastard!
		//Search for delimiter
		if(CONTROL_COMMAND_DELIMITERS.find(c) != std::string::npos){
			//Ignore cases when we get 2 delimiters in a row
			if(cur){
				str[idx] = '\0';
				//Parse this single command
				parse(&str[idx-cur], cur+1);
				//Return string to un-mutated state so it can be re-parsed
				str[idx] = c;
			}
			cur = 0;
		}else cur++;
	}
}

void ViewControl::parse(char * str, int ln){
    
// 	LOG_D("Parsing: \"%s\"", str);

    //Skip initial whitespaces
    for( ; ln && isspace((unsigned char)*str); ++str,--ln) ;
    if(!*str) return; //Failure, reached end of string

    Operand var;
    //Find operand
    switch(*str){
        case 'x': var = VAR_TX; break;
        case 'y': var = VAR_TY; break;
        case 'f': var = VAR_FO; break;
        case 'i': var = VAR_IX; break;
        default: return;
    }
    ++str; --ln;

    //Skip possible whitespaces
    for(; ln && isspace((unsigned char)*str); ++str,--ln) ;
    if(!*str) return; //Failure, reached end of string

    long arrayIdx;
    if(*str == '['){
        ++str; --ln;
        arrayIdx = checkArray(&str,&ln);
        if(arrayIdx < 0) return; //Failure
    }else{
        arrayIdx = -1;
    }

    Operator op;
    switch(*str){
        case '=': op = OP_SET;  break;
        case '+': op = OP_INC;  break;
        case '-': op = OP_DEC;  break;
        case '>': op = OP_STEP_INC; break;
        case '<': op = OP_STEP_DEC; break;
        default: return;
    }
	if(var == VAR_IX && op != OP_SET && arrayIdx>=0) return; //Only allows set-operation with index

    ++str; --ln;

    //Skip possible whitespaces
    for(; ln && isspace((unsigned char)*str); ++str,--ln) ;
    if(!*str) return; //Failure, reached end of string

    char * end;
    double value = strtod(str, &end);
    if(*end && !isspace((unsigned char)*end)) return; //No valid number

// 	LOG_D("Success: %d:%d:%f", (int)var, (int)op, value);
	
	pthread_mutex_lock(&m_queueLock);
	m_commandQueue.push_back(Command(var, op, value, arrayIdx));
	pthread_mutex_unlock(&m_queueLock);
}

void ViewControl::parseSetOp(Operand var, double value, long frameNum){
    switch(var){
		case VAR_TX: m_pos.x = value; m_movement.x = 0; break;
		case VAR_TY: m_pos.y = value; m_movement.y = 0; break;
		case VAR_FO: m_pos.f = value; m_movement.f = 0; break;
		case VAR_IX: m_lastFrameReset = frameNum - (long) value;
    }
}

void ViewControl::parseIncOp(Operand var, double value){
    switch(var){
        case VAR_TX: m_pos.x = m_pos.x+value; break;
        case VAR_TY: m_pos.y = m_pos.y+value; break;
		case VAR_FO: m_pos.f = m_pos.f+value; break;
        default: return;
    }
}

//These are a little bit arbitrary, since it is a major difference depending on fps
const double maxStep = 1.0, minStep = -1.0;
const double maxFStep = 1.6, minFStep = -1.6;
void ViewControl::parseStepOp(Operand var, double value){
    switch(var){
        case VAR_TX:
			m_movement.x = std::min<double>(maxStep,std::max<double>(value,minStep));
			break;
        case VAR_TY:
			m_movement.y = std::min<double>(maxStep,std::max<double>(value,minStep));
			break;
        case VAR_FO:
			m_movement.f = std::min<double>(maxFStep,std::max<double>(value,minFStep));
			break;
        default: return;
    }
}

void ViewControl::updateSteps(){

	//focal length isnt really linear. The further you zoom in, the faster it needs to move
	//to look correct / natural.
	double curF = m_pos.f<0 ? 0 : m_pos.f; // log negative is a bad idea
    m_pos.f += m_movement.f * log10( curF + 10);
    m_pos.x += m_movement.x;
    m_pos.y += m_movement.y;

    //Clamp to logical values
	if(m_pos.x > 100.0){
		if(m_movement.x > 0) m_movement.x = 0;
		m_pos.x = 100.0;
	}else if(m_pos.x < 0){
		if(m_movement.x < 0) m_movement.x = 0;
		m_pos.x = 0.0;
	}
	if(m_pos.y > 100.0){
		if(m_movement.y > 0) m_movement.y = 0;
		m_pos.y = 100.0;
	}else if(m_pos.y < 0){
		if(m_movement.y < 0) m_movement.y = 0;
		m_pos.y = 0.0;
	}
	if(m_pos.f > 100.0){
		if(m_movement.f > 0) m_movement.f = 0;
		m_pos.f = 100.0;
	}else if(m_pos.f < 0){
		if(m_movement.f < 0) m_movement.f = 0;
		m_pos.f = 0.0;
	}
	m_state.setState(m_pos);
}

void ViewControl::updateMatrix(long frameNum, double &focal,
		double &theta_x, double &theta_y, double &theta_z){
	
	//Record current timestamp
	m_lastFrameNum = frameNum;
	clock_gettime(CLOCK_MONOTONIC, &m_lastFrameTimestamp);

	//First, check the command queue 
	pthread_mutex_lock(&m_queueLock);
	while(!m_commandQueue.empty() && m_commandQueue.front().framenum<=(frameNum-m_lastFrameReset)){
		Command c = m_commandQueue.front();
		switch(c.op){
			//Assumption: "i = 0;" is a really bad idea. If things go to hell,
			//i.e., reset isn't applied on the expected frame, due to random delays or syncing issues,
			//every single command for all of eternity after that will be out of sync due to reset.
			case OP_SET: parseSetOp(c.var,c.value,c.framenum+m_lastFrameReset); break;
			case OP_INC: parseIncOp(c.var,c.value);  break;
			case OP_DEC: parseIncOp(c.var,c.value*-1);  break;
			case OP_STEP_INC: parseStepOp(c.var,c.value); break;
			case OP_STEP_DEC: parseStepOp(c.var,c.value*-1); break;
		}
		m_commandQueue.pop_front();
	}
	pthread_mutex_unlock(&m_queueLock);

	if(m_absCoords){
		theta_x = m_pos.x;
		theta_y = m_pos.y;
		theta_z = PANORAMA_THETA_Z(theta_y);
		focal = m_pos.f;
	}else{
		updateSteps();
		m_state.getState(theta_x,theta_y,theta_z,focal);
	}

// 	if(frameNum % 30 == 0){
// 		LOG_D("State[%8ld]: %10.4f:%10.4f:%10.4f:%10.4f", frameNum,theta_x,theta_y,theta_z,focal);
// 	}
}

/* ----------------------ViewNetworkControl----------------------*/

void ViewNetworkControl::updateMatrix(long frameNum, double &focal, double &theta_x,
		double &theta_y, double &theta_z){

    ViewControl::updateMatrix(frameNum, focal, theta_x, theta_y, theta_z);

	//Here, I want curFrame to be the number sent OUT, excluding potential skipped frames
	if(!m_curFrame)	m_curFrame = frameNum;
	else m_curFrame++;
	m_lastFrameNum = m_curFrame; //Asyc, so not completely monotonic, but its debug crap so...
}

/* ----------------------View State----------------------*/

ViewState::ViewState(double fov, int width, int height){
// Results approximate to something like this, given width=4096, height=1680, fov=2.7rad:
// focal = {810..4048}, y = {-0.33..0.25}, x = {-1.19..1.17}

	//WARN: Pano-scale should be a parameter, but need a quick fix atm :/
	double maxF = width * fov / (2 * M_PI) * VIEW_ZOOM_CONSTANT / (PANORAMA_SCALE);
	m_minF = maxF * 0.20;
	m_stepF = (maxF-m_minF)/100.0;

	double maxX = fov/2.0 * VIEW_MAX_X_CONSTANT;
	m_minX = fov/2.0 * VIEW_MIN_X_CONSTANT;
	m_stepX = (maxX-m_minX)/100.0;

	double maxY = height*fov / (width*2.0) * VIEW_MAX_Y_CONSTANT;
	m_minY = height*fov / (width*2.0) * VIEW_MIN_Y_CONSTANT;
	m_stepY = (maxY-m_minY)/100.0;

	m_f = 50 * m_stepF + m_minF;
	m_x = 50 * m_stepX + m_minX;
	m_y = 30 * m_stepY + m_minY;
}

void ViewState::getState(double &tx, double &ty, double &tz, double &tf){
	
	//Adding a logarithmic modification to reduce the warping when the
	//image is zoomed far out. This is nearly undetectable when looking fairly
	//in the center, but when looking at the edges it makes a major difference
	double tmpX = m_x*log10(m_f/4.0)/3.0;
	double tmpY = m_y*log10(m_f/4.0)/2.6;

#ifdef CONTROL_AUTO_ZOOM_ENABLED
	//Simplistic auto zoom based on distance to the source camera array.
    //TODO Hardcoded to Alfheim
	double distance = (((tmpY - m_minY)/m_stepY+1.0)/40.0) - ((fabs(tmpX)/m_stepX+1.0)/80.0) + 1.0;
	tf = m_f / distance;
#else
	tf = m_f;
#endif

	//Calculate z-rotation, and compute more logical coordinates, based on the rotation
	tz = PANORAMA_THETA_Z(tmpX);

	//Perform a z-rotation
	//NOTE! tx/ty is swapped intentionally, viewer uses a different coordinate system
	//TODO There is something fundamentally wrong with the math here,
	//the 0.5 isn't supposed to be there with the standard rotation matrix,
	//but this makes it looks nearly correct
	tx = tmpY*cos(tz) - 0.5f*tmpX*sin(tz);
	ty = tmpY*sin(tz) + tmpX*cos(tz);
}

void ViewState::setState(const Coordinate &pos){
	m_x = pos.x * m_stepX + m_minX;
	m_y = pos.y * m_stepY + m_minY;
	m_f = pos.f * m_stepF + m_minF;
}


