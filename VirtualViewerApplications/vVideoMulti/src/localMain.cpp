// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <viewInput.h>
#include <viewEnc.h>
#include <viewer.h>
#include <viewControl.h>
#include "OutFileStream.hpp"
#include "OutPipeStream.hpp"

#include <fcntl.h>
#include <signal.h>

// const int out_width = 1280;
// const int out_height = 720;
const int out_width = 1920;
const int out_height = 1080;
const int in_width = 4096;
const int in_height = 1680;
const int fps = 30;
// const int numStreams = 24;
// const int numStreams = 4;
const int numStreams = 2;
bool liveMode = true;
Viewer * v[numStreams];
ViewEnc * encoders[numStreams];
OutStream * outputStreams[numStreams];
ThreadedViewControl * controlstreams[numStreams];
ViewInput * input;

int runtimeArgc;
char ** runtimeArgv;

void terminateCallback(void * p){
	int idx = *(int*) p;

	//TODO This is not exactly safe
	LOG_D("Terminating stream %d", idx);

	LOG_D("Detatching %d : %s",idx,(input->detachViewer(v[idx], 0) ? "Success" : "Failure"));

	encoders[idx]->terminate();
}

int main(int argc, char ** argv){

	runtimeArgc = argc;
	runtimeArgv = argv;

	//Ignore broken pipes
	signal(SIGPIPE,SIG_IGN);   

	ViewInput inputstream = ViewInput(in_width,in_height);
	input = &inputstream;

	for(int i=0; i<numStreams; ++i){
		bool success;
		encoders[i] = new ViewEnc();
		encoders[i]->init(fps, out_width, out_height, 0, 0, 0);

		v[i] = new Viewer(encoders[i], 0, liveMode,in_width,in_height,2.7,1.75);
		success = inputstream.attachViewer(v[i], 0);
		LOG_I("Attaching %d : %s",i+1, (success ? "Success" : "Failure"));

		if(runtimeArgc > (i*2 + 1)){
			int pipe = open(runtimeArgv[i*2 + 1], O_RDONLY);
			if(pipe < 0){
				LOG_E("Failed to open command pipe: %s", runtimeArgv[i*2 + 1]);
			}else{
				controlstreams[i] = new ThreadedViewControl(
						2.7, in_width, in_height,
						pipe, terminateCallback, (void*) new int(i));
				v[i]->attachControlStream(controlstreams[i]);
			}
			OutPipeStream *tmp = new OutPipeStream(fps,out_width,out_height); 
			success = tmp->openPipe(runtimeArgv[i*2 + 2]);
			LOG_I("Opening pipe %s: %s", runtimeArgv[i*2 + 2], (success ? "Success" : "Failure"));
			if(!success) exit(EXIT_FAILURE);
			outputStreams[i] = tmp;
		}else{
			char str[100];
			sprintf(str, "Stream_%02d.mkv", i+1);
			OutFileStream * tmp = new OutFileStream(fps,out_width,out_height); 
			success = tmp->openFile(str);
			LOG_I("Opening file %s: %s", str, (success ? "Success" : "Failure"));
			if(!success) exit(EXIT_FAILURE);
			outputStreams[i] = tmp;
		}
		encoders[i]->attachOutputStream(outputStreams[i]);
	}

	inputstream.runReader(STDIN_FILENO);

	for(int i=0; i<numStreams; ++i){
		LOG_D("Detatching %d : %s",i,(inputstream.detachViewer(v[i], 0) ? "Success" : "Failure"));
		encoders[i]->terminate();
		if(runtimeArgc > i+1 && controlstreams[i]){
			controlstreams[i]->terminate();
			delete controlstreams[i];
		}
		delete v[i];
		delete encoders[i];
	}

	return EXIT_SUCCESS;
}

