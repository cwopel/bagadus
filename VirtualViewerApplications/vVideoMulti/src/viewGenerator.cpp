// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <viewGenerator.h>
#include "SlaveStreamGenerator.hpp"
#include "PreviewStreamGenerator.hpp"

ViewGenerator::~ViewGenerator(){
	LOG_D("Terminating Stream: %s", m_id.data());
	if(m_terminateCallback) m_terminateCallback(m_id);
	m_inputSrc->detachViewer(&m_viewer, m_devId);
	m_enc.terminate();
}

std::shared_ptr<SlaveStreamGenerator> ViewGenerator::getPreview(){
	pthread_mutex_lock(&m_previewLock);
	if(!m_previewStream){
		unsigned int fps = 15;
		unsigned int interval = round((float)m_stats.fps / fps);
		fps = m_stats.fps / interval;

		unsigned int width =  320;
		width = (width+31)&~31; //Pad to 32byte alignment
		unsigned int height = width / ((float)m_inWidth/m_inHeight);
		height = (height+7)&~7; //Pad to 8byte alignment
		
		m_previewStream = new PreviewStreamGenerator(&m_viewer, m_devId, interval,
				StreamStats(m_id, fps, width, height, 1000));
	}
	auto ret = std::make_shared<SlaveStreamGenerator>(m_previewStream, true);
	pthread_mutex_unlock(&m_previewLock);
	return ret;
}
