// AUTHOR(s): Ragnar Langseth, Vamsidhar Reddy Gaddam,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <viewer.h>
#include <cuda.h>
#include <cuda_runtime_api.h>

#include <math_functions.h>
#include <bicubicTexture_kernel.cuh>

texture<unsigned char, 2, cudaReadModeNormalizedFloat> texYLin;
texture<unsigned char, 2, cudaReadModeNormalizedFloat> texULin;
texture<unsigned char, 2, cudaReadModeNormalizedFloat> texVLin;

// #define SHOW_TILING //Turn tiling visualization on/off

#define TILE_W 320
#define TILE_H 256
#define TILE_COLOR 255 //Y-channel only
#ifdef SHOW_TILING
#define SET_TILE_ACTIVE(m,pos,s) m[ (int)(pos.y)/TILE_H * s + (int)(pos.x)/TILE_W ] = 1
#else //Set tiling function to empty-statement
#define SET_TILE_ACTIVE(m,pos,s) 
#endif
#define LOOKUP_TILE(m,x,y,s) m[ (int)y/TILE_H * s + (int)x/TILE_W ]

//Set either bilinear or cubic Y-channel interpolation. Chroma channel is always bilinear
// #define LOOKUP_Y(t) rintf(255*tex2D(texYLin, t.x+0.5f, t.y+0.5f))
#define LOOKUP_Y(t) rintf(255*tex2DFastBicubic<unsigned char,float>(texYLin, t.x+0.5f, t.y+0.5f))

__device__ float2 computeXY(float *M, float x, float y, int panoWidth,
		int panoHeight, double panoFOV){
	//Performs a backwards projection, based on matrix M.
	//x,y is a point on the output image (not on panorama)

	float2 t;
	float s1,s2,s3;
	s1 = M[0]*(float)x + M[1]*(float)y+M[2];
	s2 = M[3]*(float)x + M[4]*(float)y+M[5];
	s3 = M[6]*(float)x + M[7]*(float)y+M[8];

	t.x = std::atan2(-s1,s3)*((double)panoWidth)/panoFOV + ((double)panoWidth)/2;
	t.y = ((double)panoHeight)/2 -
		((double)panoHeight)*s2/std::sqrt(s1*s1+s3*s3);
	return t;
}

__global__ void makeView_linear_kernel(uint32_t outW, uint32_t pitch, uint32_t outH, int panoWidth,
		int panoHeight, double panoFOV, uint8_t *destination, float *mat, char * tileMap, uint32_t tileCols){

	//Blockdim.x == 4, Blockdim.y varies, but typically 16/32/64

	__shared__ float2 corners[4];

	int x, y;
	int range = blockDim.y; 
	int num = blockDim.y >> 2;

	x = blockDim.y * blockIdx.x;
	y = threadIdx.y + blockDim.y * blockIdx.y;

	if(x >= outW || y >= outH) return;

	//First, we project 4 corners in a block, f ex 32x32 block (defined by blockDim.y).
	//4 threads perform this task for the entire threadblock
	//Major performance gain if blockDim.y > 31 here, assuming 32 threads per warp (kepler/maxwell)
	if(threadIdx.y == 0){
        corners[threadIdx.x] =
			computeXY(mat, x+(range*(threadIdx.x&1)), y+(range*(threadIdx.x>>1)),
					panoWidth, panoHeight, panoFOV);
	}

	__syncthreads();
	//Now all 4 corners are known. We can then interpolate all coordinates inside the block
	//Each thread will iterate over multiple values (default is 8) and interpolate the
	//coordinates, based on the 4 corners.
	int end = (threadIdx.x+1)*num;
	for(int i = threadIdx.x*num; i < end; ++i){
		float2 t;
		float w[4];
		//Calculate the weights for each of the 4 corners. It uses a simplification of
		//'Inverse distance weighted interpolation', without using euclidiean distance
		w[0] = (float)((range-i) * (range-threadIdx.y))/(range*range);
		w[1] = (float)(i * (range-threadIdx.y))/(range*range);
		w[2] = (float)((range-i) * threadIdx.y)/(range*range);
		w[3] = (float)(i * threadIdx.y)/(range*range);
		t.x = w[0]*corners[0].x + w[1]*corners[1].x + w[2]*corners[2].x + w[3]*corners[3].x;
		t.y = w[0]*corners[0].y + w[1]*corners[1].y + w[2]*corners[2].y + w[3]*corners[3].y;
		destination[i + x + y*pitch] =
			(t.x>=0 && t.x<panoWidth && t.y>=0 && t.y<panoHeight) ? LOOKUP_Y(t) : 0;

		//Chroma. This is fairly simple subsampling, no fancy average, just grabbing a corner.
		if(!(threadIdx.y&1) && !(i&1)){
			uint8_t u,v;
			if(t.x>=0 && t.x<panoWidth && t.y>=0 && t.y<panoHeight){
				u = rintf(255*tex2D(texULin, (t.x/2), t.y));
				v = rintf(255*tex2D(texVLin, (t.x/2), t.y));
				SET_TILE_ACTIVE(tileMap,t,tileCols);
			}else{
				u = 128; v = 128;
			}
			uint8_t * dst = destination + x + outH * pitch + y/2 * pitch;
			dst[i] = u;
			dst[i+1] = v;
		}
	}
}

__global__ void makeView_kernel(uint32_t outW, uint32_t pitch, uint32_t outH, int panoWidth,
		int panoHeight, double panoFOV, uint8_t *destination, float *mat){
    //Loop through multiple pixels, 4 at the time
    //May compute arbitrary number of pixels, depends how many blocks are created
    for(int idx=4*(blockIdx.x*blockDim.x + threadIdx.x);
            idx < (outW*outH); idx += 4*(blockDim.x*gridDim.x)){
        float2 t;
        float up,vp;
		int x, y;
		uint8_t * dst;

		x = (idx>>1)%outW;
		y = ((idx>>1)/outW)<<1;
		dst = destination + x + y*pitch;
        //Determine the first pixel
        t = computeXY(mat, x, y, panoWidth, panoHeight, panoFOV);
        if(t.x>=0 && t.x<panoWidth && t.y>=0 && t.y<panoHeight){
            *dst = LOOKUP_Y(t);
            up = tex2D(texULin, (t.x/2)+0.5f, t.y+0.5f);
            vp = tex2D(texVLin, (t.x/2)+0.5f, t.y+0.5f);
        }else{
            *dst = 0; up = 0.5f; vp = 0.5f;
        }
        //Determine the second pixel
		x++; dst++;
        t = computeXY(mat, x, y, panoWidth, panoHeight, panoFOV);
        if(t.x>=0 && t.x<panoWidth && t.y>=0 && t.y<panoHeight){
            *dst = LOOKUP_Y(t);
            up += tex2D(texULin, (t.x/2)+0.5f, t.y+0.5f);
            vp += tex2D(texVLin, (t.x/2)+0.5f, t.y+0.5f);
        }else{
            *dst = 0; up += 0.5f; vp += 0.5f;
        }
        //Determine the third pixel
		x--; y++;
		dst = destination + x + y*pitch;
        t = computeXY(mat, x, y, panoWidth, panoHeight, panoFOV);
        if(t.x>=0 && t.x<panoWidth && t.y>=0 && t.y<panoHeight){
            *dst = LOOKUP_Y(t);
            up += tex2D(texULin, (t.x/2)+0.5f, t.y+0.5f);
            vp += tex2D(texVLin, (t.x/2)+0.5f, t.y+0.5f);
        }else{
            *dst = 0; up += 0.5f; vp += 0.5f;
        }
        //Determine the fourth pixel
		x++; dst++;
        t = computeXY(mat, x, y, panoWidth, panoHeight, panoFOV);
        if(t.x>=0 && t.x<panoWidth && t.y>=0 && t.y<panoHeight){
            *dst = LOOKUP_Y(t);
            up += tex2D(texULin, (t.x/2)+0.5f, t.y+0.5f);
            vp += tex2D(texVLin, (t.x/2)+0.5f, t.y+0.5f);
        }else{
            *dst = 0; up += 0.5f; vp += 0.5f;
        }
        //Write joint u/v
		dst = destination + outH * pitch + y/2 * pitch;
		dst[x-1] = rintf(63.75f*up);
		dst[x] = rintf(63.75f*vp);
	}
}

__global__ void makeEdges_kernel(uint32_t pitch, int outW, int outH, int hOffset,
		int panoWidth, int panoHeight, uint32_t previewW, uint32_t previewH,
		double panoFOV, uint8_t *destination, float *mat){

	//Project all the edges, and paint them white

    for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
            idx < (outW*2); idx += 2*(blockDim.x*gridDim.x)){
        float2 t;
		int x,y, dx,dy;
		x = idx % outW;
		y = (idx / outW)*(outH-1);
        t = computeXY(mat, x, y, panoWidth, panoHeight, panoFOV);
		dx = t.x/((float)panoWidth/previewW);
		dy = t.y/((float)panoHeight/previewH) + hOffset - 1;
		
		//Create a white pixel on this point, and 4 points surrounding it

        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
		dx--; dy++;
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
		dx++;
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
		dx++;
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
		dx--; dy++;
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
	}

    for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
            idx < (outH*2); idx += 2*(blockDim.x*gridDim.x)){
        float2 t;
		int x,y, dx,dy;
		x = (idx / outH)*(outW-1);
		y = idx % outH;
        t = computeXY(mat, x, y, panoWidth, panoHeight, panoFOV);
		dx = t.x/((float)panoWidth/previewW);
		dy = t.y/((float)panoHeight/previewH) + hOffset - 1;

		//Create a white pixel on this point, and 4 points surrounding it
        
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
		dx--; dy++;
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
		dx++;
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
		dx++;
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
		dx--; dy++;
        if(dx>=0 && dx<previewW && dy>=(hOffset) && dy<(hOffset+previewH)){
			destination[dx + dy*pitch] = 255;
		}
	}
}

__global__ void makeEdges_kernel_bgr(int outW, int outH,
		int panoWidth, int panoHeight, uint32_t previewW, uint32_t previewH,
		double panoFOV, uint8_t *destination, float *mat){
    uint8_t * dst;
    float2 t;
    int x, y, dx, dy;

	//Project all the edges, and paint them white

    for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
            idx < (outW*2); idx += 2*(blockDim.x*gridDim.x)){
		x = idx % outW;
		y = (idx / outW)*(outH-1);
        t = computeXY(mat, x, y, panoWidth, panoHeight, panoFOV);
		dx = t.x/((float)panoWidth/previewW);
		dy = t.y/((float)panoHeight/previewH);
		
		//Create a white pixel on this point, and 4 points surrounding it

        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
		dx--; dy++;
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
		dx++;
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
		dx++;
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
		dx--; dy++;
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
	}

    for(int idx=2*(blockIdx.x*blockDim.x + threadIdx.x);
            idx < (outH*2); idx += 2*(blockDim.x*gridDim.x)){
		x = (idx / outH)*(outW-1);
		y = idx % outH;
        t = computeXY(mat, x, y, panoWidth, panoHeight, panoFOV);
		dx = t.x/((float)panoWidth/previewW);
		dy = t.y/((float)panoHeight/previewH);

		//Create a white pixel on this point, and 4 points surrounding it
        
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
		dx--; dy++;
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
		dx++;
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
		dx++;
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
		dx--; dy++;
        if(dx>=0 && dx<previewW && dy>=0 && dy<(previewH)){
			dst = destination + dx*3 + dy*previewW*3;
            dst[0] = 255; dst[1] = 255; dst[2] = 255;
		}
	}
}

__global__ void makePreview_kernel_bgr(int panoWidth, int panoHeight,
		uint32_t previewW, uint32_t previewH, uint8_t *destination){

    for(int idx=(blockIdx.x*blockDim.x + threadIdx.x);
            idx < (previewW*previewH); idx += (blockDim.x*gridDim.x)){
        int x, y;
        float dx, dy, valY, valCb, valCr;
		x = idx % previewW;
        y = idx / previewW;
		dx = x*((float)panoWidth/previewW);
		dy = y*((float)panoHeight/previewH);
		valY = 255*tex2D(texYLin, dx+0.5f, dy+0.5f);
		valCb = 255*tex2D(texULin, (dx/2)+0.5f, dy+0.5f) - 128;
		valCr = 255*tex2D(texVLin, (dx/2)+0.5f, dy+0.5f) - 128;

        uint8_t * dst = destination + previewW*3*y + x*3;
        int b = (int) (valY + 1.772 * valCb);
        int g = (int) ((valY - (0.34414 * valCb)) - 0.71414 * valCr);
        int r = (int) (valY + 1.402 * valCr);
        dst[0] = b<0 ? 0 : (b>255 ? 255 : b);
        dst[1] = g<0 ? 0 : (g>255 ? 255 : g);
        dst[2] = r<0 ? 0 : (r>255 ? 255 : r);
    }
}

__global__ void makePreview_kernel(uint32_t pitch, int outH, int hOffset,
		int panoWidth, int panoHeight,
		uint32_t previewW, uint32_t previewH, uint8_t *destination,
		char * tilemap, uint32_t tileCols){

	//Create a small, scaled down version of the original panorama

    for(int idx=4*(blockIdx.x*blockDim.x + threadIdx.x);
            idx < (previewW*previewH); idx += 4*(blockDim.x*gridDim.x)){
        float up,vp;
		int x, y;
		float dx, dy;
		uint8_t * dst;

		x = (idx>>1)%previewW;
		y = ((idx>>1)/previewW)<<1;
		dst = destination + x + (y+hOffset)*pitch;

        //Determine the first pixel
		dx = x*((float)panoWidth/previewW);
		dy = y*((float)panoHeight/previewH);
#ifdef SHOW_TILING
		if( !(x % ((int)(TILE_W*(float)previewW/panoWidth))) ||
				!(y % ((int)(TILE_H*(float)previewH/panoHeight))) ) *dst = TILE_COLOR;
		else
#endif
			*dst = rintf(255*tex2D(texYLin, dx+0.5f, dy+0.5f));
		up = tex2D(texULin, (dx/2)+0.5f, dy+0.5f);
		vp = tex2D(texVLin, (dx/2)+0.5f, dy+0.5f);

        //Determine the second pixel
		x++; dst++;
		dx = x*((float)panoWidth/previewW);
		dy = y*((float)panoHeight/previewH);
#ifdef SHOW_TILING
		if( !(x % ((int)(TILE_W*(float)previewW/panoWidth))) ||
				!(y % ((int)(TILE_H*(float)previewH/panoHeight))) ) *dst = TILE_COLOR;
		else
#endif
			*dst = rintf(255*tex2D(texYLin, dx+0.5f, dy+0.5f));
		up += tex2D(texULin, (dx/2)+0.5f, dy+0.5f);
		vp += tex2D(texVLin, (dx/2)+0.5f, dy+0.5f);

        //Determine the third pixel
		x--; y++;
		dst = destination + x + (y+hOffset)*pitch;
		dx = x*((float)panoWidth/previewW);
		dy = y*((float)panoHeight/previewH);
#ifdef SHOW_TILING
		if( !(x % ((int)(TILE_W*(float)previewW/panoWidth))) ||
				!(y % ((int)(TILE_H*(float)previewH/panoHeight))) ) *dst = TILE_COLOR;
		else
#endif
			*dst = rintf(255*tex2D(texYLin, dx+0.5f, dy+0.5f));
		up += tex2D(texULin, (dx/2)+0.5f, dy+0.5f);
		vp += tex2D(texVLin, (dx/2)+0.5f, dy+0.5f);

        //Determine the fourth pixel
		x++; dst++;
		dx = x*((float)panoWidth/previewW);
		dy = y*((float)panoHeight/previewH);
#ifdef SHOW_TILING
		if( !(x % ((int)(TILE_W*(float)previewW/panoWidth))) ||
				!(y % ((int)(TILE_H*(float)previewH/panoHeight))) ) *dst = TILE_COLOR;
		else
#endif
			*dst = rintf(255*tex2D(texYLin, dx+0.5f, dy+0.5f));
		up += tex2D(texULin, (dx/2)+0.5f, dy+0.5f);
		vp += tex2D(texVLin, (dx/2)+0.5f, dy+0.5f);

#ifdef SHOW_TILING
		if(! LOOKUP_TILE(tilemap,dx,dy,tileCols)){
			up = 2.0f; vp = 2.0f;
		}
#endif
		dst = destination + outH * pitch + (y+hOffset)/2 * pitch;
		dst[x-1] = rintf(63.75f*up); 
		dst[x] = rintf(63.75f*vp);
	}
}

void Viewer::makeView(const FrameBuffer * output, uint8_t * inputY,
		uint8_t * inputUV, size_t strideY, size_t strideUV){
    size_t uOff, vOff;
	cudaBindTexture2D(NULL, &texYLin, inputY, &texYLin.channelDesc,
	                    m_inW, m_inH, strideY);
	cudaBindTexture2D(&uOff, &texULin, inputUV, &texULin.channelDesc,
	                    m_inW/2, m_inH, strideUV);
	cudaBindTexture2D(&vOff, &texVLin, inputUV+(strideUV*m_inH), &texVLin.channelDesc,
	                    m_inW/2, m_inH, strideUV);

#ifdef SHOW_TILING
	cudaSafe(cudaMemsetAsync(m_tileBytemap, 0, m_tileCols*m_tileRows, m_stream));
#endif

	//Perform standard projection
// 	makeView_kernel <<< (output->width*output->height)>>10, 128, 0, m_stream>>>
//         (output->width, output->pitch, output->height, m_inW, m_inH, m_matVals.fov,
// 		 (uint8_t*)output->devPtr, m_gpuMatrix);


	//Perform an optimized projection, projecting only a few points and interpolating the rest
	//x must be 4, y defines distance between projected points (must be even number, pref 32)
	dim3 threads(4,32); 
	dim3 blocks;
	blocks.x = 1 + (output->width  / threads.y);
	blocks.y = 1 + (output->height / threads.y);
	makeView_linear_kernel <<< blocks, threads, 0, m_stream>>>
        (output->width, output->pitch, output->height, m_inW, m_inH, m_matVals.fov,
		 (uint8_t*)output->devPtr, m_gpuMatrix, m_tileBytemap, m_tileCols);

	cudaSafe(cudaPeekAtLastError());

	//Single access to class-variable, so it can be changed without locks
	float previewScale = m_previewScale;
	if(previewScale < 0.1f) return;

	//Create preview window

	uint32_t previewW = (uint32_t)(output->width * previewScale) &~1;
	uint32_t previewH = (uint32_t)(previewW * m_inH / m_inW) &~1;
	if(previewH > output->height) previewH = output->height; //Odd segfaulting edge-cases
	int previewHOffset = output->height - previewH;

	//First, make a downscaled version of the original panorama
	makePreview_kernel <<< 1+((previewH*previewW)>>10), 128, 0, m_stream>>>
        (output->pitch, output->height, previewHOffset,
		 m_inW, m_inH, previewW, previewH,(uint8_t*)output->devPtr, m_tileBytemap, m_tileCols);
	cudaSafe(cudaPeekAtLastError());

	//Then, project on all of the edges of the virtual view and paint them white
	makeEdges_kernel <<< 1+((output->width)/128), 128, 0, m_stream>>>
        (output->pitch, output->width, output->height, previewHOffset,
		 m_inW, m_inH, previewW, previewH, m_matVals.fov, (uint8_t*)output->devPtr, m_gpuMatrix);
	cudaSafe(cudaPeekAtLastError());
}

void Viewer::makePreviewWindow(const FrameBuffer * output, unsigned int width, unsigned int height){

	uint32_t previewW = output->width;
	uint32_t previewH = output->height;

	makePreview_kernel <<< 1+((previewH*previewW)>>10), 128, 0, m_stream>>>
        (output->pitch, output->height, 0,
		 m_inW, m_inH, previewW, previewH,(uint8_t*)output->devPtr, m_tileBytemap, m_tileCols);
	cudaSafe(cudaPeekAtLastError());

	makeEdges_kernel <<< 1+((output->width)/128), 128, 0, m_stream>>>
        (output->pitch, width, height, 0,
		 m_inW, m_inH, previewW, previewH, m_matVals.fov, (uint8_t*)output->devPtr, m_gpuMatrix);
	cudaSafe(cudaPeekAtLastError());
}


void Viewer::makePreviewWindowBGR(unsigned int origWidth, unsigned int origHeight){

	makePreview_kernel_bgr <<< 1+((m_jpegWidth*m_jpegHeight)>>10), 128, 0, m_stream>>>
        (m_inW, m_inH, m_jpegWidth, m_jpegHeight, m_jpegGpuBuffer);
	cudaSafe(cudaPeekAtLastError());

	makeEdges_kernel_bgr <<< 1+((m_jpegWidth)/128), 128, 0, m_stream>>>
        (origWidth, origHeight, m_inW, m_inH, m_jpegWidth, m_jpegHeight,
         m_matVals.fov, m_jpegGpuBuffer, m_gpuMatrix);
	cudaSafe(cudaPeekAtLastError());
}

void Viewer::initCuViewer(){
	texYLin.addressMode[0]=cudaAddressModeClamp;
	texYLin.addressMode[1]=cudaAddressModeClamp;
	texYLin.filterMode = cudaFilterModeLinear;

	texULin.addressMode[0]=cudaAddressModeClamp;
	texULin.addressMode[1]=cudaAddressModeClamp;
	texULin.filterMode = cudaFilterModeLinear;

	texVLin.addressMode[0]=cudaAddressModeClamp;
	texVLin.addressMode[1]=cudaAddressModeClamp;
	texVLin.filterMode = cudaFilterModeLinear;

	m_tileCols = m_inW / TILE_W + ((m_inW%TILE_W)?1:0);
	m_tileRows = m_inH / TILE_H + ((m_inH%TILE_H)?1:0);
	cudaSafe(cudaMalloc((void**)&m_tileBytemap, m_tileCols*m_tileRows));
}

