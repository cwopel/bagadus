// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <outStream.h>
#include <cstdlib>
#include <cstdio>

class OutPipeStream : public OutStream {
	public:
		OutPipeStream(unsigned int fps, unsigned int width, unsigned int height) :
			OutStream(fps, width, height), m_curPipe(-1), m_open(false) { }

		OutPipeStream(int pipe, unsigned int fps, unsigned int width, unsigned int height) :
			OutStream(fps, width, height), m_curPipe(pipe), m_open(true) {
				writeHeader();
			}

		~OutPipeStream(){
			if(m_open) endStream();
		}

		bool openPipe(char * str){
			int f = open(str, O_WRONLY);
			if(f<0) return false;

			if(m_open){
				close(m_curPipe);
			}
			m_curPipe = f;

			writeHeader();

			m_open = true;
			return true;
		}

		void pushData(void * data, size_t byteCount, bool keyframe){
			if(!m_open){
				LOG_W("OutPipeStream: Stream not yet opened when pushData called");
				return;
			}

			std::vector<unsigned char> outData;
			muxToMkv(data, byteCount, outData);
			size_t ret = write(m_curPipe, outData.data(), outData.size());
			if(ret != outData.size()){
				LOG_W("Writing to file returned %ld : %s", ret, strerror(errno));
			}
		}

		void endStream(){
			if(!m_open) return;
			m_open = false;
			close(m_curPipe);
		}
	private:
		int m_curPipe;
		bool m_open;

		void writeHeader(){
			std::vector<unsigned char> headerData;
			getMkvHeader(headerData);
			size_t ret = write(m_curPipe, headerData.data(), headerData.size());
			if(ret != headerData.size()){
				LOG_W("Writing to file returned %ld : %s", ret, strerror(errno));
			}
		}
};

class OutH264PipeStream : public OutStreamIface {
	public:
		OutH264PipeStream() : m_curPipe(-1), m_open(false) { }

		OutH264PipeStream(int pipe) : m_curPipe(pipe), m_open(true) { }

		~OutH264PipeStream(){
			if(m_open) endStream();
		}

		bool openPipe(char * str){
			int f = open(str, O_WRONLY);
			if(f<0) return false;

			if(m_open){
				close(m_curPipe);
			}
			m_curPipe = f;

			m_open = true;
			return true;
		}

		void pushData(void * data, size_t byteCount, bool keyframe){
			if(!m_open){
				LOG_W("OutPipeStream: Stream not yet opened when pushData called");
				return;
			}

			size_t ret = write(m_curPipe, data, byteCount);
			if(ret != byteCount){
				LOG_W("Writing to pipe returned %ld : %s", ret, strerror(errno));
			}
		}

		void endStream(){
			if(!m_open) return;
			m_open = false;
			close(m_curPipe);
		}
	private:
		int m_curPipe;
		bool m_open;
};

