// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include <streamGenerator.h>
#include <time.h>

class SlaveStreamGenerator : public OutStreamIface {
	private:
		bool m_headerSent, m_gotKeyframe, m_ended, m_gotFrame, m_allowSkips;
		boost::function<void()> m_callback;
		pthread_mutex_t m_frameLock, m_cbLock;
		StreamGeneratorIface * m_stream;
		std::vector<unsigned char> m_curFrame;
		long m_lastFrameCnt, m_curFrameCnt;
		boost::function<void(OutStreamIface*)> m_terminateCallback;
		StreamStats m_stats;
		MKV m_muxer;

	public:
		SlaveStreamGenerator(StreamGeneratorIface * stream, bool allowSkips,
				boost::function<void(OutStreamIface*)> terminateCb = NULL ) :
			m_headerSent(false), m_gotKeyframe(false), m_ended(false), m_gotFrame(false),
			m_allowSkips(allowSkips), m_callback(NULL), m_stream(stream),
			m_lastFrameCnt(0), m_curFrameCnt(0), m_terminateCallback(terminateCb),
			m_stats(stream->getStats()), m_muxer(m_stats.width, m_stats.height, m_stats.fps+1)
		{
			pthread_mutex_init(&m_frameLock, NULL);
			pthread_mutex_init(&m_cbLock, NULL);
			m_stream->addSlave(this);
		}
		~SlaveStreamGenerator(){
			if(!m_ended) m_stream->removeSlave(this);
		}

		void outputClosed(){ if(m_terminateCallback) m_terminateCallback(this); }

		//This function isn't threadsafe, some race conditions can occur given certain situations:
		//1: The original stream is currently terminating, processed right before the removeSlave-call,
		//   and may mark this stream as terminated as well, falsely closing the input source and
		//   ending this client's session. Then, new stream will at some point call an invalid reference
		//2: The new stream terminates before the addSlave-call, and we call an invalid reference.
		//   Caller should own a smart-pointer to 'stream' to avoid this.
		//3: The client has already disconnected, un-mapped itself from it's previous input source and
		//   is currently inside its deconstructor. Caller should ensure that this doesn't happen using the
		//   terminateCb parameter in the constructor of this object
		void switchInputSource(StreamGeneratorIface * stream){
			m_stream->removeSlave(this);
			m_stream = stream;
			stream->addSlave(this);
		}

		void endStream() {
			m_ended = true;
			pthread_mutex_lock(&m_cbLock);
			if(m_callback){
				m_callback();
				m_callback = NULL;
			}
			pthread_mutex_unlock(&m_cbLock);
		}

		bool done() { return m_ended; }
		bool available() { return m_gotFrame | !m_headerSent; }
		void post(boost::function<void()> fn){
			pthread_mutex_lock(&m_cbLock);
			m_callback = fn;
			pthread_mutex_unlock(&m_cbLock);
		}
		void destroyCallback(){
			pthread_mutex_lock(&m_cbLock);
			m_callback = NULL;
			pthread_mutex_unlock(&m_cbLock);
		}
		void pushData(void * data, size_t byteCount, bool keyframe){
			if(!m_gotKeyframe){
				if(!keyframe) return;
				m_gotKeyframe = true;
			}
			pthread_mutex_lock(&m_frameLock);
			if(m_ended){
				pthread_mutex_unlock(&m_frameLock);
				return;
			}
            m_curFrame.resize(byteCount);
            memcpy(&m_curFrame[0], data, byteCount);
			m_gotFrame = true;
			m_curFrameCnt++;
			pthread_mutex_unlock(&m_frameLock);
			pthread_mutex_lock(&m_cbLock);
			if(m_callback){
				m_callback();
				m_callback = NULL;
			}
			pthread_mutex_unlock(&m_cbLock);
		}
		std::string operator()(){
			std::string ret;
			pthread_mutex_lock(&m_frameLock);

			if(!m_headerSent){
				std::vector<unsigned char> headerData;
				m_muxer.getHeader(headerData);
				ret = std::string(headerData.begin(), headerData.end());
				m_headerSent = true;
			}else{
				m_muxer.muxFrame(m_curFrame, ret);
				m_gotFrame = false;
			}
			if(!m_allowSkips && m_curFrameCnt != m_lastFrameCnt + 1){
				m_stream->requestKeyframe();
			}
			m_lastFrameCnt = m_curFrameCnt;
			pthread_mutex_unlock(&m_frameLock);
			return ret;
		}
};
