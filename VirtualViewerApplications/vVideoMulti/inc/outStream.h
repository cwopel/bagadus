// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once
#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <vector>
#include <string>
#include <MKV.hpp>

class OutStreamIface {
	public:
		virtual ~OutStreamIface() { };
		virtual void endStream() = 0;
		virtual void pushData(void * data, size_t byteCount, bool keyframe) = 0;
};

class OutStream : public OutStreamIface {
	public:
		OutStream(unsigned int fps, unsigned int width, unsigned int height) :
			m_muxer(width,height,fps) { }
		virtual ~OutStream() { };
		virtual void pushData(void * data, size_t byteCount, bool keyframe) = 0;
		virtual void endStream() = 0;

	protected:

		void getMkvHeader(std::vector<unsigned char> &result) {
			m_muxer.getHeader(result);
		}

		void muxToMkv(void * data, size_t byteCount, std::vector<unsigned char> &result){
			std::vector<unsigned char> inData = std::vector<unsigned char>(byteCount);
			memcpy(&inData[0], data, byteCount);
			m_muxer.muxFrame(inData, result);
		}

		void muxToMkv(void * data, size_t byteCount, std::string &result){
			std::vector<unsigned char> inData = std::vector<unsigned char>(byteCount);
			memcpy(&inData[0], data, byteCount);
			m_muxer.muxFrame(inData, result);
		}

		void inflateMKV(size_t bytes, std::vector<unsigned char> &result) {
			m_muxer.fillCluster(2, bytes, result);
		}

		void inflateMKV(size_t bytes, std::string &result) {
			m_muxer.fillCluster(2, bytes, result);
		}
		
		MKV m_muxer;
};
