// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once

#include <panoDefines.h>
#include <outStream.h>
#include <viewInput.h>
#include <viewEnc.h>
#include <viewer.h>
#include <viewControl.h>
#include <utils.h>

#include <cstdlib>
#include <time.h>
#include <string>
#include <deque>
#include <pthread.h>
#include <boost/function.hpp>

class SlaveStreamGenerator;
class PreviewStreamGenerator;
class StreamGenerator;
class ViewGenerator;

struct StreamStats {
	StreamStats(std::string i, unsigned int f, unsigned int w, unsigned int h, double b) :
		id(i), fps(f), width(w), height(h), bitrate(b), viewers(0) { }
	StreamStats() : fps(0), width(0), height(0), bitrate(0), viewers(0) { }

	std::string id;
	unsigned int fps;
	unsigned int width;
	unsigned int height;
	double bitrate;
	int viewers;
};

class StreamGeneratorIface {
	public:
		virtual ~StreamGeneratorIface() { }
		virtual StreamStats getStats() = 0;
		virtual void addSlave(OutStreamIface * slave) = 0;
		virtual void removeSlave(OutStreamIface * slave) = 0;
		virtual void requestKeyframe() = 0;
		virtual void applyCommandData(std::string cmds){
			LOG_E("Called non-implemented function 'applyCommandData'");
        }
        virtual bool getJpeg(std::vector<unsigned char> &output){
			LOG_E("Called non-implemented function 'getJpeg'");
			return false;
		}
		virtual std::shared_ptr<SlaveStreamGenerator> getPreview(){
			LOG_E("Called non-implemented function 'getPreview'");
			return NULL;
		}
};

class StreamGenerator : public OutStream , public StreamGeneratorIface {
	public:
		
		StreamGenerator(std::string id,
				ViewInput * inputSource,
				unsigned int outWidth,
				unsigned int outHeight,
				float previewScale,
				boost::function<void(std::string)> terminateCb,
				unsigned int fps,
				unsigned int muxFps,
				unsigned int targetBitrate, //Kbit/s
				unsigned int inWidth = PANORAMA_WIDTH,
				unsigned int inHeight = PANORAMA_HEIGHT,
				double fov = PANORAMA_FOV,
				double scale = PANORAMA_SCALE )
			:
			OutStream(muxFps, outWidth, outHeight),
			m_controller(ViewNetworkControl(inWidth,inHeight,fov, STREAM_ABSOLUTE_COORDS)),
			m_muxerFps(muxFps), m_headerSent(false), m_ended(false), m_gotFrame(false), m_lastKeyframe(false),
			m_devId(inputSource->getLeastUtilizedDevice()),
			m_targetFrameSize(targetBitrate*125/fps), m_frameSizeRest(0),
			m_callback(NULL), m_terminateCallback(terminateCb), m_id(id),
			m_inWidth(inWidth), m_inHeight(inHeight), m_inputSrc(inputSource), m_enc(ViewEnc()),
			m_viewer(Viewer(&m_enc, m_devId, true, inWidth, inHeight, fov, scale, previewScale)),
			m_stats(id, fps, outWidth, outHeight, 0), m_accumulatedRate(0), m_previewStream(NULL)
	{
		pthread_mutex_init(&m_frameLock, NULL);
		pthread_mutex_init(&m_cbLock, NULL);
		pthread_mutex_init(&m_previewLock, NULL);
		pthread_mutex_init(&m_slaveLock, NULL);
		pthread_cond_init(&m_condFull, NULL);
		m_stats.viewers = 1;
		m_enc.init(fps, outWidth, outHeight, m_devId, targetBitrate*1000, fps*STREAM_KEYFRAME_INTERVAL);
		m_inputSrc->attachViewer(&m_viewer, m_devId);
		m_enc.attachOutputStream(this);
		m_viewer.attachControlStream(&m_controller);

		if(STREAM_CREATE_JPEGS){
			m_viewer.setJpegConfig(fps, 320, 136); 
		}

		LOG_D("Stream created on GPU %d with id: %s (resolution: %dx%d, bitrate: %d, preview: %5.3f)",
				m_devId, id.c_str(), outWidth, outHeight, targetBitrate, previewScale);
	}
		~StreamGenerator();

		void pushData(void * data, size_t byteCount, bool keyframe);

		std::string operator()();

		std::shared_ptr<SlaveStreamGenerator> getPreview(); 

		bool available(){ return m_gotFrame || !m_headerSent; }

		void outputClosed(){ if(m_terminateCallback) m_terminateCallback(m_id); }

		void applyCommandData(std::string cmds){
			m_controller.parseCommands(&cmds[0], cmds.size());
		}

        double getCurrentTime(){
            return (double)(m_controller.getCurFrame()) / m_muxerFps;
        }

		bool done() { return m_ended; }

		StreamStats getStats(){
			m_stats.bitrate = m_enc.getCurrentBitrate();
			return m_stats;
		}

		void requestKeyframe(){	m_enc.forceKeyframe(); }

		void post(boost::function<void()> fn){
			pthread_mutex_lock(&m_cbLock);
			m_callback = fn;
			pthread_mutex_unlock(&m_cbLock);
		}

		void destroyCallback(){
			pthread_mutex_lock(&m_cbLock);
			m_callback = NULL;
			pthread_mutex_unlock(&m_cbLock);
		}

        bool getJpeg(std::vector<unsigned char> &output){
            return m_viewer.getJpeg(output);
        }

		void endStream() {
			m_ended = true;
			pthread_mutex_lock(&m_cbLock);
			if(m_callback){
				m_callback();
				m_callback = NULL;
			}
			pthread_mutex_unlock(&m_cbLock);
		}

		void addSlave(OutStreamIface * slave){
			pthread_mutex_lock(&m_slaveLock);
			m_stats.viewers++;
			m_slaves.push_back(slave);
			pthread_mutex_unlock(&m_slaveLock);
			m_enc.forceKeyframe();
		}

		void removeSlave(OutStreamIface * slave){
			pthread_mutex_lock(&m_slaveLock);
			m_stats.viewers--;
			m_slaves.erase(std::remove(m_slaves.begin(), m_slaves.end(), slave), m_slaves.end());
			pthread_mutex_unlock(&m_slaveLock);
		}

	private:

		ViewNetworkControl m_controller;
		unsigned int m_muxerFps;

		bool m_headerSent, m_ended, m_gotFrame, m_lastKeyframe;
		int m_devId;
		long m_targetFrameSize;
		long m_frameSizeRest;
		boost::function<void()> m_callback;
		boost::function<void(std::string)> m_terminateCallback;
		std::string m_id;
		unsigned int m_inWidth, m_inHeight;

		ViewInput * m_inputSrc;
		ViewEnc m_enc;
		Viewer m_viewer;
		StreamStats m_stats;

		pthread_mutex_t m_frameLock, m_cbLock, m_slaveLock, m_previewLock;
		pthread_cond_t m_condFull;
		std::string m_currentFrame;

		std::vector<OutStreamIface*> m_slaves;

		std::deque<size_t> m_prevFrameSizes;
		size_t m_accumulatedRate;

		PreviewStreamGenerator * m_previewStream;

		//Debug function, for timing latency
		long m_curFrameCnt = 0;
		long m_recordFrame = -1;
		struct timespec m_recordTime = {0,0};
		void recordTime(){
			if( m_curFrameCnt % 30 == 0){
				m_controller.getLastFrameTime(m_recordFrame, m_recordTime);
			}
			if(m_curFrameCnt == m_recordFrame){
				struct timespec end;
				clock_gettime(CLOCK_MONOTONIC, &end);
				float time = (end.tv_sec*1.0e6 + end.tv_nsec*1.0e-3)
					- (m_recordTime.tv_sec*1.0e6 + m_recordTime.tv_nsec*1.0e-3);
				LOG_D("%6ld \t %10.2f", m_recordFrame, time);
			}
			m_curFrameCnt++;
		}
};

