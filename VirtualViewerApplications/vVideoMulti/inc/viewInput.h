// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once
#include "utils.h"
#include "viewer.h"

#include <vector>
#include <pthread.h>
#include <cuda_runtime_api.h>

class ViewInput {
	public:
		ViewInput(size_t inW, size_t inH) :
			m_framesize(inW * inH * 2), m_eof(false)
	{
		int numDevices;
		cudaSafe(cudaGetDeviceCount(&numDevices));
		if(numDevices == 0){
			LOG_E("NO CUDA DEVICE FOUND");
			exit(EXIT_FAILURE);
		}

		LOG_D("Found %d cuda capable devices!", numDevices);

		for(int i=0; i<numDevices; ++i){
			m_uploaders.push_back(std::make_shared<Uploader>(inW, inH, i));
		}
	}
		void runReader(int pipe);
		//If we have multiple cuda devices, it will return the one with the smallest workload.
		int getLeastUtilizedDevice();
		bool attachViewer(Viewer * v, int devId);
		bool detachViewer(Viewer * v, int devId);
		void terminate();
	private:

		class Uploader {
			public:
				Uploader(size_t w, size_t h, int devId);
				void runUploader();
				void upload(const uint8_t * cpuBuf);
				
				friend class ViewInput;
			private:
				int m_devId;
				size_t m_width, m_height;
				uint8_t * m_gpuBufY[2];
				uint8_t * m_gpuBufUV[2];
				size_t m_strideY, m_strideUV;
				int m_bufIdx, m_nextIdx;
				std::vector<Viewer *> m_viewers;
				cudaStream_t m_stream;
				pthread_t m_thread;
				pthread_cond_t m_cond;
				pthread_mutex_t m_mutex, m_viewMutex;
				bool m_terminated;
				float m_prevExecTime;

				static void * runUploaderThread(void *p){
					((ViewInput::Uploader*)p)->runUploader();
					return NULL;
				}
		};//end class Uploader

		friend class Uploader;

		size_t m_framesize;
		bool m_eof;
		std::vector<std::shared_ptr<Uploader> > m_uploaders;

};//end class ViewInput

