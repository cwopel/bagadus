// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once
#include "utils.h"

#include <pthread.h>
#include <deque>
#include <fcntl.h>
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <helper_cuda_drvapi.h>
#include <helper_nvenc.h>
#include <nvEncodeAPI.h>

#include <viewOutputIface.h>
#include <outStream.h>

//TODO handle out of memory issues. Use cudaGetMemInfo. Should return false if failed to create

//Abs min 2. More buffers may increase delays, but fewer frames dropped when another stream enters
//Max depends on the memory of the GPU and the number of concurrent streams / size of said streams.
//f ex, max on 750 ti w/ 28 streams X 1280x720 = 4
#define NUM_INPUT_ENCODING_BUFFERS 2
//Minimum output buffers is also 2, but if bframes is turned on this MUST be increased to minimum
//2 + numConsecutiveBframes. F ex, if encoding looks like this 'i-b-b-p-b-b-p-b-b-p' you need 4
#define NUM_OUTPUT_ENCODING_BUFFERS 2
// #define NUM_OUTPUT_ENCODING_BUFFERS 4

class ViewEnc : public ViewOutputIface {
	public:
		ViewEnc() : m_initialized(false) { }
		~ViewEnc();
		
		//Initialize encoder
		//NB¸ resolution will be increased to 32byte (width) and 8byte (height) alignment
		bool init(int fps, size_t width, size_t height, int devId,
				unsigned int bitrate, unsigned int gopLength);
		
		//Force next encoded frame to be an IDR-frame.
		void forceKeyframe() { m_forceKeyframe = true; }

		double getCurrentBitrate() { return m_currentBitrate; }

		void attachOutputStream(OutStreamIface * stream);

		void detatchOutputStream(OutStreamIface * stream);

		/* Fast non-blocking call.
		 * Returns NULL if a buffer is not immidiately available (resulting in frame skip) */
		const FrameBuffer * getNextBuffer();

		/* Blocking versions, no stream left behind! */
		const FrameBuffer * getNextBufferBlocking();

		/* Fast non-blocking call. Notifies that previous buffer is ready for reading */
		void bufferReady();
		
		void terminate();
	private:
		size_t m_width, m_height;
		int m_fps;
		unsigned int m_gop, m_frameInGop;
		bool m_forceKeyframe;
		bool m_initialized, m_terminateEncoding, m_terminateWriting;

		void * m_encoder;
		NV_ENC_INITIALIZE_PARAMS m_encInitParams;
		NV_ENC_PRESET_CONFIG m_presetConfig;

		//Buffers
		FrameBuffer m_buffers[NUM_INPUT_ENCODING_BUFFERS];
		NV_ENC_OUTPUT_PTR m_bitstreams[NUM_OUTPUT_ENCODING_BUFFERS];

		//Thread communication variables
		pthread_t m_encoderThread, m_writerThread;
		pthread_mutex_t m_inputLock, m_outputLock, m_outStreamLock;
		pthread_cond_t m_condNoInput, m_condNoOutput,  m_condNoFreeOutput;

		//Using absolute indices for a circular buffer
		int m_inHeadIdx;
		int m_inTailIdx;
		int m_outHeadIdx;
		int m_outTailIdx;

		CUcontext m_cudaContext;

		std::vector<OutStreamIface *> m_outStreams;
		
		//Estimating active bitrate
		std::deque<size_t> m_prevFrameSizes;
		size_t m_accumulatedRate;
		double m_currentBitrate;

		void initializeEncoder(int fps, unsigned int bitrate, unsigned int gopLength);
		void allocateBuffers();
		void runEncoderThread();
		void runWriterThread();

		static void * launchEncoderThread(void *);
		static void * launchWriterThread(void *);
};

