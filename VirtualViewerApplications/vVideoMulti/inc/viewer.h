// AUTHOR(s): Ragnar Langseth,  
//
// Copyright (c) 2013-2014,
// The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
// 
// 1) Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2) Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation and/or
// other materials provided with the distribution.
// 
// 3) All advertising materials mentioning features or use of this software must
// display the following acknowledgement: 
// This product includes software developed by The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS.
// 
// 4) Neither the name of The University of Oslo, Simula Research Laboratory, The iAd Center and ForzaSys AS nor the names of their contributors
// may be used to endorse or promote products derived from this software
// without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ''AS IS''
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE
// FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#pragma once
#include "utils.h"
#include "viewOutputIface.h"
#include "viewControl.h"
#include "imgGenerator.h"

#include <pthread.h>
#include <cuda_runtime_api.h>

struct ViewerMatrixValues {
	double focal, theta_x, theta_y, theta_z, scale, fov;

	ViewerMatrixValues() : focal(500), theta_x(0), theta_y(0), theta_z(0),
	scale(1.75), fov(2.7) { }
	void fillMat(float * mat, int outW, int outH);
};

class Viewer {
    public:
		/* Live mode means that frames will be skipped if output is too slow to handle input.
		 * Alternative is batch mode, where you're guaranteed that all frames will be handled,
		 * but no real-time guarantee.
		 * previewScale < 0.1 turns it off completely. 1.0 = entire (horizontal) window */
        Viewer(ViewOutputIface * output, int devId, bool liveMode, int inW, int inH,
				double fov, double scale, float previewScale = 0.35);
        
		~Viewer();
		
		/* Attach a control object. Before each frame is projected, it will call 'updateMatrix'
		 * on this object, to modify the current projection matrix. */
		void attachControlStream(ViewControlIface * controller);

		//Add a small preview stream, showing only the preview window
		//the interval is how many frames to skip between generating a frame.
		//E.g., if FPS = 30, and preview FPS = 10, interval is 3.
		//Can be reset by calling with argument encoder=NULL
		void attachPreviewStream(ViewOutputIface * encoder, unsigned int generateInterval);

		//For optimal quality, width/height should have the same aspect ratio as inputW/H
		//TODO Currently a chance of segfault when changing dimensions at runtime, avoid or fix
		void setJpegConfig(uint32_t frameInterval, uint32_t width, uint32_t height);

		bool getJpeg(std::vector<unsigned char> &output);

		/* Start the asynchronous virtual view generation */
        void view(uint8_t * gpuInBufY, uint8_t * gpuInBufUV, size_t strideY, size_t strideUV);

		//Called to wait for the previous view to complete,
		//never call before having called 'view'-function
        void endView();

	private:
        int m_inW, m_inH;
		int m_devId;
		bool m_liveMode;
		float m_previewScale;
		cudaStream_t m_stream;
		
		ViewerMatrixValues m_matVals;
		float * m_gpuMatrix, * m_cpuMatrix;
		pthread_mutex_t m_matrixMutex;
		
		ViewControlIface * m_controlStream;
		long m_framesProcessed;

		ViewOutputIface * m_outputStream;
		const FrameBuffer * m_curBuffer;

		ViewOutputIface * m_previewEnc;
		unsigned int m_previewFrameInterval;
		const FrameBuffer * m_curPreviewBuffer;
		pthread_mutex_t m_previewMutex;

		ImgGenerator * m_jpegGenerator;
		uint8_t * m_jpegGpuBuffer;
		bool m_jpegCurrentFrameActive;
		uint32_t m_jpegInterval, m_jpegWidth, m_jpegHeight;

		//Debug tile
		char * m_tileBytemap;
		uint32_t m_tileCols, m_tileRows;

		void makeView(const FrameBuffer * output, uint8_t * inputY,
				uint8_t * inputUV, size_t strideY, size_t strideUV);
		void makePreviewWindow(const FrameBuffer * output, unsigned int width, unsigned int height);
		void makePreviewWindowBGR(unsigned int origWidth, unsigned int origHeight);
		void initCuViewer();
};//end class Viewer

