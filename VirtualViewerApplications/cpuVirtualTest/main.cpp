//  Author: Ragnar Langseth
//  Copyright University of Oslo Norway.
//  and
//  Simula Research laboratory Norway AS
//  from Jun 2014 and onwards
// 
// This is an SSE-2 enhanced implementation of the virtual view algorithm on CPU.
// It is real-time for 1080p, but has several flaws and drawbacks,
// such as the use of BGRA pixelformat (not yuv), that makes it challenging to use.
// However, it may prove helpful if we ever desire a CPU implementation.

#include <chrono>
#include <iostream>
#include <random>
#include <cstdint>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <algorithm>

#include <xmmintrin.h>
#include <emmintrin.h>
#include <immintrin.h>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

typedef std::chrono::high_resolution_clock Clock;
typedef std::chrono::duration<double> sec;

#define IN_WIDTH 4096
#define IN_HEIGHT 1680

#define OUT_WIDTH 1920
#define OUT_HEIGHT 1080
// #define OUT_WIDTH 1280
// #define OUT_HEIGHT 720

double focal = 900;
double theta_x = -0.1;
double theta_y = 0;
double scale = 1.75;
double fov = 2.7;

//cast double to uint
inline unsigned f2i(double d) {
	d += 4503599627370496.0;  // 1 << 52
	return (unsigned &)d;
}

// ------- BILINEAR INTERPOLATION FUNCTIONS.
// Source: http://fastcpp.blogspot.no/2011/06/bilinear-pixel-interpolation-using-sse.html

// constant values that will be needed
static const __m128 CONST_1111 = _mm_set1_ps(1);
static const __m128 CONST_256 = _mm_set1_ps(256);

inline __m128 CalcWeights(float x, float y)
{
	__m128 ssx = _mm_set_ss(x);
	__m128 ssy = _mm_set_ss(y);
	__m128 psXY = _mm_unpacklo_ps(ssx, ssy);      // 0 0 y x

	//__m128 psXYfloor = _mm_floor_ps(psXY); // use this line for if you have SSE4
	__m128 psXYfloor = _mm_cvtepi32_ps(_mm_cvtps_epi32(psXY));
	__m128 psXYfrac = _mm_sub_ps(psXY, psXYfloor); // = frac(psXY)

	__m128 psXYfrac1 = _mm_sub_ps(CONST_1111, psXYfrac); // ? ? (1-y) (1-x)
	__m128 w_x = _mm_unpacklo_ps(psXYfrac1, psXYfrac);   // ? ?     x (1-x)
	w_x = _mm_movelh_ps(w_x, w_x);      // x (1-x) x (1-x)
	__m128 w_y = _mm_shuffle_ps(psXYfrac1, psXYfrac, _MM_SHUFFLE(1, 1, 1, 1)); // y y (1-y) (1-y)

	// complete weight vector
	return _mm_mul_ps(w_x, w_y);
}

inline uint32_t GetPixelSSE(const uint32_t* img, float x, float y)
{
	const int stride = IN_WIDTH;
	const uint32_t* p0 = img + (int)x + (int)y * stride; // pointer to first pixel

	// Load the data (2 pixels in one load)
	__m128i p12 = _mm_loadl_epi64((const __m128i*)&p0[0 * stride]); 
	__m128i p34 = _mm_loadl_epi64((const __m128i*)&p0[1 * stride]); 

	__m128 weight = CalcWeights(x, y);

	// extend to 16bit
	p12 = _mm_unpacklo_epi8(p12, _mm_setzero_si128());
	p34 = _mm_unpacklo_epi8(p34, _mm_setzero_si128());

	// convert floating point weights to 16bit integer
	weight = _mm_mul_ps(weight, CONST_256); 
	__m128i weighti = _mm_cvtps_epi32(weight); // w4 w3 w2 w1
	weighti = _mm_packs_epi32(weighti, _mm_setzero_si128()); // 32->16bit

	// prepare the weights
	__m128i w12 = _mm_shufflelo_epi16(weighti, _MM_SHUFFLE(1, 1, 0, 0));
	__m128i w34 = _mm_shufflelo_epi16(weighti, _MM_SHUFFLE(3, 3, 2, 2));
	w12 = _mm_unpacklo_epi16(w12, w12); // w2 w2 w2 w2 w1 w1 w1 w1
	w34 = _mm_unpacklo_epi16(w34, w34); // w4 w4 w4 w4 w3 w3 w3 w3

	// multiply each pixel with its weight (2 pixel per SSE mul)
	__m128i L12 = _mm_mullo_epi16(p12, w12);
	__m128i L34 = _mm_mullo_epi16(p34, w34);

	// sum the results
	__m128i L1234 = _mm_add_epi16(L12, L34); 
	__m128i Lhi = _mm_shuffle_epi32(L1234, _MM_SHUFFLE(3, 2, 3, 2));
	__m128i L = _mm_add_epi16(L1234, Lhi);

	// convert back to 8bit
	__m128i L8 = _mm_srli_epi16(L, 8); // divide by 256
	L8 = _mm_packus_epi16(L8, _mm_setzero_si128());

	return _mm_cvtsi128_si32(L8);
}

void fillMat(float * mat, int outW, int outH){
	double theta_z = -0.32 * (theta_y - 0.027);

	mat[0] = (-cos(theta_y)*cos(theta_z) -
			sin(theta_x)*sin(theta_y)*sin(theta_z))/focal;

	mat[1] = (cos(theta_z)*sin(theta_z)*sin(theta_y) -
			cos(theta_y)*sin(theta_z))/(focal*scale);

	mat[2] = ((cos(theta_y)*(scale*outW*cos(theta_z) +
					outH*sin(theta_z))) +
			(sin(theta_y)*(-2.0*focal*scale*cos(theta_x) +
						   sin(theta_x)*(-outH*cos(theta_z) +
							   scale*outW*sin(theta_z)))))/(2*focal*scale);


	mat[3] = (cos(theta_x)*sin(theta_z))/focal;

	mat[4] = -((cos(theta_x)*cos(theta_z))/(focal*scale));

	mat[5] = (outH*cos(theta_x)*cos(theta_z) -
			2*focal*scale*sin(theta_x) -
			scale*outW*cos(theta_x)*sin(theta_z))/(2*focal*scale);


	mat[6] = 	(-cos(theta_z)*sin(theta_y) +
			cos(theta_y)*sin(theta_x)*sin(theta_z))/focal;

	mat[7] = -((cos(theta_y)*cos(theta_z)*sin(theta_x) +
				sin(theta_y)*sin(theta_z))/(focal*scale));

	mat[8] =     (2*focal*scale*cos(theta_x)*cos(theta_y) +
			sin(theta_y)*(scale*outW*cos(theta_z) +
				outH*sin(theta_z)) +
			cos(theta_y)*sin(theta_x)*(outH*cos(theta_z) -
				scale*outW*sin(theta_z)))/(2*focal*scale);
}

inline void computeXY(float *M, float x, float y, float *o_x, float *o_y){

	float s1,s2,s3;
	s1 = M[0]*x + M[1]*y+M[2];
	s2 = M[3]*x + M[4]*y+M[5];
	s3 = M[6]*x + M[7]*y+M[8];

	*o_x = std::atan2(-s1,s3)*((double)IN_WIDTH)/fov + ((double)IN_WIDTH)/2;
	*o_y = ((double)IN_HEIGHT)/2 - ((double)IN_HEIGHT)*s2/std::sqrt(s1*s1+s3*s3);
}

void makeViewBasic(float *matrix, uint8_t * src, uint8_t * dst){
	_MM_SET_ROUNDING_MODE(_MM_ROUND_TOWARD_ZERO);
	for(int y=0; y<OUT_HEIGHT; ++y){
		for(int x=0; x<OUT_WIDTH; ++x){
			uint32_t * target = (uint32_t*) (dst + 4*(x + y*OUT_WIDTH));
			float dx,dy;
			computeXY(matrix,x,y,&dx,&dy);
			if(dx>=0 && (dx+0.5)<IN_WIDTH && dy>=0 && (dy+0.5)<IN_HEIGHT){
				*target = GetPixelSSE((const uint32_t*)src,dx,dy);
// 				*target = *(uint32_t*)(src + 4*(f2i(dx) + f2i(dy)*IN_WIDTH));
			}else{
				*target = 0;
			}
		}
	}
}

#define B_SZ 8
// #define B_SZ 16
// #define B_SZ 32

#define B_SZ_2 (B_SZ*B_SZ)

void makeViewLinear(float *matrix, uint8_t * src, uint8_t * dst){

	_MM_SET_ROUNDING_MODE(_MM_ROUND_TOWARD_ZERO);

	float edges[(OUT_WIDTH/B_SZ + 1) * (OUT_HEIGHT/B_SZ + 1) * 2];
	int edgeStride = (OUT_WIDTH/B_SZ + 1);

#define LOOK_E(x,y) (edges + ((x + y*edgeStride)<<1))

	for(int y=0; y<=OUT_HEIGHT; y+=B_SZ){
		for(int x=0; x<=OUT_WIDTH; x+=B_SZ){
			float * target = LOOK_E((x/B_SZ),(y/B_SZ));
			computeXY(matrix,x,y,target,target+1);
		}
	}

	for(int y=0; y<OUT_HEIGHT; ++y){
		for(int x=0; x<OUT_WIDTH; ++x){
			uint32_t * target = (uint32_t*) (dst + 4*(x + y*OUT_WIDTH));
			float dx,dy;
			int modX = x % B_SZ;
			int modY = y % B_SZ;
			int edgeX = x / B_SZ;
			int edgeY = y / B_SZ;

			//Calculate weights
			float w[4];
			w[0] = (float)((B_SZ-modX) * (B_SZ-modY))/(B_SZ_2);
			w[1] = (float)((     modX) * (B_SZ-modY))/(B_SZ_2);
			w[2] = (float)((B_SZ-modX) * (     modY))/(B_SZ_2);
			w[3] = (float)((     modX) * (     modY))/(B_SZ_2);

			//Calculate coordinates
			float *c[4];
			c[0] = LOOK_E(edgeX,edgeY);
			c[1] = LOOK_E((edgeX+1),edgeY);
			c[2] = LOOK_E(edgeX,(edgeY+1));
			c[3] = LOOK_E((edgeX+1),(edgeY+1));

			dx = w[0]*c[0][0] + w[1]*c[1][0] + w[2]*c[2][0] + w[3]*c[3][0];
			dy = w[0]*c[0][1] + w[1]*c[1][1] + w[2]*c[2][1] + w[3]*c[3][1];

			if(dx>=0 && dx<IN_WIDTH && dy>=0 && (dy+0.5)<IN_HEIGHT){
				*target = GetPixelSSE((const uint32_t*)src,dx,dy);
			}else{
				*target = 0;
			}
		}
	}
#undef LOOK_E
}

static const __m128 mBlockSz = _mm_set1_ps(B_SZ);
static const __m128 mBlockSz2 = _mm_set1_ps(B_SZ_2);
static const __m128 m0123 = _mm_set_ps(0,1,2,3);
static const __m128 m4444 = _mm_set1_ps(4);

//Assumption, B_SZ is a multiple of 4
//Assumption, OUT_WIDTH and OUT_HEIGHT are multiples of B_SZ
void makeViewLinearSSE(float *matrix, uint8_t * src, uint8_t * dst){
	_MM_SET_ROUNDING_MODE(_MM_ROUND_TOWARD_ZERO);

	float edges[(OUT_WIDTH/B_SZ + 1) * (OUT_HEIGHT/B_SZ + 1) * 2];
	int edgeStride = (OUT_WIDTH/B_SZ + 1);

#define LOOK_E(x,y) (edges + ((x + y*edgeStride)<<1))

	for(int y=0; y<=OUT_HEIGHT; y+=B_SZ){
		for(int x=0; x<=OUT_WIDTH; x+=B_SZ){
			float * target = LOOK_E((x/B_SZ),(y/B_SZ));
			computeXY(matrix,x,y,target,target+1);
		}
	}
	
	float dx,dy;
	uint32_t * target;
	for(int y=0; y<OUT_HEIGHT; ++y){
		for(int x=0; x<OUT_WIDTH; ){
			int edgeX = x / B_SZ;
			int edgeY = y / B_SZ;

			//Calculate coordinates
			float *c[4];
			c[0] = LOOK_E(edgeX    ,edgeY    );
			c[1] = LOOK_E((edgeX+1),edgeY    );
			c[2] = LOOK_E(edgeX    ,(edgeY+1));
			c[3] = LOOK_E((edgeX+1),(edgeY+1));

			__m128 modX = m0123;
			__m128 modY = _mm_set1_ps(y % B_SZ);
			float dxArr[B_SZ];
			float dyArr[B_SZ];
			for(int xx=0; xx<B_SZ; xx+=4, modX = _mm_add_ps(modX,m4444)){
				__m128 w[4];
				w[0] = _mm_div_ps( _mm_mul_ps(_mm_sub_ps(mBlockSz, modX),_mm_sub_ps(mBlockSz, modY)), mBlockSz2);
				w[1] = _mm_div_ps( _mm_mul_ps( modX, _mm_sub_ps(mBlockSz, modY)), mBlockSz2);
				w[2] = _mm_div_ps( _mm_mul_ps( _mm_sub_ps(mBlockSz, modX), modY), mBlockSz2);
				w[3] = _mm_div_ps( _mm_mul_ps( modX, modY), mBlockSz2);

				__m128 dxVec = _mm_add_ps(
						_mm_mul_ps( _mm_set1_ps(c[0][0]) , w[0] ),
						_mm_add_ps(
							_mm_mul_ps( _mm_set1_ps(c[1][0]) , w[1] ),
							_mm_add_ps(
								_mm_mul_ps( _mm_set1_ps(c[2][0]) , w[2] ),
								_mm_mul_ps( _mm_set1_ps(c[3][0]) , w[3] )
								)
							)
						);
				__m128 dyVec = _mm_add_ps(
						_mm_mul_ps( _mm_set1_ps(c[0][1]) , w[0] ),
						_mm_add_ps(
							_mm_mul_ps( _mm_set1_ps(c[1][1]) , w[1] ),
							_mm_add_ps(
								_mm_mul_ps( _mm_set1_ps(c[2][1]) , w[2] ),
								_mm_mul_ps( _mm_set1_ps(c[3][1]) , w[3] )
								)
							)
						);
				//Ugly endian happyness, values are reversed :)
				dxArr[xx] = ((float*)&dxVec)[3];
				dyArr[xx] = ((float*)&dyVec)[3];
				dxArr[xx+1] = ((float*)&dxVec)[2];
				dyArr[xx+1] = ((float*)&dyVec)[2];
				dxArr[xx+2] = ((float*)&dxVec)[1];
				dyArr[xx+2] = ((float*)&dyVec)[1];
				dxArr[xx+3] = ((float*)&dxVec)[0];
				dyArr[xx+3] = ((float*)&dyVec)[0];
			}
			for(int xx=0; xx<B_SZ; ++xx,++x){
				dx = dxArr[xx];
				dy = dyArr[xx];
				target = (uint32_t*) (dst + 4*(x + 1 + y*OUT_WIDTH));
				if(dx>=0 && dx<IN_WIDTH && dy>=0 && (dy+0.5)<IN_HEIGHT){
					//Bilinear lookup
					*target = GetPixelSSE((const uint32_t*)src,dx,dy);
					//NN lookup
// 					*target = *(uint32_t*)(src + 4*(f2i(dx) + f2i(dy)*IN_WIDTH));
				}else{
					*target = 0;
				}
			}
		}
	}
#undef LOOK_E
}

int main(int argc, char** argv){

	cv::Mat in, input;
	if(argc > 1) input = cv::imread(argv[1], CV_LOAD_IMAGE_COLOR);
	else input = cv::imread("still.png", CV_LOAD_IMAGE_COLOR);
	cv::cvtColor(input, in, cv::COLOR_BGR2BGRA);

	cv::Mat output = cv::Mat(OUT_HEIGHT, OUT_WIDTH, CV_8UC4);

	float mat[9];
	double totTime = 0;
	int iterations = 20;
	for(int i=0; i<iterations; ++i){

		Clock::time_point t0 = Clock::now();

		fillMat(mat, output.cols, output.rows);
		makeViewLinearSSE(mat, in.data, output.data);
// 		makeViewLinear(mat, in.data, output.data);
// 		makeViewBasic(mat, in.data, output.data);

		Clock::time_point t1 = Clock::now();

		totTime += sec(t1-t0).count() * 1000.0;
	}

	std::cout << totTime/iterations << " ms\n";

	cv::imwrite("result.png", output);

	return 0;
}
